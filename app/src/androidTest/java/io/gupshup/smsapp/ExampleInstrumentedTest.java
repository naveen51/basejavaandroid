package io.gupshup.smsapp;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.dao.SearchDAO;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.exceptions.UnparsablePhoneNumberException;
import io.gupshup.smsapp.message.Utils;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    Context context = null;
    SearchDAO searchDAO;
    MessageDAO messageDAO;
    ConversationDAO conversationDAO;

    @Before
    public void setup() {
        this.context = InstrumentationRegistry.getTargetContext();
        this.conversationDAO = AppDatabase.getInstance(context).conversationDAO();
        this.messageDAO = AppDatabase.getInstance(context).messageDAO();
        this.searchDAO = AppDatabase.getInstance(context).searchDAO();
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("io.gupshup.smsapp", appContext.getPackageName());
    }

    @Test
    public void maskTest() throws UnparsablePhoneNumberException {
        Context context = InstrumentationRegistry.getTargetContext();
        List<String> phoneNumbers = new ArrayList<String>() {{
            add("+919831298123");
            add("+917710004077");
            add("+919831053019");
            add("+919903068120");
        }};
        List<String> phoneNumbers2 = new ArrayList<String>() {{
            add("+917710004077");
            add("+919831298123");
            add("+919831053019");
            add("+919903068120");
        }};
        List<String> phoneNumbers3 = new ArrayList<String>() {{
            add("+917710004077");
            add("+919831053019");
            add("+919831298123");
            add("+919903068120");
        }};
        List<String> phoneNumbers4 = new ArrayList<String>() {{
            add("+917710004077");
            add("+919831053019");
            add("+919903068120");
            add("+919831298123");
        }};
        String mask = Utils.getMaskFromPhoneNumbers(phoneNumbers, context);

        String mask2 = Utils.getMaskFromPhoneNumbers(phoneNumbers2, context);

        String mask3 = Utils.getMaskFromPhoneNumbers(phoneNumbers3, context);

        String mask4 = Utils.getMaskFromPhoneNumbers(phoneNumbers4, context);

        assertEquals(mask, mask2);
        assertEquals(mask2, mask3);
        assertEquals(mask3, mask4);
    }

    @Test
    public void getContactListTest() {
//        List<SearchDAO.SearchItem> searchItems = searchDAO.findEveryWhere("go");
//        for (SearchDAO.SearchItem searchItem:
//             searchItems) {
////            Toast.makeText(context, searchItem.getSource(), Toast.LENGTH_LONG).show();
//            System.out.print(searchItem.getSource());
//        }
        String mask = "AMAMAZON";
        ConversationType conversationTypeFromNumber = Utils.getConversationType(mask, "test");
        assertEquals(conversationTypeFromNumber, ConversationType.TRANSACTIONAL);
        mask = "AM123456";
        conversationTypeFromNumber = Utils.getConversationType(mask, "test");
        assertEquals(conversationTypeFromNumber, ConversationType.PROMOTIONAL);
        mask = "AM-123456";
        conversationTypeFromNumber = Utils.getConversationType(mask, "test");
        assertEquals(conversationTypeFromNumber, ConversationType.PROMOTIONAL);
        mask = "AM-AMAZON";
        conversationTypeFromNumber = Utils.getConversationType(mask, "test");
        assertEquals(conversationTypeFromNumber, ConversationType.TRANSACTIONAL);


        String mask1 = "AMAmazon";
        String mask2 = "VM-amazon";

        assertEquals(Utils.getMaskFromPhoneNumber(mask1), Utils.getMaskFromPhoneNumber(mask2));
    }


    @Test
    public void databasepath() {
        String dbpath = context.getDatabasePath("sms-app.db").getAbsolutePath();

        System.out.println("DB Path => " + dbpath);
    }

    @Test
    public void getTopContacts(){
        System.out.println("exec call db method..");
        List<String> topMasks = AppDatabase.getInstance(MainApplication.getContext()).messageDAO().getTopContactsBasedOnMaxOutGoingMsgs();
        System.out.println("The top masks count is "+topMasks.size());
        if(topMasks.size() <8) {
            int remaining = 8 - topMasks.size();
            Cursor cursorStarred = MainApplication.getContext().getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER}, "starred=?",
                    new String[]{"1"}, null);
        }
        //TODO:normalize the numbers and then get count of the normalized numbers.
            //TODO : if topmasks + normalized no count less than 8 then run the following query.
            Cursor cursorAlphabeticSort = MainApplication.getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
                    null,
                    "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");

        }
}
