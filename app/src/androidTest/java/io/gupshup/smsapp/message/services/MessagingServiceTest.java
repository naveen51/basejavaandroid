package io.gupshup.smsapp.message.services;

import android.arch.lifecycle.Observer;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.utils.DateUtils;

import static org.junit.Assert.assertEquals;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public class MessagingServiceTest {

    private static final AppDatabase appDatabase = AppDatabase.getInstance(InstrumentationRegistry.getTargetContext());
    private static final SMSService smsService = new SMSService();
    private static final ConversationDAO conversationDAO = appDatabase.conversationDAO();
    private static final MessageDAO messageDAO = appDatabase.messageDAO();

    private String[] numbers =
        {
            "+919876543210",
            "+91 98765 43210",
            "919876543210",
            "09876543210",
            "9876543210",
            "19876543210",
            "+19876543210",
            "+91 (987) 654-3210",
            "+1 (987) 654-3210",
            "(987) 654-3210",
            "+91 987-654-3210",
            "+1 987-654-3210",
            "987-654-3210",
            "54321",
            "VM-ICICI",
            "VA-ICICI",
            "VM-54321",
            "VA-54321",
            null
        };

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMaskFromPhoneNumber() {
        for (String number : numbers) {
            System.out.print(number + " : ");
            System.out.println(Utils.getMaskFromPhoneNumber(number, InstrumentationRegistry.getTargetContext()));
        }
    }

    @Test
    public void getPhoneNumberInfo() {
        for (String number : numbers) {
            System.out.print(number + " : ");
            String[] phoneNumberInfo = Utils.getPhoneNumberInfo(number, InstrumentationRegistry.getTargetContext());
            System.out.println(phoneNumberInfo != null ? (phoneNumberInfo[0] + "|" + phoneNumberInfo[1]) : null);
        }
    }

    @Test
    public void preSending() {
//        String mask = "+919999999999";
//
////        String messageId =
////            smsService.preSending(mask, null, new GupshupTextMessage("Hi !!!"));
//
//        List<ConversationEntity> conversationEntities = conversationDAO.get(mask);
//
//        assertEquals(1, conversationEntities.size());
//
//        List<MessageEntity> messageEntities = messageDAO.get(messageId);
//
//        assertEquals(1, messageEntities.size());
//
//        return;
    }

    @Test
    public void postSending() {
//        String mask = "+919999999999";
//
////        String messageId =
////            smsService.preSending(mask, null, new GupshupTextMessage("Hi !!!"));
//
//        smsService.postSending(messageId, DateUtils.currentTimestamp(), -1);
//
//        List<MessageEntity> messageEntities = messageDAO.get(messageId);
//
//        assertEquals(1, messageEntities.size());
//
//        List<ConversationEntity> conversationEntities = conversationDAO.get(messageEntities.get(0).getMask());
//
//        assertEquals(1, conversationEntities.size());

        return;
    }

    @Test
    public void postReceiving() throws Exception {
//        String mask = "+918888888888";
//
////        String messageId =
////            smsService
////                .postReceiving(mask, null, null, new GupshupTextMessage("Hi !!!"), "+917777777777", DateUtils.currentTimestamp())
////                .getMessageId();
//
//        List<ConversationEntity> conversationEntities = conversationDAO.get(mask);
//
//        assertEquals(1, conversationEntities.size());
//
//        List<MessageEntity> messageEntities = messageDAO.get(messageId);
//
//        assertEquals(1, messageEntities.size());

        return;
    }

    @Test
    public void readConversation() {
        String mask = "MASK-1";
        final long readTimestamp = DateUtils.currentTimestamp();

        new LivePagedListBuilder(
            messageDAO.getByConversation(mask),
            new PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPrefetchDistance(10)
                .setPageSize(20)
                .build()
        )
            .build()
            .observeForever(new Observer<PagedList<MessageEntity>>() {
                @Override
                public void onChanged(@Nullable PagedList<MessageEntity> messageEntities) {
                    for (MessageEntity messageEntity : messageEntities) {
                        assertEquals(readTimestamp, messageEntity.getReadTimestamp());
                    }
                }
            });

        smsService.readConversation(mask, readTimestamp);

        List<ConversationEntity> conversationEntities = conversationDAO.get(mask);

        assertEquals(1, conversationEntities.size());

        ConversationEntity conversationEntity = conversationEntities.get(0);

        assertEquals(0, conversationEntity.getNoOfUnreadMessages());
    }

}
