package io.gupshup.smsapp.appTest;

import android.content.Context;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.exceptions.UnparsablePhoneNumberException;
import io.gupshup.smsapp.message.Utils;

import static org.junit.Assert.assertEquals;

public class MaskTest {

    @Test
    public void maskTest() throws UnparsablePhoneNumberException {
        Context context = InstrumentationRegistry.getTargetContext();
        List<String> phoneNumbers = new ArrayList<String>() {{
            add("+917710004077");
            add("+919967437392");
            add("+919831053019");
        }};
        String mask = Utils.getMaskFromPhoneNumbers(phoneNumbers, context);

        String mask2 = Utils.getMaskFromPhoneNumbers(phoneNumbers, context);

        assertEquals(mask, mask2);
    }
}
