package io.gupshup.smsapp.crypto;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.gupshup.crypto.app.GupshupKeyGenerator;
import io.gupshup.crypto.app.GupshupMessageCrypter;
import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import io.gupshup.smsapp.testrules.RepeatRule;
import io.gupshup.smsapp.testrules.RepeatRule.Repeat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/*
 * @author  Bhargav Kolla
 * @since   Apr 08, 2018
 */
public class CryptoTests {

    private static final GupshupKeyGenerator gupshupKeyGenerator = GupshupKeyGenerator.getInstance();
    private static final GupshupMessageCrypter gupshupMessageCrypter = GupshupMessageCrypter.getInstance();

    @Rule
    public final RepeatRule repeatRule = new RepeatRule();

    @Before
    public void setUp() throws Exception {
        Security.addProvider(new BouncyCastleProvider());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Repeat
    public void keyPair() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        GupshupKeyPair gupshupKeyPair1 = gupshupKeyGenerator.generateKeyPair();
//
//        GupshupKeyPair gupshupKeyPair2 =
//            gupshupKeyGenerator.generateKeyPair(
//                ecKeyGenerator.generateKeyPair(
//                    gupshupKeyPair1
//                )
//            );
//
//        assertEquals(gupshupKeyPair1, gupshupKeyPair2);
    }

    @Test
    @Repeat
    public void secretKey() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        GupshupKeyPair gkpA = gupshupKeyGenerator.generateKeyPair();
        GupshupKeyPair gkpB = gupshupKeyGenerator.generateKeyPair();
        GupshupKeyPair gkpC = gupshupKeyGenerator.generateKeyPair();

//        SecretKey skAB = ecKeyGenerator.generateSecretKey(gkpA.getPrivateKey(), gkpB.getPublicKey());
//        SecretKey skBA = ecKeyGenerator.generateSecretKey(gkpB.getPrivateKey(), gkpA.getPublicKey());
//        SecretKey skBC = ecKeyGenerator.generateSecretKey(gkpB.getPrivateKey(), gkpC.getPublicKey());
//        SecretKey skCB = ecKeyGenerator.generateSecretKey(gkpC.getPrivateKey(), gkpB.getPublicKey());
//        SecretKey skCA = ecKeyGenerator.generateSecretKey(gkpC.getPrivateKey(), gkpA.getPublicKey());
//        SecretKey skAC = ecKeyGenerator.generateSecretKey(gkpA.getPrivateKey(), gkpC.getPublicKey());
//
//
//
//        assertEquals(skAB, skBA);
//        assertEquals(skBC, skCB);
//        assertEquals(skCA, skAC);
//
//        assertNotEquals(skAB, skBC);
//        assertNotEquals(skBC, skCA);
    }

    @Test
    @Repeat
    public void publicKey() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
        GupshupKeyPair gupshupKeyPair = gupshupKeyGenerator.generateKeyPair();

        GupshupPublicKey gupshupPublicKey1 = gupshupKeyPair.getPublicKey();

        GupshupPublicKey gupshupPublicKey2 =
            gupshupKeyGenerator.generatePublicKey(gupshupKeyPair.getPrivateKey());

        assertEquals(gupshupPublicKey1, gupshupPublicKey2);
    }

    @Test
    public void textMessage() throws IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchPaddingException, BadPaddingException, NoSuchProviderException, IllegalBlockSizeException, ClassNotFoundException {
        GupshupKeyPair senderKeyPair = gupshupKeyGenerator.generateKeyPair();
        GupshupKeyPair receiverKeyPair = gupshupKeyGenerator.generateKeyPair();

        String textMessage =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        String encrypted =
            gupshupMessageCrypter.encrypt(
                new GupshupTextMessage(textMessage),
                senderKeyPair.getPrivateKey(),
                receiverKeyPair.getPublicKey()
            );

        GupshupMessage decrypted;
        {
            decrypted =
                gupshupMessageCrypter.decrypt(
                    encrypted, senderKeyPair.getPrivateKey()
                );

            assertNotEquals(null, decrypted);
            assertEquals(MessageType.TEXT, decrypted.getType());
            assertEquals(textMessage, ((GupshupTextMessage) decrypted).getText());
        }
        {
            decrypted =
                gupshupMessageCrypter.decrypt(
                    encrypted, receiverKeyPair.getPrivateKey()
                );

            assertNotEquals(null, decrypted);
            assertEquals(MessageType.TEXT, decrypted.getType());
            assertEquals(textMessage, ((GupshupTextMessage) decrypted).getText());
        }
        {
            GupshupKeyPair someotherKeyPair = gupshupKeyGenerator.generateKeyPair();

            decrypted =
                gupshupMessageCrypter.decrypt(
                    encrypted, someotherKeyPair.getPrivateKey()
                );

            assertEquals(null, decrypted);
        }
    }

}