package io.gupshup.smsapp.testrules;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author  Bhargav Kolla
 * @since   Apr 09, 2018
 */
public class RepeatRule implements TestRule {

    @Override
    public Statement apply(Statement base, Description description) {
        Repeat repeat = description.getAnnotation(Repeat.class);
        if (repeat != null) {
            base = new RepeatStatement(base, repeat.times());
        }

        return base;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Repeat {
        int times() default 10000;
    }

    private static class RepeatStatement extends Statement {
        private final Statement statement;
        private final int times;

        private RepeatStatement(Statement statement, int times) {
            this.statement = statement;
            this.times = times;
        }

        @Override
        public void evaluate() throws Throwable {
            for (int i = 0; i < times; i++) {
                statement.evaluate();
            }
        }
    }

}
