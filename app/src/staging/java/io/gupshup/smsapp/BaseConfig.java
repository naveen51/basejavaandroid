package io.gupshup.smsapp;

public abstract class BaseConfig {

    public static final String BASE_URL = "https://qa-smsinbox.gupshup.io";
    public static final String APPLICATION_ID = "GupshupSMSApp";
    public static final String PRIVACY_WEB_URL = "privacy";
    public static final String TERMS_WEB_URL = "terms";
    public static final String LICENCE_WEB_URL = "license";
    public static final String WEB_BASE_URL = "https://www.gupshup.io/developer/app/";
}
