package io.gupshup.smsapp.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.receiver.NotificationActionReceiver;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.home.viewmodel.HomeFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

import static io.gupshup.smsapp.utils.AppConstants.OTP_MATCHER;

/**
 * Created by Ram Prakash Bhat on 27/4/18.
 */

public class NotificationHelper {

    private static String sActionFirst = AppConstants.EMPTY_TEXT;
    private static String sActionSecond = AppConstants.EMPTY_TEXT;
    private static int sActionIconFirst = 0, actionIconSecond = 0;
    private static String sActionTextFirst = AppConstants.EMPTY_TEXT;
    private static String sActionTextSecond = AppConstants.EMPTY_TEXT;
    private static String sOtpNumber = AppConstants.EMPTY_TEXT;

    public static void createNotification(Context context, String phone, String messageBody,
                                          Bundle data, MessageEntity messageEntity,
                                          String contactName, String profileImage,
                                          ConversationEntity conversationEntity) {

       /* if (conversationEntity != null) {
            if (!conversationEntity.isNotificationsPopup()) {
                return;
            }
        }*/

        if (Utils.canShowNotification(context, conversationEntity)) {
            MessageDAO.CountData countData = MessagingService
                .getInstance(SMSService.class).getTotalUnreadCount();
            List<MessageEntity> messageEntityList = MessagingService.getInstance(SMSService.class)
                .getTopUnreadMessages(countData.getMessageCount());
            boolean isFromSameConversation = AppUtils.isFromSameConversation(messageEntityList
                .toArray(new MessageEntity[messageEntityList.size()]), context);
            int notificationId = (int) System.currentTimeMillis();
            String messageId = null;
            if (messageEntity != null) {
                messageId = messageEntity.getMessageId();
            }
            extractOtpNumber(messageBody);
            setupActionButtonText(context, isFromSameConversation, messageEntityList, contactName);
            if (isFromSameConversation) { //messageEntityList.size() == 1
                data.putString(AppConstants.BundleKey.MOBILE_NUMBER, phone);
                data.putString(AppConstants.BundleKey.CONTACT_NAME, TextUtils.isEmpty(contactName)
                    ? phone : contactName);
                data.putSerializable(AppConstants.BundleKey.CONVERSATION_TYPE,conversationEntity.getType());
                data.putString(AppConstants.BundleKey.LAST_MESSAGE_ID, messageId);
            }
            Intent resultIntent;
            // Fix for AMA-917.
        /*if (AppConstants.IntentActions.REPLY.equalsIgnoreCase(actionSecond)) {
            resultIntent = new Intent(context, QuickReplyActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {*/
            resultIntent = new Intent(context, MainActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // }
            if (data != null) {
                resultIntent.putExtras(data);
            }
            /*  set intent so it does not start a new activity*/
            int requestCode = (int) DateUtils.currentTimestamp();
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestCode,
                resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent markAsReadIntent = createActionFirstIntent(context, notificationId,
                messageId, messageEntityList, phone, contactName,conversationEntity.getType());
            PendingIntent markAsBlockIntent = createActionSecondIntent(context, notificationId,
                messageId, phone, contactName,conversationEntity.getType());
            String channelId = context.getString(R.string.default_notification_channel_id);
            NotificationCompat.Builder notificationBuilder = createNotificationBuilder(context, channelId
                , phone, contactName, messageBody, messageEntityList, sOtpNumber, countData);
            notificationBuilder.addAction(sActionIconFirst, sActionTextFirst, markAsReadIntent);
            if (!TextUtils.isEmpty(sActionSecond)) {
                notificationBuilder.addAction(actionIconSecond, sActionTextSecond, markAsBlockIntent);
            }
            updateNotificationBuilder(context, conversationEntity, notificationBuilder, channelId,
                notificationId, pendingIntent);
        }
    }

    private static PendingIntent createActionSecondIntent(Context context,
                                                          int notificationId,
                                                          String messageId, String phone, String contactName, ConversationType type) {

        Intent actionSecondIntent = new Intent(context, NotificationActionReceiver.class);
        actionSecondIntent.putExtra(AppConstants.BundleKey.NOTIFICATION_ID, notificationId);
        actionSecondIntent.putExtra(AppConstants.BundleKey.MESSAGE_ID, messageId);
        actionSecondIntent.setAction(sActionSecond);
        actionSecondIntent.putExtra(AppConstants.BundleKey.MOBILE_NUMBER, phone);
        actionSecondIntent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE,type);
        //actionSecondIntent.putExtra(AppConstants.BundleKey.NOTIFICATION_CONVERSATION_TYPE, info.first);
        /* data.getString(AppConstants.BundleKey.MOBILE_NUMBER, EMPTY_TEXT));*/
        actionSecondIntent.putExtra(AppConstants.BundleKey.CONTACT_NAME,
            TextUtils.isEmpty(contactName) ? phone : contactName);
        return PendingIntent.getBroadcast(context, notificationId,
            actionSecondIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private static PendingIntent createActionFirstIntent(Context context,
                                                         int notificationId,
                                                         String messageId,
                                                         List<MessageEntity> messageEntityList,
                                                         String phone, String contactName, ConversationType type) {

        Intent actionFirstIntent = new Intent(context, NotificationActionReceiver.class);
        actionFirstIntent.putExtra(AppConstants.BundleKey.NOTIFICATION_ID, notificationId);
        actionFirstIntent.putExtra(AppConstants.BundleKey.MESSAGE_ID, messageId);
        actionFirstIntent.putExtra(AppConstants.BundleKey.MARK_ALL_AS_READ,
            messageEntityList.size() > 1);
        actionFirstIntent.putExtra(AppConstants.BundleKey.MOBILE_NUMBER, phone);
        actionFirstIntent.putExtra(AppConstants.BundleKey.OTP_NUMBER, sOtpNumber);
        actionFirstIntent.putExtra(AppConstants.BundleKey.CONTACT_NAME,
            TextUtils.isEmpty(contactName) ? phone : contactName);
        actionFirstIntent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE,type);
        actionFirstIntent.setAction(sActionFirst);
        return PendingIntent.getBroadcast(context, notificationId,
            actionFirstIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private static void setupActionButtonText(Context context,
                                              boolean isFromSameConversation,
                                              List<MessageEntity> messageEntityList,
                                              String contactName) {

        if (!TextUtils.isEmpty(sOtpNumber)) {
            sActionFirst = AppConstants.IntentActions.COPY;
            sActionSecond = AppConstants.IntentActions.DELETE;
            sActionTextFirst = context.getString(R.string.copy_otp);
            sActionTextSecond = context.getString(R.string.delete_message);
            sActionIconFirst = R.drawable.icn_copy_notification;
            actionIconSecond = R.drawable.icn_gray_delete;
        } else {
            if (isFromSameConversation) { //messageEntityList.size() == 1
                MessageEntity entity = messageEntityList.get(0);
                switch (Utils.getConversationType(entity.getMask(),((GupshupTextMessage)entity.getContent()).getText())) {
                    case PERSONAL:
                        if (TextUtils.isEmpty(contactName)) {
                            sActionFirst = AppConstants.IntentActions.MARK_AS_READ;
                            sActionSecond = AppConstants.IntentActions.ADD_TO_CONTACT;
                            sActionTextFirst = context.getString(R.string.mark_as_read);
                            sActionTextSecond = context.getString(R.string.add_to_contact);
                            sActionIconFirst = R.drawable.icn_mark_as_read;
                            actionIconSecond = R.drawable.icn_add_to_contact_notification;
                        } else {
                            sActionFirst = AppConstants.IntentActions.MARK_AS_READ;
                            sActionSecond = AppConstants.IntentActions.REPLY;
                            sActionTextFirst = context.getString(R.string.mark_as_read);
                            sActionTextSecond = context.getString(R.string.reply);
                            sActionIconFirst = R.drawable.icn_mark_as_read;
                            actionIconSecond = R.drawable.icn_reply_notification;
                        }
                        break;
                    case TRANSACTIONAL:
                    case PROMOTIONAL:
                        sActionFirst = AppConstants.IntentActions.MARK_AS_READ;
                        sActionSecond = AppConstants.IntentActions.MARK_AS_BLOCK;
                        sActionTextFirst = context.getString(R.string.mark_as_read);
                        sActionTextSecond = context.getString(R.string.block);
                        sActionIconFirst = R.drawable.icn_mark_as_read;
                        actionIconSecond = R.drawable.icn_mark_as_block;
                        break;
                }

            } else {
                sActionFirst = AppConstants.IntentActions.MARK_AS_READ;
                sActionIconFirst = R.drawable.icn_mark_as_read;
                sActionTextFirst = context.getString(R.string.mark_as_read);
            }
        }
    }

    private static void extractOtpNumber(String messageBody) {

        if (HomeFragmentViewModel.isManualRegistrationInProcess) {
            if (messageBody != null) {  //&& messageBody.toLowerCase().matches(".*\\botp\\b.*")
                Pattern otpPattern = Pattern.compile(OTP_MATCHER,
                    Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                Matcher otpMatcher = otpPattern.matcher(messageBody);
                if (otpMatcher.find()) {
                    if (otpMatcher.groupCount() >= 1) {
                        sOtpNumber = otpMatcher.group(1);
                        sOtpNumber = sOtpNumber.replaceAll("[^0-9]+", " ").trim();
                    }
                }
            }
        }
    }

    private static void updateNotificationBuilder(Context context,
                                                  ConversationEntity conversationEntity,
                                                  NotificationCompat.Builder notificationBuilder,
                                                  String channelId, int notificationId,
                                                  PendingIntent pendingIntent) {


        Bitmap largeIconBitmap;
        notificationBuilder.setContentIntent(pendingIntent);
        //notificationBuilder.setSound(defaultSoundUri, Notification.DEFAULT_SOUND);

        // notificationBuilder.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.sms_tone));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }
        //notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);    //must for heads up display
        //notificationBuilder.setFullScreenIntent(pendingIntent,true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            largeIconBitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.notification_icon);
            notificationBuilder.setLargeIcon(largeIconBitmap);
            notificationBuilder.setSmallIcon(R.drawable.icn_notification_status_bar);
        } else {
            notificationBuilder.setSmallIcon(R.drawable.icn_notification_status_bar);
        }
        createNotificationManager(context, channelId, conversationEntity, notificationId,
            notificationBuilder);
    }

    private static NotificationCompat.Builder createNotificationBuilder(Context context,
                                                                        String channelId,
                                                                        String phone,
                                                                        String contactName,
                                                                        String messageBody,
                                                                        List<MessageEntity> messageEntityList,
                                                                        String otpNumber,
                                                                        MessageDAO.CountData countData) {


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId);
        notificationBuilder.setSmallIcon(R.drawable.icn_notification_status_bar);
        if (!TextUtils.isEmpty(otpNumber)
            || messageEntityList.size() == 1) {
            notificationBuilder.setContentTitle(TextUtils.isEmpty(contactName) ? phone : contactName);
            notificationBuilder.setContentText(TextUtils.isEmpty(otpNumber) ? messageBody : otpNumber);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(TextUtils.isEmpty(otpNumber) ? messageBody : otpNumber));
        } else if (messageEntityList.size() > 1) {
            //StringBuilder conversationTitle = new StringBuilder();
            StringBuilder contentTitle = new StringBuilder();
            String displayName = "";
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                NotificationCompat.MessagingStyle messageStyle = new NotificationCompat
                    .MessagingStyle(context.getString(R.string.notification_me));

                //The messages should be added in chronologic order, i.e. the oldest first, the newest last.
                for (int mSgCount = messageEntityList.size() - 1; mSgCount >= 0; mSgCount--) {
                    MessageEntity entity = messageEntityList.get(mSgCount);
                    Pair<String, String> userInfo = MessageUtils
                        .getContactUserDetail(context, entity.getMask());

                    if (userInfo != null) {
                        displayName = userInfo.first;
                        //profileThumb = userInfo.second;
                    }
                    String messageTitle = TextUtils.isEmpty(displayName)
                        ? entity.getMask() : displayName;
                   *//* if (!conversationTitle.toString().contains(messageTitle)) {
                        conversationTitle.append(messageTitle);
                        if (mSgCount >= 0) {
                            conversationTitle.append(", ");
                        }
                    }*//*
                    GupshupTextMessage message = (GupshupTextMessage) entity.getContent();
                    messageStyle.addMessage(message == null ? "" : message.getText(), entity.getMessageTimestamp(),
                        TextUtils.isEmpty(displayName) ? entity.getMask() + ":" : displayName + ":");
                    *//*if (mSgCount == 4)
                        break;*//*
                }
                *//*if (conversationTitle.toString().endsWith(", ")) {
                    conversationTitle = new StringBuilder(conversationTitle.substring(0, conversationTitle.length() - 2));
                }*//*
                messageStyle.setConversationTitle(context.getString(R.string.app_name));  //conversationTitle
                notificationBuilder.setStyle(messageStyle);
            } else {*/
            //TODO: to make consistence UI we disabled MessageStyle
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            for (int mSgCount = 0; mSgCount < messageEntityList.size(); mSgCount++) {

                MessageEntity entity = messageEntityList.get(mSgCount);
                Pair<String, String> userInfo = MessageUtils
                    .getContactUserDetail(context, entity.getMask());

                if (userInfo != null) {
                    displayName = userInfo.first;
                    //profileThumb = userInfo.second;
                }
                String messageTitle = TextUtils.isEmpty(displayName)
                    ? entity.getMask() : displayName;
                    /*if (!conversationTitle.toString().contains(messageTitle)) {
                        conversationTitle.append(messageTitle);
                        if (mSgCount >= 0) {
                            conversationTitle.append(", ");
                        }
                    }*/
                GupshupTextMessage message = (GupshupTextMessage) entity.getContent();
                inboxStyle.addLine(createNotificationLine(messageTitle, message == null ? "" : message.getText()));
                if (mSgCount == 0) {
                    contentTitle.append(createNotificationLine(messageTitle, message == null ? "" : message.getText()));
                }
                if (mSgCount == 4)
                    break;
            }
            int remainingMsgCount = countData.getMessageCount() - 5;
            if (remainingMsgCount > 0) {
                inboxStyle.setSummaryText(context.getResources()
                    .getQuantityString(R.plurals.more_messages, remainingMsgCount, remainingMsgCount));
            }
               /* if (conversationTitle.toString().endsWith(", ")) {
                    conversationTitle = new StringBuilder(conversationTitle.substring(0, conversationTitle.length() - 2));
                }*/
            inboxStyle.setBigContentTitle(context.getString(R.string.app_name));//conversationTitle
            notificationBuilder.setStyle(inboxStyle);
            notificationBuilder.setContentTitle(context.getResources()
                .getQuantityString(R.plurals.messages, messageEntityList.size(), messageEntityList.size()));
            notificationBuilder.setContentText(contentTitle);
        }
        notificationBuilder.setAutoCancel(true);
        return notificationBuilder;
    }

    private static void createNotificationManager(Context context, String channelId,
                                                  ConversationEntity conversationEntity,
                                                  int notificationId,
                                                  NotificationCompat.Builder notificationBuilder) {
        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = context.getString(R.string.default_notification_channel_name);
            //String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            channel = new NotificationChannel(channelId, name, importance);
        }
        applyNotificationSetting(context, notificationBuilder, notificationManager, channel);
        notificationManager.notify(/*notificationId*/0, notificationBuilder.build());
    }

    private static void applyNotificationSetting(Context
                                                     context, NotificationCompat.Builder
                                                     notificationBuilder, NotificationManager notificationManager, NotificationChannel
                                                     channel) {

        Uri defaultSoundUri;
        PreferenceManager preferenceManager = PreferenceManager.getInstance(context);
        if (TextUtils.isEmpty(preferenceManager.getNotificationTone())) {
            defaultSoundUri = RingtoneManager.getActualDefaultRingtoneUri(context,
                RingtoneManager.TYPE_NOTIFICATION);
        } else {
            defaultSoundUri = Uri.parse(preferenceManager.getNotificationTone());
        }
        if (Uri.EMPTY.equals(defaultSoundUri)) {
            defaultSoundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.sms_tone);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (channel != null) {
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();
                if (preferenceManager.isNotificationEnabled()) {
                    channel.enableLights(preferenceManager.isNotificationLightEnabled());
                    channel.setLightColor(Color.rgb(0, Color.rgb(255, 0, 0), 0));
                    channel.enableVibration(preferenceManager.isVibrationEnabled());
                    channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    channel.setSound(defaultSoundUri, audioAttributes);
                }
                notificationManager.createNotificationChannel(channel);
            }
        } else {
            if (preferenceManager.isNotificationEnabled()) {
                notificationBuilder.setSound(defaultSoundUri, Notification.DEFAULT_SOUND);
                if (preferenceManager.isNotificationLightEnabled()) {
                    notificationBuilder.setLights(Color.rgb(255, 0, 0), 1000, 1000);
                }
                if (preferenceManager.isVibrationEnabled()) {
                    notificationBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                }
            }
        }

    }


    /**
     * To add custom font to title.
     *
     * @param title
     * @param text
     * @return
     */
    private static SpannableString createNotificationLine(String title, String text) {

        final StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        final SpannableString spannableString;
        if (title != null && title.length() > 0) {
            spannableString = new SpannableString(String.format(Locale.getDefault(), "%s : %s", title, text));
            spannableString.setSpan(boldSpan, 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            spannableString = new SpannableString(String.format(Locale.getDefault(), "%s : %s", title, text));
        }
        return spannableString;
    }

}
