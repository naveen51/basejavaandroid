package io.gupshup.smsapp.fcm;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.DataService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Ram Prakash Bhat on 13/4/18.
 */

public class SMSFirebaseMessagingService extends FirebaseMessagingService {
    public interface PushDataKeys {
        String TITLE = "title";
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (PreferenceManager.getInstance(this).isNotificationEnabled()) {
            try {
                Map<String, String> data = remoteMessage.getData();

                //TODO Data Will Contain to Number too.. Fetch Sim slot from that and use it.

                if (remoteMessage == null)
                    return;
                String toMesage = null;
                String messageText = null;
                String fromMessage = null;
                try {
                    String sms = data.get("sms");
                    JSONObject jsonObject = new JSONObject(sms);
                    toMesage = Utils.getMaskFromPhoneNumber(jsonObject.optString("to", ""));
                    fromMessage = Utils.getMaskFromPhoneNumber(jsonObject.optString("from", ""));
                    messageText = jsonObject.optString("message", "");
                    if (!TextUtils.isEmpty(toMesage)) {
                        Context context = MainApplication.getContext();
                        RegistrationInfo registrationInfo = AppUtils.getSimOneData(context);
                        int simSlot = -1;
                        if (registrationInfo != null && toMesage.equals(registrationInfo.fullNumberWithPlus)) {
                            simSlot = 0;
                        } else {
                            registrationInfo = AppUtils.getSimTwoData(context);
                            if (registrationInfo != null && toMesage.equals(registrationInfo.fullNumberWithPlus)) {
                                simSlot = 1;
                            }
                        }
                        if (simSlot != -1) {
                            MessagingService.getInstance(DataService.class).receiveMessages(simSlot);
                        }
                    }
                } catch (Exception e) {
                    AppUtils.logToFile(this, "Exception in FCM message receive.." + e.getMessage());
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                //To check whether need to show notification or not (Mute/Un mute).
                /*List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class)
                    .getConversationByMask(Utils.getMaskFromPhoneNumber(fromMessage));
                ConversationEntity conversationEntity = null;
                if (conversationEntities.size() > 0) {
                    conversationEntity = conversationEntities.get(0);
                }*/
                //To avoid crash if its a first data message, we are not saving this to DB its just temp. local holder.
                /*if (conversationEntity == null) {
                    conversationEntity = Utils.getNewConversation(Utils.getMaskFromPhoneNumber(fromMessage),
                        fromMessage, MessageChannel.DATA, messageText);
                }*/

                /*if (data.size() > 0) {
                    Bundle extras = new Bundle();
                    for (Map.Entry<String, String> entry : data.entrySet()) {
                        extras.putString(entry.getKey(), entry.getValue());
                    }// FROM FCM. Let's handle it.
                    String notificationMessage = (String) extras.get(PushDataKeys.TITLE);
                    String notificationTitle = null;
                    notificationTitle = data.get("title");
                    notificationMessage = messageText;
                    NotificationHelper.createNotification(MainApplication.getContext(),
                        fromMessage, notificationMessage, extras, null,
                        conversationEntity.getDisplay(), conversationEntity.getImage(),
                        conversationEntity);
                }*/ /*else if (notification != null) {
                NotificationHelper.createNotification(MainApplication.getContext(),
                    notification.getTitle(), notification.getBody(), null, null, "", "", conversationEntity);
            }*/
            } catch (Throwable t) {
                //LogUtility.d("SMS FCM LIST", "Error parsing FCM message", t);
            }
        }
    }
}
