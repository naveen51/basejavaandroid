package io.gupshup.smsapp.fcm;

import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.StatusResponse;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.NetworkUtil;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ram Prakash Bhat on 13/4/18.
 */

public class SMSFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Inject
    public APIService mService;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //LogUtility.d("Token", "Refreshed token: " + refreshedToken);
        PreferenceManager.getInstance(this).setFCMToken(refreshedToken);
        MainApplication.getInstance().getAppComponent().inject(this);
        sendFirebaseTokenToServer(refreshedToken);
    }

    private void sendFirebaseTokenToServer(String refreshedToken) {

        if (!NetworkUtil.isAvailable(this))
            return;
        PreferenceManager manager = PreferenceManager.getInstance(this);
        RegistrationInfo simOneData = AppUtils.getSimOneData(this);
        RegistrationInfo simTwoData = AppUtils.getSimTwoData(this);

        if (simOneData != null && !TextUtils.isEmpty(simOneData.phoneNumber)
                && RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneData.simOneState) {
            pushRegistration(manager, simOneData, refreshedToken);//Sim one push Registration
        }
        if (simTwoData != null && !TextUtils.isEmpty(simTwoData.phoneNumber)
                && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoData.simTwoState) {
            pushRegistration(manager, simTwoData, refreshedToken); //Sim two push Registration
        }
    }

    private void pushRegistration(PreferenceManager manager, RegistrationInfo data, String refreshedToken) {
        //TODO: check status of individual sim verification

        if (manager.isRegistrationDone()) {
            Call<StatusResponse> call = mService.addPNToken(Utils.verifyCountryCode(data.countryCode),
                    data.phoneNumber, refreshedToken, data.apiKey);
            call.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    //if (response.body() != null)
                        //LogUtility.d("FCM_SUCCESS", response.body().getmStatus());
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    //LogUtility.d("FCM_FAILURE", t.getMessage());

                }
            });
        }
    }

}
