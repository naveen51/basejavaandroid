package io.gupshup.smsapp.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.text.Spannable;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.database.converters.GupshupMessageConverter;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Entity(
        tableName = "messages",
        indices = @Index("mask"),
        foreignKeys = @ForeignKey(
                entity = ConversationEntity.class,
                parentColumns = "mask",
                childColumns = "mask",
                onDelete = ForeignKey.CASCADE
        )
)
public class MessageEntity {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "message_id")
    protected String messageId;

    @NonNull
    @ColumnInfo(name = "mask")
    protected String mask;

    @NonNull
    @TypeConverters(EnumConverter.MessageDirectionConverter.class)
    @ColumnInfo(name = "direction")
    protected MessageDirection direction;

    @NonNull
    @TypeConverters(GupshupMessageConverter.class)
    @ColumnInfo(name = "content")
    protected GupshupMessage content;

    @Ignore
    protected String contentString;

    @NonNull
    @TypeConverters(EnumConverter.MessageChannelConverter.class)
    @ColumnInfo(name = "channel")
    protected MessageChannel channel;


    @NonNull
    @ColumnInfo(name = "carier_name")
    protected String carierName;

    @NonNull
    public String getCarierName() {
        return carierName;
    }

    public void setCarierName(@NonNull String carierName) {
        this.carierName = carierName;
    }

    @ColumnInfo(name = "destination")
    protected String destination;

    @ColumnInfo(name = "sent_timestamp")
    protected long sentTimestamp;

    @ColumnInfo(name = "message_timestamp")
    protected long messageTimestamp;

    @ColumnInfo(name = "delivered_timestamp")
    protected long deliveredTimestamp;

    @ColumnInfo(name = "read_timestamp")
    protected long readTimestamp;

    @ColumnInfo(name = "read_acknowledged")
    protected boolean readAcknowledged;

    @ColumnInfo(name = "metadata")
    protected String metadata;

    @ColumnInfo(name = "search")
    protected String search;

    @ColumnInfo(name = "message_status")
    @TypeConverters(EnumConverter.MessageStatusConverter.class)
    protected MessageStatus messageStatus;

    @ColumnInfo(name = "incoming_sim_id")
    protected int incomingSIMID;

    @ColumnInfo(name = "creation_time")
    protected long creationTime;

    @Ignore
    protected Spannable searchedItem;

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public int getIncomingSIMID() {
        return incomingSIMID;
    }

    public void setIncomingSIMID(int incomingSIMID) {
        this.incomingSIMID = incomingSIMID;
    }

    @NonNull
    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(@NonNull MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    @NonNull
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(@NonNull String messageId) {
        this.messageId = messageId;
    }

    @NonNull
    public String getMask() {
        return mask;
    }

    public void setMask(@NonNull String mask) {
        this.mask = mask;
    }

    @NonNull
    public MessageDirection getDirection() {
        return direction;
    }

    public void setDirection(@NonNull MessageDirection direction) {
        this.direction = direction;
    }

    @NonNull
    public GupshupMessage getContent() {
        return content;
    }

    public void setContent(@NonNull GupshupMessage content) {
        this.content = content;
    }

    public String getContentString() {
        return contentString;
    }

    public void setContentString(String contentString) {
        this.contentString = contentString;
    }

    @NonNull
    public MessageChannel getChannel() {
        return channel;
    }

    public void setChannel(@NonNull MessageChannel channel) {
        this.channel = channel;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public long getSentTimestamp() {
        return sentTimestamp;
    }

    public void setSentTimestamp(long sentTimestamp) {
        this.sentTimestamp = sentTimestamp;
    }

    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    public void setMessageTimestamp(long messageTimestamp) {
        this.messageTimestamp = messageTimestamp;
    }

    public long getDeliveredTimestamp() {
        return deliveredTimestamp;
    }

    public void setDeliveredTimestamp(long deliveredTimestamp) {
        this.deliveredTimestamp = deliveredTimestamp;
    }

    public long getReadTimestamp() {
        return readTimestamp;
    }

    public void setReadTimestamp(long readTimestamp) {
        this.readTimestamp = readTimestamp;
    }

    public boolean isReadAcknowledged() {
        return readAcknowledged;
    }

    public void setReadAcknowledged(boolean readAcknowledged) {
        this.readAcknowledged = readAcknowledged;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Spannable getSearchedItem() {
        return searchedItem;
    }

    public void setSearchedItem(Spannable searchedItem) {
        this.searchedItem = searchedItem;
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || this.messageId .equalsIgnoreCase(((MessageEntity) obj).messageId);
    }
}
