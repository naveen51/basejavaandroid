package io.gupshup.smsapp.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.MessageChannel;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public abstract class JSONConverter<T> {

    private static final Gson gson = new Gson();

    @TypeConverter
    public String toString(T object) {
        if (object == null)
            return null;

        return gson.toJson(object);
    }

    @TypeConverter
    public T toObject(String string) {
        if (string == null)
            return null;

        Type type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        return gson.fromJson(string, type);
    }

    // All JSON Converters
    public static final class MessageEntityConverter extends JSONConverter<MessageEntity> {
        private final GupshupMessageConverter __gupshupMessageConverter = new GupshupMessageConverter();

        @Override
        @TypeConverter
        public String toString(MessageEntity object) {
            if (object == null)
                return null;

            String originalContentString = object.getContentString();
            GupshupMessage originalContent = object.getContent();

            object.setContentString(__gupshupMessageConverter.toString(originalContent));
            object.setContent(null);

            String returnValue = super.toString(object);

            object.setContentString(originalContentString);
            object.setContent(originalContent);

            return returnValue;
        }

        @Override
        @TypeConverter
        public MessageEntity toObject(String string) {
            if (string == null)
                return null;

            MessageEntity object = super.toObject(string);
            object.setContent(__gupshupMessageConverter.toGupshupMessage(object.getContentString()));
            object.setContentString(null);

            return object;
        }
    }

    public static final class MessageChannelListConverter extends JSONConverter<List<MessageChannel>> {
    }

}
