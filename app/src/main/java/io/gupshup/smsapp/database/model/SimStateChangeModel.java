package io.gupshup.smsapp.database.model;

/**
 * Created by Naveen BM on 9/6/2018.
 */
public class SimStateChangeModel {

    private boolean isSimOneChanged;
    private boolean isSimTwoChanged;
    private String simOneState;
    private String simTwoState;

    public String getSimOneState() {
        return simOneState;
    }

    public void setSimOneState(String simOneState) {
        this.simOneState = simOneState;
    }

    public void setSimTwoState(String simTwoStateChange) {
        this.simTwoState = simTwoStateChange;
    }

    public String getSimTwoState() {
        return simTwoState;
    }

    public boolean isSimOneChanged() {
        return isSimOneChanged;
    }

    public void setSimOneChanged(boolean simOneChanged) {
        isSimOneChanged = simOneChanged;
    }

    public boolean isSimTwoChanged() {
        return isSimTwoChanged;
    }

    public void setSimTwoChanged(boolean simTwoChanged) {
        isSimTwoChanged = simTwoChanged;
    }

}
