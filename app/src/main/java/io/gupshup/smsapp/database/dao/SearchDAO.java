package io.gupshup.smsapp.database.dao;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.List;

import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.database.converters.JSONConverter;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;

@Dao
public interface SearchDAO {

    class SearchItem {
        @ColumnInfo(name = "mask")
        String mask;
        @ColumnInfo(name = "display")
        String display;
        @ColumnInfo(name = "content")
        String content;
        @ColumnInfo(name = "last_message_timestamp")
        String lastMessageTimeStamp;
        @ColumnInfo(name = "source")
        String source;
        @TypeConverters(JSONConverter.MessageEntityConverter.class)
        @ColumnInfo(name = "lastMessageData")
        MessageEntity lastMessageData;
        @ColumnInfo(name = "lastMessageId")
        String lastMessageId;
        @ColumnInfo
        String image;

        @TypeConverters(EnumConverter.ConversationTypeConverter.class)
        @ColumnInfo(name = "type")
        ConversationType type;

        @TypeConverters(EnumConverter.ConversationSubTypeConverter.class)
        @ColumnInfo(name = "sub_type")
        ConversationSubType subType;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public MessageEntity getLastMessageData() {
            return lastMessageData;
        }

        public void setLastMessageData(MessageEntity lastMessageData) {
            this.lastMessageData = lastMessageData;
        }

        public String getLastMessageId() {
            return lastMessageId;
        }

        public void setLastMessageId(String lastMessageId) {
            this.lastMessageId = lastMessageId;
        }

        public String getSource() {
            return source;
        }

        public String getMask() {
            return mask;
        }

        public String getDisplay() {
            return display;
        }

        public String getContent() {
            return content;
        }

        public String getLastMessageTimeStamp() {
            return lastMessageTimeStamp;
        }

        public void setMask(String mask) {
            this.mask = mask;
        }

        public void setDisplay(String display) {
            this.display = display;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setLastMessageTimeStamp(String lastMessageTimeStamp) {
            this.lastMessageTimeStamp = lastMessageTimeStamp;
        }

        public void setSource(String source) {
            this.source = source;
        }

        @NonNull
        public ConversationType getType() {
            return type;
        }

        public void setType(@NonNull ConversationType type) {
            this.type = type;
        }

        public ConversationSubType getSubType() {
            return subType;
        }

        public void setSubType(ConversationSubType subType) {
            this.subType = subType;
        }
    }


    class MessageEntityExtended extends MessageEntity {
        @ColumnInfo(name = "position")
        private int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }


    @Query(
        " SELECT c.mask as mask, c.display as display, c.type as type, c.sub_type as sub_type, null as content, c.last_message_timestamp as last_message_timestamp, 'Conversation' as source, c.last_message_id as lastMessageId, c.last_message_data as lastMessageData, c.image as image from `conversations` c where c.mask like '%' || :searchQuery || '%'" +
            " UNION ALL" +
            " SELECT c.mask as mask, c.display as display, c.type as type, c.sub_type as sub_type, null as content, c.last_message_timestamp as last_message_timestamp, 'Conversation' as source, c.last_message_id as lastMessageId, c.last_message_data as lastMessageData, c.image as image  from `conversations` c where c.display like '%' || :searchQuery || '%'" +
            " UNION " +
            " SELECT * from (SELECT distinct(c.mask) as mask, c.display as display, c.type as type, c.sub_type as sub_type, m.content as content, c.last_message_timestamp as last_message_timestamp, 'Message' as source, c.last_message_id as lastMessageId, c.last_message_data as lastMessageData , c.image as image  from `conversations` c inner join `messages` m on c.mask = m.mask and m.content like '%' || :searchQuery || '%' group by m.mask order by c.last_message_timestamp desc) order by c.last_message_timestamp desc")
    List<SearchItem>/*DataSource.Factory<Integer, SearchItem>*/ findEveryWhere(String searchQuery);

    @Query("SELECT m.* , (Select count(m1.content) from `messages` m1 where m1.message_timestamp > m.message_timestamp and m1.mask = :mask) as position from `messages` m where m.mask = :mask and m.content like '%' || :searchQuery || '%' order by m.message_timestamp desc")
    List<MessageEntityExtended> findWithinMessages(String mask, String searchQuery);
}
