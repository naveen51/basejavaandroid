package io.gupshup.smsapp.database.converters;

import android.arch.persistence.room.TypeConverter;

import io.gupshup.crypto.common.key.GupshupPublicKey;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public final class GupshupPublicKeyConverter {

    @TypeConverter
    public String toString(GupshupPublicKey publicKey) {
        if (publicKey == null)
            return null;

        return publicKey.toString();
    }

    @TypeConverter
    public GupshupPublicKey toGupshupPublicKey(String publicKeyString) {
        if (publicKeyString == null)
            return null;

        return new GupshupPublicKey(publicKeyString);
    }

}
