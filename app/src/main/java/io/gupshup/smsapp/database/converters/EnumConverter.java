package io.gupshup.smsapp.database.converters;

import android.arch.persistence.room.TypeConverter;

import java.lang.reflect.ParameterizedType;

import io.gupshup.smsapp.enums.Author;
import io.gupshup.smsapp.enums.ConfigKey;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public abstract class EnumConverter<E extends Enum<E>> {

    @TypeConverter
    public String toString(E enumObject) {
        if (enumObject == null)
            return null;

        return enumObject.name();
    }

    @TypeConverter
    public E toEnum(String enumString) {
        if (enumString == null)
            return null;

        Class<E> enumClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        return E.valueOf(enumClass, enumString);
    }

    // All Enum Converters
    public static final class AuthorConverter extends EnumConverter<Author> {
    }

    public static final class ConfigKeyConverter extends EnumConverter<ConfigKey> {
    }

    public static final class ConversationSubTypeConverter extends EnumConverter<ConversationSubType> {
    }

    public static final class ConversationTypeConverter extends EnumConverter<ConversationType> {
    }

    public static final class MessageChannelConverter extends EnumConverter<MessageChannel> {
    }

    public static final class MessageDirectionConverter extends EnumConverter<MessageDirection> {
    }

    public static final class MessageStatusConverter extends EnumConverter<MessageStatus> {
    }

    public static final class RegSimOneStateKeyConverter extends EnumConverter<RegistrationSimOneState> {
    }

    public static final class RegSimTwoStateKeyConverter extends EnumConverter<RegistrationSimTwoState> {
    }

}
