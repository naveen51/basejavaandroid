package io.gupshup.smsapp.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.database.dao.ConfigDAO;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.dao.MessageMapDAO;
import io.gupshup.smsapp.database.dao.SearchDAO;
import io.gupshup.smsapp.database.entities.ConfigEntity;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.database.entities.MessageIdMap;
import io.gupshup.smsapp.utils.TestUtils;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Database(
        version = 8,
        entities = {
                ConfigEntity.class,
                ConversationEntity.class,
                MessageEntity.class,
                MessageIdMap.class
        }
)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase instance;

    public AppDatabase() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the instance");
        }
    }

    public static AppDatabase createInstance(@NonNull Context context, boolean populateWithTestData) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    RoomDatabase.Builder<AppDatabase> databaseBuilder =
                            Room.databaseBuilder(context, AppDatabase.class, "sms-app.db");

                    databaseBuilder.allowMainThreadQueries();
//                    databaseBuilder.addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5);
                    databaseBuilder.fallbackToDestructiveMigration();
                    if (populateWithTestData)
                        databaseBuilder.addCallback(TestUtils.POPULATE_WITH_TEST_DATA);

                    instance = databaseBuilder.build();
                }
            }
        }

        return instance;
    }


    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            //Added one column deviceLocation to the table OrderEntity
            try {
                database.execSQL("ALTER TABLE messages"
                        + " ADD COLUMN carier_name TEXT");
            } catch (Exception ignored) {
            }

        }
    };


    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // No changes...

        }
    };

    private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            try {
                database.execSQL("ALTER TABLE messages ADD COLUMN message_status TEXT");
            } catch (Exception ignored) {
            }
        }
    };

    private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            try {
                database.execSQL("ALTER TABLE messages ADD COLUMN incoming_sim_id INTEGER NOT NULL DEFAULT -1");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static AppDatabase getInstance(Context context) {
        if (instance == null && context != null) {
            createInstance(context, false);
        }

        return instance;
    }

    public static AppDatabase getInstance() {
        return instance;
    }

    public abstract ConfigDAO configDAO();

    public abstract ConversationDAO conversationDAO();

    public abstract MessageDAO messageDAO();

    public abstract SearchDAO searchDAO();

    public abstract MessageMapDAO messageMapDAO();

    //    public abstract ConversationMessageDAO conversationMessageDAO();

}
