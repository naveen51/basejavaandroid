package io.gupshup.smsapp.database.converters;

import android.arch.persistence.room.TypeConverter;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public final class GupshupMessageConverter {

    @TypeConverter
    public String toString(GupshupMessage gupshupMessage) {
        String toReturn = null;
        if (gupshupMessage != null) {
            switch (gupshupMessage.getType()) {
                case TEXT:
                    toReturn = ((GupshupTextMessage) gupshupMessage).getText();
                    break;
                default:
                    toReturn = "";
            }
        }
        return toReturn;
    }

    @TypeConverter
    public GupshupMessage toGupshupMessage(String gupshupMessageString) {
        GupshupMessage toReturn = null;
        if (gupshupMessageString != null) {
            toReturn = new GupshupTextMessage(gupshupMessageString);
        }
        return toReturn;
    }

}
