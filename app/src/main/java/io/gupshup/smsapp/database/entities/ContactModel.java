package io.gupshup.smsapp.database.entities;

import android.arch.persistence.room.ColumnInfo;

public class ContactModel {
    @ColumnInfo
    private String mask;
    @ColumnInfo
    private String display;
    @ColumnInfo
    private String image;

    @ColumnInfo(name = "original_number")
    private String originalPhoneNumber;


    public String getOriginalPhoneNumber() {
        return originalPhoneNumber;
    }

    public void setOriginalPhoneNumber( String originalPhoneNumber) {
        this.originalPhoneNumber = originalPhoneNumber;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
