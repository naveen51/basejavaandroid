package io.gupshup.smsapp.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;
import android.support.annotation.NonNull;

import java.util.List;

import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.enums.ConversationType;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Dao
public interface ConversationDAO {

    @Insert
    long[] insert(ConversationEntity... conversations);

    @Update
    int update(ConversationEntity... conversations);

    @Delete
    int delete(ConversationEntity... conversations);

    @Query("SELECT * FROM `conversations` WHERE `mask` = :mask")
    List<ConversationEntity> get(@NonNull String mask);

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @Query("SELECT * FROM `conversations` WHERE `type` IN (:types) AND `sub_type` is not 'ARCHIVE' ORDER BY `last_message_timestamp` DESC")
    DataSource.Factory<Integer, ConversationEntity> getRecent(ConversationType... types);

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @Query("SELECT COUNT(no_of_unread_messages) FROM `conversations` WHERE `type` IN (:types) AND  `is_unread` = 1 AND `sub_type` is not 'ARCHIVE'")
    LiveData<Integer> getTabCount(ConversationType... types);

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @Query("SELECT * FROM (SELECT * FROM `conversations` WHERE `no_of_unread_messages` > 0 or `is_unread` = 1) WHERE `type` in (:types)")
    LiveData<List<ConversationEntity>> getUnreadMessages(ConversationType... types);

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @Query("SELECT * FROM `conversations` WHERE `no_of_unread_messages` > 0  ORDER BY `last_message_timestamp` DESC")
    List<ConversationEntity> getLastReceivedMessages();

    @Query("UPDATE `conversations` SET `draft_message` = :message ,`draft_creation_time` = :draftCreationTime , `last_message_timestamp` = :lastMessageTimeStamp  where `mask` = :mask")
    int updateDraftMessage(@NonNull String mask, String message, long draftCreationTime, long lastMessageTimeStamp);

    @Query("SELECT `mask`, `display`, `image` from `conversations` WHERE  `original_conversation_type` = 'PERSONAL' order by LOWER(display) asc")
    DataSource.Factory<Integer, ContactModel> getPersonalContactList();

    @Query("Update `conversations` Set `is_pn_required` = 0 where `no_of_unread_messages` > 0 or `is_unread` = 1 and `is_pn_required` = 1")
    int updatePNStatus();

    @Query("select * from `conversations` where is_pn_required > 0")
    List<ConversationEntity> getPNConversations();

    @Query("update `conversations` set is_unread = 1 where mask=:mask")
    int markConversationAsUnread(String mask);

    @Query("Select * from `conversations` where `sub_type` = 'ARCHIVE' order by `last_message_timestamp` desc")
    DataSource.Factory<Integer, ConversationEntity> getArchivedConversations();

    @Query("select `mask` from `conversations` where `type` =:conversationType AND `sub_type` is not 'ARCHIVE' ORDER BY `last_message_timestamp` DESC")
    List<String> getAllConversationsForType(String conversationType);

    @Query("select `mask` from `conversations` where `sub_type` = 'ARCHIVE' order by `last_message_timestamp` desc")
    List<String> getAllConversationsForArchive();

    @Query("SELECT * FROM `conversations` where mask in (:masks)")
    List<ConversationEntity> getConversations(String[] masks);

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @Query("SELECT COUNT(no_of_unread_messages) FROM `conversations` WHERE `type` IN (:types) AND  `is_unread` = 1 AND `sub_type` is not 'ARCHIVE'")
    int getUnreadCount(ConversationType... types);

    @Query("SELECT * FROM `conversations` where mask in (:mask)")
    ConversationEntity getConversationFromMask(String mask);

    @Query("select `original_number` from `conversations`  ORDER BY `last_message_timestamp` DESC")
    List<String> getOriginalContactNumberList();

}
