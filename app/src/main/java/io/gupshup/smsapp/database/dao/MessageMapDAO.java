package io.gupshup.smsapp.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.gupshup.smsapp.database.entities.MessageIdMap;

@Dao
public interface MessageMapDAO {
    @Insert
    void insert(MessageIdMap... messageIdMaps);

    @Update
    int update(MessageIdMap... messageIdMaps);

    @Delete
    void delete(MessageIdMap... messageIdMaps);


    @Query("SELECT `system_message_id` from `messageMap` where `gupshup_message_id` in (:gsMessageIds)")
    List<String> fetchSystemMessageIdsFromGupshupMessageIds(String[] gsMessageIds);

    @Query("SELECT `gupshup_message_id` from `messageMap` where `system_message_id` in (:systemMessageIds)")
    List<String> fetchGupshupMessageIdsFromSystemMessageIds(String[] systemMessageIds);

    @Query("DELETE FROM `messageMap` where `gupshup_message_id` in (:gupshupMessageIds)")
    void deleteMessageIdsUsingGupshupMessageIds(String[] gupshupMessageIds);

    @Query("DELETE FROM `messageMap` where `system_message_id` in (:systemMessageIds)")
    void deleteMessageIdsUsingSystemMessageIds(String[] systemMessageIds);
}
