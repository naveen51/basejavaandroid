package io.gupshup.smsapp.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.enums.ConfigKey;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Entity(
    tableName = "config"
)
public class ConfigEntity {

    @NonNull
    @PrimaryKey
    @TypeConverters(EnumConverter.ConfigKeyConverter.class)
    @ColumnInfo(name = "key")
    private ConfigKey key;

    @ColumnInfo(name = "value")
    private String value;

    @NonNull
    public ConfigKey getKey() {
        return key;
    }

    public void setKey(@NonNull ConfigKey key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
