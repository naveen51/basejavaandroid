package io.gupshup.smsapp.database.providers;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.database.entities.GlobalSearchEntity;

public class GlobalSearchEntityProvider {
    private List<GlobalSearchEntity> globalSearchEntities;

    public GlobalSearchEntityProvider(ArrayList<GlobalSearchEntity> globalSearchEntities) {
        this.globalSearchEntities = globalSearchEntities;
    }

    public int getIndex(GlobalSearchEntity item) {
        return globalSearchEntities.indexOf(item);
    }

    public List<GlobalSearchEntity> getSearchEntities(int from, int pageSize) {
        int finalIndex = from + pageSize;
        if (finalIndex > globalSearchEntities.size()) {
            finalIndex = globalSearchEntities.size();
        }
        if (from != 0 && (from == getSize() -1))  {
            return new ArrayList<>();
        }
        return globalSearchEntities.subList(from, finalIndex);
    }

    public int getSize() {
        return globalSearchEntities.size();
    }
}
