package io.gupshup.smsapp.database.dao;

import android.arch.paging.DataSource;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.support.annotation.NonNull;

import java.util.List;

import io.gupshup.smsapp.database.entities.MessageEntity;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Dao
public interface MessageDAO {

    class CountData {
        @ColumnInfo(name = "count")
        public int messageCount;

        public int getMessageCount() {
            return messageCount;
        }
    }

    @Insert
    long[] insert(MessageEntity... messages);

    @Update
    int update(MessageEntity... messages);

    @Delete
    int delete(MessageEntity... messages);

    @Query("SELECT * FROM `messages` WHERE `message_id` = :messageId")
    List<MessageEntity> get(@NonNull String messageId);

    /*
    @Query(
        "SELECT `m`.* " +
            "FROM `conversations` AS `c` " +
            "INNER JOIN `messages` AS `m` ON `c`.`mask` = `m`.`mask` " +
            "WHERE `c`.`conversation_id` = :conversationId " +
            "ORDER BY `m`.`message_timestamp` DESC"
    )
    DataSource.Factory<Integer, MessageEntity> getByConversation(long conversationId);
    */

    @Query("SELECT * FROM `messages` WHERE `mask` = :mask ORDER BY CASE WHEN `message_timestamp` > 0 THEN `message_timestamp` ELSE `sent_timestamp` END DESC")
    DataSource.Factory<Integer, MessageEntity> getByConversation(@NonNull String mask);

    @Query("UPDATE `messages` SET `read_timestamp` = :readTimestamp WHERE `mask` = :mask AND `direction` = 'INCOMING' AND `read_timestamp` < 0")
    int markIncomingAsRead(@NonNull String mask, long readTimestamp);

    @Query("SELECT * FROM `messages` WHERE `direction` = 'INCOMING' AND `read_timestamp` > 0 AND `read_acknowledged` = 0")
    List<MessageEntity> getIncomingUnacknowledgedReads();

    @Query("SELECT * FROM `messages` WHERE `read_timestamp` < 0 AND `direction` = 'INCOMING'  ORDER BY `delivered_timestamp` DESC LIMIT :count;")
    List<MessageEntity> getTopUnreadMessages(int count);

    @Query("SELECT COUNT(*) AS `count` from `messages` WHERE `read_timestamp` < 0 AND `direction` = 'INCOMING'")
    CountData getTotalUnreadCount();

    @Query("Select * from `messages` m where m.mask = :mask order by m.message_timestamp desc LIMIT 1")
    List<MessageEntity> getLastMessageForAMask(String mask);

    @Query("Select * from `messages` m where m.mask = :mask")
    List<MessageEntity> getMessagesForMask(String mask);

    @Query("SELECT * FROM `messages` WHERE `direction` = 'INCOMING' AND `read_timestamp` < 0 AND `read_acknowledged` = 0 AND `mask` in (:masks)")
    List<MessageEntity> getIncomingUnacknowledgedReads(String... masks);

    @Query("select messages.mask from messages inner join conversations on messages.mask = conversations.mask where direction = 'OUTGOING' and contact_id is not null group by messages.mask order by count(messages.message_id)  desc  limit 8")
    List<String> getTopContactsBasedOnMaxOutGoingMsgs();

    @Query("SELECT COUNT(*) AS `count` from `messages` m where m.mask = :mask")
    CountData getTotalMessageCount(String mask);

}
