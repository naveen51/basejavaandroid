package io.gupshup.smsapp.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "messageMap")
public class MessageIdMap {
    @NonNull
    @ColumnInfo(name = "system_message_id")
    private String systemMessageId;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "gupshup_message_id")
    private String gupshupMessageId;

    public MessageIdMap(@NonNull String systemMessageId, @NonNull String gupshupMessageId) {
        this.systemMessageId = systemMessageId;
        this.gupshupMessageId = gupshupMessageId;
    }

    @NonNull
    public String getSystemMessageId() {
        return systemMessageId;
    }

    public void setSystemMessageId(@NonNull String systemMessageId) {
        this.systemMessageId = systemMessageId;
    }

    @NonNull
    public String getGupshupMessageId() {
        return gupshupMessageId;
    }

    public void setGupshupMessageId(@NonNull String gupshupMessageId) {
        this.gupshupMessageId = gupshupMessageId;
    }
}
