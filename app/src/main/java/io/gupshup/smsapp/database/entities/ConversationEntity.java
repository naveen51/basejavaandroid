package io.gupshup.smsapp.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.List;

import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.database.converters.GupshupPublicKeyConverter;
import io.gupshup.smsapp.database.converters.JSONConverter;
import io.gupshup.smsapp.enums.Author;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Entity(
    tableName = "conversations",
    indices = {
        @Index(
            value = "mask",
            unique = true
        ),
        @Index(
            value = "last_message_id",
            unique = true
        )
    },
    foreignKeys = @ForeignKey(
        entity = MessageEntity.class,
        parentColumns = "message_id",
        childColumns = "last_message_id",
        onDelete = ForeignKey.SET_NULL
    )
)
public class ConversationEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "conversation_id")
    private long conversationId;

    @ColumnInfo(name = "contact_id")
    private String contactId;

    @NonNull
    @ColumnInfo(name = "mask")
    private String mask;

    @NonNull
    @TypeConverters(JSONConverter.MessageChannelListConverter.class)
    @ColumnInfo(name = "channels")
    private List<MessageChannel> channels;

    @TypeConverters(GupshupPublicKeyConverter.class)
    @ColumnInfo(name = "ip_public_key")
    private GupshupPublicKey ipPublicKey;

    @ColumnInfo(name = "ip_last_synced_timestamp")
    private long ipLastSyncedTimestamp;

    @ColumnInfo(name = "display")
    private String display;

    @ColumnInfo(name = "image")
    private String image;

    @ColumnInfo(name = "pb_last_synced_timestamp")
    private long pbLastSyncedTimestamp;

    @NonNull
    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @ColumnInfo(name = "type")
    private ConversationType type;

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @ColumnInfo(name = "conversation_sub_type")
    private ConversationType conversationSubType;

    @NonNull
    @TypeConverters(EnumConverter.AuthorConverter.class)
    @ColumnInfo(name = "type_set_by")
    private Author typeSetBy;

    @TypeConverters(EnumConverter.ConversationSubTypeConverter.class)
    @ColumnInfo(name = "sub_type")
    private ConversationSubType subType;

    @ColumnInfo(name = "draft_message")
    private String draftMessage;

    @ColumnInfo(name = "last_message_id")
    private String lastMessageId;

    @TypeConverters(JSONConverter.MessageEntityConverter.class)
    @ColumnInfo(name = "last_message_data")
    private MessageEntity lastMessageData;

    @ColumnInfo(name = "last_message_timestamp")
    private long lastMessageTimestamp;

    @ColumnInfo(name = "no_of_unread_messages")
    private long noOfUnreadMessages;

    @ColumnInfo(name = "is_send_notifications")
    private boolean isSendNotifications;

    @ColumnInfo(name = "is_notifications_sound")
    private boolean isNotificationsSound;

    @ColumnInfo(name = "is_notifications_light")
    private boolean isNotificationsLight;

    @ColumnInfo(name = "is_notifications_popup")
    private boolean isNotificationsPopup;

    @ColumnInfo(name = "draft_creation_time")
    private long draftCreationTime;

    @ColumnInfo(name = "is_unread")
    private boolean isUnread;

    @ColumnInfo(name = "is_pn_required")
    private boolean isPNRequired;

    @ColumnInfo(name = "is_Data_Supported")
    private boolean isDataSupported;

    @ColumnInfo(name = "last_data_support_check")
    private long lastDataSyncTimestamp;

    @NonNull
    @ColumnInfo(name = "original_number")
    private String originalPhoneNumber;

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @ColumnInfo(name = "original_conversation_type")
    private ConversationType originalConversationType;

    public ConversationType getOriginalConversationType() {
        return originalConversationType;
    }

    public void setOriginalConversationType(ConversationType originalConversationType) {
        this.originalConversationType = originalConversationType;
    }

    @NonNull
    public String getOriginalPhoneNumber() {
        return originalPhoneNumber;
    }

    public void setOriginalPhoneNumber(@NonNull String originalPhoneNumber) {
        this.originalPhoneNumber = originalPhoneNumber;
    }

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @ColumnInfo(name = "last_conversation_type")
    private ConversationType lastConversationType;


    public boolean isPNRequired() {
        return isPNRequired;
    }

    public void setPNRequired(boolean PNRequired) {
        isPNRequired = PNRequired;
    }

    public boolean isUnread() {
        return isUnread;
    }

    public void setUnread(boolean unread) {
        isUnread = unread;
    }

    public long getDraftCreationTime() {
        return draftCreationTime;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public void setDraftCreationTime(long draftCreationTime) {
        this.draftCreationTime = draftCreationTime;
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    @NonNull
    public String getMask() {
        return mask;
    }

    public void setMask(@NonNull String mask) {
        this.mask = mask;
    }

    @NonNull
    public List<MessageChannel> getChannels() {
        return channels;
    }

    public void setChannels(@NonNull List<MessageChannel> channels) {
        this.channels = channels;
    }

    public GupshupPublicKey getIpPublicKey() {
        return ipPublicKey;
    }

    public void setIpPublicKey(GupshupPublicKey ipPublicKey) {
        this.ipPublicKey = ipPublicKey;
    }

    public long getIpLastSyncedTimestamp() {
        return ipLastSyncedTimestamp;
    }

    public void setIpLastSyncedTimestamp(long ipLastSyncedTimestamp) {
        this.ipLastSyncedTimestamp = ipLastSyncedTimestamp;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getPbLastSyncedTimestamp() {
        return pbLastSyncedTimestamp;
    }

    public void setPbLastSyncedTimestamp(long pbLastSyncedTimestamp) {
        this.pbLastSyncedTimestamp = pbLastSyncedTimestamp;
    }

    @NonNull
    public ConversationType getType() {
        return type;
    }

    public void setType(@NonNull ConversationType type) {
        this.type = type;
    }

    @NonNull
    public Author getTypeSetBy() {
        return typeSetBy;
    }

    public void setTypeSetBy(@NonNull Author typeSetBy) {
        this.typeSetBy = typeSetBy;
    }

    public ConversationSubType getSubType() {
        return subType;
    }

    public void setSubType(ConversationSubType subType) {
        this.subType = subType;
    }

    public String getDraftMessage() {
        return draftMessage;
    }

    public void setDraftMessage(String draftMessage) {
        this.draftMessage = draftMessage;
    }

    public String getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(String lastMessageId) {
        this.lastMessageId = lastMessageId;
    }

    public MessageEntity getLastMessageData() {
        return lastMessageData;
    }

    public void setLastMessageData(MessageEntity lastMessageData) {
        this.lastMessageData = lastMessageData;
    }

    public long getLastMessageTimestamp() {
        return lastMessageTimestamp;
    }

    public void setLastMessageTimestamp(long lastMessageTimestamp) {
        this.lastMessageTimestamp = lastMessageTimestamp;
    }

    public long getNoOfUnreadMessages() {
        return noOfUnreadMessages;
    }

    public void setNoOfUnreadMessages(long noOfUnreadMessages) {
        this.noOfUnreadMessages = noOfUnreadMessages;
    }

    public boolean isSendNotifications() {
        return isSendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications) {
        isSendNotifications = sendNotifications;
    }

    public boolean isNotificationsSound() {
        return isNotificationsSound;
    }


    public void setNotificationsSound(boolean notificationsSound) {
        isNotificationsSound = notificationsSound;
    }

    public boolean isNotificationsLight() {
        return isNotificationsLight;
    }

    public void setNotificationsLight(boolean notificationsLight) {
        isNotificationsLight = notificationsLight;
    }

    public boolean isNotificationsPopup() {
        return isNotificationsPopup;
    }

    public void setNotificationsPopup(boolean notificationsPopup) {
        isNotificationsPopup = notificationsPopup;
    }

    public boolean isDataSupported() {
        return isDataSupported;
    }

    public void setDataSupported(boolean dataSupported) {
        isDataSupported = dataSupported;
    }

    public long getLastDataSyncTimestamp() {
        return lastDataSyncTimestamp;
    }

    public void setLastDataSyncTimestamp(long lastDataSyncTimestamp) {
        this.lastDataSyncTimestamp = lastDataSyncTimestamp;
    }

    @NonNull
    public ConversationType getLastConversationType() {
        return lastConversationType;
    }

    public void setLastConversationType(@NonNull ConversationType lastConversationType) {
        this.lastConversationType = lastConversationType;
    }

    public ConversationType getConversationSubType() {
        return conversationSubType;
    }

    public void setConversationSubType(ConversationType conversationSubType) {
        this.conversationSubType = conversationSubType;
    }
    @Override
    public boolean equals(Object obj) {
        return obj == this || this.conversationId == ((ConversationEntity) obj).conversationId;
    }
}
