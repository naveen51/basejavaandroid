package io.gupshup.smsapp.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.List;

import io.gupshup.smsapp.database.converters.EnumConverter;
import io.gupshup.smsapp.database.entities.ConfigEntity;
import io.gupshup.smsapp.enums.ConfigKey;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
@Dao
public interface ConfigDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(ConfigEntity... configs);

    @Update
    int update(ConfigEntity... configs);

    @Delete
    int delete(ConfigEntity... configs);

    @Query("SELECT * FROM `config`")
    List<ConfigEntity> getAll();

    @TypeConverters(EnumConverter.ConfigKeyConverter.class)
    @Query("SELECT * FROM `config` WHERE `key` IN (:keys)")
    List<ConfigEntity> get(ConfigKey... keys);

    @TypeConverters(EnumConverter.ConfigKeyConverter.class)
    @Query("SELECT * FROM `config` WHERE `key` IN (:keys)")
    LiveData<List<ConfigEntity>> getWithLiveData(ConfigKey... keys);

}
