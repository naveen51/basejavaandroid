package io.gupshup.smsapp.database.datasource;

import android.arch.paging.ItemKeyedDataSource;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.database.providers.GlobalSearchEntityProvider;

public class GlobalSearchEntityDataSource extends ItemKeyedDataSource<Integer, GlobalSearchEntity> {

    private int pageSize;
    private GlobalSearchEntityProvider provider;

    public GlobalSearchEntityDataSource(int pageSize, GlobalSearchEntityProvider provider) {
        this.pageSize = pageSize;
        this.provider = provider;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<GlobalSearchEntity> callback) {
        List<GlobalSearchEntity> result = provider.getSearchEntities(0, pageSize);
        callback.onResult(result, 0, provider.getSize());
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<GlobalSearchEntity> callback) {
        List<GlobalSearchEntity> globalSearchEntities = provider.getSearchEntities(params.key + 1, params.requestedLoadSize);
        callback.onResult(globalSearchEntities);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<GlobalSearchEntity> callback) {
        List<GlobalSearchEntity> result;
        if (params.key > 0) {
            result = provider.getSearchEntities(params.key, params.requestedLoadSize);
        } else {
            result = new ArrayList<>();
        }
        callback.onResult(result);
    }

    @NonNull
    @Override
    public Integer getKey(@NonNull GlobalSearchEntity item) {
        return provider.getIndex(item);
    }
}
