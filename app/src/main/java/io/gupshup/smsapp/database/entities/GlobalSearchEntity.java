package io.gupshup.smsapp.database.entities;

import android.text.Spannable;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;

public class GlobalSearchEntity {
    private String mask;
    private String display;
    private List<Spannable> messages;
    private long timeStamp;
    private String lastMessageData;
    private String image;
    private ConversationType type;
    private ConversationSubType subType;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMatchedKeyword() {
        return matchedKeyword;
    }

    public void setMatchedKeyword(String matchedKeyword) {
        this.matchedKeyword = matchedKeyword;
    }

    private String matchedKeyword;

    private Spannable searchString;

    public GlobalSearchEntity() {
        messages = new ArrayList<>(3);
    }

    public Spannable getSearchString() {
        return searchString;
    }

    public void setSearchString(Spannable searchString) {
        this.searchString = searchString;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public List<Spannable> getMessages() {
        return messages;
    }


    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMask() {

        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public void addMessageEntity(Spannable searchedMessage) {
        messages.add(searchedMessage);
    }

    public String getLastMessageData() {
        return lastMessageData;
    }

    public void setLastMessageData(String lastMessageData) {
        this.lastMessageData = lastMessageData;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public ConversationSubType getSubType() {
        return subType;
    }

    public void setSubType(ConversationSubType subType) {
        this.subType = subType;
    }
}
