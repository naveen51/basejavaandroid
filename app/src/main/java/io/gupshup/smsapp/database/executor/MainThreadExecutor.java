package io.gupshup.smsapp.database.executor;

import android.os.Handler;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import io.gupshup.smsapp.app.MainApplication;

public class MainThreadExecutor implements Executor {
    private final Handler androidMainThreadHandler = new Handler(MainApplication.getContext().getMainLooper());
    @Override
    public void execute(@NonNull Runnable command) {
        androidMainThreadHandler.post(command);
    }
}
