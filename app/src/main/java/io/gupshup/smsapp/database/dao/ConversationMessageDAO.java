package io.gupshup.smsapp.database.dao;

import android.arch.persistence.room.Dao;

/*
 * @author  Bhargav Kolla
 * @since   Apr 15, 2018
 */
@Dao
public interface ConversationMessageDAO {

    /*
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query(
        "SELECT `c`.*, `m1`.* " +
            "FROM `conversations` AS `c` " +
            "INNER JOIN `messages` AS `m1` ON `c`.`mask` = `m1`.`mask` " +
            "INNER JOIN (" +
            "SELECT `m`.`mask`, MAX(`m`.`message_timestamp`) AS `max_message_timestamp` " +
            "FROM `messages` AS `m` " +
            "GROUP BY `m`.`mask`" +
            ") AS `m2` ON `m1`.`mask` = `m2`.`mask` AND `m1`.`message_timestamp` = `m2`.`max_message_timestamp` " +
            "INNER JOIN (" +
            "SELECT `m`.`mask`, COUNT(*) AS `no_of_unread_messages` " +
            "FROM `messages` AS `m` " +
            "WHERE `m`.`read_timestamp` < 0 " +
            "GROUP BY `m`.`mask`" +
            ") AS `m3` ON `m1`.`mask` = `m3`.`mask` " +
            "ORDER BY `m1`.`message_timestamp` DESC"
    )
    LiveData<List<ConversationMessage>> getRecent();
    */

}
