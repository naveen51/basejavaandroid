package io.gupshup.smsapp.database.entities;

import android.content.Context;
import android.support.annotation.NonNull;

import java.text.MessageFormat;

import io.gupshup.smsapp.exceptions.UnparsablePhoneNumberException;
import io.gupshup.smsapp.message.Utils;

public class PhoneNumberEntity implements Comparable<PhoneNumberEntity> {

    private final long[] phoneNumberInfo;

    private PhoneNumberEntity() {
        phoneNumberInfo = new long[0];
    }

    public PhoneNumberEntity(String phoneNumber) throws UnparsablePhoneNumberException {
        this.phoneNumberInfo = Utils.getPhoneNumberParts(phoneNumber);
        validateInfo(phoneNumber);
    }

    public PhoneNumberEntity(String phoneNumber, Context context) throws UnparsablePhoneNumberException {
        this.phoneNumberInfo = Utils.getPhoneNumberParts(phoneNumber, context);
        validateInfo(phoneNumber);
    }

    private void validateInfo(String phoneNumber) throws UnparsablePhoneNumberException {
        if (this.phoneNumberInfo == null) {
            throw new UnparsablePhoneNumberException(MessageFormat.format("Phone Number => {0}, cannot be parsed", phoneNumber));
        }
    }

    public long[] getPhoneNumberInfo() {
        return phoneNumberInfo;
    }

    public long getNationalNumber() {
        return phoneNumberInfo[1];
    }

    @Override
    public int compareTo(@NonNull PhoneNumberEntity o) {
        long myInternationalCode = phoneNumberInfo[0];
        long myInternationalNumber = phoneNumberInfo[1];

        long othersInternationalCode = o.getPhoneNumberInfo()[0];
        long othersInternationalNumber = o.getPhoneNumberInfo()[1];

        if (myInternationalCode > othersInternationalCode) {
            return -1;
        } else if (myInternationalCode < othersInternationalCode) {
            return 1;
        } else {
            if (myInternationalNumber > othersInternationalNumber) {
                return -1;
            } else if (myInternationalNumber < othersInternationalNumber) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
