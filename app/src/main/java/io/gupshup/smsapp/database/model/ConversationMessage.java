package io.gupshup.smsapp.database.model;

/*
 * @author  Bhargav Kolla
 * @since   Apr 15, 2018
 */
public class ConversationMessage {

    /*
    @ColumnInfo(name = "conversation_id")
    private long conversationId;

    @ColumnInfo(name = "mask")
    private String mask;

    @ColumnInfo(name = "display")
    private String display;

    @ColumnInfo(name = "image")
    private String image;

    @TypeConverters(EnumConverter.ConversationTypeConverter.class)
    @ColumnInfo(name = "type")
    private ConversationType type;

    @TypeConverters(EnumConverter.ConversationSubTypeConverter.class)
    @ColumnInfo(name = "sub_type")
    private ConversationSubType subType;

    @ColumnInfo(name = "no_of_unread_messages")
    private long noOfUnreadMessages;

    @TypeConverters(EnumConverter.MessageDirectionConverter.class)
    @ColumnInfo(name = "direction")
    private MessageDirection direction;

    @TypeConverters(GupshupMessageConverter.class)
    @ColumnInfo(name = "content")
    private GupshupMessage content;

    @TypeConverters(EnumConverter.MessageChannelConverter.class)
    @ColumnInfo(name = "channel")
    private MessageChannel channel;

    @ColumnInfo(name = "message_timestamp")
    private long messageTimestamp;

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public ConversationSubType getSubType() {
        return subType;
    }

    public void setSubType(ConversationSubType subType) {
        this.subType = subType;
    }

    public long getNoOfUnreadMessages() {
        return noOfUnreadMessages;
    }

    public void setNoOfUnreadMessages(long noOfUnreadMessages) {
        this.noOfUnreadMessages = noOfUnreadMessages;
    }

    public MessageDirection getDirection() {
        return direction;
    }

    public void setDirection(MessageDirection direction) {
        this.direction = direction;
    }

    public GupshupMessage getContent() {
        return content;
    }

    public void setContent(GupshupMessage content) {
        this.content = content;
    }

    public MessageChannel getChannel() {
        return channel;
    }

    public void setChannel(MessageChannel channel) {
        this.channel = channel;
    }

    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    public void setMessageTimestamp(long messageTimestamp) {
        this.messageTimestamp = messageTimestamp;
    }
    */

}
