package io.gupshup.smsapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Ram Prakash Bhat on 13/6/18.
 */

public class SimCardChangedReceiver extends BroadcastReceiver {

    private static final String ACTION_SIM_STATE_CHANGED = "android.intent.action.SIM_STATE_CHANGED";
    private static final String EXTRA_SIM_STATE = "ss";
    private static final String SIM_STATE_LOADED = "LOADED";
    private static final String SIM_STATE_READY = "READY";
    private static final String SIM_STATE_NOT_READY = "NOT_READY";
    private static final String SIM_STATE_ABSENT = "ABSENT";
    private static final String TAG = SimCardChangedReceiver.class.getSimpleName();
    @Inject
    public RxBus rxBus;

    public SimCardChangedReceiver() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        LogUtility.d(TAG, "SIM STATE CHANGE INETNT DATA " + AppUtils.intentToString(intent));
        String action = intent.getAction();
        //if (ACTION_SIM_STATE_CHANGED.equals(action)) {
        Bundle extras = intent.getExtras();
        if (extras == null) return;
        String simState = extras.getString("ss");
        int slotIdIndex = extras.getInt(AppConstants.BundleKey.SIM_SLOT_INDEX);
        LogUtility.d(""+ slotIdIndex,simState);
        if (AppConstants.SimChangeStatus.NOT_READY.equalsIgnoreCase(simState)
            || AppConstants.SimChangeStatus.ABSENT.equalsIgnoreCase(simState)) {
            PreferenceManager.getInstance(context).setImsiNumber(slotIdIndex, AppConstants.EMPTY_TEXT);
        }
        AppUtils.setSimStatusData(context, slotIdIndex, simState);
        if (AppConstants.SimChangeStatus.NOT_READY.equalsIgnoreCase(simState)
            || AppConstants.SimChangeStatus.ABSENT.equalsIgnoreCase(simState)
            || AppConstants.SimChangeStatus.LOADED.equalsIgnoreCase(simState)) {
            RxEvent stateChangeEvent = new RxEvent(AppConstants.RxEventName.REGISTER_DEREGISTER_EVENT_ON_SIM_STATE_CHANGE, null);
            rxBus.send(stateChangeEvent);
        }
           /* if (simState.equalsIgnoreCase(AppConstants.SimChangeStatus.READY)) {
                Pair<Boolean, Boolean> pair;
                int slotIdIndex = extras.getInt(AppConstants.BundleKey.SIM_SLOT_INDEX);
                pair = AppUtils.getIsSimCardStateChange(context, extras.getInt("slot"));
                final RegistrationInfo simOneItem = AppUtils.getSimOneData(context);
                final RegistrationInfo simTwoItem = AppUtils.getSimTwoData(context);
                RxEvent stateChangeEvent = new RxEvent(AppConstants.RxEventName.REGISTER_ON_SIM_STATE_CHANGE, null);
                if (simOneItem != null
                    && pair.first
                    && slotIdIndex == 0) {
                    simOneItem.setSimOneState(RegistrationSimOneState.SIM_ONE_STATE_CHANGED);
                    AppUtils.setSimRequestItemData(context, simOneItem);
                    rxBus.send(stateChangeEvent);
                }
                if (simTwoItem != null
                    && pair.second
                    && slotIdIndex == 1) {
                    simTwoItem.setSimTwoState(RegistrationSimTwoState.SIM_TWO_STATE_CHANGED);
                    AppUtils.setSimRequestItemData(context, simTwoItem);
                    rxBus.send(stateChangeEvent);
                }
            }*/

    }

    private List<SimInformation> getSimInfoList() {

        return AppUtils.getSimInfoList();
    }


}
