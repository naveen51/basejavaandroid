package io.gupshup.smsapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Ram Prakash Bhat on 5/4/18.
 */

public class MmsReceiver extends BroadcastReceiver {

    public MmsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving

        LogUtility.d("mms", "mms message received..");
        final byte[] data = intent.getByteArrayExtra("data");
        String dataString = new String(data);
        LogUtility.d("mms", "data is : "+dataString);
        // an Intent broadcast.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}