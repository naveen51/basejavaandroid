package io.gupshup.smsapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.service.NotificationActionService;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Ram Prakash Bhat on 27/4/18.
 */

public class NotificationActionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        int notificationId = intent.getIntExtra(AppConstants.BundleKey.NOTIFICATION_ID, 0);
        String mask = Utils.getMaskFromPhoneNumber(intent.getStringExtra(AppConstants.BundleKey.MOBILE_NUMBER));
        boolean markAllAsRead = intent.getBooleanExtra(AppConstants.BundleKey.MARK_ALL_AS_READ, false);
        String messageId = intent.getStringExtra(AppConstants.BundleKey.MESSAGE_ID);
        String contactName = intent.getStringExtra(AppConstants.BundleKey.CONTACT_NAME);
        String otpNumber = intent.getStringExtra(AppConstants.BundleKey.OTP_NUMBER);
        ConversationType conversationType = (ConversationType) intent.getSerializableExtra(AppConstants.BundleKey.CONVERSATION_TYPE);
        Intent service = new Intent(context, NotificationActionService.class);
        service.putExtra(AppConstants.BundleKey.INTENT_ACTION, action);
        service.putExtra(AppConstants.BundleKey.MOBILE_NUMBER, mask);
        service.putExtra(AppConstants.BundleKey.MARK_ALL_AS_READ, markAllAsRead);
        service.putExtra(AppConstants.BundleKey.MESSAGE_ID, messageId);
        service.putExtra(AppConstants.BundleKey.NOTIFICATION_ID, notificationId);
        service.putExtra(AppConstants.BundleKey.CONTACT_NAME, contactName);
        service.putExtra(AppConstants.BundleKey.OTP_NUMBER, otpNumber);
        service.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE,conversationType);
        context.startService(service);
    }


}
