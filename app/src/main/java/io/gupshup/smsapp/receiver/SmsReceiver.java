package io.gupshup.smsapp.receiver;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.OtpContent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.fcm.NotificationHelper;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.message.sms.SMSStatusReport;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.earlierSMS.FetchEarlierSMS;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.widget.FlashDialogActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.smsapp.utils.Triple;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

import static io.gupshup.smsapp.utils.AppConstants.IntentActions.SMS_DELIVER;
import static io.gupshup.smsapp.utils.AppConstants.IntentActions.SMS_SENT;

/**
 * Created by Ram Prakash Bhat on 5/4/18.
 */

public class SmsReceiver extends BroadcastReceiver {

    @Inject
    public RxBus rxBus;

    public SmsReceiver() {
    }

    public static String preSendMessage(String phoneNumber, String smsBody, String carier,
                                        int simSlot, String messageId, long draftMessageCreationTime) {
        return MessagingService.getInstance(SMSService.class).preSending(phoneNumber, null,
            new GupshupTextMessage(smsBody), carier, MessageStatus.SENDING, simSlot, messageId, draftMessageCreationTime);
    }

    public static String preSendCompose(boolean isFresh, String mask, String phone, String smsBody,
                                        String carier, int simSlot, String messageId, long draftMessageCreationTime) {
        if (isFresh) {
            return MessagingService.getInstance(SMSService.class).preSending(null, phone, new GupshupTextMessage(smsBody), carier, MessageStatus.SENDING, simSlot, messageId, draftMessageCreationTime);
        } else {
            return MessagingService.getInstance(SMSService.class).preSending(mask, null,
                new GupshupTextMessage(smsBody), carier, MessageStatus.SENDING, simSlot, messageId, draftMessageCreationTime);
        }
    }

    private void handleOtp(String message, Context context, int slot, String phoneNumber) {
        LogUtility.d("auto", "Handle otp for slot " + slot + " with message " + message);
        Pattern otpPattern = Pattern.compile(".*?(\\d{6}).*?(\\d+)",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher otpMatcher = otpPattern.matcher(message);
        String otp = "";
        String msgSequenceNo = "";
        if (otpMatcher.find()) {
            if (otpMatcher.groupCount() >= 2) {
                otp = otpMatcher.group(1);
                msgSequenceNo = otpMatcher.group(2);
            }
        }
        PreferenceManager manager = PreferenceManager.getInstance(context);

        String sequenceNumOne = manager.getSequenceNumber(String.valueOf(0));
        String sequenceNumTwo = manager.getSequenceNumber(String.valueOf(1));
        if (!TextUtils.isEmpty(msgSequenceNo) && msgSequenceNo.length() == 3) {
            msgSequenceNo = "0".concat(msgSequenceNo);
        }
        RegistrationInfo simOneRegData = AppUtils.getSimOneData(context);
        RegistrationInfo simTwoRegData = AppUtils.getSimTwoData(context);
        switch (slot) {
            case 0:
                if (simOneRegData != null && simOneRegData.simOneState != null) {
                    if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRegData.simOneState) {
                        RxEvent event = new RxEvent(AppConstants.RxEventName.ALREADY_REGISTERED, simOneRegData.phoneNumber);
                        rxBus.send(event);
                        return;
                    }
                }
                break;
            case 1:
                if (simTwoRegData != null && simTwoRegData.simTwoState != null) {
                    if (RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRegData.simTwoState) {
                        RxEvent event = new RxEvent(AppConstants.RxEventName.ALREADY_REGISTERED, simTwoRegData.phoneNumber);
                        rxBus.send(event);
                        return;
                    }
                }
                break;
        }


        if (sequenceNumOne.equalsIgnoreCase(msgSequenceNo)) {
            if (slot != 0) {
                manager.setSequenceNumberForActualSlot(String.valueOf(0), sequenceNumTwo);
                manager.setSequenceNumberForActualSlot(String.valueOf(slot), msgSequenceNo);
            }
        } else if (sequenceNumTwo.equalsIgnoreCase(msgSequenceNo)) {
            if (slot != 1) {
                manager.setSequenceNumberForActualSlot(String.valueOf(1), sequenceNumOne);
                manager.setSequenceNumberForActualSlot(String.valueOf(slot), msgSequenceNo);
            }
        }


        if (!TextUtils.isEmpty(msgSequenceNo)
            && (sequenceNumOne.equalsIgnoreCase(msgSequenceNo)
            || sequenceNumTwo.equalsIgnoreCase(msgSequenceNo))) {
            /*MessagingService.getInstance(SMSService.class).readConversation(
                Utils.getMaskFromPhoneNumber(phoneNumber), DateUtils.currentTimestamp());*/
            OtpContent content = new OtpContent();
            content.otp = otp;
            content.slot = slot;
            content.sequenceNumber = msgSequenceNo;
            PreferenceManager.getInstance(context.getApplicationContext()).setOtpContent(otp);
            PreferenceManager.getInstance(context.getApplicationContext()).setOtpSimSlot(String.valueOf(slot));
            PreferenceManager.getInstance(context.getApplicationContext()).setOtpSequence(String.valueOf(msgSequenceNo));
            LogUtility.d("auto", "sending rx for slot " + slot);
            RxEvent event = new RxEvent(AppConstants.RxEventName.OTP_MESSAGE, content);
            rxBus.send(event);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        MainApplication.getInstance().getAppComponent().inject(this);
        //if (AppUtils.isDefaultSmsPackage(context, context.getPackageName())) {
        try {
            if (intent.getAction() != null
                && intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    int sub = bundle.getInt("subscription", -1);

                    //Xiaomi Mi-4i(Android 5.0.2,API 21) fix
                    if (sub < 0) {
                        sub = bundle.getInt("subscription_id", -1);
                    }
                    int receivedSimSlot = 0;
                    if (bundle.containsKey("slot_id")) {
                        receivedSimSlot = bundle.getInt("slot_id", 0);
                    } else {
                        receivedSimSlot = bundle.getInt("slot", 0);
                    }


                    if (pdusObj == null) return;
                    StringBuilder messageBody = new StringBuilder();
                    String receivedOnCarrier = AppConstants.EMPTY_TEXT;
                    Pair<String, Integer> info = AppUtils.getChanelName(context, receivedSimSlot);
                    if (info != null) {
                        receivedOnCarrier = info.first;
                    }
                    SmsMessage currentMessage = null;
                    for (Object currentObj : pdusObj) {
                        currentMessage = SmsMessage.createFromPdu((byte[]) currentObj);
                        //To auto read OTP
                        //String sender = currentMessage.getDisplayOriginatingAddress();
                        // String messageBody = currentMessage.getMessageBodyWithoutSequenceNo();
                        messageBody.append(currentMessage.getMessageBody());
                    }
                    if (currentMessage == null) return;
                    //User can receive otp.
                    if (AppUtils.isDefaultSmsPackage(context, context.getPackageName())) {
                        if (currentMessage.getMessageClass().name().equalsIgnoreCase("CLASS_0")) {
                            //DialogUtils.showFlashDialog(context, messageBody.toString());
                            Intent intentFlash = new Intent(context, FlashDialogActivity.class);
                            intentFlash.putExtra(AppConstants.FLASH_MESSAGE_BODY, messageBody.toString());
                            context.startActivity(intentFlash);
                            return;
                        }

                        StringBuilder messageBodyWithoutSequenceNo = getMessageBodyWithoutSequenceNo(messageBody, currentMessage);
                        Uri smsUri = insertSms(currentMessage, messageBodyWithoutSequenceNo, context, Telephony.Sms.Inbox.CONTENT_URI);
                        String messageId = MessageUtils.extractMessageId(smsUri);

                        Triple<String, String, String> userContactInfo = MessageUtils.getContactUserDetails(MainApplication.getContext(),
                            currentMessage.getOriginatingAddress());
                        String contactName = "", profileImage = "", contactId = "";

                        /*if (userInfo != null) {
                            contactName = userInfo.first;
                            profileImage = userInfo.second;
                        }*/
                        //To check whether need to show notification or not (Mute/Un mute).
                        List<ConversationEntity> conversationEntities =
                            MessagingService.getInstance(SMSService.class).getConversationByMask(Utils.getMaskFromPhoneNumber(currentMessage.getOriginatingAddress()));
                        ConversationEntity conversationEntity = null;
                        if (conversationEntities.size() > 0) {
                            conversationEntity = conversationEntities.get(0);
                        }

                        if (conversationEntity == null) {
                            conversationEntity = Utils.getNewConversation(Utils.getMaskFromPhoneNumber(currentMessage.getOriginatingAddress()), currentMessage.getOriginatingAddress(), MessageChannel.SMS, currentMessage.getMessageBody());
                            long[] conversationIds = AppDatabase.getInstance().conversationDAO().insert(conversationEntity);
                            conversationEntity.setConversationId(conversationIds[0]);
                        }

                        if (conversationEntity != null) {
                            if (Utils.getConversationType(conversationEntity.getMask(),messageBody.toString()) == ConversationType.PERSONAL) {
                                if (userContactInfo != null) {
                                    conversationEntity.setDisplay(userContactInfo.getFirst());
                                    conversationEntity.setImage(userContactInfo.getSecond());
                                    conversationEntity.setContactId(userContactInfo.getThird());
                                    updateConversation(conversationEntity, context);
                                    contactName = userContactInfo.getFirst();
                                    profileImage = userContactInfo.getSecond();
                                    contactId = userContactInfo.getThird();
                                }
                            } else {
                                conversationEntity.setDisplay(conversationEntity.getDisplay().toUpperCase());
                            }
                        }
                        MessageEntity entity = MessagingService.getInstance(SMSService.class)
                            .postReceiving(null, currentMessage.getOriginatingAddress(),
                                messageId, new GupshupTextMessage(messageBodyWithoutSequenceNo.toString()),
                                null, currentMessage.getTimestampMillis(), receivedOnCarrier,
                                TextUtils.isEmpty(contactName) ? currentMessage.getOriginatingAddress() : contactName,
                                profileImage, receivedSimSlot, contactId);
                        Bundle extras = new Bundle();

                        NotificationHelper.createNotification(MainApplication.getContext(),
                            Utils.getMaskFromPhoneNumber(currentMessage.getOriginatingAddress()), messageBody.toString(),
                            extras, entity, contactName, profileImage, conversationEntity);
                    /*    if (conversationEntity != null
                            && conversationEntity.getType() != ConversationType.BLOCKED) {
                            if (PreferenceManager.getInstance(context).isNotificationEnabled()) {
                                if (MainApplication.getInstance() != null && !MainApplication.getInstance().isAppActive()) {
                                    NotificationHelper.createNotification(MainApplication.getContext(),
                                        Utils.getMaskFromPhoneNumber(currentMessage.getOriginatingAddress()), messageBody.toString(),
                                        extras, entity, contactName, profileImage, conversationEntity);
                                }
                            }
                        }*/

                    } else {
                        sendNotificationForNonDefaultApp(context, currentMessage.getOriginatingAddress(), messageBody.toString(), bundle);
                        FetchEarlierSMS fetchSms = new FetchEarlierSMS(context);
                        fetchSms.importEarlierSMSForInitialLoad(300);
                    }

                    handleOtp(messageBody.toString(), context, receivedSimSlot, currentMessage.getOriginatingAddress());
                    // }
                }
            } else if (intent.getAction() != null
                && intent.getAction().equals(SMS_DELIVER)) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        if (PreferenceManager.getInstance(context).isRegistrationDone()) {
                            Toast.makeText(context, context.getString(R.string.delivered_success), Toast.LENGTH_SHORT).show();
                        }
                        long serviceCenterTimeStamp = -1, dischargeTime = -1;
                        try {
                            if (intent.getExtras() != null) {
                                SMSStatusReport smsStatusReport = new SMSStatusReport((byte[]) intent.getExtras().get("pdu"));
                                serviceCenterTimeStamp = smsStatusReport.getServiceCenterTimeStamp();
                                dischargeTime = smsStatusReport.getDischargeTime();
                            }
                        } catch (Throwable t) {
                            AppUtils.logToFile(MainApplication.getContext(), "Exception while SMS_DELIVER .." + t.getMessage());
                            t.printStackTrace();
                        }
                        if (intent.getExtras() != null) {
                            if (AppUtils.isDefaultSmsPackage(context, context.getPackageName())) {
                                MessagingService.getInstance(SMSService.class)
                                    .postSending(intent.getStringExtra(AppConstants.BundleKey.MESSAGE_ID),
                                        serviceCenterTimeStamp, dischargeTime > 0 ? dischargeTime : DateUtils.currentTimestamp(), MessageStatus.DELIVERED);
                            }
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        //Toast.makeText(context, "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else if (intent.getAction() != null
                && intent.getAction().equals(SMS_SENT)) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        if (PreferenceManager.getInstance(context).isRegistrationDone()) {
                            //Toast.makeText(context, R.string.sent_success, Toast.LENGTH_SHORT).show();
                        }
                        if (intent.getExtras() != null) {
                            updateMessageStatus(context, intent, MessageStatus.SENT);
                        }
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        if (intent.getExtras() != null) {
                            updateMessageStatus(context, intent, MessageStatus.FAILED);
                        }
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        if (PreferenceManager.getInstance(context).isRegistrationDone()) {
                            Toast.makeText(context, context.getString(R.string.no_network_error), Toast.LENGTH_SHORT).show();
                        }
                        if (intent.getExtras() != null) {
                            updateMessageStatus(context, intent, MessageStatus.FAILED);
                        }
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        if (intent.getExtras() != null) {
                            updateMessageStatus(context, intent, MessageStatus.FAILED);
                        }
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        if (PreferenceManager.getInstance(context).isRegistrationDone()) {
                            Toast.makeText(context, context.getString(R.string.no_network_error), Toast.LENGTH_SHORT).show();
                        }
                        if (intent.getExtras() != null) {
                            updateMessageStatus(context, intent, MessageStatus.FAILED);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            LogUtility.d("auto", "exc in sms recvr " + e.getMessage());
        }
        // }
    }


    private void updateConversation(ConversationEntity conversationEntity, Context ctx) {
        //LogUtility.d("GDC", "Exec updateConversation before");

        AppDatabase.getInstance(ctx);
      AppDatabase.getInstance().conversationDAO().update(conversationEntity);

    }


    private void sendNotificationForNonDefaultApp(Context ctx, String fromNumber, String message, Bundle data) {

        String contactName = fromNumber;
        Triple<String, String, String> userContactInfo = MessageUtils.getContactUserDetails(MainApplication.getContext(),
            fromNumber);
        if (userContactInfo != null && userContactInfo.getFirst() != null) {
            contactName = userContactInfo.getFirst();
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx, NotificationChannel.DEFAULT_CHANNEL_ID)
            .setSmallIcon(R.drawable.icn_notification_status_bar)
            .setContentTitle(contactName)
            .setContentText(message)
            .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManager notificationManager =
            (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = ctx.getString(R.string.default_notification_channel_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            String channelId = ctx.getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            notificationManager.createNotificationChannel(channel);
        }


        Intent resultIntent = new Intent(ctx, MainActivity.class);
        //data.putString(AppConstants.BundleKey.MOBILE_NUMBER, fromNumber);
        if (data != null) {
            resultIntent.putExtras(data);
        }
        /*  set intent so it does not start a new activity*/
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = (int) DateUtils.currentTimestamp();
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, requestCode,
            resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        notificationManager.notify(0 /* ID of notification */, mBuilder.build());
    }

    private StringBuilder getMessageBodyWithoutSequenceNo(StringBuilder messageBody, SmsMessage currentMessage) {
        if ("MESSGS".equals(Utils.getMaskFromPhoneNumber(currentMessage.getOriginatingAddress()))) {
            return new StringBuilder(messageBody.toString().replaceAll("\nseq no \\d{4}", ""));
        } else {
            return new StringBuilder(messageBody);
        }
    }

    private Uri insertSms(SmsMessage currentMessage, StringBuilder smsMessage, Context context, Uri contentUri) {
        ContentValues smsValues = new ContentValues();
        smsValues.put(Telephony.Sms.ADDRESS, currentMessage.getOriginatingAddress());
        smsValues.put(Telephony.Sms.BODY, smsMessage.toString());
        smsValues.put(Telephony.Sms.DATE, System.currentTimeMillis());
        smsValues.put(Telephony.Sms.READ, 0);
        return context.getContentResolver().insert(contentUri, smsValues);
    }

    private void updateMessageStatus(Context context, Intent intent, MessageStatus status) {
        if (AppUtils.isDefaultSmsPackage(context, context.getPackageName())) {
            MessagingService.getInstance(SMSService.class)
                .postSending(intent.getStringExtra(AppConstants.BundleKey.MESSAGE_ID),
                    DateUtils.currentTimestamp(), -1, status);
        }
    }
}

