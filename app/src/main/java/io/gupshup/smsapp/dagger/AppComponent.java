package io.gupshup.smsapp.dagger;


import javax.inject.Singleton;

import dagger.Component;
import io.gupshup.smsapp.fcm.SMSFirebaseInstanceIdService;
import io.gupshup.smsapp.networking.NetworkModule;
import io.gupshup.smsapp.receiver.SimCardChangedReceiver;
import io.gupshup.smsapp.receiver.SmsReceiver;
import io.gupshup.smsapp.service.AppConfigurationAPI;
import io.gupshup.smsapp.service.DeregisterPhoneNumberApi;
import io.gupshup.smsapp.service.RegistrationService;
import io.gupshup.smsapp.ui.adapter.ReplyAdapter;
import io.gupshup.smsapp.ui.adapter.WelcomeScreenPagerAdapter;
import io.gupshup.smsapp.ui.archive.view.ArchivedMessageFragment;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.compose.view.ComposeSmsActivity;
import io.gupshup.smsapp.ui.compose.view.ConversationContactFragment;
import io.gupshup.smsapp.ui.compose.view.ConversationFragment;
import io.gupshup.smsapp.ui.compose.view.ShareContactActivity;
import io.gupshup.smsapp.ui.compose.viewmodel.ContactBubleViewModel;
import io.gupshup.smsapp.ui.compose.viewmodel.ContactViewModel;
import io.gupshup.smsapp.ui.compose.viewmodel.ConversationContactFragViewmodel;
import io.gupshup.smsapp.ui.compose.viewmodel.ConversationFragmentViewModel;
import io.gupshup.smsapp.ui.compose.viewmodel.ShareContactViewModel;
import io.gupshup.smsapp.ui.home.view.HomePagerFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.home.view.MessageListFragment;
import io.gupshup.smsapp.ui.home.view.SplashScreenFragment;
import io.gupshup.smsapp.ui.home.view.WelComeScreenFragment;
import io.gupshup.smsapp.ui.home.viewmodel.HomeFragmentViewModel;
import io.gupshup.smsapp.ui.home.viewmodel.MainActivityViewModel;
import io.gupshup.smsapp.ui.home.viewmodel.MessageViewModel;
import io.gupshup.smsapp.ui.registration.view.ReRegistrationFragment;import io.gupshup.smsapp.ui.registration.view.RegistrationSkipFragment;
import io.gupshup.smsapp.ui.registration.view.RegistrationVerificationErrorFragment;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationDialogViewModel;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationSkipFragmentViewModel;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationVerificationErrorFragViewModel;
import io.gupshup.smsapp.ui.reply.view.MessageReplyActivity;
import io.gupshup.smsapp.ui.reply.view.MessageReplyFragment;
import io.gupshup.smsapp.ui.reply.view.MessageReplyPagerFragment;
import io.gupshup.smsapp.ui.reply.viewmodel.ForwardMessageViewModel;
import io.gupshup.smsapp.ui.reply.viewmodel.ReplyMessageReciepentViewModel;
import io.gupshup.smsapp.ui.reply.viewmodel.ReplyMessageSenderViewModel;
import io.gupshup.smsapp.ui.search.view.MessageSearchResultFragment;
import io.gupshup.smsapp.ui.settings.view.SettingsFragment;
import io.gupshup.smsapp.ui.settings.viewmodel.AboutFragmentViewModel;
import io.gupshup.smsapp.ui.settings.viewmodel.SettingsFragViewModel;


@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(MainActivity mainActivity);

    void inject(MainActivityViewModel mainActivityViewModel);

    void inject(HomePagerFragment homePagerFragment);

    void inject(SplashScreenFragment splashScreenFragment);

    void inject(ComposeSmsActivity composeSmsActivity);

    void inject(ContactViewModel contactViewModel);

    void inject(ConversationFragment conversationFragment);

    void inject(ConversationContactFragViewmodel conversationContactFragViewmodel);

    void inject(ConversationContactFragment conversationContactFragment);

    void inject(MessageListFragment messageListFragment);

    void inject(SmsReceiver smsReceiver);

    void inject(ConversationFragmentViewModel conversationFragmentViewModel);

    void inject(SMSFirebaseInstanceIdService smsFirebaseInstanceIdService);

    void inject(io.gupshup.smsapp.message.services.MessagingService messagingService);


    void inject(ContactBubleViewModel contactBubleViewModel);

    void inject(MessageViewModel messageViewModel);

    void inject(ReplyMessageSenderViewModel replyMessageSenderViewModel);

    void inject(ReplyMessageReciepentViewModel replyMessageReciepentViewModel);

    void inject(MessageReplyActivity messageReplyActivity);

    void inject(ForwardMessageViewModel forwardMessageViewModel);

    void inject(MessageSearchResultFragment messageSearchResultFragment);

    void inject(ArchivedMessageFragment archivedMessageFragment);

    void inject(RegistrationDialogViewModel registrationDialogViewModel);


    void inject(RegistrationService registrationService);

    void inject(AppConfigurationAPI appConfigurationAPI);

    void inject(DeregisterPhoneNumberApi deregisterPhoneNumberApi);

    void inject(RegistrationSkipFragment registrationSkipFragment);

    void inject(RegistrationSkipFragmentViewModel registrationSkipFragmentViewModel);

    void inject(RegistrationVerificationErrorFragViewModel registrationVerificationErrorFragViewModel);

    void inject(RegistrationVerificationErrorFragment registrationVerificationErrorFragment);

    void inject(HomeFragmentViewModel homeFragmentViewModel);

    void inject(SettingsFragViewModel settingsFragViewModel);

    void inject(AboutFragmentViewModel aboutFragmentViewModel);

    void inject(WelComeScreenFragment welComeScreenFragment);

    void inject(WelcomeScreenPagerAdapter welcomeScreenPagerAdapter);

    void inject(MessageReplyPagerFragment messageReplyPagerFragment);

    void inject(MessageReplyFragment messageReplyPagerFragItem);

    void inject(SettingsFragment settingsFragment);

    void inject(ShareContactActivity sharecontactactivity);

    void inject(ShareContactViewModel shareContactViewModel);

    void inject(SimCardChangedReceiver simCardChangedReceiver);


    void inject(ReplyAdapter replyAdapter);

    void inject(ReRegistrationFragment reRegistrationFragment);
}
