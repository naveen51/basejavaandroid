package io.gupshup.smsapp.dagger;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.rxbus.RxBusImpl;
import io.gupshup.smsapp.ui.archive.view.ArchivedMessageFragment;
import io.gupshup.smsapp.ui.home.view.HomePagerFragment;
import io.gupshup.smsapp.ui.home.view.SplashScreenFragment;
import io.gupshup.smsapp.ui.search.view.MessageSearchResultFragment;


@Module
public class AppModule {
    private MainApplication mMainApplication;

    public AppModule(MainApplication mainApplication) {
        this.mMainApplication = mainApplication;
    }

    @Provides
    @Singleton
    public RxBus providesRxBus() {
        return new RxBusImpl();
    }

    @Provides
    @Singleton
    public RxBusHelper providesRxBusHelper() {
        return new RxBusHelper();
    }


    @Provides
    @Singleton
    public HomePagerFragment providesHomePagerFragment() {
        return new HomePagerFragment();
    }

    @Provides
    @Singleton
    public SplashScreenFragment providesSplashFragment() {
        return new SplashScreenFragment();
    }

    @Provides
    @Singleton
    public ArchivedMessageFragment providesArchivedMessageFragment() {
        return new ArchivedMessageFragment();
    }

    @Provides
    @Singleton
    public MessageSearchResultFragment provideMessageSearchResultFragment() {
        return new MessageSearchResultFragment();
    }
}