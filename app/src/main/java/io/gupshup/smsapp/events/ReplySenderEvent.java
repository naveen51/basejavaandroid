package io.gupshup.smsapp.events;

import io.gupshup.smsapp.database.entities.MessageEntity;

/**
 * Created by Naveen BM on 5/21/2018.
 */
public class ReplySenderEvent {
    private MessageEntity messageEntity;
   private  int position;

    public ReplySenderEvent(MessageEntity messageEntity, int position) {
        this.messageEntity = messageEntity;
        this.position = position;
    }

    public MessageEntity getMessageEntity() {
        return messageEntity;
    }

    public int getPosition() {
        return position;
    }
}
