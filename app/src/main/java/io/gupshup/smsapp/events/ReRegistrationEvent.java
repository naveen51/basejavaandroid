package io.gupshup.smsapp.events;

import io.gupshup.smsapp.data.model.RegistrationInfo;

/**
 * Created by Ram Prakash Bhat on 24/7/18.
 */
public class ReRegistrationEvent {

    public RegistrationInfo registrationInfo;
    public int simSlot;
}
