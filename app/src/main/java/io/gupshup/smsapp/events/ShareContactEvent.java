package io.gupshup.smsapp.events;

/**
 * Created by Naveen BM on 8/22/2018.
 */
public class ShareContactEvent {
    private String formatedShareContact;

    public ShareContactEvent(String formatedShareContact) {
        this.formatedShareContact = formatedShareContact;
    }

    public String getFormatedShareContact() {
        return formatedShareContact;
    }
}
