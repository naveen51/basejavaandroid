package io.gupshup.smsapp.events;

import io.gupshup.smsapp.data.model.RegistrationInfo;

/**
 * Created by Ram Prakash Bhat on 14/6/18.
 */

public class ConfigUpdateEvent {
    private RegistrationInfo simOneData;
    private RegistrationInfo simTwoData;

    public ConfigUpdateEvent(RegistrationInfo simOneData, RegistrationInfo simTwoData) {
        this.simOneData = simOneData;
        this.simTwoData = simTwoData;
    }

    public RegistrationInfo getSimOneData() {
        return simOneData;
    }

    public RegistrationInfo getSimTwoData() {
        return simTwoData;
    }
}
