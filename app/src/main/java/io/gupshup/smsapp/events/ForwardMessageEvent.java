package io.gupshup.smsapp.events;

import io.gupshup.smsapp.database.entities.MessageEntity;

/**
 * Created by Naveen BM on 5/24/2018.
 */
public class ForwardMessageEvent {
    private MessageEntity messageEntity;
    private String mask;

    public ForwardMessageEvent(MessageEntity messageEntity, String mask) {
        this.messageEntity = messageEntity;
        this.mask = mask;
    }

    public MessageEntity getMessageEntity() {
        return messageEntity;
    }

    public String getMask() {
        return mask;
    }
}
