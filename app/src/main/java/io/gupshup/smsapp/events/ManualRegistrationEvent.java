package io.gupshup.smsapp.events;

/**
 * Created by Naveen BM on 6/11/2018.
 */
public class ManualRegistrationEvent {
    private String mSimOneManualNumber;
    private String mSimTwoManualNumber;

    public ManualRegistrationEvent(String mSimOneManualNumber, String mSimTwoManualNumber) {
        this.mSimOneManualNumber = mSimOneManualNumber;
        this.mSimTwoManualNumber = mSimTwoManualNumber;
    }

    public String getmSimOneManualNumber() {
        return mSimOneManualNumber;
    }

    public String getmSimTwoManualNumber() {
        return mSimTwoManualNumber;
    }
}
