package io.gupshup.smsapp.events;

/**
 * Created by Naveen BM on 6/11/2018.
 */
public class RegistrationVerificationStatus {
    private boolean mVerificationStatus;
    private String mPhoneNumber;
    private int mSimSlot;

    public RegistrationVerificationStatus(boolean mVerificationStatus, String mPhonenUmber, int mSimSlot) {
        this.mVerificationStatus = mVerificationStatus;
        this.mPhoneNumber = mPhonenUmber;
        this.mSimSlot = mSimSlot;
    }

    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public int getmSimSlot() {
        return mSimSlot;
    }

    public boolean ismVerificationStatus() {
        return mVerificationStatus;
    }
}
