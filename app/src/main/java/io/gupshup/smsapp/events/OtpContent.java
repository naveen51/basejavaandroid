package io.gupshup.smsapp.events;

/**
 * Created by Naveen BM on 6/12/2018.
 */
public class OtpContent {
    public String otp;
    public int slot;
    public String sequenceNumber;
}
