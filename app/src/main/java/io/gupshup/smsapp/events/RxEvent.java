package io.gupshup.smsapp.events;

/**
 * Created by Ram Prakash Bhat on 10/4/18.
 */

public class RxEvent {

    public RxEvent(String eventName, Object eventData) {

        this.eventName = eventName;
        this.eventData = eventData;
    }

    private String eventName;

    private Object eventData;

    public String getEventName() {
        return eventName;
    }

    public Object getEventData() {
        return eventData;
    }
}
