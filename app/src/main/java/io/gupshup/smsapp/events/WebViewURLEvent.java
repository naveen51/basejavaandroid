package io.gupshup.smsapp.events;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class WebViewURLEvent {
    private String URL;
    private String mToolBarTitle;

    public String getmToolBarTitle() {
        return mToolBarTitle;
    }

    public WebViewURLEvent(String URL, String mToolBarTitle) {
        this.URL = URL;
        this.mToolBarTitle = mToolBarTitle;
    }


    public String getURL() {
        return URL;
    }
}
