package io.gupshup.smsapp.service;

import android.content.Context;
import android.provider.Settings;
import android.support.v4.util.Pair;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Locale;

import javax.inject.Inject;

import io.gupshup.crypto.app.Utils;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.StatusResponse;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.exceptions.AutoRegistrationFailedException;
import io.gupshup.smsapp.exceptions.GupshupAPICallException;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.networking.responses.AppConfigurationAPIResponse;
import io.gupshup.smsapp.networking.responses.AutoVerificationResponse;
import io.gupshup.smsapp.networking.responses.ContactPhoneResponse;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.NetworkUtil;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.reactivex.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationService {

    private static volatile RegistrationService registrationService = null;
    private static final String AUTO_REGISTER_KEYWORD = "gssa";
    private static final String TAG = RegistrationService.class.getName();
    private static String longCodeNumber1 = "9223971017";
    private static String longCodeNumber2 = "9212145567";
    private static String longCodeNumber3 = "";
    private static String longCodeNumber4 = "";
    private static boolean isGetContactPhoneNumberAPICalled = false;
    public static boolean isLongCode1CalledForSIM1 = false;
    public static boolean isLongCode2CalledForSIM1 = false;
    public static boolean isLongCode1CalledForSIM2 = false;
    public static boolean isLongCode2CalledForSIM2 = false;
    private static String autoRegMessage = "";
    private static Call<ContactPhoneResponse> callStaticSim1;
    private static Response<ContactPhoneResponse> staticFetchContactPhoneDetailsSim1;
    private static Call<ContactPhoneResponse> callStaticSim2;
    private static Response<ContactPhoneResponse> staticFetchContactPhoneDetailsSim2;
    @Inject
    public APIService apiService;

    private RegistrationService() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public static RegistrationService getInstance() {
        if (registrationService == null) {
            synchronized (RegistrationService.class) {
                if (registrationService == null) {
                    registrationService = new RegistrationService();
                }
            }
        }
        return registrationService;
    }

    public void autoRegisterSimNo1(final Context context, final String accountId, final String publicKey, final String apiKey, final int simSlot, final Callback<ContactPhoneResponse> callback) {
        if (!PreferenceManager.getInstance(context).isLongCodeFetchSuccessful()) {

            apiService.getContactPhoneNumberFromISOCountryCode(Locale.getDefault().getCountry()).enqueue(new Callback<ContactPhoneResponse>() {
                @Override
                public void onResponse(Call<ContactPhoneResponse> call, Response<ContactPhoneResponse> fetchContactPhoneDetails) {
                    int subscriptionId = 0;
                    PreferenceManager.getInstance(context).setLongCodeFetchStatus(true);
                    staticFetchContactPhoneDetailsSim1 = fetchContactPhoneDetails;
                    callStaticSim1 = call;
                    if (fetchContactPhoneDetails.isSuccessful()) {
                        ContactPhoneResponse contactPhoneResponse = fetchContactPhoneDetails.body();
                        String phoneNumbers = contactPhoneResponse.getContactPhone();
                        String[] parts = phoneNumbers.split(",");


                        if (parts.length == 1) {
                            longCodeNumber1 = parts[0];
                        } else if (parts.length == 2) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                        } else if (parts.length == 3) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                            longCodeNumber3 = parts[2];
                        } else if (parts.length == 4) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                            longCodeNumber3 = parts[2];
                            longCodeNumber4 = parts[3];
                        }
                        PreferenceManager.getInstance(context).setLongCodePrimary(longCodeNumber1);
                        PreferenceManager.getInstance(context).setLongCodeSecondary(longCodeNumber2);

                        //String mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(contactPhoneResponse.getContactPhone());
                        String mask1 = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber1);
                        isLongCode1CalledForSIM1 = true;
                        final Pair<String, Integer> simSubscriptionInfo = AppUtils.getSimSubscriptionInfo(context, simSlot);
                        if (simSubscriptionInfo != null) {
                            subscriptionId = simSubscriptionInfo.second;
                        } else {
                            callback.onFailure(call, new AutoRegistrationFailedException(AutoRegistrationFailedException.ExceptionCode.SIMSUB_NOT_FOUND));
                            return;
                        }

                        String autoRegistrationMessage = getAutoRegistrationJSONObject(context, accountId, publicKey, apiKey, simSlot, callback);
                        autoRegMessage = autoRegistrationMessage;
                        //Commentting Out Below Code for AMA-456
//                    Uri smsUri = MessageUtils.insertSms(mask, autoRegisrationMessage, context, Telephony.Sms.Sent.CONTENT_URI);
//                    String messageId = MessageUtils.extractMessageId(smsUri);
//                    SmsReceiver.preSendMessage(mask, autoRegisrationMessage, "", simSlot, messageId);
                        MessageUtils.sendSMS(context, mask1, autoRegistrationMessage, "", subscriptionId);
                        PreferenceManager.getInstance(context).setSmsSent(true);
                        callback.onResponse(call, fetchContactPhoneDetails);
                    } else {
                        callback.onFailure(call, new GupshupAPICallException("Unable to fetch Contact Phone Details"));
                    }
                }

                @Override
                public void onFailure(Call<ContactPhoneResponse> call, Throwable t) {
                    PreferenceManager.getInstance(context).setLongCodeFetchStatus(false);
                    callback.onFailure(call, t);
                }
            });
        } else {
            longCodeNumber1 = PreferenceManager.getInstance(context).getLongCodePrimary();
            longCodeNumber2 = PreferenceManager.getInstance(context).getLongCodeSecondary();
            //in case fetchPhoneNumber is already called and we have the long codes..
            if (longCodeNumber1.equals("") && longCodeNumber2.equals("") && longCodeNumber3.equals("") && longCodeNumber4.equals("")) {
                return;
            }

            int subscriptionId = 0;
            String autoRegisrationMessage = getAutoRegistrationJSONObject(context, accountId, publicKey, apiKey, simSlot, callback);
            autoRegMessage = autoRegisrationMessage;

            String mask = null;
            if (!isLongCode1CalledForSIM1) {
                mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber1);
                isLongCode1CalledForSIM1 = true;
            } else if (!isLongCode2CalledForSIM1) {
                if (longCodeNumber2.equals("")) {
                    isLongCode2CalledForSIM1 = true;
                    return;
                }
                RegistrationInfo simOneData = AppUtils.getSimOneData(context);
                if (simOneData != null) {
                    simOneData.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_IN_PROGRESS);
                    AppUtils.setSimRequestItemData(context, simOneData);
                }
                mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber2);
                isLongCode2CalledForSIM1 = true;
            } else {
                return;
            }

            final Pair<String, Integer> simSubscriptionInfo = AppUtils.getSimSubscriptionInfo(context, simSlot);
            if (simSubscriptionInfo != null) {
                subscriptionId = simSubscriptionInfo.second;
            } else {
                callback.onFailure(callStaticSim1, new AutoRegistrationFailedException(AutoRegistrationFailedException.ExceptionCode.SIMSUB_NOT_FOUND));
                return;
            }

            LogUtility.d("auto", "Sending message from SIM 1 in else part outside api...");
            MessageUtils.sendSMS(context, mask, autoRegisrationMessage, "", subscriptionId);
            PreferenceManager.getInstance(context).setSmsSent(true);
            callback.onResponse(callStaticSim1, staticFetchContactPhoneDetailsSim1);
        }
    }


    public void autoRegisterSimNo2(final Context context, final String accountId, final String publicKey, final String apiKey, final int simSlot, final Callback<ContactPhoneResponse> callback) {

        if (!PreferenceManager.getInstance(context).isLongCodeFetchSuccessful()) {
            apiService.getContactPhoneNumberFromISOCountryCode(Locale.getDefault().getCountry()).enqueue(new Callback<ContactPhoneResponse>() {
                @Override
                public void onResponse(Call<ContactPhoneResponse> call, Response<ContactPhoneResponse> fetchContactPhoneDetails) {
                    int subscriptionId = 0;
                    staticFetchContactPhoneDetailsSim2 = fetchContactPhoneDetails;
                    callStaticSim2 = call;
                    PreferenceManager.getInstance(context).setLongCodeFetchStatus(true);
                    if (fetchContactPhoneDetails.isSuccessful()) {
                        ContactPhoneResponse contactPhoneResponse = fetchContactPhoneDetails.body();
                        String phoneNumbers = contactPhoneResponse.getContactPhone();
                        String[] parts = phoneNumbers.split(",");
                        LogUtility.d("auto", "size of parts is " + parts.length);
                        if (parts.length == 1) {
                            longCodeNumber1 = parts[0];
                        } else if (parts.length == 2) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                        } else if (parts.length == 3) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                            longCodeNumber3 = parts[2];
                        } else if (parts.length == 4) {
                            longCodeNumber1 = parts[0];
                            longCodeNumber2 = parts[1];
                            longCodeNumber3 = parts[2];
                            longCodeNumber4 = parts[3];
                        }
                        //String mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(contactPhoneResponse.getContactPhone());
                        String mask1 = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber1);
                        isLongCode1CalledForSIM2 = true;
                        final Pair<String, Integer> simSubscriptionInfo = AppUtils.getSimSubscriptionInfo(context, simSlot);
                        if (simSubscriptionInfo != null) {
                            subscriptionId = simSubscriptionInfo.second;
                        } else {
                            callback.onFailure(call, new AutoRegistrationFailedException(AutoRegistrationFailedException.ExceptionCode.SIMSUB_NOT_FOUND));
                            return;
                        }

                        String autoRegisrationMessage = getAutoRegistrationJSONObject(context, accountId, publicKey, apiKey, simSlot, callback);
                        autoRegMessage = autoRegisrationMessage;
                        MessageUtils.sendSMS(context, mask1, autoRegisrationMessage, "", subscriptionId);
                        PreferenceManager.getInstance(context).setSmsSent(true);
                        callback.onResponse(call, fetchContactPhoneDetails);
                    } else {
                        callback.onFailure(call, new GupshupAPICallException("Unable to fetch Contact Phone Details"));
                        return;
                    }
                }

                @Override
                public void onFailure(Call<ContactPhoneResponse> call, Throwable t) {
                    PreferenceManager.getInstance(context).setLongCodeFetchStatus(true);
                    callback.onFailure(call, t);
                }
            });
        } else {
            //in case fetchPhoneNumber is already called and we have the long codes..
            longCodeNumber1 = PreferenceManager.getInstance(context).getLongCodePrimary();
            longCodeNumber2 = PreferenceManager.getInstance(context).getLongCodeSecondary();
            if (longCodeNumber1 == "" && longCodeNumber2 == "" && longCodeNumber3 == "" && longCodeNumber4 == "") {
                return;
            }
            int subscriptionId = 0;

            String autoRegisrationMessage = getAutoRegistrationJSONObject(context, accountId, publicKey, apiKey, simSlot, callback);
            autoRegMessage = autoRegisrationMessage;

            String mask = null;
            if (!isLongCode1CalledForSIM2) {
                mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber1);
                isLongCode1CalledForSIM2 = true;
            } else if (!isLongCode2CalledForSIM2) {
                if (longCodeNumber2 == "") {
                    isLongCode2CalledForSIM2 = true;
                    return;
                }
                RegistrationInfo simTwoData = AppUtils.getSimTwoData(context);
                simTwoData.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_IN_PROGRESS);
                AppUtils.setSimRequestItemData(context, simTwoData);
                mask = io.gupshup.smsapp.message.Utils.getMaskFromPhoneNumber(longCodeNumber2);
                isLongCode2CalledForSIM2 = true;
            } else {
                return;
            }

            final Pair<String, Integer> simSubscriptionInfo = AppUtils.getSimSubscriptionInfo(context, simSlot);
            if (simSubscriptionInfo != null) {
                subscriptionId = simSubscriptionInfo.second;
            } else {
                callback.onFailure(callStaticSim2, new AutoRegistrationFailedException(AutoRegistrationFailedException.ExceptionCode.SIMSUB_NOT_FOUND));
                return;
            }
            MessageUtils.sendSMS(context, mask, autoRegisrationMessage, "", subscriptionId);
            PreferenceManager.getInstance(context).setSmsSent(true);
            callback.onResponse(callStaticSim2, staticFetchContactPhoneDetailsSim2);
        }
    }

    @android.support.annotation.Nullable
    private String getAutoRegistrationJSONObject(Context context, String accountId, String publicKey, String apiKey, int simSlot, Callback<ContactPhoneResponse> callback) {
        JSONObject autoRegisterObject = new JSONObject();
        try {
            autoRegisterObject.put("accountId", accountId);
            autoRegisterObject.put("publicKey", publicKey);
            autoRegisterObject.put("apiKey", apiKey);
            String imeiNo = fetchIMEINumber(context, simSlot);
            if (imeiNo != null) {
                autoRegisterObject.put("iMEI", imeiNo);
            } else {
                autoRegisterObject.put("iMEI", "NA");
            }
            //autoRegisterObject.put("appId", Config.APPLICATION_ID);
            //LogUtility.d("auto", "the app id in registrat is : "+PreferenceManager.getInstance(context).getAppId());
            autoRegisterObject.put("appId", PreferenceManager.getInstance(context).getAppId());

        } catch (JSONException e) {
            callback.onFailure(callStaticSim2, new AutoRegistrationFailedException(AutoRegistrationFailedException.ExceptionCode.GENERIC_EXCEPTION));
            return null;
        }
        return MessageFormat.format("{0} {1}", AUTO_REGISTER_KEYWORD, Utils.base64Encode(autoRegisterObject.toString().getBytes()));
    }


    @Nullable
    public void getPhoneNumberVerificationStatus(String publicKey, final Callback<AutoVerificationResponse> callback) {
        try {
            apiService.getAutoverificationStatus(URLEncoder.encode(publicKey, "UTF-8")).enqueue(
                    new Callback<AutoVerificationResponse>() {
                        @Override
                        public void onResponse(Call<AutoVerificationResponse> call, Response<AutoVerificationResponse> autoVerificationStatus) {
                            if (autoVerificationStatus.isSuccessful()) {
                                callback.onResponse(call, autoVerificationStatus);
                            } else {
                                callback.onFailure(call, new GupshupAPICallException("API Call Failed"));
                            }
                        }

                        @Override
                        public void onFailure(Call<AutoVerificationResponse> call, Throwable t) {
                            callback.onFailure(call, t);
                        }
                    }
            );
        } catch (UnsupportedEncodingException e) {
            callback.onFailure(null, e);
        }
    }

    public void verifyOtpForManualRegistration(String countryCode, String phoneNumber, String otp, String apiKey, String sequenceNumber, final Callback<StatusResponse> callback) {

        Call<StatusResponse> call = apiService.verifyPhoneNumber(io.gupshup.smsapp.message.Utils.verifyCountryCode(countryCode),
                phoneNumber, otp, apiKey, sequenceNumber);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }


    public void sendFirebaseTokenToServer(Context ctx, String countryCode, String phoneNumber, String apiKey) {

        if (!NetworkUtil.isAvailable(ctx))
            return;
        PreferenceManager manager = PreferenceManager.getInstance(ctx);

        // if (manager.isRegistrationDone()) {
        Call<StatusResponse> call = apiService.addPNToken(io.gupshup.smsapp.message.Utils.verifyCountryCode(countryCode),
                phoneNumber, manager.getFCMToken(), apiKey);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                //LogUtility.d("FCM_SUCCESS", "response.body()");
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                //LogUtility.d("FCM_FAILURE", t.getMessage());

            }
        });
        //  }
        //TODO: call  API to register device token in gupshup Server
    }

    public void manualRegistration(final Context ctx, final String countryCode, final String mobileNum,
                                   final String fullNumberWithPlus,
                                   final String sequenceNum, final String ipPublicKey, final String apiKey,
                                   final int simSlot, final Callback<StatusResponse> callback) {

        String iMEINumber = "NA";
        if (simSlot == 0) {
            iMEINumber = AppUtils.getSimOneData(ctx).imeNumber;
        } else if (simSlot == 1) {
            iMEINumber = AppUtils.getSimTwoData(ctx).imeNumber;
        }

        if (iMEINumber == null) {
            iMEINumber = fetchIMEINumber(ctx, simSlot);
        }

        if (iMEINumber == null || iMEINumber == "") {
            iMEINumber = "NA";
        }
        final String finalImeiNumber = iMEINumber;

        if (PreferenceManager.getInstance(ctx).getAppId().equals("0")) {
            AppConfigurationAPI.getInstance().getAppId(PreferenceManager.getInstance(ctx).getAccountId(), AppUtils.getBuildName(), AppUtils.getBuildNumber(), AppUtils.getBoardName(), AppUtils.getManufacturerName(), new Callback<AppConfigurationAPIResponse>() {
                @Override
                public void onResponse(Call<AppConfigurationAPIResponse> call, Response<AppConfigurationAPIResponse> response) {

                    //LogUtility.d("auto", "onresponse of AppConfigurationAPI for manual....");

                    if (response.isSuccessful()) {
                        //LogUtility.d("auto", "response is successful...");
                        //LogUtility.d("auto", "the app id in manual reg is : "+response.body().getAppId());
                        PreferenceManager.getInstance(ctx).setAppId(response.body().getAppId());
                        //TODO: with getappID.....
                    }

                    Call<StatusResponse> callRegisterResponse = apiService.registerPhoneNumber(countryCode.startsWith("+") ? countryCode : String.format("+%s", countryCode),
                            mobileNum, Integer.parseInt(sequenceNum), ipPublicKey, apiKey, finalImeiNumber, PreferenceManager.getInstance(ctx).getAppId(), PreferenceManager.getInstance(ctx).getAccountId());
                    PreferenceManager.getInstance(ctx).setSequenceNumber(String.valueOf(simSlot), sequenceNum);
                    PreferenceManager.getInstance(ctx).setManualRegistrationTried(true);
                    callRegisterResponse.enqueue(new Callback<StatusResponse>() {
                        @Override
                        public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                            callback.onResponse(call, response);
                        }

                        @Override
                        public void onFailure(Call<StatusResponse> call, Throwable t) {
                            callback.onFailure(call, t);
                        }
                    });
                }

                @Override
                public void onFailure(Call<AppConfigurationAPIResponse> call, Throwable t) {
                    //LogUtility.d("auto", "OnFailure of getAppIDApi....");
                }
            });
        } else {
            Call<StatusResponse> call = apiService.registerPhoneNumber(countryCode.startsWith("+") ? countryCode : String.format("+%s", countryCode),
                    mobileNum, Integer.parseInt(sequenceNum), ipPublicKey, apiKey, iMEINumber, PreferenceManager.getInstance(ctx).getAppId(), PreferenceManager.getInstance(ctx).getAccountId());
            PreferenceManager.getInstance(ctx).setSequenceNumber(String.valueOf(simSlot), sequenceNum);
            PreferenceManager.getInstance(ctx).setManualRegistrationTried(true);
            call.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    callback.onResponse(call, response);
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    callback.onFailure(call, t);
                }
            });
        }


    }

    private String fetchIMEINumber(Context context, int simSlot) {
        String iMEINumber = "NA";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        if (telephonyManager != null) {
//            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    iMEINumber = telephonyManager.getDeviceId(simSlot);
//                }
//            }
//        }
        if (TextUtils.isEmpty(iMEINumber)) {
            iMEINumber = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return iMEINumber;
    }
}
