package io.gupshup.smsapp.service;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;

import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.ui.reply.view.QuickReplyActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.DateUtils;

import static io.gupshup.smsapp.utils.AppConstants.IntentActions.ADD_TO_CONTACT;
import static io.gupshup.smsapp.utils.AppConstants.IntentActions.MARK_AS_BLOCK;
import static io.gupshup.smsapp.utils.AppConstants.IntentActions.MARK_AS_READ;
import static io.gupshup.smsapp.utils.AppConstants.IntentActions.REPLY;

/**
 * Created by Ram Prakash Bhat on 14/6/18.
 */

public class NotificationActionService extends IntentService {

    public NotificationActionService() {
        super("NotificationActionService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        handleIncomingIntents(intent);
    }

    private void handleIncomingIntents(Intent intent) {

        Intent closeTrayIntent = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeTrayIntent);
        String action = intent.getStringExtra(AppConstants.BundleKey.INTENT_ACTION);
        int notificationId = intent.getIntExtra(AppConstants.BundleKey.NOTIFICATION_ID, 0);
        final String mask = intent.getStringExtra(AppConstants.BundleKey.MOBILE_NUMBER);
        final ConversationType conversationType = (ConversationType) intent.getSerializableExtra(AppConstants.BundleKey.CONVERSATION_TYPE);
        final boolean markAllAsRead = intent.getBooleanExtra(AppConstants.BundleKey.MARK_ALL_AS_READ, false);
        String messageId = intent.getStringExtra(AppConstants.BundleKey.MESSAGE_ID);
        String contactName = intent.getStringExtra(AppConstants.BundleKey.CONTACT_NAME);

        if (action == null) {
            cancel(this, /*notificationId*/0);
            return;
        }
        switch (action) {

            case MARK_AS_READ:
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (markAllAsRead) {   /*!TextUtils.isEmpty(mask)*/
                            ConversationDAO conversationDAO = AppDatabase.getInstance(NotificationActionService.this).conversationDAO();
                            List<ConversationEntity> conversationEntities = conversationDAO.getLastReceivedMessages();
                            for (ConversationEntity entity : conversationEntities) {
                                MessagingService.getInstance(SMSService.class).
                                    readConversation(entity.getMask(),
                                        DateUtils.currentTimestamp());
                            }
                        } else {
                            MessagingService.getInstance(SMSService.class).readConversation(mask,
                                DateUtils.currentTimestamp());
                        }
                    }
                });
                //}).run();
                cancel(this, /*notificationId*/0);
                break;
            case MARK_AS_BLOCK:
                ConversationEntity conversation;
                if (!TextUtils.isEmpty(mask)) {
                    List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
                    if (conversationEntities.size() > 0) {
                        conversation = conversationEntities.get(0);
                        conversation.setType(ConversationType.BLOCKED);
                        MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                    }
                }
                cancel(this, /*notificationId*/0);
                break;
            case ADD_TO_CONTACT:
                Utils.addNewContact(this, mask, null, false);
                cancel(this, /*notificationId*/0);
                break;
            case REPLY:
                Intent replyActivity = new Intent(this, QuickReplyActivity.class);//MainActivity
                replyActivity.putExtra(AppConstants.BundleKey.MOBILE_NUMBER, mask);
                replyActivity.putExtra(AppConstants.BundleKey.CONTACT_NAME, contactName);
                replyActivity.putExtra(AppConstants.BundleKey.LAST_MESSAGE_ID, messageId);
                replyActivity.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE, conversationType);
                replyActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(replyActivity);
                cancel(this, /*notificationId*/0);
                break;
            case AppConstants.IntentActions.COPY:
                String otpNumber = intent.getStringExtra(AppConstants.BundleKey.OTP_NUMBER);
                Utils.copyToClipBoard(this, otpNumber);
                cancel(this, /*notificationId*/0);
                break;
            case AppConstants.IntentActions.DELETE:
                ConversationEntity otpEntity;
                if (!TextUtils.isEmpty(mask)) {
                    List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
                    if (conversationEntities.size() > 0) {
                        otpEntity = conversationEntities.get(0);
                        MessagingService.getInstance(SMSService.class).deleteConversation(otpEntity);
                    }
                }
              /*  MessageDAO dao = AppDatabase.getInstance(this).messageDAO();
                List<MessageEntity> messageEntities = dao.get(messageId);
                MessageEntity[] message = new MessageEntity[messageEntities.size()];
                message = messageEntities.toArray(message);
                dao.delete(message);*/

                cancel(this, /*notificationId*/0);
                break;
        }

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);

    }

    public static void cancel(Context context, int id) {
        NotificationManager notificationManager = (NotificationManager)
            context.getSystemService(Activity.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }

}
