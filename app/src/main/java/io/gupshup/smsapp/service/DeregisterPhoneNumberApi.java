package io.gupshup.smsapp.service;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.networking.responses.DeregisterPhoneNumberAPIResponse;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeregisterPhoneNumberApi {

    @Inject
    protected APIService apiService;

    private DeregisterPhoneNumberApi() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    private static volatile DeregisterPhoneNumberApi deregisterPhoneNumberApi = null;

    public static DeregisterPhoneNumberApi getInstance() {
        if (deregisterPhoneNumberApi == null) {
            synchronized (RegistrationService.class) {
                if (deregisterPhoneNumberApi == null) {
                    deregisterPhoneNumberApi = new DeregisterPhoneNumberApi();
                }
            }
        }
        return deregisterPhoneNumberApi;
    }

    public void deregisterPhoneNumberApi(String countryCode, String phoneNumber,
                                         String apiKey, final int simSlot) {
        LogUtility.d("auto", "the key being used is " + apiKey + "  phone number :" + phoneNumber + "  country " + countryCode);
        Call<DeregisterPhoneNumberAPIResponse> deregisterPhoneNumberAPIResponseCall = apiService.deregister(countryCode, phoneNumber, apiKey);
        deregisterPhoneNumberAPIResponseCall.enqueue(new Callback<DeregisterPhoneNumberAPIResponse>() {
            @Override
            public void onResponse(Call<DeregisterPhoneNumberAPIResponse> call, Response<DeregisterPhoneNumberAPIResponse> response) {
                LogUtility.d("auto", "deregister done.." + response.message());
                if (response != null && response.isSuccessful()) {
                    if (response.body().getStatus().equals("success")) {
                        LogUtility.d("auto", "No. de-registered successfully..");

                        RegistrationInfo registrationInfo = new RegistrationInfo();
                        registrationInfo.simSlot = simSlot;
                        registrationInfo.sequenceNumber = PreferenceManager
                            .getInstance(MainApplication.getInstance())
                            .getSequenceNumberFromActualSlot(String.valueOf(simSlot));
                        AppUtils.setSimRequestItemData(MainApplication.getInstance(), registrationInfo);
                        AppUtils.setSimStatusData(MainApplication.getInstance(), simSlot,
                            AppConstants.SimChangeStatus.DE_REGISTRATION_DONE);

                    }
                }
            }

            @Override
            public void onFailure(Call<DeregisterPhoneNumberAPIResponse> call, Throwable t) {
                LogUtility.d("auto", "No. deregistration failure with exc " + t.getMessage());
            }
        });
    }
}
