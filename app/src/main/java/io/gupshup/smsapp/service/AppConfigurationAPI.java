package io.gupshup.smsapp.service;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.networking.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import io.gupshup.smsapp.networking.responses.AppConfigurationAPIResponse;
public class AppConfigurationAPI {

    @Inject
    protected APIService apiService;

    private AppConfigurationAPI() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    private static volatile AppConfigurationAPI appConfigurationAPI = null;
    public static AppConfigurationAPI getInstance() {
        if (appConfigurationAPI == null) {
            synchronized (RegistrationService.class) {
                if (appConfigurationAPI == null) {
                    appConfigurationAPI = new AppConfigurationAPI();
                }
            }
        }
        return appConfigurationAPI;
    }


    public void getAppId(String accountId, final String deviceName, final String deviceNumber, final String tag1, final String tag2, final Callback<AppConfigurationAPIResponse> callback) {
        //LogUtility.d("auto", "Calling getAppiD.....");
        Call<AppConfigurationAPIResponse> appConfigurationAPIResponse = getAppIdApi(accountId,deviceName, deviceNumber, tag1, tag2);
        appConfigurationAPIResponse.enqueue(new Callback<AppConfigurationAPIResponse>() {
            @Override
            public void onResponse(Call<AppConfigurationAPIResponse> call, Response<AppConfigurationAPIResponse> response) {
                if (response.isSuccessful()) {
                    //LogUtility.d("auto","getAppId api successful...");
                    }
                    callback.onResponse(call, response);
                }

            @Override
            public void onFailure(Call<AppConfigurationAPIResponse> call, Throwable t) {

                //LogUtility.d("auto","getAppId api failed...");
                callback.onFailure(call, t);
            }
        });
    }

    private Call<AppConfigurationAPIResponse> getAppIdApi(String accountId, final String deviceName, final String deviceNumber, final String tag1, final String tag2)
    {
        Call<AppConfigurationAPIResponse> response =  apiService.getAppIdApi(accountId, deviceName, deviceNumber, tag1, tag2);
        return response;
    }
}
