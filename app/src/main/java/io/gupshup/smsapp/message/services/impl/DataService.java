package io.gupshup.smsapp.message.services.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.Nullable;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import io.gupshup.crypto.app.GupshupMessageCrypter;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.ConfigEntity;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.database.entities.MessageIdMap;
import io.gupshup.smsapp.enums.ConfigKey;
import io.gupshup.smsapp.enums.GupshupMessageStatus;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.fcm.NotificationHelper;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.networking.responses.MessageResponse;
import io.gupshup.smsapp.networking.responses.PhoneNumberDetailsResponse;
import io.gupshup.smsapp.networking.responses.SendMessageResponse;
import io.gupshup.smsapp.networking.responses.SendMessageStatusResponse;
import io.gupshup.smsapp.ui.reply.view.MessageReplyActivity;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.Triple;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.gupshup.smsapp.app.MainApplication.getContext;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public final class DataService extends MessagingService {

    private final Map<ConfigKey, String> configEntitiesMap;
    private final String mPhoneNumberInfoSimOne[] = new String[2];
    private final String mPhoneNumberInfoSimTwo[] = new String[2];
    private LiveData<List<ConfigEntity>> withLiveData;

    MessageResponse dsMessageResponse = null;

    {
        configEntitiesMap = new HashMap<>();

        ConfigKey[] configKeys =
            new ConfigKey[]{
                ConfigKey.PHONE_NUMBER_SIM_1,
                ConfigKey.PHONE_NUMBER_SIM_2,
                ConfigKey.API_KEY_1,
                ConfigKey.API_KEY_2,
                ConfigKey.IP_PRIVATE_KEY_1,
                ConfigKey.IP_PRIVATE_KEY_2
            };

        Observer<List<ConfigEntity>> configEntitiesObserver =
            new Observer<List<ConfigEntity>>() {
                @Override
                public void onChanged(@Nullable List<ConfigEntity> configEntities) {
                    if (configEntities == null || configEntities.size() == 0) {
                        configEntitiesMap.clear();
                        mPhoneNumberInfoSimOne[0] = mPhoneNumberInfoSimOne[1] = null;
                        mPhoneNumberInfoSimTwo[0] = mPhoneNumberInfoSimTwo[1] = null;
                        return;
                    }

                    for (ConfigEntity configEntity : configEntities) {
                        if (configEntity != null) {
                            configEntitiesMap.put(configEntity.getKey(), configEntity.getValue());
                        }
                    }

                    String[] tempPhoneNumberInfo;
                    if (configEntitiesMap.containsKey(ConfigKey.PHONE_NUMBER_SIM_1)) {
                        tempPhoneNumberInfo = Utils.getPhoneNumberInfo(configEntitiesMap.get(ConfigKey.PHONE_NUMBER_SIM_1));
                        if (tempPhoneNumberInfo == null)
                            return;
                        mPhoneNumberInfoSimOne[0] = tempPhoneNumberInfo[0];
                        mPhoneNumberInfoSimOne[1] = tempPhoneNumberInfo[1];
                    }

                    if (configEntitiesMap.containsKey(ConfigKey.PHONE_NUMBER_SIM_2)) {
                        tempPhoneNumberInfo = Utils.getPhoneNumberInfo(configEntitiesMap.get(ConfigKey.PHONE_NUMBER_SIM_2));
                        if (tempPhoneNumberInfo == null)
                            return;
                        mPhoneNumberInfoSimTwo[0] = tempPhoneNumberInfo[0];
                        mPhoneNumberInfoSimTwo[1] = tempPhoneNumberInfo[1];
                    }
                }
            };

        configEntitiesObserver.onChanged(configDAO.get(configKeys));
        withLiveData = configDAO.getWithLiveData(configKeys);
        withLiveData.observeForever(configEntitiesObserver);
    }

    public DataService() {
        super(MessageChannel.DATA);
    }

    @Override
    public final long sending(final String systemMessageId, final String gupshupMessageId, final int simSlot, final String mask, final String phoneNumber) throws Exception {
        MessageEntity messageEntity;
        ConversationEntity conversationEntity;

        List<MessageEntity> messageEntities = messageDAO.get(systemMessageId);
        if (messageEntities.size() > 0)
            messageEntity = messageEntities.get(0);
        else
            throw new IllegalArgumentException("No message exists with the message_id" + '[' + systemMessageId + ']');

        List<ConversationEntity> conversationEntities = conversationDAO.get(messageEntity.getMask());
        if (conversationEntities.size() > 0)
            conversationEntity = conversationEntities.get(0);
        else
            throw new SQLiteDatabaseCorruptException("No conversation exists with the mask" + '[' + messageEntity.getMask() + ']');

        GupshupPublicKey ipPublicKey = conversationEntity.getIpPublicKey();
        if (ipPublicKey == null || ipPublicKey.toString() == null || ipPublicKey.toString().isEmpty()) {
            Response<PhoneNumberDetailsResponse> phoneNumberDetailsResponseResponse = Executors.newSingleThreadExecutor().submit(new Callable<Response<PhoneNumberDetailsResponse>>() {
                @Override
                public Response<PhoneNumberDetailsResponse> call() throws Exception {
                    return getPhoneNumberDetailsResponseCall(simSlot, mask == null ? phoneNumber : mask).execute();
                }
            }).get();
            conversationEntity.setIpPublicKey(new GupshupPublicKey(phoneNumberDetailsResponseResponse.body().getPublicKey()));
            conversationDAO.update(conversationEntity);
        }

        final MessageEntity _messageEntity = messageEntity;
        final GupshupPublicKey _ipPublicKey = ipPublicKey;

        Response<SendMessageResponse> sendMessageResponseResponse =
            Executors.newSingleThreadExecutor()
                .submit(
                    new Callable<Response<SendMessageResponse>>() {
                        @Override
                        public Response<SendMessageResponse> call() throws Exception {
                            String[] phoneDetails = getDetailsFromSimSlot(simSlot);
                            String countryCode = phoneDetails[0];
                            String nationalNumber = phoneDetails[1];
                            String apiKey = phoneDetails[2];
                            String privateKey = phoneDetails[3];
                            JSONObject messageJSON = new JSONObject();
                            messageJSON.put("id", gupshupMessageId);
                            messageJSON.put("msg", GupshupMessageCrypter.getInstance().encrypt(
                                _messageEntity.getContent(),
                                new GupshupPrivateKey(privateKey),
                                _ipPublicKey
                            ));

                            return
                                apiService
                                    .sendMessage(
                                        countryCode,
                                        nationalNumber,
                                        _messageEntity.getMask(),
                                        messageJSON.toString(),
                                        apiKey
                                    )
                                    .execute();
                        }
                    }
                )
                .get();

        SendMessageResponse sendMessageResponse = sendMessageResponseResponse.body();
        return sendMessageResponse.getMessageTimestamp();
    }

    @Override
    public void sendMessage(
        String systemMessageId, String mask,
        String phoneNumber,
        GupshupMessage message,
        int simSlot
    ) throws Exception {
        String gupshupMessageId = Utils.generateMessageId();
        long messageTimestamp = sending(systemMessageId, gupshupMessageId, simSlot, mask, phoneNumber);
        postSending(systemMessageId, messageTimestamp, -1, MessageStatus.SENT);
        messageMapDao.insert(new MessageIdMap(systemMessageId, gupshupMessageId));
    }

    @Override
    public final void receiveMessages(int simSlot) throws Exception {
        Response<List<MessageResponse>> messageResponsesResponse =
            (Response<List<MessageResponse>>) receiving(simSlot);

        List<MessageResponse> messageResponses = messageResponsesResponse.body();
        for (MessageResponse messageResponse : messageResponses) {
            String fromPhoneNumber = messageResponse.getFromPhoneNumber();
            try {
                String messageType = messageResponse.getMessageType();
                if (GupshupMessageStatus.MSG.getStatus().equals(messageType)) {
                    GupshupMessage gupshupMessage = null;
                    String privateKey;
                    if (simSlot == 0) {
                        privateKey = configEntitiesMap.get(ConfigKey.IP_PRIVATE_KEY_1);
                    } else {
                        privateKey = configEntitiesMap.get(ConfigKey.IP_PRIVATE_KEY_2);
                    }
                    gupshupMessage =
                        GupshupMessageCrypter.getInstance().decrypt(
                            messageResponse.getMessageContent(),
                            new GupshupPrivateKey(privateKey)
                        );
                    Triple<String, String, String> contactUserDetail = MessageUtils.getContactUserDetails(getContext(), fromPhoneNumber);
                    String contactName = "", profileImage = "", contactId = "";
                    if (contactUserDetail != null) {
                        contactName = contactUserDetail.getFirst();
                        profileImage = contactUserDetail.getSecond();
                        contactId = contactUserDetail.getThird();
                    }
                    //LogUtility.d("auto", "Calling post Receiving...from ds");
                    dsMessageResponse = messageResponse;
                    MessageEntity messageEntity = postReceiving(
                        null,
                        fromPhoneNumber,
                        messageResponse.getMessageId(),
                        gupshupMessage,
                        null,
                        messageResponse.getTimestamp(), "",
                        contactName, profileImage, simSlot, contactId);
                    if (messageEntity != null) {
                        createNotificationForDataMessage(messageEntity);
                    }

                } else if (GupshupMessageStatus.DLVR.getStatus().equals(messageType)) {
                    updateAuxiliaryStatus(simSlot, messageResponse, fromPhoneNumber, GupshupMessageStatus.DACK, MessageStatus.DELIVERED);
                } else if (GupshupMessageStatus.READ.getStatus().equals(messageType)) {
                    updateAuxiliaryStatus(simSlot, messageResponse, fromPhoneNumber, GupshupMessageStatus.RACK, MessageStatus.READ);
                }
            } catch (Exception e) {
                e.printStackTrace();
                sendMessageStatus(fromPhoneNumber, messageResponse.getMessageId(), simSlot, GupshupMessageStatus.FAIL);
                continue;
            }
        }
    }

    /**To show notification for data message once it got received.
     * @param messageEntity
     */
    private void createNotificationForDataMessage(MessageEntity messageEntity) {

        List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class)
            .getConversationByMask(messageEntity.getMask());
        ConversationEntity conversationEntity = null;
        if (conversationEntities.size() > 0) {
            conversationEntity = conversationEntities.get(0);
        }

        GupshupTextMessage message = (GupshupTextMessage) messageEntity.getContent();
        //To avoid crash if its a first data message, we are not saving this to DB its just temp. local holder.
        if (conversationEntity == null) {
            conversationEntity = Utils.getNewConversation(messageEntity.getMask(),
                messageEntity.getMask(), MessageChannel.DATA, message.getText());
        }
        Bundle extras = new Bundle();
        NotificationHelper.createNotification(MainApplication.getContext(),
            messageEntity.getMask(), message.getText(), extras, messageEntity,
            conversationEntity.getDisplay(), conversationEntity.getImage(),
            conversationEntity);
    }

    private void updateAuxiliaryStatus(int simSlot, MessageResponse messageResponse, String fromPhoneNumber, GupshupMessageStatus gupshupMessageStatus, MessageStatus delivered) throws InterruptedException, java.util.concurrent.ExecutionException, java.io.IOException {
        List<String> systemMessageIds = messageMapDao.fetchSystemMessageIdsFromGupshupMessageIds(new String[]{messageResponse.getMessageId()});
        if (systemMessageIds.size() > 0) {
            String systemMessageId = systemMessageIds.get(0);
            sendMessageStatus(fromPhoneNumber, messageResponse.getMessageId(), simSlot, gupshupMessageStatus);
            MessageEntity messageEntity = messageDAO.get(systemMessageId).get(0);
            messageEntity.setMessageStatus(delivered);
            messageDAO.update(messageEntity);
        }
    }

    @Override
    public final Object receiving(final int receivedSimSlot) throws Exception {
        return
            Executors.newSingleThreadExecutor()
                .submit(
                    new Callable<Response<List<MessageResponse>>>() {
                        @Override
                        public Response<List<MessageResponse>> call() throws Exception {
                            String[] phoneDetails = getDetailsFromSimSlot(receivedSimSlot);
                            return
                                apiService
                                    .fetchMessages(
                                        phoneDetails[0],
                                        phoneDetails[1],
                                        phoneDetails[2]
                                    )
                                    .execute();
                        }
                    }
                )
                .get();
    }

    @Override
    public final MessageEntity postReceiving(
        String mask,
        final String phoneNumber,
        final String messageId,
        GupshupMessage message,
        String destination,
        long messageTimestamp,
        String carierName,
        String contactName, String profileImage, final int receivedSimSlot, String contactId) throws Exception {

        String systemMessageId = "";
        try {
            //LogUtility.d("auto", "data service post recv called...");
            boolean isMessageAlreadyPresent = checkMessageExists(messageId);
            if (isMessageAlreadyPresent) {
                //LogUtility.d("auto", "Message is already present...with id :"+messageId);
                SendMessageStatusResponse sendMessageStatusResponse = sendMessageStatus(phoneNumber, messageId, receivedSimSlot, GupshupMessageStatus.DLVR);
                updateDeliveryTimestampForPreviouslyReceivedMessage(sendMessageStatusResponse, messageId);
                dsMessageResponse = null;
                //LogUtility.d("auto", "returning as null as message is already present...");
                return null;
            }
            if (AppUtils.isDefaultSmsApp(getContext())) {
                systemMessageId = saveSystemSMS(phoneNumber, (GupshupTextMessage) message, MessageDirection.INCOMING);
            } else {
                systemMessageId = messageId;
            }
            //LogUtility.d("auto", "saved with system message id :"+systemMessageId);
            messageMapDao.insert(new MessageIdMap(systemMessageId, messageId));
        } catch (Exception e) {
            //LogUtility.d("auto", "exc in message map insert with exc : "+e.getMessage());
            dsMessageResponse = null;
            MessageReplyActivity.deleteSMSFromDefaultDB(getContext(), Telephony.Sms.CONTENT_URI, systemMessageId);
        }
        final MessageEntity messageEntity =
            super.postReceiving(mask, phoneNumber, systemMessageId, message, destination, messageTimestamp, "", contactName,
                profileImage, receivedSimSlot, contactId);
        if (messageEntity == null) {
            return null;
        }
        SendMessageStatusResponse sendMessageStatusResponse = sendMessageStatus(phoneNumber, messageId, receivedSimSlot, GupshupMessageStatus.DLVR);
        messageEntity.setDeliveredTimestamp(sendMessageStatusResponse.getDeliveredTimestamp());
        messageDAO.update(messageEntity);
        return messageEntity;
    }

    private void updateDeliveryTimestampForPreviouslyReceivedMessage(SendMessageStatusResponse sendStatusResp, String messageId) {
        //List<String> sysIdList = messageMapDao.fetchSystemMessageIdsFromGupshupMessageIds(new String[]{messageId});
        List<MessageEntity> messageEntityList = getMessageEntity(messageId);
        if (messageEntityList != null && messageEntityList.size() != 0) {
            MessageEntity messageEntity = messageEntityList.get(0);
            messageEntity.setDeliveredTimestamp(sendStatusResp.getDeliveredTimestamp());
            messageDAO.update(messageEntity);
        }
        //LogUtility.d("auto", "Updating message with id "+messageId+"  with delivery timestamp "+sendStatusResp.getDeliveredTimestamp());
    }

    private boolean checkMessageExists(String gupId) {
        List<String> sysIdList = messageMapDao.fetchSystemMessageIdsFromGupshupMessageIds(new String[]{gupId});
        if (sysIdList == null || sysIdList.size() == 0) {
            //LogUtility.d("auto", "The mesg is not present in gupshup map ...");
            return false;
        }
        //LogUtility.d("auto", "The message is present in db..");
        return true;
    }

    private SendMessageStatusResponse sendMessageStatus(final String phoneNumber, final String messageId, final int receivedSimSlot, final GupshupMessageStatus gupshupMessageStatus) throws InterruptedException, java.util.concurrent.ExecutionException, java.io.IOException {
        Response<SendMessageStatusResponse> sendMessageStatusResponseResponse =
            Executors.newSingleThreadExecutor()
                .submit(
                    new Callable<Response<SendMessageStatusResponse>>() {
                        @Override
                        public Response<SendMessageStatusResponse> call() throws Exception {
                            String[] phoneDetails = getDetailsFromSimSlot(receivedSimSlot);
                            return
                                apiService
                                    .sendMessageStatus(
                                        phoneDetails[0],
                                        phoneDetails[1],
                                        messageId,
                                        phoneNumber,
                                        gupshupMessageStatus.getStatus(),
                                        0,
                                        phoneDetails[2]
                                    )
                                    .execute();
                        }
                    }
                )
                .get();

        return sendMessageStatusResponseResponse.body();
    }

    @Override
    public final void acknowledgeReads() throws Exception {
        List<MessageEntity> messageEntities = messageDAO.getIncomingUnacknowledgedReads();
        if (messageEntities.size() == 0)
            return;
        for (final MessageEntity messageEntity : messageEntities) {
            final int simSlot = messageEntity.getIncomingSIMID() - 1;
            final List<String> gupshupMessageIds = messageMapDao.fetchGupshupMessageIdsFromSystemMessageIds(new String[]{messageEntity.getMessageId()});
            if (gupshupMessageIds.size() == 1) {
                Response<SendMessageStatusResponse> sendMessageStatusResponseResponse =
                    Executors.newSingleThreadExecutor()
                        .submit(
                            new Callable<Response<SendMessageStatusResponse>>() {
                                @Override
                                public Response<SendMessageStatusResponse> call() throws Exception {
                                    String[] phoneDetails = getDetailsFromSimSlot(simSlot);
                                    return
                                        apiService
                                            .sendMessageStatus(
                                                phoneDetails[0],
                                                phoneDetails[1],
                                                gupshupMessageIds.get(0),
                                                messageEntity.getMask(),
                                                "read",
                                                0,
                                                phoneDetails[2]
                                            )
                                            .execute();
                                }
                            }
                        )
                        .get();

                SendMessageStatusResponse sendMessageStatusResponse = sendMessageStatusResponseResponse.body();

                messageEntity.setReadTimestamp(sendMessageStatusResponse.getReadTimestamp());
            }
            messageEntity.setReadAcknowledged(true);
        }

        messageDAO.update(messageEntities.toArray(new MessageEntity[messageEntities.size()]));
    }


    public void fetchPhoneNumberDetails(int simSlot, final String destinationPhoneNumber, final Callback<PhoneNumberDetailsResponse> callback) {
        Call<PhoneNumberDetailsResponse> phoneNumberDetailsResponseCall = getPhoneNumberDetailsResponseCall(simSlot, destinationPhoneNumber);
        phoneNumberDetailsResponseCall.enqueue(new Callback<PhoneNumberDetailsResponse>() {
            @Override
            public void onResponse(Call<PhoneNumberDetailsResponse> call, Response<PhoneNumberDetailsResponse> response) {
                if (response.isSuccessful()) {
                    if (AppConstants.VERIFIED.equals(response.body().getStatus())) {
                        List<ConversationEntity> conversations = conversationDAO.getConversations(new String[]{Utils.getMaskFromPhoneNumber(destinationPhoneNumber)});
                        if (conversations != null && conversations.size() > 0) {
                            ConversationEntity conversationEntity = conversations.get(0);
                            String publicKey = response.body().getPublicKey();
                            if (!publicKey.equals(conversationEntity.getIpPublicKey())) {
                                conversationEntity.setIpPublicKey(new GupshupPublicKey(publicKey));
                                conversationDAO.update(conversationEntity);
                            }
                        }
                        callback.onResponse(call, response);
                        return;
                    }
                }
                callback.onFailure(call, new Exception("Phone Number Not Verified"));
            }

            @Override
            public void onFailure(Call<PhoneNumberDetailsResponse> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }

    private Call<PhoneNumberDetailsResponse> getPhoneNumberDetailsResponseCall(int simSlot, String destinationPhoneNumber) {
        String[] phoneDetails = getDetailsFromSimSlot(simSlot);
        String countryCode = phoneDetails[0];
        String nationalPhoneNumber = phoneDetails[1];
        String apiKey = phoneDetails[2];
        return apiService.fetchPhoneNumberDetails(countryCode, nationalPhoneNumber, destinationPhoneNumber, apiKey);
    }

    private String[] getDetailsFromSimSlot(int simSlot) {
        if (simSlot == 0) {
            return new String[]{
                mPhoneNumberInfoSimOne[0],
                mPhoneNumberInfoSimOne[1],
                configEntitiesMap.get(ConfigKey.API_KEY_1),
                configEntitiesMap.get(ConfigKey.IP_PRIVATE_KEY_1)
            };
        } else {
            return new String[]{
                mPhoneNumberInfoSimTwo[0],
                mPhoneNumberInfoSimTwo[1],
                configEntitiesMap.get(ConfigKey.API_KEY_2),
                configEntitiesMap.get(ConfigKey.IP_PRIVATE_KEY_2)
            };
        }
    }
}
