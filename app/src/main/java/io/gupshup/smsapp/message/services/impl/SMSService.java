package io.gupshup.smsapp.message.services.impl;

import android.content.Context;
import android.support.v4.util.Pair;

import java.util.List;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public final class SMSService extends MessagingService {

    public SMSService() {
        super(MessageChannel.SMS);
    }

    @Override
    public final void sendMessage(
            String messageId, String mask,
            String phoneNumber,
            GupshupMessage message,
            int simSlot
    ) throws Exception {
        String text = ((GupshupTextMessage) message).getText();
        Context context = MainApplication.getContext();
        Pair<String, Integer> simSubscriptionInfo = AppUtils.getSimSubscriptionInfo(context, simSlot);
        int subscriptionId = 0;
        if (simSubscriptionInfo != null) {
            subscriptionId = simSubscriptionInfo.second;
        }
        String carrierName = AppConstants.EMPTY_TEXT;
        Pair<String, Integer> info = AppUtils.getChanelName(context, simSlot);
        if (info != null) {
            carrierName = info.first;
        }
        List<MessageEntity> messageEntities = messageDAO.get(messageId);
        if (messageEntities.size() > 0) {
            MessageEntity messageEntity = messageEntities.get(0);
            messageEntity.setCarierName(carrierName);
            messageEntity.setChannel(MessageChannel.SMS);  //Fix for AMA-1005
            messageDAO.update(messageEntity);
        }
        /*SmsReceiver.preSendMessage(phoneNumber, text, carrierName, simSlot, messageId);*/
        MessageUtils.sendSMS(context, phoneNumber, text, messageId, subscriptionId);
    }

    @Override
    public final long sending(String systemMessageId, String gupshupMessageId, int simSlot, String mask, String phoneNumber) throws Exception {
        throw new UnsupportedOperationException("SMSService doesn't support this operation");
    }

    @Override
    public final void receiveMessages(int simSlot) throws Exception {
        throw new UnsupportedOperationException("SMSService doesn't support this operation");
    }

    @Override
    public final Object receiving(int receivedSimSlot) throws Exception {
        throw new UnsupportedOperationException("SMSService doesn't support this operation");
    }

    @Override
    public final void acknowledgeReads() throws Exception {
        throw new UnsupportedOperationException("SMSService doesn't support this operation");
    }

}
