package io.gupshup.smsapp.message;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import io.gupshup.InitializationException;
import io.gupshup.SegregateApp;
import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.database.entities.PhoneNumberEntity;
import io.gupshup.smsapp.enums.Author;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.exceptions.UnparsablePhoneNumberException;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.DataService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.networking.responses.PhoneNumberDetailsResponse;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.NetworkUtil;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.gupshup.smsapp.utils.AppConstants.INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public final class Utils {

    //Sorry :/
    private static List<String> operatorSpecialStore = new ArrayList<String>() {{
        add("VODAFONE");
        add("ETISALAT");
    }};

    public static String getMaskFromPhoneNumber(@NonNull String phoneNumber) {
        return getMaskFromPhoneNumber(phoneNumber, MainApplication.getContext());
    }

    public static String getMaskFromPhoneNumber(@NonNull String phoneNumber, @NonNull Context context) {
        if (phoneNumber == null)
            return null;

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(context);
        Phonenumber.PhoneNumber phoneNumberProto = null;

        try {
            phoneNumberProto = phoneNumberUtil.parse(phoneNumber, AppConstants.INDIA);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }

        if (phoneNumberProto != null && (phoneNumberUtil.getNumberType(phoneNumberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneNumberUtil.getNumberType(phoneNumberProto) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE)) {
            return phoneNumberUtil.format(
                phoneNumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL
            );
        }

        String tempNumber;
        if (phoneNumber.length() > 5 && phoneNumber.charAt(2) == '-') {
            tempNumber = phoneNumber.substring(3, phoneNumber.length());
            return isAllChars(tempNumber) ? capitalize(tempNumber) : tempNumber;
        } else if (phoneNumber.length() == 8 && Character.isLetter(phoneNumber.charAt(0)) && Character.isLetter(phoneNumber.charAt(1)) && operatorSpecialStore.indexOf(phoneNumber.toUpperCase()) == -1) {
            tempNumber = phoneNumber.substring(2);
            return isAllChars(tempNumber) ? capitalize(tempNumber) : tempNumber;
        } else {
            return phoneNumber;
        }
    }

    public static String[] getPhoneNumberInfo(@NonNull String phoneNumber) {
        return getPhoneNumberInfo(phoneNumber, MainApplication.getContext());
    }

    public static String[] getPhoneNumberInfo(@NonNull String phoneNumber, @NonNull Context context) {
        long[] phoneNumberParts = getPhoneNumberParts(phoneNumber, context);

        if (phoneNumberParts == null) {
            return null;
        }

        return new String[]{
            MessageFormat.format("+{0}", String.valueOf(phoneNumberParts[0])),
            String.valueOf(phoneNumberParts[1])
        };
    }

    public static long[] getPhoneNumberParts(String phoneNumber, Context context) {
        if (phoneNumber == null)
            return null;

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(context);
        Phonenumber.PhoneNumber phoneNumberProto = null;

        try {
            phoneNumberProto = phoneNumberUtil.parse(phoneNumber, AppConstants.INDIA);
        } catch (NumberParseException e) {
            e.printStackTrace();
        }


        if (phoneNumberProto != null) {
            PhoneNumberUtil.PhoneNumberType numberType = phoneNumberUtil.getNumberType(phoneNumberProto);
            if ((numberType == PhoneNumberUtil.PhoneNumberType.MOBILE || numberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE)) {
                return new long[]{
                    phoneNumberProto.getCountryCode(),
                    phoneNumberProto.getNationalNumber()
                };
            }
        }
        return null;
    }

    public static long[] getPhoneNumberParts(String phoneNumber) {
        return getPhoneNumberParts(phoneNumber, MainApplication.getContext());
    }

    public static String generateMessageId() {
        return UUID.randomUUID().toString();
    }

    public static String getMaskFromPhoneNumbers(@NonNull List<String> phoneNumbers, Context context) throws UnparsablePhoneNumberException {
        if (context == null) {
            context = MainApplication.getContext();
        }
        List<PhoneNumberEntity> phoneNumberEntities = new ArrayList<>(phoneNumbers.size());
        for (String phoneNumber : phoneNumbers) {
            phoneNumberEntities.add(new PhoneNumberEntity(phoneNumber));
        }
        Collections.sort(phoneNumberEntities);
        StringBuilder sb = new StringBuilder();
        Iterator<PhoneNumberEntity> entityIterator = phoneNumberEntities.iterator();
        while (entityIterator.hasNext()) {
            PhoneNumberEntity pnE = entityIterator.next();
            sb = sb.append(pnE.getNationalNumber());
            if (!entityIterator.hasNext()) {
                sb = sb.append(",");
            }
        }
//            MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digest = new byte[0];
        try {
            digest = io.gupshup.crypto.app.Utils.sha256(sb.toString().getBytes());
        } catch (NoSuchAlgorithmException e) {
            AppUtils.logToFile(MainApplication.getContext(), "Exception while getMaskFromPhoneNumbers.." + e.getMessage());
            e.printStackTrace();
        }
        return io.gupshup.crypto.app.Utils.base64Encode(digest).trim();
    }

    public static ConversationType getConversationType(String phoneNumber, String message) {
        return getConversationType(phoneNumber, message, MainApplication.getContext());
    }

    public static ConversationType getConversationType(String phoneNumber, String message, Context context) {
        SegregateApp segregateApp = new SegregateApp();
        try {
            int category = segregateApp.getCategory(phoneNumber, message);
            switch (category) {
                case 0:
                    return ConversationType.TRANSACTIONAL;
                case 1:
                    return ConversationType.PROMOTIONAL;
                case 2:
                    return ConversationType.OTP;
                case 3:
                    return ConversationType.OTP;
                default:
                    return ConversationType.PERSONAL;
            }
        } catch (IOException | InitializationException e) {
            e.printStackTrace();
        }
        return ConversationType.PERSONAL;
    }


    public static void showContactDetail(Context context, String number, int phoneContactID) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI,
            String.valueOf(phoneContactID));
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static void shareMessage(Context context, String message) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        Intent chooser = Intent.createChooser(sendIntent, context.getResources().getText(R.string.share_via));
        // Verify the intent will resolve to at least one activity
        if (sendIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(chooser);
        }
        //context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.share_via)));
    }


   /* public static ConversationType getConversationTypeFromMask(String mask) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(MainApplication.getContext());
        try {
            Phonenumber.PhoneNumber parse = phoneNumberUtil.parse(mask, "");
            return ConversationType.PERSONAL;
        } catch (NumberParseException e) {
        }
        if (TextUtils.isDigitsOnly(mask)) {
            return ConversationType.PROMOTIONAL;
        } else {
            return ConversationType.TRANSACTIONAL;
        }
    }*/

    public static void copyToClipBoard(Context context, String textTobeCopied) {

        // Gets a handle to the clipboard service.
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // Creates a new text clip to put on the clipboard
        ClipData clip = ClipData.newPlainText(context.getString(R.string.copy_to_clip_board), textTobeCopied);
        // Set the clipboard's primary clip.
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }

    public static void addNewContact(Context context, String phoneNumber, BaseFragment fragment, boolean addToExistingContact) {


       /* // Creates a new Intent to insert a contact
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        // Sets the MIME type to match the Contacts Provider
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);*/
        Intent intent;

        if (addToExistingContact) {
            intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
            intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        } else {
            intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            // Sets the MIME type to match the Contacts Provider
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        }

        /*
         * Inserts new data into the Intent. This data is passed to the
         * contacts app's Insert screen
         */
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
            // Inserts a phone number
            .putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber)
            /*
             * In this example, sets the phone type to be a work phone.
             * You can set other phone types as necessary.
             */
            .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
            .putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
        /* Sends the Intent*/
        if (fragment != null) {
            fragment.startActivityForResult(intent, AppConstants.ADD_CONTACT_ACTIVITY_RESULT);
        } else {
            if (context instanceof AppCompatActivity) {
                ((AppCompatActivity) context).startActivityForResult(intent, AppConstants.ADD_CONTACT_ACTIVITY_RESULT);
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }
/*
    public static void viewContact(String mask, Context context) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactID));
        Intent intent = new Intent(Intent.ACTION_VIEW, contactUri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }*/

    public static class GetContactNameAndPhoto extends AsyncTask<Void, Void, Pair<String, String>> {
        public String displayName, displayThumb, phoneNumber;
        Context context;

        public GetContactNameAndPhoto(Context con, String phnNumber) {
            phoneNumber = phnNumber;
            context = con;
        }

        protected Pair<String, String> doInBackground(Void... params) {
            displayName = "";
            displayThumb = "";
            return MessageUtils.getContactUserDetail(context, phoneNumber);
        }

        protected void onPostExecute(Pair<String, String> userInfo) {
            LogUtility.d("GC", "In OnPostExecute..");
            if (userInfo != null) {
                LogUtility.d("GC", "The name is " + userInfo.first);
                LogUtility.d("GC", "The thumb is " + userInfo.second);

                displayName = userInfo.first;
                displayThumb = userInfo.second;
            }
            if (TextUtils.isEmpty(displayName)) {
                displayName = phoneNumber;
            }

            LogUtility.d("GG", "Display name is " + displayName);
            LogUtility.d("GG", "Display thumb is " + displayThumb);
        }

    }

    public static MessageEntity getNewMessage(
        String messageId,
        String mask,
        MessageDirection messageDirection,
        GupshupMessage message,
        String destination,
        long messageTimestamp,
        String carrier,
        MessageStatus status,
        int simSlot, MessageChannel channel, long draftMessageCreationTime) {
        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setMessageId((messageId != null && !messageId.isEmpty()) ? messageId : Utils.generateMessageId());
        messageEntity.setMask(mask);
        messageEntity.setDirection(messageDirection);
        messageEntity.setContent(message);
        messageEntity.setChannel(channel);
        messageEntity.setDestination(destination);
        messageEntity.setSentTimestamp(messageDirection == MessageDirection.OUTGOING ? DateUtils.currentTimestamp() : -1);
        messageEntity.setMessageTimestamp(messageTimestamp > 0 ? messageTimestamp : -1);
        messageEntity.setDeliveredTimestamp(messageDirection == MessageDirection.INCOMING ? DateUtils.currentTimestamp() : -1);
        messageEntity.setReadTimestamp(-1);
        messageEntity.setReadAcknowledged(false);
        messageEntity.setMetadata(null);
        messageEntity.setSearch(null);
        messageEntity.setCarierName(carrier);
        messageEntity.setMessageStatus(status);
        messageEntity.setIncomingSIMID(simSlot);
        messageEntity.setCreationTime(draftMessageCreationTime > 0 ? draftMessageCreationTime : DateUtils.currentTimestamp());
        return messageEntity;
    }

    public static ConversationEntity getNewConversation(String mask, String phoneNumber, MessageChannel channel, String message) {
        ConversationEntity conversationEntity = new ConversationEntity();

        ConversationType conversationType = Utils.getConversationType(phoneNumber, message);
        ConversationType conversationSubType = null;
        if (conversationType == ConversationType.OTP) {
            conversationSubType = ConversationType.OTP;
            conversationType = ConversationType.TRANSACTIONAL;
        } else if (conversationType == ConversationType.COUPON) {
            conversationSubType = ConversationType.COUPON;
            conversationType = ConversationType.PROMOTIONAL;
        }
        conversationEntity.setMask(mask);
        conversationEntity.setChannels(Collections.singletonList(channel));
        conversationEntity.setIpPublicKey(null);
        conversationEntity.setIpLastSyncedTimestamp(-1);
        conversationEntity.setDisplay(mask);
        conversationEntity.setImage(null);
        conversationEntity.setPbLastSyncedTimestamp(-1);
        conversationEntity.setType(conversationType);
        conversationEntity.setOriginalConversationType(conversationType);
        conversationEntity.setConversationSubType(conversationSubType);
        conversationEntity.setTypeSetBy(Author.APP);
        conversationEntity.setSubType(null);
        conversationEntity.setDraftMessage(null);
        conversationEntity.setLastMessageId(null);
        conversationEntity.setLastMessageData(null);
        conversationEntity.setLastMessageTimestamp(-1);
        conversationEntity.setNoOfUnreadMessages(0);
        conversationEntity.setSendNotifications(true);
        conversationEntity.setNotificationsSound(true);
        conversationEntity.setNotificationsLight(true);
        conversationEntity.setNotificationsPopup(true);
        conversationEntity.setDraftCreationTime(-1);
        if (!TextUtils.isEmpty(phoneNumber)) {
            conversationEntity.setOriginalPhoneNumber(phoneNumber);
        }
        return conversationEntity;
    }

    public static ConversationEntity updateConversation(ConversationEntity conversationEntity, MessageEntity lastMessageEntity) {
        conversationEntity.setLastMessageId(lastMessageEntity.getMessageId());
        conversationEntity.setLastMessageData(lastMessageEntity);
        if (TextUtils.isEmpty(conversationEntity.getDraftMessage())) {
            conversationEntity.setLastMessageTimestamp(lastMessageEntity.getMessageTimestamp());
        } else {
            conversationEntity.setLastMessageTimestamp(conversationEntity.getDraftCreationTime());
        }
        return conversationEntity;
    }

    public static Uri insertSms(String phoneNumber, String smsMessage, Context context, Uri contentUri) {
        ContentValues smsValues = new ContentValues();
        smsValues.put(Telephony.Sms.ADDRESS, phoneNumber);
        smsValues.put(Telephony.Sms.BODY, smsMessage);
        smsValues.put(Telephony.Sms.DATE, System.currentTimeMillis());
        smsValues.put(Telephony.Sms.READ, 0);
        return context.getContentResolver().insert(contentUri, smsValues);
    }

    private static boolean isAllChars(String phoneNumber) {
        for (int index = 0; index < phoneNumber.length(); index++) {
            char characterAtIndex = phoneNumber.charAt(index);
            if (Character.isDigit(characterAtIndex)) {
                return false;
            }
        }
        return true;
    }

    private static String capitalize(String mask) {
        //Removing Logic of Just Making mask as CapitalCase. Making them upperCase
        /*char[] maskChars = mask.toLowerCase().toCharArray();
        maskChars[0] = Character.toUpperCase(maskChars[0]);
        return new String(maskChars);*/
        return mask.toUpperCase();
    }

    public static String verifyCountryCode(String countryCode) {
        return countryCode.startsWith("+") ? countryCode : String.format("+%s", countryCode);
    }

    public static void trySendingDataMessage(final Context context, final GupshupMessage gupshupMessage,
                                             final String phoneNumber, final int simSlot,
                                             long draftMessageCreationTime, boolean isFresh) {
        final DataService dataService = MessagingService.getInstance(DataService.class);
        final SMSService smsService = MessagingService.getInstance(SMSService.class);
        final String messageId;
        messageId = getMessageId(context, smsService, dataService, gupshupMessage, phoneNumber,
            isFresh, draftMessageCreationTime, simSlot);
        List<ConversationEntity> conversationEntities = AppDatabase.getInstance(context).conversationDAO().get(phoneNumber);
        ConversationEntity conversationEntity = null;
        if (conversationEntities.size() > 0) {
            conversationEntity = conversationEntities.get(0);
            if (canSendDataSMS(conversationEntity)) {
                try {
                    dataService.sendMessage(messageId, phoneNumber, phoneNumber, gupshupMessage, simSlot);
                } catch (Exception ex) {
                    try {
                        smsService.sendMessage(messageId, phoneNumber, phoneNumber, gupshupMessage, simSlot);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return;
            }
        }
        final ConversationEntity finalConversationEntity = conversationEntity;
        dataService.fetchPhoneNumberDetails(simSlot, phoneNumber, new Callback<PhoneNumberDetailsResponse>() {
            @Override
            public void onResponse(Call<PhoneNumberDetailsResponse> call, Response<PhoneNumberDetailsResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        dataService.sendMessage(messageId, phoneNumber, phoneNumber, gupshupMessage, simSlot);
                        if (finalConversationEntity != null) {
                            finalConversationEntity.setDataSupported(true);
                            finalConversationEntity.setLastDataSyncTimestamp(System.currentTimeMillis());
                            AppDatabase.getInstance(context).conversationDAO().update(finalConversationEntity);
                        }
                        return;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                try {
                    smsService.sendMessage(messageId, phoneNumber, phoneNumber, gupshupMessage,
                        simSlot);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<PhoneNumberDetailsResponse> call, Throwable t) {
                try {
                    smsService.sendMessage(messageId, phoneNumber, phoneNumber,
                        gupshupMessage, simSlot);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * This method will return messageId.
     *
     * @param context
     * @param smsService
     * @param dataService
     * @param gupshupMessage
     * @param phoneNumber
     * @param isFresh
     * @param draftMessageCreationTime
     * @param simSlot
     * @return
     */
    private static String getMessageId(Context context, SMSService smsService,
                                       DataService dataService, GupshupMessage gupshupMessage,
                                       String phoneNumber, boolean isFresh,
                                       long draftMessageCreationTime, int simSlot) {

        if (NetworkUtil.isAvailable(context)) {
            if (isFresh) {
                return dataService.preSending(null, phoneNumber, gupshupMessage,
                    "", MessageStatus.SENDING, simSlot, "",
                    draftMessageCreationTime);
            } else {
                return dataService.preSending(phoneNumber, null, gupshupMessage,
                    "", MessageStatus.SENDING, simSlot, "",
                    draftMessageCreationTime);
            }
        } else {
            if (isFresh) {
                return smsService.preSending(null, phoneNumber, gupshupMessage,
                    "", MessageStatus.SENDING, simSlot, "",
                    draftMessageCreationTime);
            } else {
                return smsService.preSending(phoneNumber, null, gupshupMessage,
                    "", MessageStatus.SENDING, simSlot, "",
                    draftMessageCreationTime);
            }
        }
    }

    /**
     * Method used to check all phone numbers are valid
     *
     * @return
     */
    public static boolean isValidPhoneNumber(String phone) {
        return !TextUtils.isEmpty(phone) && Patterns.PHONE.matcher(Utils.getMaskFromPhoneNumber(phone)).matches();
    }

    /**
     * This method will sync system contacts with SMS DB.
     *
     * @param context
     */
    public static void syncContactDatabase(Context context) {

        String lastSyncTime = PreferenceManager.getInstance(context).getContactLastSyncTime();
        String[] projection = new String[]{
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
        };
        Cursor cursor = context.getContentResolver().query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
            ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP + ">=" +
                lastSyncTime, null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String contactId = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                String displayName = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String profileImage = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                String phoneNumber = cursor.getString(
                    cursor.getColumnIndex((ContactsContract.CommonDataKinds.Phone.NUMBER)));
                MessagingService.getInstance(SMSService.class)
                    .updateDetailOfParticularContact(phoneNumber, contactId, displayName, profileImage);
            }
            cursor.close();
        }
        PreferenceManager.getInstance(context).setContactLastSyncTime(String.valueOf(System.currentTimeMillis()));
    }

    private static boolean canSendDataSMS(ConversationEntity conversationEntity) {

        return conversationEntity.isDataSupported()
            && (System.currentTimeMillis() - conversationEntity.getLastDataSyncTimestamp()) < 86400000;
    }

    /**
     * To check whether need to show notification or not if user in active chat then don't show notification.
     * AMA-711 fixes.
     *
     * @param context
     * @param conversationEntity
     * @return
     */
    public static boolean canShowNotification(Context context, ConversationEntity conversationEntity) {
        boolean canShowNotification = false;
        if (MainApplication.getInstance() != null
            && !MainApplication.getInstance().isAppActive()) {
            if (conversationEntity != null
                && conversationEntity.getType() != ConversationType.BLOCKED) {
                if (PreferenceManager.getInstance(context).isNotificationEnabled()) {
                    canShowNotification = true;
                }
            }

        }
        return canShowNotification;
    }

    /**
     * Method used to get the last message in the conversation
     *
     * @param entity
     * @return
     */
    public static String getLastMessageInConversation(ConversationEntity entity) {
        return ((GupshupTextMessage) entity.getLastMessageData().getContent()).getText() != null ?
            ((GupshupTextMessage) entity.getLastMessageData().getContent()).getText() : AppConstants.EMPTY_TEXT;
    }

}