package io.gupshup.smsapp.message.services;

import android.arch.paging.DataSource;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberUtils;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConfigDAO;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.dao.MessageMapDAO;
import io.gupshup.smsapp.database.dao.SearchDAO;
import io.gupshup.smsapp.database.datasource.GlobalSearchEntityDataSource;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.database.executor.MainThreadExecutor;
import io.gupshup.smsapp.database.providers.GlobalSearchEntityProvider;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.networking.responses.AutoVerificationCheckResponse;
import io.gupshup.smsapp.ui.reply.view.MessageReplyActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.Triple;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import retrofit2.Callback;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public abstract class MessagingService {

    private static final AppDatabase appDatabase = AppDatabase.getInstance(MainApplication.getContext());
    protected static final ConfigDAO configDAO = appDatabase.configDAO();
    protected static final ConversationDAO conversationDAO = appDatabase.conversationDAO();
    protected static final MessageDAO messageDAO = appDatabase.messageDAO();
    protected static final SearchDAO searchDAO = appDatabase.searchDAO();
    protected static final MessageMapDAO messageMapDao = appDatabase.messageMapDAO();

    private static final Map<Class<? extends MessagingService>, MessagingService> instances = new ConcurrentHashMap<>();

    protected final String TAG = getClass().getSimpleName();

    private final MessageChannel channel;

    @Inject
    protected APIService apiService;

    private MessagingService() throws InstantiationException {
        throw new InstantiationException("Can not instantiate with nullary constructor");
    }

    protected MessagingService(@NonNull MessageChannel channel) {
        if (instances.get(getClass()) != null) {
            throw new RuntimeException("Use getInstance(channel) method to get the instance");
        }

        this.channel = channel;

        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public static <S extends MessagingService> S getInstance(@NonNull Class<S> serviceClass) {
        if (serviceClass == null) {
            return null;
        }

        if (instances.get(serviceClass) == null) {
            synchronized (MessagingService.class) {
                if (instances.get(serviceClass) == null) {
                    try {
                        instances.put(
                            serviceClass, serviceClass.newInstance()
                        );
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }

        return (S) instances.get(serviceClass);
    }

    /**
     * This method will call {@code preSending}, {@code sending} & {@code postSending} consecutively
     *
     * @param messageId
     * @param mask        the mask of the conversation
     * @param phoneNumber the phone number in case of new conversation
     * @param message     the message to be sent
     * @throws Exception
     */
    public abstract void sendMessage(
        String messageId, String mask,
        String phoneNumber,
        GupshupMessage message,
        int simSlot
    ) throws Exception;

    /**
     * This method will be called just before attempting to send a message
     *
     * @param mask                     the mask of the conversation
     * @param phoneNumber              the phone number in case of new conversation
     * @param message                  the message to be sent
     * @param draftMessageCreationTime
     * @return the message_id of the message
     */
    public final String preSending(
        String mask,
        String phoneNumber,
        GupshupMessage message,
        String carier,
        MessageStatus status,
        int simSlot, String systemMessageId, long draftMessageCreationTime) {
        if (mask == null && phoneNumber == null)
            throw new IllegalArgumentException("One of mask & phoneNumber need to be passed");

        final String givenMask = mask;

        if (givenMask == null)
            mask = Utils.getMaskFromPhoneNumber(phoneNumber);

        ConversationEntity conversationEntity = null;

        systemMessageId = saveSystemSMS(mask, (GupshupTextMessage) message, MessageDirection.OUTGOING);

        {
            List<ConversationEntity> conversationEntities = conversationDAO.get(mask);
            if (conversationEntities.size() > 0)
                conversationEntity = conversationEntities.get(0);
            else if (givenMask != null)
                throw new IllegalArgumentException("No conversation exists with the mask" + '[' + givenMask + ']');
        }

        MessageEntity messageEntity = Utils.getNewMessage(systemMessageId, mask, MessageDirection.OUTGOING,
            message, mask, DateUtils.currentTimestamp(), carier, status, simSlot, channel, draftMessageCreationTime);
        String viaText = AppUtils.getViaText(MainApplication.getContext(), messageEntity);
        messageEntity.setCarierName(viaText);
        if (conversationEntity == null) {
            conversationEntity = Utils.getNewConversation(mask, phoneNumber, channel, ((GupshupTextMessage) messageEntity.getContent()).getText());
            Triple<String, String, String> userInfo = MessageUtils.getContactUserDetails(MainApplication.getContext(), mask);

            if (userInfo != null) {
                conversationEntity.setDisplay(userInfo.getFirst());
                conversationEntity.setImage(userInfo.getSecond());
                conversationEntity.setContactId(userInfo.getThird());
            }
            long[] conversationIds = conversationDAO.insert(conversationEntity);

            conversationEntity.setConversationId(conversationIds[0]);
        }

        conversationEntity = Utils.updateConversation(conversationEntity, messageEntity);

        messageDAO.insert(messageEntity);

        conversationDAO.update(conversationEntity);

        return messageEntity.getMessageId();
    }

    /**
     * This method will send the message
     *
     * @param mask
     * @param phoneNumber
     * @return the message_timestamp of the message
     * @throws Exception
     */
    public abstract long sending(String systemMessageId, String gupshupMessageId, int simSlot, String mask, String phoneNumber) throws Exception;

    /**
     * This method will be called after a message is sent/delivered
     *
     * @param messageId          the message_id of the message
     * @param messageTimestamp   the message_timestamp of the message
     * @param deliveredTimestamp the delivered_timestamp of the message
     */
    public final void postSending(
        String messageId,
        long messageTimestamp,
        long deliveredTimestamp,
        MessageStatus messageStatus

    ) {
        MessageEntity messageEntity;
        ConversationEntity conversationEntity;

        {
            List<MessageEntity> messageEntities = messageDAO.get(messageId);
            if (messageEntities.size() > 0)
                messageEntity = messageEntities.get(0);
            else
                throw new IllegalArgumentException("No message exists with the message_id" + '[' + messageId + ']');
        }

        if (messageTimestamp > 0 && messageEntity.getDirection() != MessageDirection.OUTGOING)
            messageEntity.setMessageTimestamp(messageTimestamp);

        if (deliveredTimestamp > 0)
            messageEntity.setDeliveredTimestamp(deliveredTimestamp);

        messageEntity.setMessageStatus(messageStatus);

        {
            List<ConversationEntity> conversationEntities = conversationDAO.get(messageEntity.getMask());
            if (conversationEntities.size() > 0)
                conversationEntity = conversationEntities.get(0);
            else
                throw new SQLiteDatabaseCorruptException("No conversation exists with the mask" + '[' + messageEntity.getMask() + ']');
        }

        if (messageId.equals(conversationEntity.getLastMessageId()))
            conversationEntity = Utils.updateConversation(conversationEntity, messageEntity);

        Triple<String, String, String> userInfo = MessageUtils.getContactUserDetails(MainApplication.getContext(), conversationEntity.getMask());

        if (userInfo != null) {
            conversationEntity.setDisplay(userInfo.getFirst());
            conversationEntity.setImage(userInfo.getSecond());
            conversationEntity.setContactId(userInfo.getThird());
        }
        messageDAO.update(messageEntity);

        conversationDAO.update(conversationEntity);
    }

    /**
     * This method will call {@code receiving} & {@code postReceiving} consecutively
     *
     * @throws Exception
     */
    public abstract void receiveMessages(int simSlot) throws Exception;

    /**
     * This method will fetch the messages
     *
     * @return the fetched messages object
     * @throws Exception
     */
    public abstract Object receiving(int receivedSimSlot) throws Exception;

    /**
     * This method will be called just after a message is received
     *
     * @param mask             the mask of the conversation
     * @param phoneNumber      the phone number in case of new conversation
     * @param messageId        the message_id of the received message
     * @param message          the message received
     * @param destination      the receiver mask of the conversation (helpful in case of dual sim mobiles)
     * @param messageTimestamp the message_timestamp of the message
     * @param contactName
     * @param profileImage     @return the messageEntity object of the message
     * @param receivedSimSlot
     * @throws Exception
     */
    public /*final*/ MessageEntity postReceiving(
        String mask,
        String phoneNumber,
        String messageId,
        GupshupMessage message,
        String destination,
        long messageTimestamp,
        String carierName,
        String contactName,
        String profileImage, int receivedSimSlot, String contactId) throws Exception {
        if (mask == null && phoneNumber == null)
            throw new IllegalArgumentException("One of mask & phoneNumber need to be passed");

        final String givenMask = mask;

        if (givenMask == null)
            mask = Utils.getMaskFromPhoneNumber(phoneNumber);

        if (destination != null)
            destination = Utils.getMaskFromPhoneNumber(destination);

        ConversationEntity conversationEntity = null;

        {
            List<ConversationEntity> conversationEntities = conversationDAO.get(mask);
            if (conversationEntities.size() > 0)
                conversationEntity = conversationEntities.get(0);
            else if (givenMask != null)
                throw new IllegalArgumentException("No conversation exists with the mask" + '[' + givenMask + ']');
        }

        MessageEntity messageEntity = Utils.getNewMessage(messageId, mask, MessageDirection.INCOMING,
            message, destination, messageTimestamp, carierName, null, receivedSimSlot, channel, -1);

        String viaText = AppUtils.getViaText(MainApplication.getContext(), messageEntity);
        messageEntity.setCarierName(viaText);
        if (conversationEntity == null) {
            conversationEntity = Utils.getNewConversation(mask, phoneNumber, channel, ((GupshupTextMessage) message).getText());

            long[] conversationIds = conversationDAO.insert(conversationEntity);

            conversationEntity.setConversationId(conversationIds[0]);
        }

        conversationEntity = Utils.updateConversation(conversationEntity, messageEntity);
        conversationEntity.setSubType(null); // Will be unarchived in case the conversation is archived
        conversationEntity.setNoOfUnreadMessages(conversationEntity.getNoOfUnreadMessages() + 1);
        conversationEntity.setUnread(true);
        conversationEntity.setPNRequired(true);
        conversationEntity.setContactId(contactId);
        if (TextUtils.isEmpty(contactName) || conversationEntity.getType() != ConversationType.PERSONAL) {
            conversationEntity.setDisplay(mask);
        } else {
            conversationEntity.setDisplay(contactName);
        }
        conversationEntity.setImage(profileImage);

        try {
            messageDAO.insert(messageEntity);
        } catch (SQLiteConstraintException e) {
            e.printStackTrace();
            return null;
        }

        conversationDAO.update(conversationEntity);

        return messageEntity;
    }

    /**
     * This method marks all messages in a conversation as read
     *
     * @param mask          the mask of the conversation
     * @param readTimestamp the read_timestamp
     */
    public final void readConversation(String mask, long readTimestamp) {
        if (mask == null)
            throw new NullPointerException();
        if (readTimestamp <= 0)
            throw new IllegalArgumentException("readTimestamp must be in epoch");

        ConversationEntity conversationEntity = null;

        {
            List<ConversationEntity> conversationEntities = conversationDAO.get(mask);
            if (conversationEntities.size() > 0)
                conversationEntity = conversationEntities.get(0);
            else
                throw new IllegalArgumentException("No conversation exists with the mask" + '[' + mask + ']');
        }

        messageDAO.markIncomingAsRead(mask, readTimestamp);

        MessageEntity lastMessageEntity = null;

        {
            List<MessageEntity> messageEntities = messageDAO.get(conversationEntity.getLastMessageId());
            if (messageEntities.size() > 0)
                lastMessageEntity = messageEntities.get(0);
            else
                throw new IllegalArgumentException("No message exists with the message_id" + '[' + conversationEntity.getLastMessageId() + ']');
        }

        conversationEntity = Utils.updateConversation(conversationEntity, lastMessageEntity);
        conversationEntity.setNoOfUnreadMessages(0);
        conversationEntity.setUnread(false);
        conversationEntity.setPNRequired(false);

        int rows = conversationDAO.update(conversationEntity);
        LogUtility.d("MARK AS READ", "" + rows);
        //LogUtility.d("auto2", "calling update in db...");
        final ConversationEntity ce = conversationEntity;
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... unused) {
                try {
                    updateConversationAsReadInDefaultSMSDb(MainApplication.getContext(), new ConversationEntity[]{ce});
                } catch (Exception ex) {
                    //LogUtility.d("auto2", "Exception in updating read in default db table.."+ex.getMessage());
                }
                return null;
            }
        }.execute();
    }

    private void updateConversationAsReadInDefaultSMSDb(Context context, ConversationEntity[] conversations) {

        //LogUtility.d("auto2", "the conv read status update for len:"+conversations.length);
        MessageDAO messageDAO = AppDatabase.getInstance(context).messageDAO();
        String masks[] = new String[conversations.length];
        for (int i = 0; i < conversations.length; i++) {
            masks[i] = conversations[i].getMask();
        }
        for (String mask : masks) {
            List<MessageEntity> messageEntityList = messageDAO.getMessagesForMask(mask);
            MessageEntity[] message = new MessageEntity[messageEntityList.size()];
            message = messageEntityList.toArray(message);
            //LogUtility.d("auto2", "message update called for message nos....."+message.length);
            MessageReplyActivity.updateSMSInDefaultDB(context, Telephony.Sms.CONTENT_URI, message);
            //MessageReplyActivity.updateSMSInDefaultDB(context, Telephony.MmsSms.CONTENT_URI, message);
        }
    }

    /**
     * This method acknowledges the unacknowledged reads
     *
     * @throws Exception
     */
    public abstract void acknowledgeReads() throws Exception;

    public List<MessageEntity> getMessagesForPN() {
        final List<ConversationEntity> pnConversations = conversationDAO.getPNConversations();
        String[] masks = new String[pnConversations.size()];
        int index = 0;
        for (ConversationEntity conversation : pnConversations) {
            masks[index++] = conversation.getMask();
        }
        return messageDAO.getIncomingUnacknowledgedReads(masks);
    }

    public synchronized PagedList<GlobalSearchEntity> globalSearch(final String keyword) {
        final List<SearchDAO.SearchItem> everyWhere = searchDAO.findEveryWhere(keyword);
        final Map<String, GlobalSearchEntity> maskSearchEntityMap = new HashMap<>();
        final int PAGE_SIZE = 5;

        for (SearchDAO.SearchItem searchItem : everyWhere) {
            GlobalSearchEntity gsEntity;
            if (!maskSearchEntityMap.containsKey(searchItem.getMask())) {
                gsEntity = new GlobalSearchEntity();
                gsEntity.setMask(searchItem.getMask());
                gsEntity.setDisplay(searchItem.getDisplay());
                gsEntity.setTimeStamp(Long.valueOf(searchItem.getLastMessageTimeStamp()));
                gsEntity.setLastMessageData(((GupshupTextMessage) searchItem.getLastMessageData().getContent()).getText());
                gsEntity.setMatchedKeyword(keyword);
                gsEntity.setImage(searchItem.getImage());
                gsEntity.setType(searchItem.getType());
                gsEntity.setSubType(searchItem.getSubType());
            } else {
                gsEntity = maskSearchEntityMap.get(searchItem.getMask());
            }
            switch (searchItem.getSource()) {
                case "Conversation":
                    if (gsEntity.getMask().toLowerCase().contains(keyword.toLowerCase())) {
                        final String mask = gsEntity.getMask().toLowerCase();
                        int startPosition = mask.indexOf(keyword.toLowerCase());
                        gsEntity.setSearchString(getSpannableText(keyword, gsEntity.getMask(), startPosition));
                    } else if (gsEntity.getDisplay().toLowerCase().contains(keyword.toLowerCase())) {
                        final String display = gsEntity.getDisplay().toLowerCase();
                        int startPosition = display.indexOf(keyword.toLowerCase());
                        gsEntity.setSearchString(getSpannableText(keyword, gsEntity.getDisplay(), startPosition));
                    } else {
                        throw new IllegalStateException("Search State Mismatch");
                    }
                    break;
                case "Message":
                    if (gsEntity.getMessages().size() == 0) {
                        final int startPosition = searchItem.getContent().toLowerCase().indexOf(keyword.toLowerCase());
                        if (startPosition > -1) {
                            gsEntity.addMessageEntity(getSpannableText(keyword, searchItem.getContent(), startPosition));
                        }
                    } else {
                        return null;
                    }
                    break;
            }
            maskSearchEntityMap.put(searchItem.getMask(), gsEntity);
        }
        final ArrayList<GlobalSearchEntity> list = new ArrayList<>(maskSearchEntityMap.values());
        Collections.sort(list, new Comparator<GlobalSearchEntity>() {
            @Override
            public int compare(GlobalSearchEntity o1, GlobalSearchEntity o2) {
                return Long.compare(o2.getTimeStamp(), o1.getTimeStamp());
            }
        });
        GlobalSearchEntityProvider globalSearchEntityProvider = new GlobalSearchEntityProvider(list);
        GlobalSearchEntityDataSource globalSearchEntityDataSource = new GlobalSearchEntityDataSource(PAGE_SIZE, globalSearchEntityProvider);
        PagedList.Config myConfig = new PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .build();
        return new PagedList.Builder<>(globalSearchEntityDataSource, myConfig)
            .setInitialKey(0)
            .setNotifyExecutor(new MainThreadExecutor())
            .setFetchExecutor(Executors.newSingleThreadExecutor())
            .build();
    }

    public List<SearchDAO.MessageEntityExtended> searchWithinThread(final String mask, final String keyword) {
        return searchDAO.findWithinMessages(mask, keyword);
    }

    private Spannable getSpannableText(String keyword, String text, int startPosition) {
        ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.BLUE});
        TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
        Spannable searchText = new SpannableString(text);
        searchText.setSpan(highlightSpan, startPosition, (startPosition + keyword.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return searchText;
    }

    public DataSource.Factory<Integer, ConversationEntity> getArchivedConversations() {
        return conversationDAO.getArchivedConversations();
    }

    public void isReRegistrationRequired(@NonNull final String publicKey1, @Nullable final String publicKey2, final Callback<AutoVerificationCheckResponse> callback) {
        apiService.getPhoneVerificationRequirementStatus(publicKey1, publicKey2).enqueue(callback);
    }

    public void unBlockMessage(ConversationEntity conversation) {
        //From Blocked tab
        if (conversation.getLastConversationType() != null) {
            conversation.setType(conversation.getLastConversationType());
        } else {
            ConversationType conversationType = Utils.getConversationType(conversation.getMask(), Utils.getLastMessageInConversation(conversation));
            conversation.setType(conversationType);
        }
        conversationDAO.update(conversation);

    }

    protected String saveSystemSMS(String phoneNumber, GupshupTextMessage message, MessageDirection messageDirection) {
        Uri uri = Utils.insertSms(phoneNumber, message.getText(), MainApplication.getContext(), (messageDirection == MessageDirection.INCOMING ? Telephony.Sms.Inbox.CONTENT_URI : Telephony.Sms.Sent.CONTENT_URI));
        return MessageUtils.extractMessageId(uri);
    }

    public void unArchiveMessage(ConversationEntity conversation) {
        //From Archive screen
        conversation.setSubType(null);
        conversationDAO.update(conversation);

    }

    /**
     * Method used to get the conversation entity by mask
     *
     * @param mask
     */
    public ConversationType getConversationType(String mask) {

        List<ConversationEntity> conversationEntity = conversationDAO.get(mask);
        if (conversationEntity != null && conversationEntity.size() > 0) {
            return conversationEntity.get(0).getType();
        }
        return ConversationType.PERSONAL;
    }

    public long[] insertConversation(ConversationEntity... conversations) {
        return conversationDAO.insert(conversations);
    }

    public void updatePNStatus() {
        conversationDAO.updatePNStatus();
    }

    public void markConversationAsUnread(String mask) {
        conversationDAO.markConversationAsUnread(mask);
    }

    /* public List<ContactModel> getTopContacts() {
         return conversationDAO.getTopContacts();
     }
 */
    public int getConversations(String[] masks) {
        return conversationDAO.getConversations(masks).size();
    }

    public void updateDraftMessage(String mask, String conv, long draftCreationTime, long lastMessageTimeStamp) {

        conversationDAO.updateDraftMessage(mask, conv, draftCreationTime, lastMessageTimeStamp);
    }

    public List<MessageEntity> getLastMessageForAMask(String mask) {
        return messageDAO.getLastMessageForAMask(mask);
    }

    public List<ConversationEntity> getConversationByMask(String mask) {

        return conversationDAO.get(mask);
    }

    public void updateConversation(ConversationEntity... conversations) {
        conversationDAO.update(conversations);
    }

    public void deleteConversation(ConversationEntity... conversations) {
        conversationDAO.delete(conversations);
    }

    public void deleteMessage(MessageEntity... messageEntities) {
        messageDAO.delete(messageEntities);
    }

    public List<MessageEntity> getMessageEntity(@NonNull String messageId) {
        return messageDAO.get(messageId);
    }

    public List<MessageEntity> getTopUnreadMessages(int count) {
        return messageDAO.getTopUnreadMessages(count);
    }

    public MessageDAO.CountData getTotalUnreadCount() {
        return messageDAO.getTotalUnreadCount();
    }

    public MessageDAO.CountData getTotalMessageCount(String mask) {
        return messageDAO.getTotalMessageCount(mask);
    }

    public void updateDetailOfParticularContact(String maskFromPhoneNumber, String contactId, String displayName, String profileThumb) {

        List<ConversationEntity> conversationEntities = getConversationByMask(Utils.getMaskFromPhoneNumber(maskFromPhoneNumber));
        if (conversationEntities != null
            && conversationEntities.size() > 0) {
            ConversationEntity conversation = conversationEntities.get(0);
            if (conversation != null) {
                if (contactId != null && (contactId.equalsIgnoreCase(conversation.getContactId())
                    || conversation.getContactId() == null)) {
                    conversation.setContactId(contactId);
                    conversation.setDisplay(displayName);
                    conversation.setImage(profileThumb);
                    conversationDAO.update(conversation);
                }
            }
        }
    }

    public int getUnreadConversationCount(ConversationType type) {
        return conversationDAO.getUnreadCount(type);
    }

    public List<String> getAllConversationsForType(String conversationType, String conversationSubType) {

        if (AppConstants.CONVERSATION_SUB_TYPE.equalsIgnoreCase(conversationSubType)) {
            return conversationDAO.getAllConversationsForArchive();
        } else {
            return conversationDAO.getAllConversationsForType(conversationType);
        }
    }

    /* public List<Long> getAllConversationId() {

         return conversationDAO.getAllConversationId();
     }*/
    public List<String> getOriginalContactNumberList() {

        return conversationDAO.getOriginalContactNumberList();
    }

    /**
     * Method used to get top contacts
     * AMA-931
     */
    public List<ContactModel> getTopContactsNew() {

        String[] projection = new String[]{
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI,
            //plus any other properties you wish to query
        };

        List<ContactModel> contactModel = new ArrayList<>();
        List<String> topMasks = AppDatabase.getInstance(MainApplication.getContext()).messageDAO().getTopContactsBasedOnMaxOutGoingMsgs();

        getContactsFromDB(topMasks, contactModel);

        if (topMasks.size() < 8) {
            getStarredContacts(topMasks, contactModel, projection);
        }

        if (contactModel.size() < 8) {
            getAlphabeticalwiseContact(topMasks, contactModel, projection);
        }

        return contactModel;
    }

    /**
     * Method used to get the contacts on alphabetical order
     *
     * @param topMasks
     * @param contactModel
     * @param projection
     */
    private void getAlphabeticalwiseContact(List<String> topMasks, List<ContactModel> contactModel, String[] projection) {
        String normalizedNumber;
        Cursor cursorAlphabeticSort = MainApplication.getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
            ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
            null,
            "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");

        int indexOfDisplayName = cursorAlphabeticSort.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexOfDisplayNumber = cursorAlphabeticSort.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int indexOfThumbNailUri = cursorAlphabeticSort.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI);


        while (cursorAlphabeticSort.moveToNext()) {
            String displayName = cursorAlphabeticSort.getString(indexOfDisplayName);
            String displayNumber = cursorAlphabeticSort.getString(indexOfDisplayNumber);
            String displayThumbNail = cursorAlphabeticSort.getString(indexOfThumbNailUri);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                normalizedNumber = PhoneNumberUtils.normalizeNumber(displayNumber);
            } else {
                normalizedNumber = displayNumber;
            }

            if (!topMasks.contains(Utils.getMaskFromPhoneNumber(normalizedNumber))) {
                ContactModel contactModelItem = new ContactModel();
                contactModelItem.setDisplay(displayName);
                contactModelItem.setImage(displayThumbNail);
                contactModelItem.setMask(displayNumber);
                contactModelItem.setMask(Utils.getMaskFromPhoneNumber(displayNumber));
                contactModelItem.setOriginalPhoneNumber(displayNumber);

                contactModel.add(contactModelItem);
            }

            if (contactModel.size() == 8) {
                cursorAlphabeticSort.close();
                break;
            }
        }

    }

    /**
     * Method used to get the starred contacts from contact book
     *
     * @param topMasks
     * @param contactModel
     * @param projection
     */
    private void getStarredContacts(List<String> topMasks, List<ContactModel> contactModel, String[] projection) {
        String normalizedNumber;
        int remaining = 8 - topMasks.size();
        Cursor cursorStarred = MainApplication.getContext().getContentResolver().query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, "starred=?",
            new String[]{"1"}, null);

        int indexOfDisplayName = cursorStarred.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexOfDisplayNumber = cursorStarred.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int indexOfThumbNailUri = cursorStarred.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI);

        while (cursorStarred.moveToNext()) {
            String displayName = cursorStarred.getString(indexOfDisplayName);
            String displayNumber = cursorStarred.getString(indexOfDisplayNumber);
            String displayThumbNail = cursorStarred.getString(indexOfThumbNailUri);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                normalizedNumber = PhoneNumberUtils.normalizeNumber(displayNumber);
            } else {
                normalizedNumber = displayNumber;
            }

            if (!topMasks.contains(Utils.getMaskFromPhoneNumber(normalizedNumber))) {
                ContactModel contactModelItem = new ContactModel();
                contactModelItem.setDisplay(displayName);
                contactModelItem.setImage(displayThumbNail);
                contactModelItem.setMask(Utils.getMaskFromPhoneNumber(displayNumber));
                contactModelItem.setOriginalPhoneNumber(displayNumber);

                contactModel.add(contactModelItem);
            }

            if (contactModel.size() == 8) {
                cursorStarred.close();
                break;
            }
        }
    }

    /**
     * method used to get the contacts from the DB
     *
     * @param topMasks
     * @param contactModel
     */
    private void getContactsFromDB(List<String> topMasks, List<ContactModel> contactModel) {
        //add name, mask and image to the contact model'
        for (String mask : topMasks) {
            ConversationEntity entity = getConversationByMask(mask).get(0);
            ContactModel contactModelItem = new ContactModel();
            contactModelItem.setDisplay(entity.getDisplay());
            contactModelItem.setImage(entity.getImage());
            contactModelItem.setMask(entity.getMask());
            contactModelItem.setOriginalPhoneNumber(entity.getOriginalPhoneNumber());
            contactModel.add(contactModelItem);
        }
    }

}
