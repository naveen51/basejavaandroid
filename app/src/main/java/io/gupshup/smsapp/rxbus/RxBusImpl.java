package io.gupshup.smsapp.rxbus;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBusImpl implements RxBus {

    private PublishSubject<Object> bus = PublishSubject.create();

    @Override
    public void send(Object object) {
        bus.onNext(object);
    }

    @Override
    public Observable<Object> toObservable() {
        return bus;
    }
}
