package io.gupshup.smsapp.rxbus;

public interface RxBusCallback {
     void onEventTrigger(Object object);
}
