package io.gupshup.smsapp.rxbus;

import io.reactivex.Observable;

public interface RxBus {

     void send(Object object);

     Observable<Object> toObservable();

}
