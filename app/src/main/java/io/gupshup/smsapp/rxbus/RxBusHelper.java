package io.gupshup.smsapp.rxbus;

import android.annotation.SuppressLint;

import io.gupshup.smsapp.utils.LogUtility;
import io.reactivex.observers.DisposableObserver;

public class RxBusHelper {
    private DisposableObserver<Object> disposable = null;

    @SuppressLint("RxLeakedSubscription")
    public void registerEvents(RxBus rxBus, final String tag, final RxBusCallback callback) {

        if (rxBus != null) {
            disposable = new DisposableObserver<Object>() {

                @Override
                public void onNext(Object o) {
                    callback.onEventTrigger(o);
                }

                @Override
                public void onError(Throwable e) {
                    LogUtility.d(tag, "Event : onError : " + e.getMessage());
                }

                @Override
                public void onComplete() {
                    LogUtility.d(tag, "Event : onComplete");
                }
            };

            rxBus.toObservable().share().subscribeWith(disposable);

        }
    }

    public void unSubScribe() {
        if (disposable != null) {
            disposable.dispose();
            disposable = null;
        }
    }
}
