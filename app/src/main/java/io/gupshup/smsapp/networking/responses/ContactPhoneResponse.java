package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class ContactPhoneResponse {
    @SerializedName("contactPhone")
    private String contactPhone;

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
}
