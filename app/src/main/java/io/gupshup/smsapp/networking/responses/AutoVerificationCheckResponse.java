package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class AutoVerificationCheckResponse {
    @SerializedName("firstPhoneNumber")
    private String firstPhoneNumber;
    @SerializedName("firstPhoneVerificationStatus")
    private boolean firstPhoneVerificationStatus;
    @SerializedName("secondPhoneNumber")
    private String secondPhoneNumber;
    @SerializedName("secondPhoneVerificationStatus")
    private boolean secondPhoneVerificationStatus;

    public String getFirstPhoneNumber() {
        return firstPhoneNumber;
    }

    public void setFirstPhoneNumber(String firstPhoneNumber) {
        this.firstPhoneNumber = firstPhoneNumber;
    }

    public boolean isFirstPhoneVerificationStatus() {
        return firstPhoneVerificationStatus;
    }

    public void setFirstPhoneVerificationStatus(boolean firstPhoneVerificationStatus) {
        this.firstPhoneVerificationStatus = firstPhoneVerificationStatus;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public boolean isSecondPhoneVerificationStatus() {
        return secondPhoneVerificationStatus;
    }

    public void setSecondPhoneVerificationStatus(boolean secondPhoneVerificationStatus) {
        this.secondPhoneVerificationStatus = secondPhoneVerificationStatus;
    }
}
