package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class AppConfigurationAPIResponse {

    @SerializedName("appId")
    private String appId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

}
