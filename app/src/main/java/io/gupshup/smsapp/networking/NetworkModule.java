package io.gupshup.smsapp.networking;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import io.gupshup.smsapp.Config;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger module provides injectable components
 */

@Module
public class NetworkModule {

    private static final String TAG = NetworkModule.class.getSimpleName();
    private Context mContext;

    public NetworkModule(Context context) {
        mContext = context;
    }

    @Provides
    @NonNull
    OkHttpClient getHttpClientBuilder() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        clientBuilder.addInterceptor(loggingInterceptor);
        clientBuilder.retryOnConnectionFailure(true);
        //}
      /*  clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //To override base url based on selection(Prod/Staging/Dev)
                PreferenceManager manager = PreferenceManager.getInstance(mContext);
                Request request = chain.request();
                Request.Builder builder = request.newBuilder();
                String endPoint = getEndPoint(request.url().toString());
                builder.url(manager.getConfigEnvironment() + endPoint);
                LogUtility.d(TAG, manager.getConfigEnvironment() + endPoint);
                return chain.proceed(builder.build());
            }
        });*/
        clientBuilder.readTimeout(60, TimeUnit.SECONDS);
        clientBuilder.connectTimeout(60, TimeUnit.SECONDS);

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            clientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);

            clientBuilder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        return clientBuilder.build();
    }

    /**
     * method used to get the end point of the URL from the request
     */
    private String getEndPoint(String requestURL) {
        URI uri;
        try {
            uri = new URI(requestURL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return requestURL;
        }
        String apiEndPoint = uri.getPath();
        if (!TextUtils.isEmpty(uri.getRawQuery())
                && !TextUtils.isEmpty(apiEndPoint)) {
            apiEndPoint = apiEndPoint.concat("?");
            apiEndPoint = apiEndPoint.concat(uri.getRawQuery());
        }
        return apiEndPoint;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    Retrofit ProvideRetrofit(Gson gson, final OkHttpClient okHttpClient) {

        //PreferenceManager manager = PreferenceManager.getInstance(mContext);
        return new Retrofit.Builder()
                .baseUrl(Config.BASE_URL) //manager.getConfigEnvironment()
                .client(okHttpClient).callFactory(new Call.Factory() {
                    @Override
                    public Call newCall(Request request) {
                        request = request.newBuilder().tag(new Object[]{null}).build();


                        Call call = okHttpClient.newCall(request);

                        // We set the element to the call, to (at least) keep some consistency
                        // If you want to only have Strings, create a String array and put the default value to null;
                        ((Object[]) request.tag())[0] = call;

                        return call;
                    }
                })
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


    @Provides
    @Singleton
    public APIService providesNetworkApiInterface(
            Retrofit retrofit) {
        return retrofit.create(APIService.class);
    }


}
