package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class MessageResponse {

    @SerializedName("id")
    private String messageId;
    @SerializedName("message")
    private String messageContent;
    @SerializedName("from")
    private String fromPhoneNumber;
    @SerializedName("ts")
    private long timestamp;
    @SerializedName("type")
    private String messageType;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getFromPhoneNumber() {
        return fromPhoneNumber;
    }

    public void setFromPhoneNumber(String fromPhoneNumber) {
        this.fromPhoneNumber = fromPhoneNumber;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

}
