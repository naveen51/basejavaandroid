package io.gupshup.smsapp.networking;

import java.util.List;

import io.gupshup.smsapp.data.model.StatusResponse;
import io.gupshup.smsapp.networking.responses.AppConfigurationAPIResponse;
import io.gupshup.smsapp.networking.responses.AutoVerificationCheckResponse;
import io.gupshup.smsapp.networking.responses.AutoVerificationResponse;
import io.gupshup.smsapp.networking.responses.ContactPhoneResponse;
import io.gupshup.smsapp.networking.responses.DeregisterPhoneNumberAPIResponse;
import io.gupshup.smsapp.networking.responses.MessageResponse;
import io.gupshup.smsapp.networking.responses.PhoneNumberDetailsResponse;
import io.gupshup.smsapp.networking.responses.SendMessageResponse;
import io.gupshup.smsapp.networking.responses.SendMessageStatusResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("/inbox/v1/phone/{country_code}/{phone_number}/register")
    Call<StatusResponse> registerPhoneNumber(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Field("seqno") int sequenceNumber,
            @Field("publicKey") String ipPublicKey,
            @Field("key") String apiKey,
            @Field("imei") String iMEI,
            @Field("appId") String appId,
            @Field("accountId") String accountId
    );

    @FormUrlEncoded
    @POST("/inbox/v1/phone/{country_code}/{phone_number}/verify")
    Call<StatusResponse> verifyPhoneNumber(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Field("otp") String otp,
            @Header("key") String apiKey,
            @Field("sequenceNumber") String sequenceNumber
    );

    @FormUrlEncoded
    @PUT("/inbox/phone/{country_code}/{phone_number}/pn-token")
    Call<StatusResponse> addPNToken(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Field("token") String pnToken,
            @Header("key") String apiKey
    );

    @GET("/inbox/phone/{country_code}/{phone_number}/status/{to_phone_number}")
    Call<PhoneNumberDetailsResponse> fetchPhoneNumberDetails(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Path("to_phone_number") String toPhoneNumber,
            @Header("key") String apiKey
    );

    @GET("/inbox/phone/{country_code}/{phone_number}/messages")
    Call<List<MessageResponse>> fetchMessages(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Header("key") String apiKey
    );

    @FormUrlEncoded
    @POST("/inbox/phone/{country_code}/{phone_number}/messages")
    Call<SendMessageResponse> sendMessage(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Field("dest") String toPhoneNumber,
            @Field("message") String messageJSON,
            @Header("key") String apiKey
    );

    @FormUrlEncoded
    @POST("/inbox/phone/{country_code}/{phone_number}/messages/{message_id}")
    Call<SendMessageStatusResponse> sendMessageStatus(
            @Path("country_code") String countryCode,
            @Path("phone_number") String phoneNumber,
            @Path("message_id") String messageId,
            @Field("dest") String toPhoneNumber,
            @Field("status") String status,
            @Field("ts") long timestamp,
            @Header("key") String apiKey
    );

    @GET("/inbox/register/verify")
    Call<AutoVerificationResponse> getAutoverificationStatus(
            @Query(value = "publicKey", encoded = true) final String publicKey
    );

    @GET("/inbox/register/contactPhone")
    Call<ContactPhoneResponse> getContactPhoneNumberFromISOCountryCode(
            @Query("isoCountryCode") final String isoCountryCode
    );

    @GET("/inbox/auto/required/")
    Call<AutoVerificationCheckResponse> getPhoneVerificationRequirementStatus(@Query("publicKey1") final String publicKey1, @Query("publicKey2") final String publicKey2);

    @FormUrlEncoded
    @POST("/inbox/account/{accountId}/device")
    Call<AppConfigurationAPIResponse> getAppIdApi(
            @Path("accountId") final String accountId,
            @Field("modelName") String modelName,
            @Field("modelNumber") String modelNumber,
            @Field("tags") String tag1,
            @Field("tags") String tag2
    );

    @POST("/inbox/v1/phone/{country_code}/{phone_number}/deregister")
    Call<DeregisterPhoneNumberAPIResponse> deregister(
            @Path("country_code") final String countryCode,
            @Path("phone_number") final String phoneNumber,
            @Header("key") String apiKey
    );
}
