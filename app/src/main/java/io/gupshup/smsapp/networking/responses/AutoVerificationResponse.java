package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class AutoVerificationResponse {
    @SerializedName("countryCode")
    private String countryCode;
    @SerializedName("nationalNumber")
    private String nationalNumber;
    @SerializedName("status")
    private String status;
    @SerializedName("lastVerificationDate")
    private long lastVerificationTime;

    public String getCountryCode() {
        return countryCode;
    }

    public String getNationalNumber() {
        return nationalNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setNationalNumber(String nationalNumber) {
        this.nationalNumber = nationalNumber;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getLastVerificationTime() {
        return lastVerificationTime;
    }

    public void setLastVerificationTime(long lastVerificationTime) {
        this.lastVerificationTime = lastVerificationTime;
    }
}
