package io.gupshup.smsapp.networking.responses;

public class DeregisterPhoneNumberAPIResponse {

    private String status;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status =status;
    }

}
