package io.gupshup.smsapp.networking.responses;

import com.google.gson.annotations.SerializedName;

public class PhoneNumberDetailsResponse {

    @SerializedName("phoneno")
    private String phoneNumber;
    @SerializedName("publicKey")
    private String publicKey;
    @SerializedName("status")
    private String status;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
