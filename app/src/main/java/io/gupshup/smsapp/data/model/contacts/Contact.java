package io.gupshup.smsapp.data.model.contacts;

import io.gupshup.smsapp.data.model.BaseItem ;

/**
 * Created by Naveen BM on 4/10/2018.
 */
public class Contact extends BaseItem {
    private String mContactName;
    private String mContactPhoneNumber;
    private String mContactThumbNail;
    private boolean isSelected;
    private int clickedFrom;
    private boolean isFromPlus;
    private String mContactEmailAddress;
    private int mItemPosition;
    private String contactID;

    public int getmItemPosition() {
        return mItemPosition;
    }

    public void setmItemPosition(int mItemPosition) {
        this.mItemPosition = mItemPosition;
    }

    public String getmContactEmailAddress() {
        return mContactEmailAddress;
    }

    public void setmContactEmailAddress(String mContactEmailAddress) {
        this.mContactEmailAddress = mContactEmailAddress;
    }

    public boolean isFromPlus() {
        return isFromPlus;
    }

    public void setFromPlus(boolean fromPlus) {
        isFromPlus = fromPlus;
    }

    public int getClickedFrom() {
        return clickedFrom;
    }

    public void setClickedFrom(int clickedFrom) {
        this.clickedFrom = clickedFrom;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getContactThumbNail() {
        return mContactThumbNail;
    }

    public void setmContactThumbNail(String mContactThumbNail) {
        this.mContactThumbNail = mContactThumbNail;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setmContactName(String mContactName) {
        this.mContactName = mContactName;
    }

    public String getContactPhoneNumber() {
        return mContactPhoneNumber;
    }

    public void setmContactPhoneNumber(String mContactPhoneNumber) {
        this.mContactPhoneNumber = mContactPhoneNumber;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof Contact)
        {
            Contact temp = (Contact) obj;
            if(this.mContactPhoneNumber != null && temp.mContactPhoneNumber != null) {
                if (this.mContactPhoneNumber.equalsIgnoreCase(temp.mContactPhoneNumber))
                    return true;
            }
        }
        return false;

    }
    @Override
    public int hashCode() {

        return (this.mContactPhoneNumber.hashCode());
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }
}
