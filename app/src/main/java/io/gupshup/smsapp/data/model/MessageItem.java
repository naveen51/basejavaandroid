package io.gupshup.smsapp.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */

public class MessageItem extends BaseItem {

    @SerializedName("profilePic")
    public String profilePic;

    @SerializedName("mobileNumber")
    public String mobileNumber;

    @SerializedName("textMessage")
    public String textMessage;

    @SerializedName("messageCount")
    public int messageCount;

    @SerializedName("timeOfReceived")
    public String timeOfReceived;

    @SerializedName("isRead")
    public boolean isRead;

    @SerializedName("isDataMessage")
    public boolean isDataMessage;

    public String meessageType;

    public String getMeessageType() {
        return meessageType;
    }

    public void setMeessageType(String meessageType) {
        this.meessageType = meessageType;
    }
}
