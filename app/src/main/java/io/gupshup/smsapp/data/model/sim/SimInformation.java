package io.gupshup.smsapp.data.model.sim;

import android.graphics.Bitmap;

/**
 * Created by Naveen BM on 5/7/2018.
 */
public class SimInformation {
    private String mSimName;
    private int mSubscriptionID;
    private String displayName;
    private Bitmap iconBitmap;
    private int iconTint;
    private int simSlotIndex;
    private String mPhoneNumber;
    private String imeiNumber;
    private String simMeidNumber;


    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getSimMeidNumber() {
        return simMeidNumber;
    }

    public void setSimMeidNumber(String simMeidNumber) {
        this.simMeidNumber = simMeidNumber;
    }
    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public void setmPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Bitmap getIconBitmap() {
        return iconBitmap;
    }

    public void setIconBitmap(Bitmap iconBitmap) {
        this.iconBitmap = iconBitmap;
    }

    public int getIconTint() {
        return iconTint;
    }

    public void setIconTint(int iconTint) {
        this.iconTint = iconTint;
    }


    public String getmSimName() {
        return mSimName;
    }

    public void setmSimName(String mSimName) {
        this.mSimName = mSimName;
    }

    public int getmSubscriptionID() {
        return mSubscriptionID;
    }

    public void setmSubscriptionID(int mSubscriptionID) {
        this.mSubscriptionID = mSubscriptionID;
    }

    public int getSimSlotIndex() {
        return simSlotIndex;
    }

    public void setSimSlotIndex(int simSlotIndex) {
        this.simSlotIndex = simSlotIndex;
    }
}
