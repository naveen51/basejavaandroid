package io.gupshup.smsapp.data.model;

import io.gupshup.smsapp.ui.base.view.ViewType;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */

public class BaseItem {

    public ViewType viewType = ViewType.ITEM;

    public BaseItem(ViewType type) {
        viewType = type;
    }

    public BaseItem() {
    }

    @Override
    public boolean equals(Object o) {
        return ((BaseItem) o).viewType == viewType;
    }
}