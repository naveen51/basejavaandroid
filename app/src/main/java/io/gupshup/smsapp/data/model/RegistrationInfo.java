package io.gupshup.smsapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.gupshup.smsapp.enums.AutoRegistrationRetryPolicy;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;

/**
 * Created by Ram Prakash Bhat on 16/4/18.
 */

public class RegistrationInfo implements Parcelable {

    public String apiKey;

    public String privateKey;

    public String phoneNumber;

    public String countryCode;

    public String fullNumberWithPlus;

    public String status;

    public int simSlot;

    public RegistrationSimOneState getSimOneState() {
        return simOneState;
    }

    public void setSimOneState(RegistrationSimOneState simOneState) {
        this.simOneState = simOneState;
    }

    public RegistrationSimTwoState getSimTwoState() {
        return simTwoState;
    }

    public void setSimTwoState(RegistrationSimTwoState simTwoState) {
        this.simTwoState = simTwoState;
    }

    public int count;

    public String sequenceNumber;

    public int isManualRegistration;

    public RegistrationSimOneState simOneState;

    public RegistrationSimTwoState simTwoState;

    public AutoRegistrationRetryPolicy retryPolicySim1;

    public AutoRegistrationRetryPolicy retryPolicySim2;

    public boolean isSkipClicked;

    public String imeNumber;

    public RegistrationInfo() {
    }

    protected RegistrationInfo(Parcel in) {
        apiKey = in.readString();
        privateKey = in.readString();
        phoneNumber = in.readString();
        countryCode = in.readString();
        fullNumberWithPlus = in.readString();
        status = in.readString();
        simSlot = in.readInt();
        count = in.readInt();
        sequenceNumber = in.readString();
        isManualRegistration = in.readInt();
        simOneState = RegistrationSimOneState.values()[in.readInt()];
        simTwoState = RegistrationSimTwoState.values()[in.readInt()];
        isSkipClicked = in.readByte() != 0;
        retryPolicySim1 = AutoRegistrationRetryPolicy.values()[in.readInt()];
        retryPolicySim2 = AutoRegistrationRetryPolicy.values()[in.readInt()];
        imeNumber = in.readString();
        //retryPolicySimTwo = AutoRegistrationRetryPolicy.values()[in.readInt()];
    }

    public static final Creator<RegistrationInfo> CREATOR = new Creator<RegistrationInfo>() {
        @Override
        public RegistrationInfo createFromParcel(Parcel in) {
            return new RegistrationInfo(in);
        }

        @Override
        public RegistrationInfo[] newArray(int size) {
            return new RegistrationInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(apiKey);
        parcel.writeString(privateKey);
        parcel.writeString(phoneNumber);
        parcel.writeString(countryCode);
        parcel.writeString(fullNumberWithPlus);
        parcel.writeString(status);
        parcel.writeInt(simSlot);
        parcel.writeInt(count);
        parcel.writeString(sequenceNumber);
        parcel.writeInt(isManualRegistration);
        parcel.writeInt(simOneState.ordinal());
        parcel.writeInt(simTwoState.ordinal());
        parcel.writeByte((byte) (isSkipClicked ? 1 : 0));
        parcel.writeInt(retryPolicySim1.ordinal());
        parcel.writeString(imeNumber);
        parcel.writeInt(retryPolicySim2.ordinal());
    }
}
