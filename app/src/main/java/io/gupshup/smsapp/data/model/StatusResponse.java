package io.gupshup.smsapp.data.model;

import com.google.gson.annotations.SerializedName;

public class StatusResponse extends BaseModel{

    @SerializedName("status")
    private String mStatus;

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }
}
