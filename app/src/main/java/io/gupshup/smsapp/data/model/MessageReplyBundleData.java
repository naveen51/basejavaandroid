package io.gupshup.smsapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.gupshup.smsapp.enums.ConversationType;

/**
 * Created by Naveen BM on 7/19/2018.
 */
public class MessageReplyBundleData implements Parcelable {
    public String searchQuery;
    public boolean isFromBlock;
    public String forwardingMessage;
    public int mPagerPosition;
    public ConversationType conversationType;

    public MessageReplyBundleData(String searchQuery, boolean isFromBlock, String forwardingMessage,
                                  int pagerPosition, ConversationType conversationType) {
        this.searchQuery = searchQuery;
        this.isFromBlock = isFromBlock;
        this.forwardingMessage = forwardingMessage;
        this.mPagerPosition = pagerPosition;
        this.conversationType = conversationType;
    }

    public MessageReplyBundleData(Parcel in) {
        searchQuery = in.readString();
        isFromBlock = in.readByte() != 0;
        forwardingMessage = in.readString();
        mPagerPosition = in.readInt();
        conversationType = (ConversationType) in.readSerializable();
    }


    public static final Creator<MessageReplyBundleData> CREATOR = new Creator<MessageReplyBundleData>() {
        @Override
        public MessageReplyBundleData createFromParcel(Parcel in) {
            return new MessageReplyBundleData(in);
        }

        @Override
        public MessageReplyBundleData[] newArray(int size) {
            return new MessageReplyBundleData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(searchQuery);
        parcel.writeByte((byte) (isFromBlock ? 1 : 0));
        parcel.writeString(forwardingMessage);
        parcel.writeInt(mPagerPosition);
        parcel.writeSerializable(conversationType);
    }
}
