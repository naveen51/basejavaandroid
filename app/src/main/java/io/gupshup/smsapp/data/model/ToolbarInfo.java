package io.gupshup.smsapp.data.model;

/**
 * Created by Ram Prakash Bhat on 10/4/18.
 */

public class ToolbarInfo {

    public String toolbarTitle;
    public boolean isVissible;
    public int toolBarColor;
}
