package io.gupshup.smsapp.data.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Ram Prakash Bhat on 10/4/18.
 */

public class TabItem {

    public String tabTitle;

    public Drawable tabIcon;

    public int tabMsgCount;
}
