package io.gupshup.smsapp.utils;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import io.gupshup.smsapp.message.Utils;


public class ContactChangeObserver extends ContentObserver {

    private Context context;

    public ContactChangeObserver(Handler handler, Context ctx) {
        super(handler);
        context = ctx;
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {

        LogUtility.d("Change", "contact onchange trigerred");
        Utils.syncContactDatabase(context);
    }

}