package io.gupshup.smsapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;

import io.gupshup.smsapp.R;

/**
 * Created by Naveen BM on 4/11/2018.
 */
public class LetterTileProvider {

    /**
     * The {@link TextPaint} used to draw the letter onto the tile
     */
    private final TextPaint mPaint = new TextPaint();
    /**
     * The bounds that enclose the letter
     */
    private final Rect mBounds = new Rect();
    /**
     * The {@link Canvas} to draw on
     */
    private final Canvas mCanvas = new Canvas();
    /**
     * The first char of the name being displayed
     */
    private final char[] mFirstChar = new char[1];

    /**
     * The background colors of the tile
     */
    private final TypedArray mColors;
    /**
     * The font size used to display the letter
     */
    private final int mTileLetterFontSize;
    /**
     * The default image to display
     */
    private final Bitmap mDefaultBitmap;

    /**
     * Constructor for <code>LetterTileProvider</code>
     *
     * @param context The {@link Context} to use
     */
    public LetterTileProvider(Context context) {
        final Resources res = context.getResources();
        //Typeface typeface = ResourcesCompat.getFont(context,R.font.roboto_regular);
        //mPaint.setTypeface(typeface); //Typeface.create("sans-serif-light", Typeface.NORMAL)
        Typeface typeface = null;
        try {
            //typeface = ResourcesCompat.getFont(context, R.font.roboto_regular);
            typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        }catch(Exception ex){
            //typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        }
        if(typeface != null) {
            mPaint.setTypeface(typeface);
        }

        mPaint.setColor(Color.WHITE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setAntiAlias(true);

        mColors = res.obtainTypedArray(R.array.letter_tile_colors);
        mTileLetterFontSize = res.getDimensionPixelSize(R.dimen.tile_letter_font_size);

        mDefaultBitmap = BitmapFactory.decodeResource(res, R.drawable.profile_icon);
    }

    /**
     * @param displayName The name used to create the letter for the tile
     * @param key         The key used to generate the background color for the tile
     * @param width       The desired width of the tile
     * @param height      The desired height of the tile
     * @return A {@link Bitmap} that contains a letter used in the English
     * alphabet or digit, if there is no letter or digit available, a
     * default image is shown instead
     */
    public Bitmap getLetterTile(String displayName, String key, int width, int height) {
        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final char firstChar = displayName.charAt(0);

        final Canvas c = mCanvas;
        c.setBitmap(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(AppUtils.pickColor(key, mColors));
        c.drawCircle(width / 2, height / 2, width / 2, paint);

        if (isEnglishLetterOrDigit(firstChar)) {
            mFirstChar[0] = Character.toUpperCase(firstChar);
            mPaint.setTextSize(mTileLetterFontSize);
            mPaint.getTextBounds(mFirstChar, 0, 1, mBounds);
            c.drawText(mFirstChar, 0, 1, width / 2, height / 2
                + (mBounds.bottom - mBounds.top) / 2, mPaint);
        } else {
            int startX= (c.getWidth()-mDefaultBitmap.getWidth())/2;//for horisontal position
            int startY=(c.getHeight()-mDefaultBitmap.getHeight())/2;//for vertical position
            c.drawBitmap(mDefaultBitmap, startX, startY, null);
        }
        return bitmap;
    }

    /**
     * @param c The char to check
     * @return True if <code>c</code> is in the English alphabet or is a digit,
     * false otherwise
     */
    private static boolean isEnglishLetterOrDigit(char c) {
        return 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z';
    }

}
