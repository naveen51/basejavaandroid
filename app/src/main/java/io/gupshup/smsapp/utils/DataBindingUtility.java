package io.gupshup.smsapp.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.crypto.common.message.MessageType;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.ui.base.common.GlideApp;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;
import static io.gupshup.smsapp.utils.AppConstants.DD_MM_YYYY_HH_MM;
import static io.gupshup.smsapp.utils.AppConstants.HH_MM_A;


/**
 * Created by Ram Prakash Bhat on 10/4/18.
 */

public class DataBindingUtility {

    @BindingAdapter({"title", "count"})
    public static void setText(TextView view, String title, int count) {
        String finalText;
        if (count > 0) {
            finalText = view.getContext().getString(R.string.message_count, title, count);
        } else {
            finalText = title;
        }
        view.setText(finalText);
    }

    @BindingAdapter("loadContactPic")
    public static void loadProfilePic(AppCompatImageView imageView, Bitmap bitmap) {

        GlideApp.with(imageView.getContext())
            .load(bitmap)
            .circleCrop()
            .placeholder(R.drawable.icn_personal)
            .into(imageView);
    }

    @BindingAdapter("textInt")
    public static void setText(AppCompatTextView view, int count) {
        view.setText(String.valueOf(count));
    }


    @BindingAdapter({"contactName"})
    public static void loadBackgroundTile(AppCompatImageView imageView, String contactname) {
        if (contactname != null && !TextUtils.isEmpty(contactname)) {
            LetterTileProvider tileProvider = new LetterTileProvider(imageView.getContext());
            int tileSize = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.letter_tile_size);
            final Bitmap letterTile = tileProvider.getLetterTile(contactname, contactname, tileSize, tileSize);
            imageView.setImageBitmap(letterTile);
        }
    }


    @BindingAdapter({"prevHeader"})
    public static void setTopMargin(View view, boolean isHeader) {
        if (!isHeader) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.setMargins(layoutParams.leftMargin, (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                15,
                view.getContext().getResources().getDisplayMetrics()
                ),
                layoutParams.rightMargin, layoutParams.bottomMargin);
            view.setLayoutParams(layoutParams);
        } else {
            //while recycling we need to set To margin zero else it will take prev item margin
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.setMargins(layoutParams.leftMargin, 0,
                layoutParams.rightMargin, layoutParams.bottomMargin);
            view.setLayoutParams(layoutParams);
        }
    }


    @BindingAdapter("message")
    public static void setMessageText(AppCompatTextView messageView, GupshupMessage content) {

        if (content != null && MessageType.TEXT == content.getType()) {
            GupshupTextMessage message = (GupshupTextMessage) content; // Cast to GupshupTextMessage object
            messageView.setText(message.getText());
        }
    }


    @BindingAdapter({"message", "searchQuery", "darkHeighLight"})
    public static void setReplyMessageText(AppCompatTextView messageView, GupshupMessage content, String searchQuery, boolean darkHighLight) {

        if (content != null && MessageType.TEXT == content.getType()) {
            GupshupTextMessage message = (GupshupTextMessage) content;
            String contentVal = message.getText();
            if (TextUtils.isEmpty(searchQuery)) {
                // Cast to GupshupTextMessage object
                messageView.setText(contentVal);
            } else {
                //serach the content for the query in it and make it spannable
                int pos = contentVal.toLowerCase().indexOf(searchQuery.toLowerCase());
                if (pos > -1) {
                    Spannable spannableStr = AppUtils.getSpannableMessageBody(searchQuery, contentVal, pos, darkHighLight);
                    messageView.setText(spannableStr);
                } else {
                    messageView.setText(contentVal);
                }
            }

        }
    }


    @BindingAdapter("time")
    public static void setMessageReceiveTime(AppCompatTextView messageView, long sentTimeStamp) {
        String time = HH_MM_A.format(new Date(sentTimeStamp).getTime());
        messageView.setText(time);
    }

    @BindingAdapter("messageChannel")
    public static void setDetailMessageChannel(AppCompatTextView messageView, MessageEntity messageEntity) {
        String type;
        type = getViaText(messageView.getContext(), messageEntity);
        messageView.setText(messageView.getContext().getResources().getString(R.string.channel_txt, type));
    }


    @BindingAdapter("contactThumb")
    public static void loadContactThumb(AppCompatImageView imageView, String thumbPath) {
        Bitmap bitmap = BitmapFactory.decodeResource(imageView.getContext().getResources(),
            R.drawable.icn_personal);
        if (!TextUtils.isEmpty(thumbPath)) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(imageView.getContext().getContentResolver(), Uri.parse(thumbPath));
            } catch (IOException e) {
                // Do nothing
                AppUtils.logToFile(MainApplication.getContext(), "Exception while loadContactThumb (DataBindingUtility).." + e.getMessage());
                e.printStackTrace();
            }
        }
        GlideApp.with(imageView.getContext())
            .load(bitmap)
            .circleCrop()
            .placeholder(R.drawable.icn_personal)
            .into(imageView);
    }

    /**
     * Method used to load the contact page Thumbnail, if thumb nail is not available then show first letter of contact name
     *
     * @param imageView
     * @param thumbPath
     */
    @BindingAdapter({"contactThumbPage", "tileContactName"})
    public static void loadContactPageThumbNail(AppCompatImageView imageView, String thumbPath, String contactName) {
        Bitmap bitmap = BitmapFactory.decodeResource(imageView.getContext().getResources(),
            R.drawable.icn_personal);
        if (thumbPath != null && !TextUtils.isEmpty(thumbPath)) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(imageView.getContext().getContentResolver(), Uri.parse(thumbPath));
                GlideApp.with(imageView.getContext())
                    .load(bitmap)
                    .circleCrop()
                    .into(imageView);
            } catch (IOException e) {
                // Do nothing
                e.printStackTrace();
            }
        } else {
            if (contactName != null && !TextUtils.isEmpty(contactName)) {
                LetterTileProvider tileProvider = new LetterTileProvider(imageView.getContext());
                int tileSize = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.letter_tile_size);
                final Bitmap letterTile = tileProvider.getLetterTile(contactName, contactName, tileSize, tileSize);
                imageView.setImageBitmap(letterTile);
            }
        }

    }

    @BindingAdapter({"messageFrom", "direction"})
    public static void setMessageType(AppCompatTextView messageFrom, String mask, MessageDirection direction) {
        if (mask != null) {
            if (direction == MessageDirection.INCOMING) {
                messageFrom.setText(messageFrom.getContext().getString(R.string.from, mask));
            } else {
                messageFrom.setText(messageFrom.getContext().getString(R.string.to, mask));
            }
        }
    }

    @BindingAdapter("messageSentTimeStamp")
    public static void setMessageTimeStampSent(AppCompatTextView messageSent, MessageEntity messageEntity) {
        if (messageEntity.getDirection() == MessageDirection.OUTGOING) {
            messageSent.setVisibility(View.VISIBLE);
            if (messageEntity.getMessageStatus() == MessageStatus.FAILED
                || messageEntity.getMessageStatus() == MessageStatus.SENDING) {
                messageSent.setText(messageSent.getContext().getString(R.string.sent,
                    messageSent.getContext().getString(R.string.na_text)));
            } else {
                long timeStamp;
                if (messageEntity.getSentTimestamp() != 0) {
                    timeStamp = messageEntity.getSentTimestamp();
                } else {
                    timeStamp = messageEntity.getCreationTime();
                }
                messageSent.setText(messageSent.getContext().getString(R.string.sent,
                    DD_MM_YYYY_HH_MM.format(new Date(timeStamp).getTime())));
            }
        } else {
            messageSent.setVisibility(View.GONE);
            messageSent.setText(messageSent.getContext().getString(R.string.sent,
                DD_MM_YYYY_HH_MM.format(new Date(messageEntity.getMessageTimestamp()).getTime())));
        }
    }

    @BindingAdapter("messageCreatedTimeStamp")
    public static void setMessageCreationTimeStamp(AppCompatTextView messageCreated, MessageEntity messageEntity) {
        if (messageEntity.getDirection() == MessageDirection.OUTGOING) {
            messageCreated.setVisibility(View.VISIBLE);
            long creationTime = -1;
            if (messageEntity.getCreationTime() > 0) {
                creationTime = messageEntity.getCreationTime();
            } else if (messageEntity.getMessageTimestamp() > 0) {
                creationTime = messageEntity.getMessageTimestamp();
            }
            if (creationTime > 0) {
                messageCreated.setText(messageCreated.getContext().getString(R.string.created,
                    DD_MM_YYYY_HH_MM.format(new Date(creationTime).getTime())));
            } else {
                messageCreated.setText(messageCreated.getContext().getString(R.string.created,
                    messageCreated.getContext().getString(R.string.na_text)));
            }
            //}
        } else {
            messageCreated.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("messageReceivedTimeStamp")
    public static void setMessageTimeStampReceived(AppCompatTextView messageTime, MessageEntity messageEntity) {
        if (messageEntity.getDirection() == MessageDirection.OUTGOING
            && messageEntity.getMessageStatus() != MessageStatus.DELIVERED) {
            setNotApplicableText(messageTime);
        } else {
            if (messageEntity.getDirection() == MessageDirection.OUTGOING
                && messageEntity.getMessageStatus() == MessageStatus.DELIVERED) {
                if (messageEntity.getDeliveredTimestamp() > 0) {
                    messageTime.setText(messageTime.getContext().getString(R.string.delivered,
                        DD_MM_YYYY_HH_MM.format(new Date(messageEntity.getDeliveredTimestamp()).getTime())));
                } else {
                    setNotApplicableText(messageTime);
                }
            } else if (messageEntity.getDirection() == MessageDirection.INCOMING) {
                if (messageEntity.getMessageTimestamp() > 0) {
                    messageTime.setText(messageTime.getContext().getString(R.string.received,
                        DD_MM_YYYY_HH_MM.format(new Date(messageEntity.getMessageTimestamp()).getTime())));
                } else {
                    setNotApplicableText(messageTime);
                }
            } else {
                messageTime.setVisibility(View.GONE);
            }
        }
    }

    private static void setNotApplicableText(AppCompatTextView messageTime) {

        messageTime.setText(messageTime.getContext().getString(R.string.delivered,
            messageTime.getContext().getString(R.string.na_text)));
    }


    @BindingAdapter({"sentTime", "messageTime", "carrier", "status", "direction", "messageEntity"})
    public static void setMessageWithStatus(AppCompatTextView messageView, long sentTimeStamp,
                                            long messageTimeStamp, String carierName,
                                            MessageStatus status, MessageDirection msgDirection,
                                            MessageEntity messageEntity) {
        String time, on = null, completeText, via = null;

        //via = getViaText(messageView.getContext(), messageEntity);

        if (status != null && status == MessageStatus.SENDING) {
            completeText = messageView.getContext().getString(R.string.sending_text);
            messageView.setText(completeText);
            messageView.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(messageView.getContext(), R.drawable.timer), null);
        } else {
            //Below code is for SENT AND DELIVER STATUS
            time = HH_MM_A.format(new Date(messageTimeStamp).getTime());
            completeText = time; //String.format("%s %s %s", time, " ", via);
            if (msgDirection == MessageDirection.OUTGOING) {
                if (status != null) {
                    if (status == MessageStatus.SENT) {
                        messageView.setText(completeText);
                        messageView.setCompoundDrawablesWithIntrinsicBounds(null, null,
                            ContextCompat.getDrawable(messageView.getContext(), R.drawable.sent_tick_mark), null);
                    } else if (status == MessageStatus.DELIVERED) {
                        messageView.setText(completeText);
                        messageView.setCompoundDrawablesWithIntrinsicBounds(null, null,
                            ContextCompat.getDrawable(messageView.getContext(), R.drawable.delivered_tick_mark), null);
                    } else if (status == MessageStatus.FAILED) {
                        messageView.setText(completeText);
                        if (MessageChannel.SMS == messageEntity.getChannel()) {
                            messageView.setCompoundDrawablesWithIntrinsicBounds(null, null,
                                ContextCompat.getDrawable(messageView.getContext(), R.drawable.sending_failed_alert), null);
                        } else {
                            messageView.setCompoundDrawablesWithIntrinsicBounds(null, null,
                                ContextCompat.getDrawable(messageView.getContext(), R.drawable.icn_sending_failed_marker), null);
                        }
                    }
                }
            }
            messageView.setText(completeText);
        }

    }

    @BindingAdapter("viaRecipientText")
    public static void setRecipientText(AppCompatTextView messageView, MessageEntity messageEntity) {

        String completeText;
        completeText = String.format("%s %s", " ", getViaText(messageView.getContext(), messageEntity));
        messageView.setText(completeText);
    }


    @BindingAdapter("messageStatusDetail")
    public static void setMessageTimeStampSent(AppCompatTextView statustext, MessageStatus status) {
        if (status != null) {
            if (status == MessageStatus.SENT) {
                statustext.setText(R.string.sent_text);
            } else if (status == MessageStatus.DELIVERED) {
                statustext.setText(R.string.delivered_txt);
            } else if (status == MessageStatus.FAILED) {
                statustext.setText(R.string.failed_txt);
            } else {
                statustext.setText(R.string.sending_txt);
            }
        } else {
            statustext.setText(R.string.status_na_txt);
        }
    }


    @BindingAdapter("draftMessage")
    public static void setDraftMessageVisibility(AppCompatTextView messageView, String draftMsgContent) {
        if (!TextUtils.isEmpty(draftMsgContent)) {
            messageView.setVisibility(View.VISIBLE);
            messageView.setText(messageView.getContext().getResources().getString(R.string.msg_draft_title));
            messageView.setTextColor(ContextCompat.getColor(messageView.getContext(),
                R.color.manatee));
        } else {
            messageView.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("textLong")
    public static void setText(AppCompatTextView view, long count) {


         /*AMA-356 Fixes.
           Unread message counter is not required on message thread.*/
        view.setVisibility(View.GONE);
    }

    @BindingAdapter({"textTimeStamp"})  //, "unreadCount"
    public static void setTimeStampText(AppCompatTextView appCompatTextView, ConversationEntity messageItem) {
        if (TextUtils.isEmpty(messageItem.getDraftMessage())) {
            //If Draft message is not there then show the Time Stamp
            appCompatTextView.setVisibility(View.VISIBLE);
            UiUtils.setFontWithColor(appCompatTextView, messageItem);
            long timeStamp = -1;
            if (messageItem.getLastMessageData() == null) {
                if (!TextUtils.isEmpty(messageItem.getDraftMessage())) {
                    timeStamp = messageItem.getDraftCreationTime();
                }
            } else {
                timeStamp = messageItem.getLastMessageTimestamp();
            }

            if (!TextUtils.isEmpty(messageItem.getDraftMessage())) {
                timeStamp = messageItem.getDraftCreationTime();
            }
            appCompatTextView.setText(AppUtils.getFormattedTimeDate(timeStamp, appCompatTextView.getContext(), false));
        } else {
            //Draft message is there so dont show the message timestamp
            appCompatTextView.setVisibility(View.GONE);
        }


    }

    @BindingAdapter("messageContent") //, "unreadCount", "draftMessage"
    public static void setMessageText(AppCompatTextView messageView, ConversationEntity conversationEntity) {

        UiUtils.setFontWithColor(messageView, conversationEntity);
        if (TextUtils.isEmpty(conversationEntity.getDraftMessage())) {
            if (conversationEntity.getLastMessageData() != null) {
                if (MessageType.TEXT == conversationEntity.getLastMessageData().getContent().getType()) {
                    GupshupTextMessage message = (GupshupTextMessage) conversationEntity.getLastMessageData().getContent(); // Cast to GupshupTextMessage object
                    messageView.setText(message.getText());
                }
            } else {
                messageView.setText("");
            }
        } else {
            Typeface typeface = ResourcesCompat.getFont(messageView.getContext(),
                R.font.roboto_medium_italic);
            messageView.setTypeface(typeface);
            messageView.setText(conversationEntity.getDraftMessage());
            messageView.setTextColor(ContextCompat.getColor(messageView.getContext(),
                R.color.manatee));
        }

    }

    @BindingAdapter({"textToSet", "unreadCount"})
    public static void setTextWithFont(AppCompatTextView appCompatTextView, String text,
                                       ConversationEntity conversationEntity) {

        UiUtils.setFontWithColor(appCompatTextView, conversationEntity);
        appCompatTextView.setText(text);
    }

    @BindingAdapter({"searchDisplayName", "searchEntity"})
    public static void setTextWithFont(AppCompatTextView appCompatTextView, String display,
                                       GlobalSearchEntity searchItem) {
        Spannable displayName = new SpannableString("");

        if (searchItem != null) {
            if (searchItem.getSearchString() != null) {
                displayName = searchItem.getSearchString();
            } else {
                displayName = new SpannableString(searchItem.getDisplay() == null ? searchItem.getMask() : searchItem.getDisplay());
            }
        }
        appCompatTextView.setText(displayName);
    }


    @BindingAdapter({"searchedMessageTime"})
    public static void setTimeStampText(AppCompatTextView appCompatTextView, GlobalSearchEntity searchItem) {
        // UiUtils.setFontWithColor(appCompatTextView, searchEntity);
        String timeStamp = AppConstants.EMPTY_TEXT;
        if (searchItem != null
            && searchItem.getTimeStamp() > 0) {
            timeStamp = AppUtils.getFormattedTimeDate(searchItem.getTimeStamp(), appCompatTextView.getContext(), false);
        }
        appCompatTextView.setText(timeStamp);
    }

    @BindingAdapter("searchedMessageContent")
    public static void setMessageText(AppCompatTextView messageView, GlobalSearchEntity searchItem) {

        //UiUtils.setFontWithColor(messageView, searchItem);
        Spannable messageBody = new SpannableString("");
        if (searchItem != null) {
            if (searchItem.getMessages() != null
                && searchItem.getMessages().size() > 0) {
                messageBody = searchItem.getMessages().get(0);
            } else if (!TextUtils.isEmpty(searchItem.getLastMessageData())) {
                messageBody = new SpannableString(searchItem.getLastMessageData());
            }
            if (isLargeText(messageView, messageBody.toString())) {
                String message = messageBody.toString();
                int startIndex = message.toLowerCase().indexOf(searchItem.getMatchedKeyword().toLowerCase());
                if (startIndex > -1) {
                    messageView.setText("...");
                    messageView.append(messageBody.subSequence(startIndex, message.length()));
                } else {
                    messageView.setText(messageBody);
                }
            } else {
                messageView.setText(messageBody);
            }
        } else {
            messageView.setText(messageBody);
        }
    }

    private static boolean isLargeText(AppCompatTextView messageView, String messageBody) {

        TextPaint paint = messageView.getPaint();
        float textWidth = paint.measureText(messageBody);

        return (textWidth >= messageView.getMeasuredWidth());
    }

    @BindingAdapter({"customBackgroundTint"})
    public static void setCustomBackgroundTint(View view, int color) {
        ViewCompat.setBackgroundTintList(
            view,
            ColorStateList.valueOf(color));
    }

    private static String getViaText(Context context, MessageEntity messageEntity) {

        String viaText = AppConstants.EMPTY_TEXT;

        MessageChannel chan = messageEntity.getChannel();
        if (chan.ordinal() == 0) {
            if (!TextUtils.isEmpty(messageEntity.getCarierName())) {
                viaText = messageEntity.getCarierName();
            } else {
                viaText = context.getResources().getString(R.string.via);
                if (messageEntity.getIncomingSIMID() == 0) {
                    //Imported through SIM one
                    viaText = String.format("%s %s %s", viaText, "", context.getString(R.string.via_sim_one));
                } else if (messageEntity.getIncomingSIMID() == 1) {
                    //Imported through SIM Two
                    viaText = String.format("%s %s %s", viaText, "", context.getString(R.string.via_sim_two));
                } else {
                    viaText = context.getString(R.string.sms_via);
                }
            }
        } else if (chan.ordinal() == 1) {
            viaText = context.getResources().getString(R.string.via_data);
        }
        return viaText;
    }

    @BindingAdapter("countDown")
    public static void setCountDownTime(AppCompatTextView textView, String countDown) {

        if (TextUtils.isEmpty(countDown)) {
            return;
        }
        Context context = textView.getContext();
        String secondText = context.getString(R.string.timer_sec);
        int textSize1 = context.getResources().getDimensionPixelSize(R.dimen.dimen_24sp);
        int textSize2 = context.getResources().getDimensionPixelSize(R.dimen.dimen_10sp);

        SpannableString span1 = new SpannableString(countDown);
        span1.setSpan(new AbsoluteSizeSpan(textSize1), 0, countDown.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(secondText);
        span2.setSpan(new AbsoluteSizeSpan(textSize2), 0, secondText.length(), SPAN_INCLUSIVE_INCLUSIVE);

        // let's put both spans together with a separator and all
        CharSequence finalText = TextUtils.concat(span1, "\n", span2);
        textView.setText(finalText);
    }


    @BindingAdapter("simNumberLayout")
    public static void showSimNumberLayout(View simNumberLayout, MessageEntity messageEntity) {
        int totalActiveSimInDevice = AppUtils.getSimInfoList().size();
        if (totalActiveSimInDevice > 1) {
            simNumberLayout.setVisibility(View.VISIBLE);
        } else {
            simNumberLayout.setVisibility(View.INVISIBLE);
        }

    }


}
