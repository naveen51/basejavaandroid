package io.gupshup.smsapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.widget.Toast;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.entities.ConversationEntity;

/**
 * Created by Ram Prakash Bhat on 3/5/18.
 */

public class UiUtils {

    public static void setFontWithColor(AppCompatTextView messageView, ConversationEntity conversationEntity) {

        if (conversationEntity.isUnread()) {
            Typeface typeface = null;
            try {
                //typeface = ResourcesCompat.getFont(messageView.getContext(), R.font.roboto_bold);
                typeface = Typeface.createFromAsset(messageView.getContext().getAssets(), "robotobold.ttf");
            }catch(Exception ex){
                //typeface = Typeface.createFromAsset(messageView.getContext().getAssets(), "robotobold.ttf");
            }

            if(typeface != null) {
                messageView.setTypeface(typeface);
            }
            messageView.setTextColor(ContextCompat.getColor(messageView.getContext(),
                R.color.color_333333));
        } else {
            Typeface typefaceRegular = null;
            try {
                /*typefaceRegular = ResourcesCompat.getFont(messageView.getContext(),
                        R.font.roboto_regular);*/
                typefaceRegular = Typeface.createFromAsset(messageView.getContext().getAssets(), "robotoregular.ttf");
            }catch(Exception ex){
                //typefaceRegular = Typeface.createFromAsset(messageView.getContext().getAssets(), "robotoregular.ttf");
            }

            if(typefaceRegular != null) {
                messageView.setTypeface(typefaceRegular);
            }
            messageView.setTextColor(ContextCompat.getColor(messageView.getContext(),
                R.color.davy_grey));
        }
    }

    public static void showToast(Context context, String toastMessage) {
        if (context != null) {
            Toast toast = Toast.makeText(context, toastMessage, Toast.LENGTH_LONG);
            //toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }
    }

    public static void showCenterToastShort(Context context, String toastMessage) {
        if (context != null) {
            Toast toast = Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
