package io.gupshup.smsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.gupshup.smsapp.R;

import static io.gupshup.smsapp.utils.AppConstants.PermissionRequestCode.SMS_PERMISSION_REQUEST_CODE;

/**
 * Created by Ram Prakash Bhat on 20/4/18.
 */

public class PermissionUtils {

    private Activity currentActivity;
    private PermissionResultCallback permissionResultCallback;
    private ArrayList<String> permissionList = new ArrayList<>();
    private ArrayList<String> listPermissionsNeeded = new ArrayList<>();
    private String dialogContent = "";
    private int reqCode;

    public PermissionUtils(Context context) {
        this.currentActivity = (Activity) context;
        permissionResultCallback = (PermissionResultCallback) context;
    }

    /**
     * Check the API Level & Permission
     *
     * @param permissions
     * @param dialogContent
     * @param requestCode
     */

    public void checkPermission(ArrayList<String> permissions, String dialogContent, int requestCode) {
        this.permissionList = permissions;
        this.dialogContent = dialogContent;
        this.reqCode = requestCode;

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkAndRequestPermissions(permissions, requestCode)) {
                permissionResultCallback.PermissionGranted(requestCode);
            }
        } else {
            permissionResultCallback.PermissionGranted(requestCode);
        }
    }

    /**
     * Check and request the Permissions
     *
     * @param permissions
     * @param requestCode
     * @return
     */

    private boolean checkAndRequestPermissions(ArrayList<String> permissions, int requestCode) {

        if (permissions.size() > 0) {
            listPermissionsNeeded = new ArrayList<>();
            for (int i = 0; i < permissions.size(); i++) {
                int hasPermission = ContextCompat.checkSelfPermission(currentActivity, permissions.get(i));

                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissions.get(i));
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(currentActivity,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), requestCode);
                return false;
            }
        }
        return true;
    }

    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SMS_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    Map<String, Integer> perms = new HashMap<>();
                    for (int i = 0; i < permissions.length; i++) {
                        perms.put(permissions[i], grantResults[i]);
                    }
                    final ArrayList<String> pendingPermissions = new ArrayList<>();
                    for (int i = 0; i < listPermissionsNeeded.size(); i++) {
                        if (perms.get(listPermissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(currentActivity, listPermissionsNeeded.get(i)))
                                pendingPermissions.add(listPermissionsNeeded.get(i));
                            else {
                                permissionResultCallback.NeverAskAgain(reqCode);
                                return;
                            }
                        }
                    }
                    if (pendingPermissions.size() > 0) {
                        showMessageOKCancel(dialogContent,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                checkPermission(permissionList, dialogContent, reqCode);
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                if (permissionList.size() == pendingPermissions.size())
                                                    permissionResultCallback.PermissionDenied(reqCode);
                                                else
                                                    permissionResultCallback.PartialPermissionGranted(reqCode, pendingPermissions);
                                                break;
                                        }
                                    }
                                });
                    } else {
                        permissionResultCallback.PermissionGranted(reqCode);
                    }
                }
                break;
        }
    }


    /**
     * Explain why the app needs permissions
     *
     * @param message
     * @param okListener
     */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(currentActivity)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(currentActivity.getString(R.string.ok_btn), okListener)
                .setNegativeButton(currentActivity.getString(R.string.cancel_btn), okListener)
                .create()
                .show();
    }

}
