package io.gupshup.smsapp.utils;

import java.util.ArrayList;

/**
 * Created by Ram Prakash Bhat on 20/4/18.
 */

public interface PermissionResultCallback
{
    void PermissionGranted(int requestCode);
    void PartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions);
    void PermissionDenied(int requestCode);
    void NeverAskAgain(int requestCode);
}