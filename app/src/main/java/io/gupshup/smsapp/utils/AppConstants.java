package io.gupshup.smsapp.utils;

import android.Manifest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AppConstants {
    public static final String EMPTY_TEXT = "";
    public static final String EVENT_CONTACT_SELECTED = "event_contact_selected";
    public static final String EVENT_ADDCONTACT = "event_addcontact";
    public static final String DISPLAY_NAME = "display_name";
    public static final DateFormat HH_MM_A = new SimpleDateFormat("hh:mm a", Locale.getDefault());
    public static final DateFormat DD_MMM = new SimpleDateFormat("dd MMM", Locale.getDefault());
    public static final DateFormat MMMM_DD_YYYY_H_MM_AA = new SimpleDateFormat("MMMM dd yyyy, h:mm aa", Locale.getDefault());
    public static final DateFormat EEE = new SimpleDateFormat("EEE", Locale.getDefault());
    public static final DateFormat HH_MM = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final DateFormat DD_MM_YYYY_HH_MM = new SimpleDateFormat("dd/MM/yyyy, HH:mm", Locale.getDefault());
    public static final String OTP_MATCHER = "((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))";


    public static final String INDIA = "IN";
    public static final String EVENT_CONTACT_SEARCH_FILTER = "event_contact_search_filter";
    public static final String DELETE_CONTACT = "delete_contact";
    public static final String RECEPIENT_THUMB_NAIL = "recepient_thumb_nail";
    public static final String EVENT_UPDATE_TITLE = "event_update_title";
    public static final String EVENT_CONTACT_GRID_BEHAVIOUR_EVENT = "event_contact_grid_behaviour_event";
    public static final String FLASH_MESSAGE_BODY = "flash_message_body";
    public static final String EVENT_DEFAULT_APP_PERMISSION_DENIED = "event_default_app_permission_denied";
    public static final String IS_FROM_BLOCK_SCREEN = "is_from";
    public static final int ADD_CONTACT_ACTIVITY_RESULT = 5001;
    public static final String INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED = "finishActivityOnSaveCompleted";
    public static final String KEY_MESSAGE_DRAFT = "key_message_draft";
    public static final String FORWARD_MESSAGE_EVENT = "forward_message_event";
    public static final String KEY_FORWARD_MESSAGE = "key_forward_message";
    public static final String NEW_MESSAGE_CLICK_EVENT = "new_message_click_event";
    public static final String FORWARD_DIALOG_CANCEL_CLICKED = "forward_dialog_cancel_clicked";
    public static final String ACTIVITY_EXIT_EVENT = "activity_exit_event";
    public static final String UPDATE_CONTACT_TOP_BAR = "update_contact_top_bar";
    public static final String EVENT_NOTIFY_ADAPTER = "event_notify_adapter";
    public static final String HANDLE_TOOLBAR_CONTENT_EVENT = "handle_toolbar_content_event";
    public static final String VERIFIED = "verified";
    public static final String MANUAL_REGISTRATION_SUBMIT_CLICK = "manual_registration_submit_click";
    public static final String SKIP_MANULA_REGISTRATION_NEXT_CLICKED = "skip_manula_registration_next_clicked";
    public static final String SKIP_MANUAL_SKIP_CLICKED = "skip_manual_skip_clicked";
    public static final String SKIP_ERROR_LYT_CLICK = "skip_error_lyt_click";
    public static final String SKIP_RETRY_CLICK = "skip_retry_click";
    public static final String SLOT_ZERO = "0";
    public static final String SLOT_ONE = "1";
    public static final DateFormat EEEE_DD_MMMM = new SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault());
    public static final DateFormat EEEE_DD_MMMM_YYYY = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.getDefault());
    public static final DateFormat EEEE = new SimpleDateFormat("EEEE", Locale.getDefault());
    public static final String EVENT_CONVERSATION_THEME_CHANGE = "event_conversation_theme_change";
    public static final String EVENT_NAVIGATION_CLICKED = "event_navigation_clicked";
    public static final String SUCCESS_STATUS = "success";
    public static int TONE_PICKER_REQUEST_CODE = 102;
    public static String CONVERSATION_SUB_TYPE = "ARCHIVE";
    public static final String EVENT_SHARE_CONTACT_CLICK = "event_share_contact_click";


    public interface RxEventName {
        String TOOLBAR_INFO = "toolbar_info";
        String SHOW_REGISTRATION_SCREEN = "show_register_screen";
        String SHOW_HOME_SCREEN = "show_home_screen";
        String SHOW_OTP_SCREEN = "show_otp_screen";
        String OTP_MESSAGE = "otp_message";
        String SHOW_HIDE_VIEW = "show_hide_view";
        String SHOW_SEARCH_VIEW = "show_search_view";
        String SHOW_SKIP_SCREEN = "skip_screen";
        String SHOW_SKIP_SCREEN_FOR_RETRY = "skip_screen_for_retry";
        String SHOW_MR_TWO_SKIP_SCREEN = "mr_two";
        String HIDE_DIALOG = "hide_dialog";
        String AUTO_REGISTRATION_VERIFICATION_STATUS = "auto_registration_verification_status";
        String MANUAL_REGISTRATION_VERIFICATION_STATUS = "manual_registration_verification_status";
        String MANUAL_REGISTRATION_MR_TWO_VERIFICATION_STATUS = "manual_registration_mr_two_verification_status";
        String SIM_ONE_REGISTRATION_FAILURE = "sim_one_registration_failure";
        String SIM_TWO_REGISTRATION_FAILURE = "sim_two_registration_failure";
        String UPDATE_CONFIG_DATA = "update_config";
        String ALREADY_REGISTERED = "already_registered";
        String INSTRUCTION_OVERLAY = "instruction_overlay";
        String ABOUT_FRAGMENT_EVENT = "about_fragment_event";
        String WEB_VIEW_FRAG_EVENT = "web_view_frag_event";
        String NOTIFICATION_FRAGMENT_EVENT = "notification_fragment_event";
        String SHO_HIDE_SIM_LAY_OUT = "sho_hide_sim_lay_out";
        String MESSAGE_REPLY_TOOLBAR_INFO = "message_reply_toolbar_info";
        String SETTING_RE_REGISTRATION = "re_registration";
        String REGISTER_ON_SIM_STATE_CHANGE = "register_on_sim_state_change";
        String ACTION_MODE_MESSAGE_COUNT_UPDATE = "action_mode_message_count_update";
        String UPDATE_SETTINGS_UI_ON_MANUAL_REGISTRATION_SUCCES = "update_settings_ui_on_manual_registration_succes";
        String RE_REGISTRATION_FRAGMENT_EVENT = "re_registration_fragment_event";
        String EXIT_RE_REGISTRATION_SCREEN_EVENT = "exit_re_registration_screen_event";
        String REGISTER_DEREGISTER_EVENT_ON_SIM_STATE_CHANGE = "register_deregister_event_on_sim_state_change";
    }

    public interface BundleKey {
        String PARCEL = "parcel";
        String MOBILE_NUMBER = "mobile_number";
        String CONVERSATION_TYPE = "conversationtype";
        String NOTIFICATION_ID = "notification_id";
        String MESSAGE_ID = "message_id";
        String MARK_ALL_AS_READ = "mark_all_as_read";
        String OTP_NUMBER = "otp_number";
        String LAST_MESSAGE_ID = "last_message_id";
        String CONVERSATION_SUB_TYPE = "conversationSubType";
        String SEARCH_QUERY = "search_query";
        String INTENT_ACTION = "action";
        String WEB_URL = "web_url";
        String NOTIFICATION_CONVERSATION_TYPE = "notification_conversation_type";
        String CONTACT_NAME = "contact_name";
        String PAGER_POSITION = "pager_position";
        String MASK = "mask";
        String FORWARD_MESSAGE = "forward_message";
        String SIM_SLOT_INDEX = "slot_id";
    }

    public interface PermissionRequest {

        String[] SMS_PERMISSION_REQUEST = new String[]{
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_PHONE_STATE,
            //Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        int REQUEST_CALL_PHONE = 1005;
    }

    public interface PermissionRequestCode {
        int SMS_PERMISSION_REQUEST_CODE = 1001;
    }

    public interface ApiResponseStatus {

        String SUCCESS = "success";
        String FAILURE = "failure";
    }

    public interface IntentActions {

        String MARK_AS_READ = "io.gupshup.smsapp.MARK_AS_READ";
        String MARK_AS_BLOCK = "io.gupshup.smsapp.MARK_AS_BLOCK";
        String SMS_SENT = "io.gupshup.smsapp.SENT_ACT";
        String SMS_DELIVER = "io.gupshup.smsapp.SMS_DELIVER";
        String ADD_TO_CONTACT = "io.gupshup.smsapp.ADD_TO_CONTACT";
        String REPLY = "io.gupshup.smsapp.REPLY";
        String COPY = "io.gupshup.smsapp.COPY";
        String DELETE = "io.gupshup.smsapp.DELETE";
    }

    public interface RegistrationType {
        int MANUAL_REGISTRATION = 0;
        int AUTO_REGISTRATION = 1;
    }

    public interface MimeTypeType {

        String MIME_TYPE_TEXT_PLAIN = "text/plain";
        String MIME_TYPE_TEXT_X_VCARD = "text/x-vcard";
        String MIME_TYPE_TEXT_VCARD = "text/vcard";
    }


    public interface SimChangeStatus {
        public static final String READY = "ready";
        public static final String LOCKED = "locked";
        public static final String IMSI = "imsi";
        public static final String LOADED = "loaded";
        public static final String NOT_READY = "not_ready";
        public static final String ABSENT = "absent";
        public static final String DE_REGISTRATION_DONE = "de_registration_done";
    }
}
