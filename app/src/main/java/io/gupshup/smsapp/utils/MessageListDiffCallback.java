package io.gupshup.smsapp.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import io.gupshup.smsapp.database.entities.ConversationEntity;

/**
 * Created by Ram Prakash Bhat on 10/5/18.
 */

public class MessageListDiffCallback extends DiffUtil.Callback {

    private final List<ConversationEntity> mOldConversationList;
    private final List<ConversationEntity> mConversationList;

    public MessageListDiffCallback(List<ConversationEntity> mOldConversationList,
                                   List<ConversationEntity> mConversationList) {
        this.mOldConversationList = mOldConversationList;
        this.mConversationList = mConversationList;
    }

    @Override
    public int getOldListSize() {
        return mOldConversationList.size();
    }

    @Override
    public int getNewListSize() {
        return mConversationList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldConversationList.get(oldItemPosition).getConversationId() == mConversationList.get(newItemPosition).getConversationId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ConversationEntity newEntity = mConversationList.get(newItemPosition);
        ConversationEntity oldEntity = mOldConversationList.get(oldItemPosition);
        return (newEntity.getLastMessageTimestamp() == oldEntity.getLastMessageTimestamp()
            && newEntity.getNoOfUnreadMessages() == oldEntity.getNoOfUnreadMessages()
            && oldEntity.getType() == newEntity.getType());
    }
}
