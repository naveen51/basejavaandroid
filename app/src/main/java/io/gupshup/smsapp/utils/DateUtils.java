package io.gupshup.smsapp.utils;

/*
 * @author  Bhargav Kolla
 * @since   Apr 17, 2018
 */
public final class DateUtils {

    public static long currentTimestamp() {
        return System.currentTimeMillis();
    }

}
