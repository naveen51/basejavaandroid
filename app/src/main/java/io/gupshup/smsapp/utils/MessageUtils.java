package io.gupshup.smsapp.utils;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.util.DiffUtil;
import android.telephony.SmsManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.receiver.SmsReceiver;

import static io.gupshup.smsapp.utils.AppConstants.IntentActions.SMS_DELIVER;
import static io.gupshup.smsapp.utils.AppConstants.IntentActions.SMS_SENT;

/**
 * Created by Ram Prakash Bhat on 9/4/18.
 */

public class MessageUtils {

    public static final DiffUtil.ItemCallback<MessageEntity> MESSAGE_ENTITY_DIFF_CALLBACK = new DiffUtil.ItemCallback<MessageEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull MessageEntity oldMessageEntity, @NonNull MessageEntity newMessageEntity) {
            return oldMessageEntity.getMessageId().equalsIgnoreCase(newMessageEntity.getMessageId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull MessageEntity oldMessageEntity, @NonNull MessageEntity newMessageEntity) {
            return newMessageEntity.getMessageTimestamp() == oldMessageEntity.getMessageTimestamp()
                && newMessageEntity.getMessageStatus() == oldMessageEntity.getMessageStatus();
        }
    };

    public static final DiffUtil.ItemCallback<ConversationEntity> CONVERSATION_ENTITY_DIFF_CALLBACK = new DiffUtil.ItemCallback<ConversationEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull ConversationEntity oldEntity, @NonNull ConversationEntity newEntity) {
            return oldEntity.getConversationId() == newEntity.getConversationId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ConversationEntity oldEntity, @NonNull ConversationEntity newEntity) {
            return newEntity.getConversationId() == oldEntity.getConversationId()
                && (newEntity.getLastMessageTimestamp() == oldEntity.getLastMessageTimestamp()
                && (newEntity.isUnread() == oldEntity.isUnread())
                && (isContactUpdated(newEntity, oldEntity)));
            //&& newEntity.getDraftCreationTime() == oldEntity.getDraftCreationTime()
        }
    };

    private static boolean isContactUpdated(ConversationEntity newEntity, ConversationEntity oldEntity) {

        if (oldEntity.getDisplay() == null && newEntity.getDisplay() != null) {
            return false;
        }
        if (oldEntity.getImage() == null && newEntity.getImage() != null) {
            return false;
        }

        if (!TextUtils.isEmpty(newEntity.getDisplay()) && !TextUtils.isEmpty(oldEntity.getDisplay())) {
            if (!newEntity.getDisplay().equalsIgnoreCase(oldEntity.getDisplay())) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(newEntity.getImage()) && !TextUtils.isEmpty(oldEntity.getImage())) {
            if (!newEntity.getImage().equalsIgnoreCase(oldEntity.getImage())) {
                return false;
            }
        }
        return true;
    }

    public static final DiffUtil.ItemCallback<ConversationEntity> TRANSACTION_ENTITY_DIFF_CALLBACK = new DiffUtil.ItemCallback<ConversationEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull ConversationEntity oldEntity, @NonNull ConversationEntity newEntity) {
            return oldEntity.getConversationId() == newEntity.getConversationId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ConversationEntity oldEntity, @NonNull ConversationEntity newEntity) {
            return (newEntity.getLastMessageTimestamp() == oldEntity.getLastMessageTimestamp()
                && newEntity.getNoOfUnreadMessages() == oldEntity.getNoOfUnreadMessages()
                && oldEntity.getType() == newEntity.getType());
        }

    };

    public static final DiffUtil.ItemCallback<GlobalSearchEntity> GLOBAL_SEARCH_ENTITY_DIFF_CALLBACK = new DiffUtil.ItemCallback<GlobalSearchEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull GlobalSearchEntity oldEntity, @NonNull GlobalSearchEntity newEntity) {
            return oldEntity.getMask().equalsIgnoreCase(newEntity.getMask());
        }

        @Override
        public boolean areContentsTheSame(@NonNull GlobalSearchEntity oldEntity, @NonNull GlobalSearchEntity newEntity) {
            return oldEntity.getMatchedKeyword().equalsIgnoreCase(newEntity.getMatchedKeyword());
        }
    };

    public static void readAllSms(Context context) {
        Uri uriInbox = Uri.parse("content://sms/inbox");

        Cursor c = context.getContentResolver().query(uriInbox, null,
            null, null, null);


        while (c.moveToNext()) {
            for (int i = 0; i < c.getCount(); i++) {
                String name = null;
                String phone = "";
                String _id = c.getString(c.getColumnIndexOrThrow("_id"));
                String thread_id = c.getString(c.getColumnIndexOrThrow("thread_id"));
                String msg = c.getString(c.getColumnIndexOrThrow("body"));
                String type = c.getString(c.getColumnIndexOrThrow("type"));
                String timestamp = c.getString(c.getColumnIndexOrThrow("date"));
                phone = c.getString(c.getColumnIndexOrThrow("address"));
                //name = getContactUserDetail(context, c.getString(c.getColumnIndexOrThrow("address")));
            }
        }
        c.close();

    }

    public static Pair<String, String> getContactUserDetail(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
            Uri.encode(phoneNumber));
        Cursor cursor;
        try {
            cursor = cr.query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI}, null, null, null);
        } catch (Exception ex) {
            cursor = null;
        }
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        String thumbNailUri = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            thumbNailUri = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI));
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return new Pair<>(contactName, thumbNailUri);
    }

    public static Triple<String, String, String> getContactUserDetailFromId(Context context, String id) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
            Uri.encode(id));
        Cursor cursor;
        try {
            cursor = cr.query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI, ContactsContract.PhoneLookup.NUMBER}, null, null, null);
        } catch (Exception ex) {
            cursor = null;
        }
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        String thumbNailUri = null;
        String contactNo = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            thumbNailUri = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI));
            contactNo = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.NUMBER));
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return new Triple<>(contactName, thumbNailUri, contactNo);
    }

    public static Triple<String, String, String> getContactUserDetails(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
            Uri.encode(phoneNumber));
        Cursor cursor;
        try {
            cursor = cr.query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI, ContactsContract.PhoneLookup._ID}, null, null, null);
        } catch (Exception ex) {
            cursor = null;
        }
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        String thumbNailUri = null;
        String contactId = null;
        DatabaseUtils.dumpCursor(cursor);
        if (cursor.moveToFirst()) {

            contactName = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            thumbNailUri = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI));
            contactId = cursor.getString(cursor
                .getColumnIndex(ContactsContract.PhoneLookup._ID));
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        //Triple<>
        return new Triple<>(contactName, thumbNailUri, contactId);
    }

    /**
     * Test if device can send SMS
     *
     * @param context
     * @return
     */
    private static boolean canSendSMS(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    /**
     * To send SMS as multi part or normal text message.
     *
     * @param context
     * @param phoneNumber
     * @param message
     * @param messageId
     * @param subscriptionID
     */
    public static void sendSMS(final Context context, String phoneNumber, String message,
                               String messageId, int subscriptionID) {

        PendingIntent sentPendingIntent;
        PendingIntent deliveredPendingIntent;

        if (!canSendSMS(context)) {
            //Toast.makeText(context, context.getString(R.string.cannot_send_sms), Toast.LENGTH_LONG).show();
            return;
        }


        Intent sendIntent = new Intent();
        sendIntent.setClass(context, SmsReceiver.class);
        sendIntent.setAction(SMS_SENT);
        sendIntent.putExtra(AppConstants.BundleKey.MESSAGE_ID, messageId);

        Intent deliverIntent = new Intent();
        deliverIntent.setClass(context, SmsReceiver.class);
        deliverIntent.setAction(SMS_DELIVER);
        deliverIntent.putExtra(AppConstants.BundleKey.MESSAGE_ID, messageId);

        SmsManager smsManager;
        if (subscriptionID != 0) {
            if (Build.VERSION.SDK_INT >= 22) {
                smsManager = SmsManager.getSmsManagerForSubscriptionId(subscriptionID);
            } else {
                smsManager = SmsManager.getDefault();
            }
        } else {
            smsManager = SmsManager.getDefault();
        }
        ArrayList<String> parts = smsManager.divideMessage(message);
        if (parts != null && parts.size() > 0) {
            ArrayList<PendingIntent> sendList = new ArrayList<>();
            ArrayList<PendingIntent> deliverList = new ArrayList<>();
            for (int i = 0; i < parts.size(); i++) {
                int requestCode = (int) DateUtils.currentTimestamp();
                sentPendingIntent = PendingIntent.getBroadcast(context, requestCode,
                    sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                deliveredPendingIntent = PendingIntent.getBroadcast(context, requestCode,
                    deliverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                sendList.add(sentPendingIntent);
                deliverList.add(deliveredPendingIntent);
            }
            try {
                smsManager.sendMultipartTextMessage(phoneNumber, null, parts, sendList,
                    deliverList);
            } catch (Exception e) {
                //TODO: handle cases where phone no. is incorrect or contains special characters..
            }
        } else {
            int requestCode = (int) DateUtils.currentTimestamp();
            sentPendingIntent = PendingIntent.getBroadcast(context, requestCode,
                sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            deliveredPendingIntent = PendingIntent.getBroadcast(context, requestCode,
                deliverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            smsManager.sendTextMessage(phoneNumber, null, message,
                sentPendingIntent, deliveredPendingIntent);
        }

    }

    public static Uri insertSms(String phoneNumber, String message, Context context, Uri contentUri) {
        ContentValues smsValues = new ContentValues();
        smsValues.put(Telephony.Sms.ADDRESS, phoneNumber);
        smsValues.put(Telephony.Sms.BODY, message);
        smsValues.put(Telephony.Sms.DATE_SENT, System.currentTimeMillis());
        smsValues.put(Telephony.Sms.READ, 0);
        return context.getContentResolver().insert(contentUri, smsValues);
    }


    public static int getContactIDFromNumber(String contactNumber, Context context) {

        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = -1;
        Cursor contactLookupCursor = context.getContentResolver()
            .query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                contactNumber), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME,
                    ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI},
                null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor
                .getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }

    public static String extractMessageId(Uri smsUri) {

        Pattern otpPattern = Pattern.compile("^content.+?/(\\d+)$",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher messageIdMatcher = otpPattern.matcher(smsUri.toString());
        String messageId = null;
        if (messageIdMatcher.find()) {
            messageId = messageIdMatcher.group(1);
        }
        return messageId;
    }


    public static final DiffUtil.ItemCallback<ContactModel> FORWARD_CONTACT_DIFF_UTIL = new DiffUtil.ItemCallback<ContactModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull ContactModel oldContact, @NonNull ContactModel newContact) {
            return oldContact.getMask().equalsIgnoreCase(newContact.getMask());
        }

        @Override
        public boolean areContentsTheSame(@NonNull ContactModel oldContact, @NonNull ContactModel newContact) {
            return oldContact.getMask().equalsIgnoreCase(newContact.getMask());
        }
    };

    public static String getTabTitle(Context context, ConversationType conversationType) {

        String movedToText = AppConstants.EMPTY_TEXT;
        if (ConversationType.PERSONAL == conversationType) {
            movedToText = context.getString(R.string.moved_to_tab, context.getString(R.string.personal_tab));
        } else if (ConversationType.TRANSACTIONAL == conversationType) {
            movedToText = context.getString(R.string.moved_to_tab, context.getString(R.string.trans_tab));
        } else if (ConversationType.PROMOTIONAL == conversationType) {
            movedToText = context.getString(R.string.moved_to_tab, context.getString(R.string.promo_tab));
        }
        return movedToText;
    }

    /**
     * To share contact from contact app.
     * @param context
     * @param id
     * @return
     */
    public static String getContactFromId(Context context, String id) {

        ContentResolver cr = context.getContentResolver();
        Cursor cursor;
        try {
            cursor = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{id}, null);

        } catch (Exception ex) {
            cursor = null;
        }
        if (cursor == null) {
            return null;
        }
        StringBuilder contactName = new StringBuilder();
        StringBuilder contactNumber = new StringBuilder();

        while (cursor.moveToNext()) {
            if (TextUtils.isEmpty(contactName)) {
                contactName.append(context.getString(R.string.share_contact_name_format,
                    cursor.getString(cursor
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))));
            }
            contactNumber.append(context.getString(R.string.share_contact_number_format,
                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))));
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return contactName.append(contactNumber).toString();
    }
}
