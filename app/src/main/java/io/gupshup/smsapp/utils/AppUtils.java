package io.gupshup.smsapp.utils;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import io.gupshup.crypto.app.GupshupKeyGenerator;
import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.database.model.SimStateChangeModel;
import io.gupshup.smsapp.databinding.BubleLytBinding;
import io.gupshup.smsapp.enums.AutoRegistrationRetryPolicy;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.ui.compose.viewmodel.ContactBubleViewModel;
import io.gupshup.smsapp.ui.reply.view.MessageReplyActivity;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

import static android.telephony.TelephonyManager.SIM_STATE_ABSENT;
import static android.telephony.TelephonyManager.SIM_STATE_PERM_DISABLED;
import static io.gupshup.smsapp.utils.AppConstants.DD_MMM;
import static io.gupshup.smsapp.utils.AppConstants.EEE;
import static io.gupshup.smsapp.utils.AppConstants.EEEE;
import static io.gupshup.smsapp.utils.AppConstants.EEEE_DD_MMMM;
import static io.gupshup.smsapp.utils.AppConstants.EEEE_DD_MMMM_YYYY;
import static io.gupshup.smsapp.utils.AppConstants.HH_MM;

/*
 * @author  Bhargav Kolla
 * @since   Apr 09, 2018
 */
public final class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();

    public static String generateRandomNumber(int noOfDigits) {
        if (noOfDigits <= 0 || noOfDigits > 100) {
            throw new IllegalArgumentException("Number of digits must be positive number not more than 100");
        }

        StringBuilder sb = new StringBuilder();

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < noOfDigits; i++) {
            sb.append(random.nextInt(10));
        }

        return sb.toString();
    }

    public static String generateAPIKey() {
        return UUID.randomUUID().toString();
    }


    private static Uri configureUri() {
        //Uri uri = ContactsContract.Contacts.CONTENT_URI;
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        //uri=uri.buildUpon().appendQueryParameter(ContactsContract.Contacts.EXTRA_ADDRESS_BOOK_INDEX, "true").build();
        final Uri.Builder builder = uri.buildUpon();
        /*builder.appendQueryParameter(
                ContactsContract.DIRECTORY_PARAM_KEY, String.valueOf(ContactsContract.Directory.DEFAULT));*/
        builder.appendQueryParameter(ContactsContract.REMOVE_DUPLICATE_ENTRIES, "1");
        uri = builder.build();
        return uri;
    }

    /**
     * @param c The char to check
     * @return True if <code>c</code> is in the English alphabet or is a digit,
     * false otherwise
     */
    public static boolean isEnglishLetterOrDigit(char c) {
        return 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z';
    }


    public static View createContactTextView(Contact contact, Context context, LinearLayout parent) {
        BubleLytBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.buble_lyt, parent, false);
        ContactBubleViewModel viewModel = new ContactBubleViewModel();
        binding.setContact(contact);
        binding.setViewModel(viewModel);
        binding.getRoot().setTag(contact);
        return binding.getRoot();
    }


    /**
     * Method used to get Time, Weekday or Date from the milliseconds
     *
     * @param smsTimeInMillis
     * @return
     */
    public static String getFormattedTimeDate(long smsTimeInMillis, Context context, boolean fromConversation) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMillis);

        Calendar now = Calendar.getInstance();
        int week = now.get(Calendar.WEEK_OF_YEAR);
        int year = now.get(Calendar.YEAR);
        int targetWeek = smsTime.get(Calendar.WEEK_OF_YEAR);
        int targetYear = smsTime.get(Calendar.YEAR);
        if (year == targetYear) {
            if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)
                && week == targetWeek) {
                if (fromConversation) {
                    return context.getString(R.string.today);
                } else {
                    return HH_MM.format(smsTime.getTime());
                }
            } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1
                && week == targetWeek) {
                return context.getString(R.string.yesterday);
            } else if (week == targetWeek) {
                if (fromConversation) {
                    return EEEE.format(smsTime.getTime());
                } else {
                    return EEE.format(smsTime.getTime());
                }
            } else {
                if (fromConversation) {
                    return EEEE_DD_MMMM.format(smsTime.getTime());
                } else {
                    return DD_MMM.format(smsTime.getTime());
                }
            }
        } else {
            if (fromConversation) {
                //If sent Date is not Today, not Yesterday, not Same Year but in the Different Year
                return EEEE_DD_MMMM_YYYY.format(smsTime.getTime());
            } else {
                return DD_MMM.format(smsTime.getTime());
            }
        }

    }


    /**
     * Method used to get the simlist from the device and its information
     *
     * @param ctx
     */
    @SuppressLint("MissingPermission")
    public static ArrayList<SimInformation> getSimList(Context ctx) {
        final ArrayList<SimInformation> simCardList = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionManager subscriptionManager;
            subscriptionManager = SubscriptionManager.from(ctx);
            TelephonyManager manager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

            if (ContextCompat.checkSelfPermission(ctx, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                final List<SubscriptionInfo> subscriptionInfoList = subscriptionManager
                    .getActiveSubscriptionInfoList();
                if (subscriptionInfoList != null && subscriptionInfoList.size() > 0) {
                    for (SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
                        SimInformation singleSim = new SimInformation();
                        if (manager != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                singleSim.setImeiNumber("NA"/*manager.getImei(subscriptionInfo.getSimSlotIndex())*/);
                                singleSim.setSimMeidNumber("NA"/*manager.getMeid(subscriptionInfo.getSimSlotIndex())*/);
                                int simState = -99;
                                if (manager != null && subscriptionInfo != null) {
                                    simState = manager.getSimState(subscriptionInfo.getSimSlotIndex());
                                }
                                if (simState == SIM_STATE_PERM_DISABLED
                                    || simState == SIM_STATE_ABSENT || simState == TelephonyManager.SIM_STATE_NETWORK_LOCKED || simState == TelephonyManager.SIM_STATE_CARD_RESTRICTED) {
                                    continue;
                                }
                            }
                        }
                        String simName = subscriptionInfo.getCarrierName().toString();
                        int subscriptionId = subscriptionInfo.getSubscriptionId();
                        singleSim.setmSimName(simName);

                        //as per ticket AMA-603
                        String displayName = "";
                        if (!TextUtils.isEmpty(subscriptionInfo.getNumber())) {
                            displayName = subscriptionInfo.getNumber();
                        } else if (!TextUtils.isEmpty(subscriptionInfo.getDisplayName())) {
                            displayName = subscriptionInfo.getDisplayName().toString();
                        } else {
                            displayName = subscriptionInfo.getCarrierName().toString();
                        }

                        singleSim.setDisplayName(displayName);
                        singleSim.setIconBitmap(subscriptionInfo.createIconBitmap(ctx));
                        singleSim.setIconTint(subscriptionInfo.getIconTint());
                        singleSim.setmSubscriptionID(subscriptionId);
                        if (subscriptionInfo != null) {
                            singleSim.setSimSlotIndex(subscriptionInfo.getSimSlotIndex());
                            singleSim.setmPhoneNumber(subscriptionInfo.getNumber());
                        }
                        singleSim.setImeiNumber("NA"/*subscriptionInfo.getIccId()*/);
                        singleSim.setSimMeidNumber("NA"/*subscriptionInfo.getIccId()*/);
                        if (manager != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                singleSim.setImeiNumber("NA"/*manager.getImei(subscriptionInfo.getSimSlotIndex())*/);
                                singleSim.setSimMeidNumber("NA"/*manager.getMeid(subscriptionInfo.getSimSlotIndex())*/);
                            }
                            // simCardList.add(singleSim);
                        }
                        simCardList.add(singleSim);
                    }
                } else {
                    //Request permission...
                }
            }


        }
        return simCardList;
    }


    /**
     * To get subscription detail sim display name,icon etc. .
     *
     * @param context
     * @param simSlot
     * @return
     */
    @SuppressLint("MissingPermission")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public static Pair<String, Integer> getSimSubscriptionInfo(Context context, int simSlot) {
        String carrierName = AppConstants.EMPTY_TEXT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionInfo info = SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(simSlot);
            if (info == null) {
                return null;
            }
            if (!TextUtils.isEmpty(info.getNumber())) {
                carrierName = info.getNumber();
            } else if (!TextUtils.isEmpty(info.getDisplayName())) {
                carrierName = info.getDisplayName().toString();
            } else {
                carrierName = info.getCarrierName().toString();
            }
            return new Pair<>(carrierName, info.getSubscriptionId());
        } else if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager == null) {
                return null;
            }
            carrierName = manager.getSimOperatorName();

            if (!TextUtils.isEmpty(manager.getLine1Number())) {
                carrierName = manager.getLine1Number();
            } else {
                carrierName = manager.getNetworkOperatorName();
            }

            return new Pair<>(TextUtils.isEmpty(carrierName)
                ? context.getString(R.string.no_service_text) : carrierName, 0);
        }
        return null;
    }

    /**
     * This will return true only if device support to change default messaging app as our SMSApp.
     *
     * @param context
     * @param appPackage
     * @return
     */

    public static boolean isDefaultSmsPackage(Context context, String appPackage) {

        String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(context);
        return !TextUtils.isEmpty(defaultSmsPackage)
            && defaultSmsPackage.equalsIgnoreCase(appPackage);
    }


    /**
     * Method used to check if phone contains sim card or not
     */
    public static boolean checkSimCardExistence(Context ctx) {
        boolean isSimAvailable = false;
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionManager subscriptionManager;
            subscriptionManager = SubscriptionManager.from(ctx);
            final List<SubscriptionInfo> subscriptionInfoList = subscriptionManager
                .getActiveSubscriptionInfoList();
            isSimAvailable = subscriptionInfoList != null && subscriptionInfoList.size() > 0;

        } else if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
            if (tm != null) {
                if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
                    //the device has a sim card
                    isSimAvailable = true;
                } else {
                    //no sim card available
                    isSimAvailable = false;
                }
            }
        }
        LogUtility.d("SIMCHECKING", "--------- IS SIM AVAILABLE ------" + isSimAvailable);
        return isSimAvailable;

    }

    public static GupshupKeyPair generateGupshupKeyPair() {
        GupshupKeyPair gupshupKeyPair = null;
        try {
            gupshupKeyPair = GupshupKeyGenerator.getInstance().generateKeyPair();
        } catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            AppUtils.logToFile(MainApplication.getContext(), "Exception while generating gupshupKeyPair.." + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return gupshupKeyPair;
    }


    public static ImageView createTile(Context context, LinearLayout parent, String contactname) {
        ImageView imageView = new ImageView(context);
        imageView.setPadding(10, 0, 0, 0);
        LetterTileProvider tileProvider = new LetterTileProvider(imageView.getContext());
        int tileSize = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.letter_tile_size);
        final Bitmap letterTile = tileProvider.getLetterTile(contactname, contactname, tileSize, tileSize);
        imageView.setImageBitmap(letterTile);
        return imageView;
    }

    /**
     * Method used to create a spannable text with changing the selected text background
     *
     * @param keyword
     * @param text
     * @param startPosition
     * @return
     */
    public static Spannable getSpannableMessageBody(String keyword, String text, int startPosition, boolean isAtPosition) {
        Spannable searchText = new SpannableString(text);
        if (isAtPosition) {
            searchText.setSpan(new BackgroundColorSpan(0xCCffba00), startPosition, (startPosition + keyword.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            searchText.setSpan(new BackgroundColorSpan(0xCCffda77), startPosition, (startPosition + keyword.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return searchText;
    }


    @SuppressLint("MissingPermission")
    public static ArrayList<SimInformation> getSimInfo(Context ctx) {
        final ArrayList<SimInformation> simCardList = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager manager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager == null) {
                return simCardList;
            }
            SimInformation singleSim = new SimInformation();
            singleSim.setmPhoneNumber(manager.getLine1Number());
            singleSim.setSimSlotIndex(0);
            singleSim.setImeiNumber(manager.getDeviceId());
            singleSim.setSimMeidNumber(manager.getSimSerialNumber());
            simCardList.add(singleSim);
        }
        return simCardList;
    }

    public static void saveSimInfoToPreference(List<SimInformation> simList, PreferenceManager prefManager) {

        if (simList == null) return;
        try {
            for (SimInformation information : simList) {
                if (information != null) {
                    prefManager.setImeiNumber(String.valueOf(information.getSimSlotIndex()), information.getImeiNumber());
                    prefManager.setMeidNumber(String.valueOf(information.getSimSlotIndex()), information.getSimMeidNumber());
                    //prefManager.setImsiNumber(information.getSimSlotIndex(), String.valueOf(information.getmSubscriptionID()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    @SuppressLint("MissingPermission")
    public static void saveSimIMSINoToPhone(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {

            if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(0) != null) {
                PreferenceManager.getInstance(context).setImsiNumber(0, SubscriptionManager
                    .from(context).getActiveSubscriptionInfoForSimSlotIndex(0).getIccId());
            }
            if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(1) != null) {
                PreferenceManager.getInstance(context).setImsiNumber(1, SubscriptionManager
                    .from(context).getActiveSubscriptionInfoForSimSlotIndex(1).getIccId());
            }
        } else if (Build.VERSION.SDK_INT >= 19) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null && manager.getSimSerialNumber() != null) {
                PreferenceManager.getInstance(context).setImsiNumber(0, manager.getSimSerialNumber());
            }
        }
    }

    public static void saveSimIMSINoForParticularSlot(Context context, int slot) {

        PreferenceManager.getInstance(context).setImsiNumber(slot, getImsiNumberForSlot(context, slot));
    }

    private static String getImsiNumberForSlot(Context context, int slot) {
        TelephonyManager telephonyManager = (TelephonyManager) context
            .getSystemService(Context.TELEPHONY_SERVICE);

        int subscriptionId;
        String imsi = AppConstants.EMPTY_TEXT;
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                SubscriptionInfo subscriptionInfo = SubscriptionManager.from(context)
                    .getActiveSubscriptionInfoForSimSlotIndex(slot);
                if (subscriptionInfo != null) {
                    subscriptionId = subscriptionInfo.getSubscriptionId();
                    try {
                        Class c = Class.forName("android.telephony.TelephonyManager");
                        Method m = c.getMethod("getSubscriberId", int.class);
                        Object o = m.invoke(telephonyManager, subscriptionId);

                        imsi = (String) o;
                    } catch (Exception e) {
                        e.printStackTrace();
                        imsi = subscriptionInfo.getIccId();
                    }
                }
            } else if (Build.VERSION.SDK_INT >= 19) {
                if (telephonyManager != null) {
                    imsi = telephonyManager.getSubscriberId();
                }
            }
        }
        return imsi;

    }

    public static void checkSimChangedStatus(Context ctx) {

        // Compare to preference
        PreferenceManager prefManager = PreferenceManager.getInstance(ctx);
        String simOneSlot = AppConstants.SLOT_ZERO;
        String simTwoSlot = AppConstants.SLOT_ONE;
        String simOneImeiNumber = prefManager.getImeiNumber(simOneSlot), simTwoImeiNumber = prefManager.getImeiNumber(simTwoSlot);
        String simOneMeidNumber = prefManager.getGetMeid(simOneSlot), simTwoMeidNumber = prefManager.getGetMeid(simTwoSlot);

        RegistrationInfo simOneItem = AppUtils.getSimOneData(ctx);
        RegistrationInfo simTwoItem = AppUtils.getSimTwoData(ctx);

        List<SimInformation> simInfoList = getSimInfoList();
        if (simInfoList != null) {
            if (TextUtils.isEmpty(simOneImeiNumber)
                || TextUtils.isEmpty(simTwoImeiNumber)) {
                return;
            }
            if (simInfoList.size() > 1) {
                if ((!simInfoList.get(0).getImeiNumber().equalsIgnoreCase(simOneImeiNumber)
                    && !simInfoList.get(1).getImeiNumber().equalsIgnoreCase(simTwoImeiNumber))
                    || (!simInfoList.get(0).getSimMeidNumber().equalsIgnoreCase(simOneMeidNumber)
                    && !simInfoList.get(1).getSimMeidNumber().equalsIgnoreCase(simTwoMeidNumber))) {
                    prefManager.setMrFourTextVisibility(true);
                    //Both sim state changed
                    if (simOneItem != null) {
                        simOneItem.simOneState = RegistrationSimOneState.SIM_ONE_STATE_CHANGED;
                        simOneItem.retryPolicySim1 = AutoRegistrationRetryPolicy.TWO_TIME;
                        AppUtils.setSimRequestItemData(ctx, simOneItem);
                    }
                    if (simTwoItem != null) {
                        simTwoItem.simTwoState = RegistrationSimTwoState.SIM_TWO_STATE_CHANGED;
                        simTwoItem.retryPolicySim2 = AutoRegistrationRetryPolicy.TWO_TIME;
                        AppUtils.setSimRequestItemData(ctx, simTwoItem);
                    }
                } else if ((!simInfoList.get(0).getImeiNumber().equalsIgnoreCase(simOneImeiNumber))
                    || (!simInfoList.get(0).getSimMeidNumber().equalsIgnoreCase(simOneMeidNumber))) {
                    // clear sim one preference and re-register again
                    prefManager.setMrFourTextVisibility(true);
                    if (simOneItem != null) {
                        simOneItem.simOneState = RegistrationSimOneState.SIM_ONE_STATE_CHANGED;
                        simOneItem.retryPolicySim1 = AutoRegistrationRetryPolicy.TWO_TIME;
                        AppUtils.setSimRequestItemData(ctx, simOneItem);
                    }
                } else if ((!simInfoList.get(1).getImeiNumber().equalsIgnoreCase(simTwoImeiNumber))
                    || !simInfoList.get(1).getSimMeidNumber().equalsIgnoreCase(simTwoMeidNumber)) {
                    prefManager.setMrFourTextVisibility(true);
                    if (simTwoItem != null) {
                        simTwoItem.simTwoState = RegistrationSimTwoState.SIM_TWO_STATE_CHANGED;
                        simTwoItem.retryPolicySim2 = AutoRegistrationRetryPolicy.TWO_TIME;
                        AppUtils.setSimRequestItemData(ctx, simTwoItem);
                    }
                }
            } else if (simInfoList.size() == 1) {
                SimInformation information = simInfoList.get(0);
                if (information != null) {
                    if ((!information.getImeiNumber().equalsIgnoreCase(prefManager.getImeiNumber
                        (String.valueOf(information.getSimSlotIndex()))))
                        || (!information.getSimMeidNumber().equalsIgnoreCase(prefManager.getGetMeid
                        (String.valueOf(information.getSimSlotIndex()))))) {
                        prefManager.setMrFourTextVisibility(true);
                        int simSlot = information.getSimSlotIndex();
                        RegistrationInfo simData = AppUtils.getSimDataBasedOnSlot(ctx, String.valueOf(simSlot));
                        if (simData != null) {
                            if (simSlot == 0) {
                                simData.simOneState = RegistrationSimOneState.SIM_ONE_STATE_CHANGED;
                                simData.retryPolicySim1 = AutoRegistrationRetryPolicy.TWO_TIME;
                                AppUtils.setSimRequestItemData(ctx, simData);
                            } else {
                                simData.simTwoState = RegistrationSimTwoState.SIM_TWO_STATE_CHANGED;
                                simData.retryPolicySim2 = AutoRegistrationRetryPolicy.TWO_TIME;
                                AppUtils.setSimRequestItemData(ctx, simData);
                            }
                        }
                    }
                }
            }
        }
    }


    @SuppressLint("MissingPermission")
    public static Pair<Boolean, Boolean> getIsSimCardStateChange(Context context, int simSlot) {
        Boolean isSimOneChanged = false, isSimTwoChanged = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            // PreferenceManager.getInstance(context).getImsiNumber(0)
            List<SubscriptionInfo> subInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (subInfoList != null) {
                for (int i = 0; i < subInfoList.size(); i++) {
                    if (subInfoList.get(i).getSimSlotIndex() == 0) {
                        isSimOneChanged = !PreferenceManager.getInstance(context).getImsiNumber(0).equals(subInfoList.get(i).getIccId());
                    } else if (subInfoList.get(i).getSimSlotIndex() == 1) {
                        isSimTwoChanged = !PreferenceManager.getInstance(context).getImsiNumber(1).equals(subInfoList.get(i).getIccId());
                    }
                }
            }

        } else if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null && manager.getSimSerialNumber() != null) {
                isSimOneChanged = !PreferenceManager.getInstance(context).getImsiNumber(0).equals(manager.getSimSerialNumber());
            }
        }
        return new Pair<>(isSimOneChanged, isSimTwoChanged);
    }


    public static Pair<Boolean, Boolean> getIsSimCardChanged(Context context) {

        Boolean isSimOneChanged = false, isSimTwoChanged = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            int simCount = SubscriptionManager.from(context).getActiveSubscriptionInfoCount();
            List<SubscriptionInfo> subInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (simCount == 0) {   //when there are no sims plugged in.
                if (!PreferenceManager.getInstance(context).getImsiNumber(0).equals(AppConstants.EMPTY_TEXT)) {
                    isSimOneChanged = true;
                }
                if (!PreferenceManager.getInstance(context).getImsiNumber(1).equals(AppConstants.EMPTY_TEXT)) {
                    isSimTwoChanged = true;
                }
                //LogUtility.d("auto", "No sims plugged in ..returning as : "+isSimOneChanged +"  "+isSimTwoChanged);
                return new Pair<>(isSimOneChanged, isSimTwoChanged);
            }
            //List<SimInformation> simInfoList = getSimInfoList();
            int previousSimCount = 0;

            if (!PreferenceManager.getInstance(context).getImsiNumber(0).equals(AppConstants.EMPTY_TEXT)) {
                previousSimCount++;
            }
            if (!PreferenceManager.getInstance(context).getImsiNumber(1).equals(AppConstants.EMPTY_TEXT)) {
                previousSimCount++;
            }

            if (previousSimCount != simCount) {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                //Change in no. of sims
                if (previousSimCount > simCount) {
                    //some sim removed
                    if (!PreferenceManager.getInstance(context).getImsiNumber(0).equals(AppConstants.EMPTY_TEXT)) {
                        if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(0) == null) {
                            //sim 1 removed.
                            //PreferenceManager.getInstance(context).setImsiNumber(0, AppConstants.EMPTY_TEXT);
                            isSimOneChanged = true;
                        }
                    }
                    if (!PreferenceManager.getInstance(context).getImsiNumber(1).equals(AppConstants.EMPTY_TEXT)) {
                        if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(1) == null) {
                            //sim 2 removed.
                            //PreferenceManager.getInstance(context).setImsiNumber(1, AppConstants.EMPTY_TEXT);
                            isSimTwoChanged = true;
                        }
                    }
                } else {
                    //sim added
                    if (PreferenceManager.getInstance(context).getImsiNumber(0).equals(AppConstants.EMPTY_TEXT)) {
                        if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(0) != null) {
                            //sim 1 added.
                            isSimOneChanged = true;
                        }
                    }
                    if (!PreferenceManager.getInstance(context).getImsiNumber(1).equals(AppConstants.EMPTY_TEXT)) {
                        if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(1) != null) {
                            //sim 2 added.
                            isSimTwoChanged = true;
                        }
                    }
                }
            }

            //if(PreferenceManager.getInstance(context).getImsiNumber(0))

            for (int i = 0; i < simCount; i++) {
                //LogUtility.d("auto", "From prefs for sim "+ i+ "  "+ PreferenceManager.getInstance(context).getImsiNumber(i));
                //LogUtility.d("auto", "Subscription info list id "+subInfoList.get(i).getIccId());
                if (subInfoList != null) {
                    if (subInfoList.get(i).getSimSlotIndex() == 0) {
                        isSimOneChanged = !PreferenceManager.getInstance(context).getImsiNumber(0).equals(subInfoList.get(i).getIccId());
                    } else if (subInfoList.get(i).getSimSlotIndex() == 1) {
                        isSimTwoChanged = !PreferenceManager.getInstance(context).getImsiNumber(1).equals(subInfoList.get(i).getIccId());
                    }
                }
            }
            if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(0) == null) {
                //sim 0 removed.
                isSimOneChanged = true;
            }
            if (SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(1) == null) {
                //sim 1 removed.
                isSimTwoChanged = true;
            }

        } else {
            //TODO: Detect sim change for versions less than Lollipop_MR1
        }
        //LogUtility.d("auto", "Sims plugged in ..returning as : "+isSimOneChanged +"  "+isSimTwoChanged);
        return new Pair<>(isSimOneChanged, isSimTwoChanged);
    }

    public static List<SimInformation> getSimInfoList() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            return AppUtils.getSimList(MainApplication.getContext());
        } else {
            return AppUtils.getSimInfo(MainApplication.getContext());
        }
    }

    public static RegistrationInfo getSimOneData(Context context) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        Gson gson = new Gson();

        String simOneReqItem = manager
            .getSimRequestItem(manager.getSequenceNumberFromActualSlot(AppConstants.SLOT_ZERO));
        return gson.fromJson(simOneReqItem, RegistrationInfo.class);

    }

    public static RegistrationInfo getSimTwoData(Context context) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        Gson gson = new Gson();
        String simTwoReqItem = manager
            .getSimRequestItem(manager.getSequenceNumberFromActualSlot(AppConstants.SLOT_ONE));
        return gson.fromJson(simTwoReqItem, RegistrationInfo.class);

    }

    public static RegistrationInfo getSimDataBasedOnSlot(Context context, String simSlot) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        Gson gson = new Gson();

        String simData = manager
            .getSimRequestItem(manager.getSequenceNumberFromActualSlot(simSlot));
        return gson.fromJson(simData, RegistrationInfo.class);

    }

    public static void setSimRequestItemData(Context context, RegistrationInfo requestItem) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        requestItem.imeNumber = manager.getImeiNumber(String.valueOf(requestItem.simSlot));
        Gson gson = new Gson();
        String jsonOne = gson.toJson(requestItem);
        manager.setSequenceNumberForActualSlot(String.valueOf(requestItem.simSlot), requestItem.sequenceNumber);
        manager.setSimRequestItem(requestItem.sequenceNumber, jsonOne);

    }

    /**
     * @param key The key used to generate the tile color
     * @return A new or previously chosen color for <code>key</code> used as the
     * tile background color
     */
    public static int pickColor(String key, TypedArray colors) {
        int NUM_OF_TILE_COLORS = 14;
        // String.hashCode() is not supposed to change across java versions, so
        // this should guarantee the same key always maps to the same color
        final int color = Math.abs(key.hashCode()) % NUM_OF_TILE_COLORS;
        try {
            return colors.getColor(color, Color.BLACK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void openTabLayout(View view) {

        view.setVisibility(View.VISIBLE);
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthSpec, heightSpec);
        final TypedArray styledAttributes = view.getContext().getTheme().obtainStyledAttributes(
            new int[]{android.R.attr.actionBarSize});
        int actionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        if (actionBarSize <= 0) {
            actionBarSize = view.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_56dp);
        }
        ValueAnimator mAnimator = slideAnimator(0, actionBarSize, view);
        mAnimator.start();
    }

    public static void collapseTabLayout(final View view) {

        if (view.getVisibility() == View.VISIBLE) {
            int finalHeight = view.getHeight();
            ValueAnimator mAnimator = slideAnimator(finalHeight, 0, view);
            mAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            mAnimator.start();
        }
    }

    private static ValueAnimator slideAnimator(int start, int end, final View view) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    /**
     * To get subscription detail sim display name,icon etc. .
     *
     * @param context
     * @param simSlot
     * @return
     */
    @SuppressLint("MissingPermission")
    public static Pair<String, Integer> getChanelName(Context context, int simSlot) {

        String carrierName = AppConstants.EMPTY_TEXT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionInfo info = SubscriptionManager.from(context).getActiveSubscriptionInfoForSimSlotIndex(simSlot);
            if (info == null) {
                return null;
            }
            if (!TextUtils.isEmpty(info.getDisplayName())) {
                carrierName = info.getDisplayName().toString();
            } else if (!TextUtils.isEmpty(info.getCarrierName())) {
                carrierName = info.getCarrierName().toString();
            } else {
                carrierName = info.getNumber();
            }
            return new Pair<>(carrierName, info.getSubscriptionId());
        } else if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager == null) {
                return null;
            }
            if (!TextUtils.isEmpty(manager.getNetworkOperatorName())) {
                carrierName = manager.getSimOperatorName();
            } else {
                carrierName = manager.getLine1Number();
            }
            return new Pair<>(TextUtils.isEmpty(carrierName)
                ? context.getString(R.string.no_service_text) : carrierName, 0);
        }
        return null;
    }

    public static int getTotalActiveSimCount(Context context) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            return 1;
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
            return subscriptionManager.getActiveSubscriptionInfoCountMax();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null) {
                return manager.getPhoneCount();
            }
        }
        return 0;
    }

    public static String getViaText(Context context, MessageEntity messageEntity) {

        String viaText = AppConstants.EMPTY_TEXT;

        MessageChannel chan = messageEntity.getChannel();
        int activeSimCount = AppUtils.getSimInfoList().size();
        if (chan.ordinal() == 0) {
            if (activeSimCount == 2) {
                viaText = context.getResources().getString(R.string.via);  //AMA-339  via_sms
                if (!TextUtils.isEmpty(messageEntity.getCarierName())) {
                    viaText = String.format("%s %s %s", viaText, "", messageEntity.getCarierName());
                } else {
                    if (messageEntity.getIncomingSIMID() == 1) {
                        //Imported through SIM one
                        viaText = String.format("%s %s %s", viaText, "", context.getString(R.string.via_sim_one));
                    } else if (messageEntity.getIncomingSIMID() == 2) {
                        //Imported through SIM Two
                        viaText = String.format("%s %s %s", viaText, "", context.getString(R.string.via_sim_two));
                    } else {
                        viaText = context.getString(R.string.sms_via);
                    }
                }
            } else {
                viaText = context.getString(R.string.sms_via);
            }
        } else if (chan.ordinal() == 1) {
            viaText = context.getResources().getString(R.string.via_data);
        }
        return viaText;
    }

    public static boolean isFromSameConversation(MessageEntity[] me, Context context) {    //pass reference converation entity
        String[] masks = new String[me.length];
        for (int i = 0; i < me.length; i++) {
            masks[i] = me[i].getMask();
        }
        return !(MessagingService.getInstance(SMSService.class).getConversations(masks) > 1);
    }

    public static boolean isMessagesFromSameConversation(MessageEntity[] me, Context context) {    //pass reference converation entity
        ConversationEntity refConversationEntity = null;
        List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class)
            .getConversationByMask(me[0].getMask());
        if (conversationEntities.size() > 0) {
            refConversationEntity = conversationEntities.get(0);
        }
        for (MessageEntity aMe : me) {
            if (aMe.getMask().equalsIgnoreCase(refConversationEntity.getMask())) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
            = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getIMEI(Context ctx, String fullNumberWithPlus) {
        String imei = "";
        RegistrationInfo simOneData = AppUtils.getSimOneData(ctx);
        if (fullNumberWithPlus.equals(simOneData.fullNumberWithPlus)) {
            imei = simOneData.imeNumber;
        } else {
            imei = AppUtils.getSimTwoData(ctx).imeNumber;
        }
        return imei;
    }

    public static String getAppVersionName(Context con) {
        String version = "";
        try {
            PackageInfo pInfo = con.getPackageManager().getPackageInfo(con.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static void logToFile(Context context, String content) {

        FileOutputStream out = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "smsAppLog.txt");
            out = new FileOutputStream(file, true);
            content += " \n";
            out.write(content.getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    // The public static function which can be called from other classes
    public static void darkenStatusBar(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(
                darkenColor(
                    color));
        }

    }


    // Code to darken the color supplied (mostly color of toolbar)
    private static int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }

    public static int getDefaultSelectedSim(Context context) {

        int defaultSimSlot = 0;
        if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionManager manager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                try {
                    if (manager != null) {
                        int defaultSubId = SubscriptionManager.getDefaultSmsSubscriptionId();
                        if (manager.getActiveSubscriptionInfo(defaultSubId) != null) {
                            defaultSimSlot = manager.getActiveSubscriptionInfo(defaultSubId).getSimSlotIndex();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return defaultSimSlot;
    }

    public static String getCurrentDefaultSMSAppName(Context context) {

        String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(context);
        PackageManager packageManager = context.getPackageManager();
        String appName;
        ApplicationInfo appInfo;
        try {
            appInfo = packageManager.getApplicationInfo(defaultSmsPackage, 0);
            appName = (String) ((appInfo != null) ? packageManager.getApplicationLabel(appInfo)
                : AppConstants.EMPTY_TEXT);
        } catch (final PackageManager.NameNotFoundException e) {
            return AppConstants.EMPTY_TEXT;
        }
        return appName;
    }

    public static String getSelectedRingToneTitle(Context context, Uri notificationToneUri) {

        Ringtone ringtone = RingtoneManager.getRingtone(context, notificationToneUri);
        return ringtone.getTitle(context);
    }

    /**
     * Method used to launch the activity general case.
     */
    public static void launchMessageReplyActivity(Context context, ConversationEntity item, int position,
                                                  boolean isFromBlock) {

        Intent intent = new Intent(context, MessageReplyActivity.class);
        intent.putExtra(AppConstants.BundleKey.MASK, item.getMask());
        intent.putExtra(AppConstants.BundleKey.LAST_MESSAGE_ID, item.getLastMessageId());
        intent.putExtra(AppConstants.IS_FROM_BLOCK_SCREEN, isFromBlock);
        intent.putExtra(AppConstants.KEY_MESSAGE_DRAFT, item.getDraftMessage());
        intent.putExtra(AppConstants.BundleKey.PAGER_POSITION, position);
        intent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE, item.getType());
        if (item.getSubType() != null) {
            intent.putExtra(AppConstants.BundleKey.CONVERSATION_SUB_TYPE, item.getSubType().name());
        }
        context.startActivity(intent);
    }

    /*public static ConversationType getConversationTypes(ConversationType conversationType) {
        ConversationType type;
        if(conversationType == ConversationType.TRANSACTIONAL
            || conversationType == ConversationType.OTP){
            type = new ConversationType[]{ConversationType.TRANSACTIONAL, ConversationType.OTP};
        }else {
            type = new ConversationType[]{conversationType};
        }

        return type;
    }
*/

    /**
     * Method used to launch the activity from push notification.
     */
    public static void launchMessageReplyActivityForPushDeepLink(Context context, String phoneNumber,
                                                                 String lastMessageId,
                                                                 ConversationType conversationType) {

        Intent intent = new Intent(context, MessageReplyActivity.class);
        intent.putExtra(AppConstants.BundleKey.MASK, Utils.getMaskFromPhoneNumber(phoneNumber));
        intent.putExtra(AppConstants.DISPLAY_NAME, Utils.getMaskFromPhoneNumber(phoneNumber));
        intent.putExtra(AppConstants.BundleKey.LAST_MESSAGE_ID, lastMessageId);
        intent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE, conversationType);
        context.startActivity(intent);
    }

    /**
     * Method used to launch the activity for forwarding message
     */
    public static void launchMessageActivityForForward(Context context, ConversationEntity entity,
                                                       MessageEntity messageEntity) {

        Intent intent = new Intent(context, MessageReplyActivity.class);
        intent.putExtra(AppConstants.BundleKey.MASK, entity.getMask());
        intent.putExtra(AppConstants.DISPLAY_NAME, entity.getDisplay());
        intent.putExtra(AppConstants.RECEPIENT_THUMB_NAIL, entity.getImage());
        intent.putExtra(AppConstants.BundleKey.LAST_MESSAGE_ID, entity.getLastMessageId());
        intent.putExtra(AppConstants.KEY_MESSAGE_DRAFT, entity.getDraftMessage());
        GupshupTextMessage message = (GupshupTextMessage) messageEntity.getContent();
        intent.putExtra(AppConstants.KEY_FORWARD_MESSAGE, message.getText());
        context.startActivity(intent);
    }

    /**
     * Method used to launch the activity from search screen
     */
    public static void launchMessageReplyActivityForSearch(Context context, GlobalSearchEntity item, String searchQuery) {

        Intent intent = new Intent(context, MessageReplyActivity.class);
        intent.putExtra(AppConstants.BundleKey.MASK, item.getMask());
        intent.putExtra(AppConstants.BundleKey.SEARCH_QUERY, searchQuery);
        intent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE, item.getType());
        context.startActivity(intent);
    }

    /**
     * Check whether or not the string contains at least four characters;
     * And return  number with mask.
     *
     * @param context
     * @param phoneNumber
     * @return
     */
    public static String replaceLastFourNumber(Context context, String phoneNumber) {

        int length = phoneNumber.length();
        if (length < 4) return phoneNumber;
        return phoneNumber.substring(0, length - 4) + context.getString(R.string.phone_number_mask);
    }


    public static int getThemeColor(Context ctx, ConversationEntity entity) {
        TypedArray colors = ctx.getResources().obtainTypedArray(R.array.letter_tile_colors);
        String display = entity.getDisplay();
        String mask = entity.getMask();
        return AppUtils.pickColor(TextUtils.isEmpty(display) ? mask : display, colors);
    }

    public static String getDisplayName(ConversationEntity entity) {
        String display = entity.getDisplay();
        String mask = entity.getMask();
        return TextUtils.isEmpty(display) ? mask : display;
    }

    public static void callToPhone(String phoneNum, Activity context) {
        if (TextUtils.isEmpty(phoneNum))
            return;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,
                new String[]{android.Manifest.permission.CALL_PHONE},
                AppConstants.PermissionRequest.REQUEST_CALL_PHONE);
        } else {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phoneNum));
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static void deleteSMSFromDefaultDB(Context context, Uri contentUri, MessageEntity[] messageEntities) {
        int i = 0;
        for (i = 0; i < messageEntities.length; i++) {

            try {
                // Delete the SMS
                String uri = contentUri + "/" + messageEntities[i].getMessageId();
                int count = context.getContentResolver().delete(Uri.parse(uri), null, null);
                LogUtility.d("Cursor count", "Deleted :" + count);
            } catch (Exception e) {
                LogUtility.d("Exception del", "exc in delete  with val :" + e.getMessage());
            }
        }
    }


    public static String characterCounter(String smsText) {
        int messages;
        int remainingMessages;
        String finalResult;

        int[] countArray = SmsMessage.calculateLength(smsText, false);

        messages = countArray[0];
        remainingMessages = countArray[2];
        if (messages <= 1 && remainingMessages > 10) {

            finalResult = AppConstants.EMPTY_TEXT;

        } else if (messages <= 1) {

            finalResult = String.valueOf(remainingMessages);

        } else {
            finalResult = String.valueOf(remainingMessages) + "/" + String.valueOf(messages);

        }

        return finalResult;
    }


    /**
     * Returns the consumer friendly device name
     */
    public static String getBuildNumber() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String getBuildName() {
        return Build.MODEL;
    }

    public static String getBoardName() {
        return Build.BOARD;
    }

    public static String getManufacturerName() {
        return Build.MANUFACTURER;
    }

    public static boolean isDefaultSmsApp(Context context) {
        return context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));
    }

    public static void updateConversationAsUnreadInDefaultSMSDb(Context context, ConversationEntity[] conversations) {
        MessageReplyActivity.updateSMSAsUnreadInDefaultDB(context, Telephony.Sms.CONTENT_URI, new MessageEntity[]{conversations[0].getLastMessageData()});
    }

    public static String fetchApiKey(int simSlot) {
        String key = "";
        if (simSlot == 0) {
            if (PreferenceManager.getInstance(MainApplication.getContext()).getApiKeyForSim1().equals("0")) {
                key = AppUtils.generateAPIKey();
                PreferenceManager.getInstance(MainApplication.getContext()).setApiKeyForSim1(key);
            } else {
                key = PreferenceManager.getInstance(MainApplication.getContext()).getApiKeyForSim1();
            }
        } else if (simSlot == 1) {
            if (PreferenceManager.getInstance(MainApplication.getContext()).getApiKeyForSim2().equals("0")) {
                key = AppUtils.generateAPIKey();
                PreferenceManager.getInstance(MainApplication.getContext()).setApiKeyForSim2(key);
            } else {
                key = PreferenceManager.getInstance(MainApplication.getContext()).getApiKeyForSim2();
            }
        }
        return key;
    }

    public static String getFormatedShareContact(String previousText, String shareContacts) {
        StringBuilder builder = new StringBuilder();
        builder.append(previousText);
        builder.append(shareContacts);
        return builder.toString();
    }

    public static String intentToString(Intent intent) {
        if (intent == null) {
            return null;
        }

        return intent.toString() + " " + bundleToString(intent.getExtras());
    }

    public static String bundleToString(Bundle bundle) {
        StringBuilder out = new StringBuilder("Bundle[");

        if (bundle == null) {
            out.append("null");
        } else {
            boolean first = true;
            for (String key : bundle.keySet()) {
                if (!first) {
                    out.append(", ");
                }

                out.append(key).append('=');

                Object value = bundle.get(key);

                if (value instanceof int[]) {
                    out.append(Arrays.toString((int[]) value));
                } else if (value instanceof byte[]) {
                    out.append(Arrays.toString((byte[]) value));
                } else if (value instanceof boolean[]) {
                    out.append(Arrays.toString((boolean[]) value));
                } else if (value instanceof short[]) {
                    out.append(Arrays.toString((short[]) value));
                } else if (value instanceof long[]) {
                    out.append(Arrays.toString((long[]) value));
                } else if (value instanceof float[]) {
                    out.append(Arrays.toString((float[]) value));
                } else if (value instanceof double[]) {
                    out.append(Arrays.toString((double[]) value));
                } else if (value instanceof String[]) {
                    out.append(Arrays.toString((String[]) value));
                } else if (value instanceof CharSequence[]) {
                    out.append(Arrays.toString((CharSequence[]) value));
                } else if (value instanceof Parcelable[]) {
                    out.append(Arrays.toString((Parcelable[]) value));
                } else if (value instanceof Bundle) {
                    out.append(bundleToString((Bundle) value));
                } else {
                    out.append(value);
                }

                first = false;
            }
        }
        out.append("]");
        return out.toString();
    }


    /**
     * This method will return current state of existing sim.
     *
     * @param context
     * @return
     */
    private static SimStateChangeModel getSimStatusData(Context context) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        Gson gson = new Gson();
        String simStateChange = manager.getSimChangeStatus();
        return gson.fromJson(simStateChange, SimStateChangeModel.class);

    }


    /**
     * This method to save/update existing sim state(ABSENT,LOADING, etc...)
     *
     * @param context
     * @param simSlot
     * @param status
     */
    public static void setSimStatusData(Context context, int simSlot, String status) {

        PreferenceManager manager = PreferenceManager.getInstance(context);
        SimStateChangeModel model = getSimStatusData(context);
        if (model == null) {
            model = new SimStateChangeModel();
        }
        if (simSlot == 0) {
            //model.setSimOneChanged(true);
            model.setSimOneState(status);
        } else if (simSlot == 1) {
            //model.setSimTwoChanged(true);
            model.setSimTwoState(status);

        }
        Gson gson = new Gson();
        String json = gson.toJson(model);
        manager.setSimChangeStatus(json);

    }

    @SuppressLint("MissingPermission")
    public static SimStateChangeModel getIsSimCardStatusChange(Context context) {

        SimStateChangeModel model = getSimStatusData(context);

        if (model == null) {
            model = new SimStateChangeModel();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            List<SubscriptionInfo> subInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (subInfoList != null) {
                for (int i = 0; i < subInfoList.size(); i++) {
                    if (subInfoList.get(i).getSimSlotIndex() == 0) {
                        model.setSimOneChanged(!PreferenceManager.getInstance(context)
                            .getImsiNumber(0).equals(getImsiNumberForSlot(context,subInfoList.get(i).getSimSlotIndex())));
                    } else if (subInfoList.get(i).getSimSlotIndex() == 1) {
                        model.setSimTwoChanged(!PreferenceManager.getInstance(context)
                            .getImsiNumber(1).equals(getImsiNumberForSlot(context,subInfoList.get(i).getSimSlotIndex())));
                    }
                }
            } else {
                model.setSimOneChanged(true);
                model.setSimTwoChanged(true);
            }
        } else if (Build.VERSION.SDK_INT >= 19) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null && manager.getSimSerialNumber() != null) {
                model.setSimOneChanged(!PreferenceManager.getInstance(context)
                    .getImsiNumber(0).equals(getImsiNumberForSlot(context,0)));
            }
        }

        return model;
    }
}

