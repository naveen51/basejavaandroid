package io.gupshup.smsapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.databinding.AdapterRecentMessageItemBinding;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.ui.adapter.MessageListAdapter;

/**
 * Created by Ram Prakash Bhat on 8/5/18.
 */

public class SwipeItemTouchHelperCallback extends ItemTouchHelper.SimpleCallback {

    private OnItemSwipeListener onItemSwipeLeftListener, onItemSwipeRightListener;
    private boolean swipeEnabled;
    private boolean mSwipeBack;
    /*private Paint backGroundPaint;
    private TextPaint textPaint;
*/

    private SwipeItemTouchHelperCallback(int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
    }

    private SwipeItemTouchHelperCallback(Builder builder) {
        this(builder.dragDirs, builder.swipeDirs);
        swipeEnabled = builder.swipeEnabled;
        onItemSwipeLeftListener = builder.onItemSwipeLeftListener;
        onItemSwipeRightListener = builder.onItemSwipeRightListener;
       /* backGroundPaint = new Paint();
        textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);*/
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return swipeEnabled;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        if (direction == ItemTouchHelper.LEFT) {
            onItemSwipeLeftListener.onItemSwipedLeft(position);
        } else if (direction == ItemTouchHelper.RIGHT) {
            onItemSwipeRightListener.onItemSwipedRight(position);
        }
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
                viewHolder).getBinding()).itemContainer;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
            viewHolder).getBinding()).itemContainer;

        drawBackground(viewHolder, dX, actionState);
        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
            actionState, isCurrentlyActive);
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
            viewHolder).getBinding()).itemContainer;
        drawBackground(viewHolder, dX, actionState);
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
            actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        final View backgroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
            viewHolder).getBinding()).viewMoveToBackground;
        final View foregroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
            viewHolder).getBinding()).itemContainer;
        backgroundView.setRight(0);
        getDefaultUIUtil().clearView(foregroundView);
    }

    private static void drawBackground(RecyclerView.ViewHolder viewHolder, float dX, int actionState) {

        final View backgroundView = ((AdapterRecentMessageItemBinding) ((MessageListAdapter.MessageViewHolder)
            viewHolder).getBinding()).viewMoveToBackground;

        Context context = viewHolder.itemView.getContext();
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            ConversationEntity item = null;
            boolean isRead;
            try {
                item = (ConversationEntity) viewHolder.itemView.getTag();
                isRead = item.isUnread();
            } catch (ClassCastException e) {
                item = null;
                isRead = false;
            }
            AppCompatTextView moveTo = ((AdapterRecentMessageItemBinding)
                ((MessageListAdapter.MessageViewHolder) viewHolder).getBinding()).moveToText;
            AppCompatTextView readUnread = ((AdapterRecentMessageItemBinding)
                ((MessageListAdapter.MessageViewHolder) viewHolder).getBinding()).readUnreadText;
            if (dX > 0) {
                readUnread.setVisibility(View.GONE);
                moveTo.setVisibility(View.VISIBLE);
                if (item != null && ConversationSubType.ARCHIVE == item.getSubType()) {
                    moveTo.setText(context.getString(R.string.action_un_archive));
                } else if (item != null && ConversationType.BLOCKED == item.getType()) {
                    moveTo.setText(context.getString(R.string.unblock));
                } else {
                    moveTo.setText(context.getString(R.string.move_to));
                }
                backgroundView.setRight((int) dX);
            } else {
                moveTo.setVisibility(View.GONE);
                readUnread.setVisibility(View.VISIBLE);
                String text = isRead ? context.getString(R.string.read_text) : context.getString(R.string.un_read_text);
                readUnread.setText(text);
                backgroundView.setRight(backgroundView.getWidth() - (int) dX);
            }
        }
    }


    // By using paint
  /*  @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View itemView = viewHolder.itemView;
        Context context = itemView.getContext();

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX > 0) {
                //color : left side (swiping towards right)
                //p.setARGB(255, 255, 0, 0);
                backGroundPaint.setColor(ContextCompat.getColor(context, R.color.pastel_gray));
                c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
                    (float) itemView.getBottom(), backGroundPaint);

                // text : left side (swiping towards right)
                textPaint.setColor(ContextCompat.getColor(context, R.color.davy_grey));
                textPaint.setTextSize(30);
                Typeface typeface = ResourcesCompat.getFont(context, R.font.roboto_light);
                textPaint.setTypeface(typeface);
                c.drawText(context.getString(R.string.move_to), (float) itemView.getLeft() + convertDpToPx(30, context),
                    ((float) itemView.getTop() + ((float) itemView.getBottom())) / 2,
                    textPaint);
            } else {
                //color : right side (swiping towards left)
                //p.setARGB(255, 0, 255, 0);
                backGroundPaint.setColor(ContextCompat.getColor(context, R.color.pastel_gray));
                c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                    (float) itemView.getRight(), (float) itemView.getBottom(), backGroundPaint);
                //text : left side (swiping towards right)
                textPaint.setColor(ContextCompat.getColor(context, R.color.davy_grey));
                textPaint.setTextSize(30);
                //textPaint.setTextAlign(TextPaint.Align.CENTER);
                Typeface typeface = ResourcesCompat.getFont(context, R.font.roboto_light);
                textPaint.setTypeface(typeface);
                boolean isRead;
                try {
                    isRead = (boolean) viewHolder.itemView.getTag();
                } catch (ClassCastException e) {
                    isRead = false;
                }
                String text = isRead ? context.getString(R.string.read_text) : context.getString(R.string.un_read_text);
                c.drawText(text, (float) itemView.getRight() - convertDpToPx(60, context),
                    ((float) itemView.getTop() + ((float) itemView.getBottom())) / 2,
                    textPaint);
            }
        }
        getDefaultUIUtil().onDraw(c, recyclerView, viewHolder.itemView, dX, dY, actionState, isCurrentlyActive);
    }*/

    private int convertDpToPx(int dp, Context context) {
        return Math.round(dp * (context.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
   /* @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        if (mSwipeBack) {
            mSwipeBack = false;
            return 0;
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setTouchListener(Canvas c,
                                  RecyclerView recyclerView,
                                  RecyclerView.ViewHolder viewHolder,
                                  float dX, float dY,
                                  int actionState, boolean isCurrentlyActive) {

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSwipeBack = event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP;
                return false;
            }
        });
    }*/

    public interface OnItemSwipeListener {

        void onItemSwiped(int position);

        void onItemSwipedLeft(int position);

        void onItemSwipedRight(int position);
    }

    public static final class Builder {
        private int dragDirs, swipeDirs;
        private Drawable drawableSwipeLeft, drawableSwipeRight;
        private int bgColorSwipeLeft, bgColorSwipeRight;
        private OnItemSwipeListener onItemSwipeLeftListener, onItemSwipeRightListener;
        private boolean swipeEnabled;

        public Builder(int dragDirs, int swipeDirs) {
            this.dragDirs = dragDirs;
            this.swipeDirs = swipeDirs;
        }

        public Builder drawableSwipeLeft(Drawable val) {
            drawableSwipeLeft = val;
            return this;
        }

        public Builder drawableSwipeRight(Drawable val) {
            drawableSwipeRight = val;
            return this;
        }

        public Builder bgColorSwipeLeft(int val) {
            bgColorSwipeLeft = val;
            return this;
        }

        public Builder bgColorSwipeRight(int val) {
            bgColorSwipeRight = val;
            return this;
        }

        public Builder onItemSwipeLeftListener(OnItemSwipeListener val) {
            onItemSwipeLeftListener = val;
            return this;
        }

        public Builder onItemSwipeRightListener(OnItemSwipeListener val) {
            onItemSwipeRightListener = val;
            return this;
        }

        public Builder setSwipeEnabled(boolean val) {
            swipeEnabled = val;
            return this;
        }

        public SwipeItemTouchHelperCallback build() {
            return new SwipeItemTouchHelperCallback(this);
        }

    }

    public void setSwipeEnabled(boolean val) {
        swipeEnabled = val;
    }
}
