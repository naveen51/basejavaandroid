package io.gupshup.smsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;

import static io.gupshup.smsapp.utils.PreferenceManager.Keys.API_KEY;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.COMPOSED_TEXT;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.IMEI_NUMBER;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.IMSI_NUMBER_SIM_1;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.IMSI_NUMBER_SIM_2;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.MEID_NUMBER;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.PUBLIC_KEY;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.SEQUENCE_NUMBER;
import static io.gupshup.smsapp.utils.PreferenceManager.Keys.SEQUENCE_NUMBER_ACTUAL_SLOT;


/**
 * Created by Ram Prakash Bhat on 13/4/18.
 */

public class PreferenceManager {


    public interface Keys {
        String FCM_TOKEN = "fcmId";
        String IS_REGISTRATION_DONE = "is_registration_done";
        String IS_MANUAL_REGISTRATION_TRIED = "is_manual_registration_tried";
        String IS_SMS_SENT = "is_sms_sent";
        String COUNTRY_CODE = "country_code";
        String SUBSCRIPTION_KEY = "subscription_key";
        String SUBSCRIBED_CARIER = "subscribed_carier";
        String SUBSCRIBED_SIM_SLOT_NO = "subscribed_sim_slot_no";
        String LAST_SYNC_TIME = "last_sync_time";
        String REGISTRATION_SIM_ONE_STATE = "registration_sim_one_state";
        String REGISTRATION_SIM_TWO_STATE = "registration_sim_two_state";
        String IMEI_NUMBER = "imei_number";
        String MEID_NUMBER = "meid_number";
        String IMSI_NUMBER_SIM_1 = "imsi_number_sim_one";
        String IMSI_NUMBER_SIM_2 = "imsi_number_sim_two";
        String SEQUENCE_NUMBER = "sequence_number";
        String PUBLIC_KEY = "public_key";
        String API_KEY = "api_key";
        String SEQUENCE_NUMBER_ACTUAL_SLOT = "sequence_number_actual_slot";
        String COMPOSED_TEXT = "composed_text";
        String MR_FOUR_SCREEN = "mr_four_view";
        String OTP_CONTENT = "otp";
        String OTP_SIM_SLOT = "slot";
        String OTP_SEQUENCE_NO = "sequence_no";
        String LONG_CODE_PRIMARY = "long code_primary";
        String LONG_CODE_SECONDARY = "long code_secondary";
        String IS_SIM_1_REGISTRATION_DONE = "sim1RegistrationDone";
        String IS_SIM_2_REGISTRATION_DONE = "sim2RegistrationDone";
        String HOME_OVERLAY_SHOW = "home_overlay_show";
        String IS_FIRST_LAUNCH = "is_first_launch";
        String IS_GET_LONG_CODE_API_SUCCESSFUL = "longCodeApiSuccessful";
        String CONTACT_LAST_SYNC_TIME = "contact_last_sync_time";
        String APP_ID = "app_id";
        String IS_NOTIFICATION_ENABLED = "is_notification_enabled";
        String NOTIFICATION_TONE_NAME = "notification tone name";
        String IS_VIBRATION_ENABLED = "is_vibration_enabled";
        String IS_NOTIFICATION_LIGHT_ENABLED = "is_notification_light_enabled";
        String IS_SIM_LAY_OUT_VISIBLE = "is_sim_lay_out_visible";
        String ACCOUNT_ID = "account_id";
        String API_KEY_SIM_1 = "api_key_sim_one";
        String API_KEY_SIM_2 = "api_key_sim_two";
        String SIM_CHANGED_STATUS = "sim_changed_status";
    }

    private static PreferenceManager sInstance;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;

    private PreferenceManager(Context ctx) {
        mPrefs = ctx.getApplicationContext().getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }

    public static PreferenceManager getInstance(Context ctx) {
        if (sInstance == null) {
            sInstance = new PreferenceManager(ctx);
        }
        return sInstance;
    }

    public void setRegistrationDone(boolean isDone) {
        mEditor.putBoolean(Keys.IS_REGISTRATION_DONE, isDone).apply();
    }

    public boolean isRegistrationDone() {
        return mPrefs.getBoolean(Keys.IS_REGISTRATION_DONE, false);
    }

    public void setManualRegistrationTried(boolean isTried) {
        mEditor.putBoolean(Keys.IS_MANUAL_REGISTRATION_TRIED, isTried).apply();
    }

    public boolean isManualRegistrationTried() {
        return mPrefs.getBoolean(Keys.IS_MANUAL_REGISTRATION_TRIED, false);
    }

    public void setSmsSent(boolean isTried) {
        mEditor.putBoolean(Keys.IS_SMS_SENT, isTried).apply();
    }

    public void setLongCodeFetchStatus(boolean isSuccessful) {
        mEditor.putBoolean(Keys.IS_GET_LONG_CODE_API_SUCCESSFUL, isSuccessful).apply();
    }

    public boolean isLongCodeFetchSuccessful() {
        return mPrefs.getBoolean(Keys.IS_GET_LONG_CODE_API_SUCCESSFUL, false);
    }

    //TODO: Make this method store custom class data....and remove separate methods for sim no and sequence no...
    public void setOtpContent(String otp) {
        mEditor.putString(Keys.OTP_CONTENT, otp).apply();
    }

    public void setOtpSimSlot(String slot) {
        mEditor.putString(Keys.OTP_SIM_SLOT, slot).apply();
    }

    public void setOtpSequence(String sequence) {
        mEditor.putString(Keys.OTP_SEQUENCE_NO, sequence).apply();
    }

    public void setFCMToken(String token) {
        mEditor.putString(Keys.FCM_TOKEN, token).apply();
    }

    public String getFCMToken() {
        return mPrefs.getString(Keys.FCM_TOKEN, AppConstants.EMPTY_TEXT);
    }

    public void setCountryCode(String countryCode) {
        mEditor.putString(Keys.COUNTRY_CODE, countryCode).apply();
    }

    public String getCountryCode() {
        return mPrefs.getString(Keys.COUNTRY_CODE, AppConstants.EMPTY_TEXT);
    }

    public void setLongCodePrimary(String longCodePrimary) {
        mEditor.putString(Keys.LONG_CODE_PRIMARY, longCodePrimary).apply();
    }

    public String getLongCodePrimary() {
        return mPrefs.getString(Keys.LONG_CODE_PRIMARY, "9223971017");
    }

    public void setLongCodeSecondary(String longCodePrimary) {
        mEditor.putString(Keys.LONG_CODE_SECONDARY, longCodePrimary).apply();
    }

    public String getLongCodeSecondary() {
        return mPrefs.getString(Keys.LONG_CODE_SECONDARY, "9212145567");
    }

    public void setSelectedSubscriptionID(int subscriptionID) {
        mEditor.putInt(Keys.SUBSCRIPTION_KEY, subscriptionID).apply();
    }

    public void setSelectedCarier(String carier, String slotNo) {
        mEditor.putString(Keys.SUBSCRIBED_CARIER, carier);
        mEditor.putString(Keys.SUBSCRIBED_SIM_SLOT_NO, slotNo);
        mEditor.apply();
    }

    public void setLastSyncTime(String lastSyncTime) {
        mEditor.putString(Keys.LAST_SYNC_TIME, lastSyncTime);
        mEditor.apply();
    }

    public String getLastSyncTime() {
        return mPrefs.getString(Keys.LAST_SYNC_TIME, "0");
    }

    public void setSim1RegistrationDone(boolean value) {
        mEditor.putBoolean(Keys.IS_SIM_1_REGISTRATION_DONE, value);
        mEditor.apply();
    }

    public boolean isSim1RegistrationDone() {
        return mPrefs.getBoolean(Keys.IS_SIM_1_REGISTRATION_DONE, false);
    }


    public void setSim2RegistrationDone(boolean value) {
        mEditor.putBoolean(Keys.IS_SIM_2_REGISTRATION_DONE, value);
        mEditor.apply();
    }

    public boolean isSim2RegistrationDone() {
        return mPrefs.getBoolean(Keys.IS_SIM_2_REGISTRATION_DONE, false);
    }

   /* public void setApiKey(String apiKey, String prefKey) {
        mEditor.putString(prefKey.concat(API_KEY_1), apiKey).apply();
    }

    public String getApiKey(String slotKey) {
        return mPrefs.getString(slotKey.concat(API_KEY_1), AppConstants.EMPTY_TEXT);
    }*/


    public void setSimTwoState(String simTwoState) {
        mEditor.putString(Keys.REGISTRATION_SIM_TWO_STATE, simTwoState).apply();
    }

    public String getSimTwoState() {
        return mPrefs.getString(Keys.REGISTRATION_SIM_TWO_STATE, AppConstants.EMPTY_TEXT);
    }


    public void setSimOneState(String simOneState) {
        mEditor.putString(Keys.REGISTRATION_SIM_ONE_STATE, simOneState).apply();
    }

    public String getSimOneState() {
        return mPrefs.getString(Keys.REGISTRATION_SIM_ONE_STATE, AppConstants.EMPTY_TEXT);
    }


   /* public void setSimOneRequestItem(String simOneRequestItem) {
        mEditor.putString(Keys.SIM_ONE_REQUEST_ITEM, simOneRequestItem).apply();
    }*/

    /*public String getAutoSimRequestItem(String simSlot) {
        return mPrefs.getString(simSlot, AppConstants.EMPTY_TEXT);
    }

    public void setAutoSimRequestItem(String simSlot, String simRequestItem) {
        mEditor.putString(simSlot, simRequestItem).apply();
    }*/

    public String getSimRequestItem(String sequenceNumber) {
        return mPrefs.getString(sequenceNumber, AppConstants.EMPTY_TEXT);
    }

    public void setSimRequestItem(String sequenceNumber, String simRequestItem) {
        mEditor.putString(sequenceNumber, simRequestItem).apply();
    }

   /* public String getSimTwoRequestItem() {
        return mPrefs.getString(Keys.SIM_TWO_REQUEST_ITEM, AppConstants.EMPTY_TEXT);
    }*/

    public void setImeiNumber(String prefKey, String imeiNumber) {
        mEditor.putString(prefKey.concat(IMEI_NUMBER), imeiNumber).apply();
    }

    public String getImeiNumber(String prefKey) {
        return mPrefs.getString(prefKey.concat(IMEI_NUMBER), AppConstants.EMPTY_TEXT);
    }

    public void setMeidNumber(String prefKey, String meidNumber) {
        mEditor.putString(prefKey.concat(MEID_NUMBER), meidNumber).apply();
    }

    public String getGetMeid(String prefKey) {
        return mPrefs.getString(prefKey.concat(MEID_NUMBER), AppConstants.EMPTY_TEXT);
    }

    public String getImsiNumber(int simSlotNo) {
        if (simSlotNo == 0) {
            return mPrefs.getString(IMSI_NUMBER_SIM_1, AppConstants.EMPTY_TEXT);
        } else {
            return mPrefs.getString(IMSI_NUMBER_SIM_2, AppConstants.EMPTY_TEXT);
        }
    }

    public void setImsiNumber(int simSlotNo, String imsi) {
        if (simSlotNo == 0) {
            mEditor.putString(IMSI_NUMBER_SIM_1, imsi).apply();
        } else {
            mEditor.putString(IMSI_NUMBER_SIM_2, imsi).apply();
        }
    }

    /**
     * Call this to clear preference value on re-register,account expired condition etc etc.
     */
    public void clearAllPreferencevalue() {
        mEditor.clear();
        mEditor.commit();
    }

    public void setSequenceNumber(String slotKey, String sequenceNumber) {
        mEditor.putString(slotKey.concat(SEQUENCE_NUMBER), sequenceNumber).apply();
    }

    public String getSequenceNumber(String slotKey) {
        return mPrefs.getString(slotKey.concat(SEQUENCE_NUMBER), AppConstants.EMPTY_TEXT);
    }


    public void setSequenceNumberForActualSlot(String slotKey, String sequenceNumber) {
        mEditor.putString(slotKey.concat(SEQUENCE_NUMBER_ACTUAL_SLOT), sequenceNumber).apply();
    }

    public String getSequenceNumberFromActualSlot(String slotKey) {
        return mPrefs.getString(slotKey.concat(SEQUENCE_NUMBER_ACTUAL_SLOT), AppConstants.EMPTY_TEXT);
    }

    public void clearSimPreference(String simSlot, String sequenceNumber) {
        mEditor.remove(simSlot.concat(API_KEY)).commit();
        mEditor.remove(simSlot.concat(PUBLIC_KEY)).commit();
        mEditor.remove(simSlot.concat(SEQUENCE_NUMBER)).commit();
        mEditor.putBoolean(Keys.IS_REGISTRATION_DONE, false).commit();
        mEditor.putString(simSlot, AppConstants.EMPTY_TEXT);
        mEditor.putString(sequenceNumber, AppConstants.EMPTY_TEXT);
        if (Integer.parseInt(simSlot) == 0) {
            mEditor.putString(Keys.REGISTRATION_SIM_ONE_STATE, RegistrationSimOneState.SIM_ONE_STATE_CHANGED.toString()).commit();
        } else {
            mEditor.putString(Keys.REGISTRATION_SIM_TWO_STATE, RegistrationSimTwoState.SIM_TWO_STATE_CHANGED.toString()).commit();
        }
        //mEditor.remove(sequenceNumber).commit();
        mEditor.remove(simSlot.concat(IMEI_NUMBER)).commit();
        mEditor.remove(simSlot.concat(MEID_NUMBER)).commit();
    }


    public void setComposedText(String composedText) {
        mEditor.putString(Keys.COMPOSED_TEXT, composedText).apply();
    }

    public String getComposedText() {
        return mPrefs.getString(Keys.COMPOSED_TEXT, AppConstants.EMPTY_TEXT);
    }

    public void setMrFourTextVisibility(boolean isMrFourScreen) {
        mEditor.putBoolean(Keys.MR_FOUR_SCREEN, isMrFourScreen).apply();
    }

    public boolean isMrFourScreenView() {
        return mPrefs.getBoolean(Keys.MR_FOUR_SCREEN, false);
    }


    public void removeCoposedText() {
        mEditor.remove(COMPOSED_TEXT).commit();
    }

    public void setHomeOverlayShow(boolean isShow) {
        mEditor.putBoolean(Keys.HOME_OVERLAY_SHOW, isShow).apply();
    }

    public boolean isHomeOverlayShow() {
        return mPrefs.getBoolean(Keys.HOME_OVERLAY_SHOW, false);
    }

    public void setIsFirstLaunch(boolean isFirst) {
        mEditor.putBoolean(Keys.IS_FIRST_LAUNCH, isFirst).apply();
    }

    public boolean isFirstLaunch() {
        return mPrefs.getBoolean(Keys.IS_FIRST_LAUNCH, true);
    }

    public void setContactLastSyncTime(String lastSyncTime) {
        mEditor.putString(Keys.CONTACT_LAST_SYNC_TIME, lastSyncTime);
        mEditor.apply();
    }

    public String getContactLastSyncTime() {
        return mPrefs.getString(Keys.CONTACT_LAST_SYNC_TIME, "0");
    }

    public String getNotificationTone() {
        return mPrefs.getString(Keys.NOTIFICATION_TONE_NAME, AppConstants.EMPTY_TEXT);
    }

    public void setNotificationTone(String toneName) {
        mEditor.putString(Keys.NOTIFICATION_TONE_NAME, toneName);
        mEditor.apply();
    }

    public void setIsNotificationLightEnabled(boolean isNotificationLightEnabled) {
        mEditor.putBoolean(Keys.IS_NOTIFICATION_LIGHT_ENABLED, isNotificationLightEnabled).apply();
    }

    public boolean isNotificationLightEnabled() {
        return mPrefs.getBoolean(Keys.IS_NOTIFICATION_LIGHT_ENABLED, true);
    }

    public void setIsVibrationEnabled(boolean isVibrationEnabled) {
        mEditor.putBoolean(Keys.IS_VIBRATION_ENABLED, isVibrationEnabled).apply();
    }

    public boolean isVibrationEnabled() {
        return mPrefs.getBoolean(Keys.IS_VIBRATION_ENABLED, true);
    }

    public void setIsNotificationEnabled(boolean isNotificationEnabled) {
        mEditor.putBoolean(Keys.IS_NOTIFICATION_ENABLED, isNotificationEnabled).apply();
    }

    public boolean isNotificationEnabled() {
        return mPrefs.getBoolean(Keys.IS_NOTIFICATION_ENABLED, true);
    }


    public void setSimLayoutVisibility(boolean visible) {
        mEditor.putBoolean(Keys.IS_SIM_LAY_OUT_VISIBLE, visible).apply();
    }

    public boolean getSimLayoutVisibility() {
        return mPrefs.getBoolean(Keys.IS_SIM_LAY_OUT_VISIBLE, false);
    }

    public void setAppId(String appId) {
        mEditor.putString(Keys.APP_ID, appId);
        mEditor.apply();
    }

    public String getAppId() {
        return mPrefs.getString(Keys.APP_ID, "0");
    }

    public String getAccountId() {
        return mPrefs.getString(Keys.ACCOUNT_ID, "0");
    }

    public void setAccountId(String accId) {
        mEditor.putString(Keys.ACCOUNT_ID, accId);
        mEditor.apply();
    }

    public String getApiKeyForSim1() {
        return mPrefs.getString(Keys.API_KEY_SIM_1, "0");
    }

    public void setApiKeyForSim1(String apiKeyForSim1) {
        mEditor.putString(Keys.API_KEY_SIM_1, apiKeyForSim1);
        mEditor.apply();
    }

    public String getApiKeyForSim2() {
        return mPrefs.getString(Keys.API_KEY_SIM_2, "0");
    }

    public void setApiKeyForSim2(String apiKeyForSim2) {
        mEditor.putString(Keys.API_KEY_SIM_2, apiKeyForSim2);
        mEditor.apply();
    }


    public String getSimChangeStatus() {
        return mPrefs.getString(Keys.SIM_CHANGED_STATUS, AppConstants.EMPTY_TEXT);
    }

    public void setSimChangeStatus(String simChangeStatus) {
        mEditor.putString(Keys.SIM_CHANGED_STATUS, simChangeStatus).apply();
    }

}
