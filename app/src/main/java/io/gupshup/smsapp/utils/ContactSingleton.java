package io.gupshup.smsapp.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import io.gupshup.smsapp.data.model.contacts.Contact;

/**
 * Created by Naveen BM on 4/13/2018.
 */
public class ContactSingleton {

    private static ContactSingleton INSTANCE = null;

    private Map<String,Contact> mSelectedContactList;

    private Map<String,Contact> mShareSelectedContactList;

    private ContactSingleton() {
    }

    public static ContactSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ContactSingleton();
        }
        return (INSTANCE);
    }

    public void addContact(Contact contact) {

        if (mSelectedContactList == null) {
            mSelectedContactList = new LinkedHashMap<>();
        }
        mSelectedContactList.put(contact.getContactPhoneNumber(),contact);
    }

    public Map<String,Contact> getSelectedContacts() {
        return mSelectedContactList == null ? mSelectedContactList = new LinkedHashMap<>() : mSelectedContactList;
    }

    public void removeSelectedContacts() {
        if(mSelectedContactList != null){
            mSelectedContactList.clear();
        }
    }


    public void removeSelectedContact(Contact contact) {
        if (mSelectedContactList.size() > 0) {
            mSelectedContactList.remove(contact.getContactPhoneNumber());
        }
    }



    public void addShareContact(Contact contact) {

        if (mShareSelectedContactList == null) {
            mShareSelectedContactList = new LinkedHashMap<>();
        }
        mShareSelectedContactList.put(contact.getContactPhoneNumber(),contact);
    }


    public Map<String,Contact> getSelectedSharedContacts() {
        return mShareSelectedContactList == null ? mShareSelectedContactList = new LinkedHashMap<>() : mShareSelectedContactList;
    }


    public void removeSelectedSharedContacts() {
        if(mShareSelectedContactList != null){
            mShareSelectedContactList.clear();
        }
    }


    public void removeSelectedSharedContact(Contact contact) {
        if (mShareSelectedContactList.size() > 0) {
            mShareSelectedContactList.remove(contact.getContactPhoneNumber());
        }
    }
}
