package io.gupshup.smsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import io.gupshup.smsapp.R;

/**
 * Created by Ram Prakash Bhat on 16/4/18.
 */

public class DialogUtils {

    public interface DialogClickListener {

        void onButtonClicked(DialogInterface dialog, int which);

    }


    public static AlertDialog getCustomDialog(Context ctx, View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setView(view);

        return dialog;
    }

    public static void hideKeyboard(Context ctx) {
        if (ctx == null) return;
        InputMethodManager inputManager = (InputMethodManager) ctx
            .getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void dismissKeyboard(Context ctx, EditText editText) {
        final InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void showKeyBoard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void goToAppSettings(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", activity.getPackageName(), null));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }


    public static void showAlertDialog(Context ctx, String msg, String msgTitle, String positiveBtn, final DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(msgTitle);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton(positiveBtn,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener != null) {
                        listener.onClick(dialog, which);
                    } else {
                        dialog.cancel();
                    }
                }
            });

        builder.show();

    }

    public static void showConfirmPopUp(Context ctx, String message, String msgTitle,
                                              String positiveBtn, String negativeBtn,
                                              final DialogClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(msgTitle);
        builder.setIcon(R.drawable.sending_failed_alert);
        builder.setMessage(message);
        builder.setPositiveButton(positiveBtn,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener != null) {
                        listener.onButtonClicked(dialog, which);
                    } else {
                        dialog.cancel();
                    }
                }
            });
        builder.setNegativeButton(negativeBtn,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener != null) {
                        listener.onButtonClicked(dialog, which);
                    } else {
                        dialog.cancel();
                    }
                }
            });

        builder.show();

    }


}
