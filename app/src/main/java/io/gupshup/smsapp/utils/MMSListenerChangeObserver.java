package io.gupshup.smsapp.utils;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.Telephony;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import io.gupshup.smsapp.ui.earlierSMS.FetchEarlierSMS;

/**
 * Created by Ram Prakash Bhat on 23/5/18.
 */

public class MMSListenerChangeObserver extends ContentObserver {

    Context con;
    public MMSListenerChangeObserver(Handler handler, Context ctx) {
        super(handler);
        con=ctx;
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange,null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        //Write your code here
        //Whatever is written here will be
        //executed whenever a change is made

        LogUtility.d("Change", "Change triggered");
        Cursor mCursor = con.getContentResolver().query(Telephony.Mms.CONTENT_URI, null, null,
                null, null);
        mCursor.moveToNext();
        int type = mCursor.getInt(mCursor.getColumnIndex("type"));

        FetchEarlierSMS fetchMessages = new FetchEarlierSMS(con);
        fetchMessages.importCurrentMMS();
        /*if(type==1){
            //it's received MMS
            LogUtility.v("Debug", "it's received MMS");
            getReceivedMMSinfo();
        }
        else if(type==2)
        {
            //it's sent MMS
            LogUtility.v("Debug", "it's Sent MMS");
            getSentMMSinfo();
        }*/
    }



    // 1. method to get details about Received (inbox)  MMS...
    private void getReceivedMMSinfo() {
        Uri uri = Uri.parse("content://mms/inbox");
        String str = "";
        Cursor cursor = con.getContentResolver().query(uri, null,null,
                null, null);
        cursor.moveToNext();

        String mms_id= cursor.getString(cursor.
                getColumnIndex("_id"));
        String phone = cursor.getString(cursor.
                getColumnIndex("address"));
        String dateVal = cursor.getString(cursor.
                getColumnIndex("date"));
        Date date = new Date(Long.valueOf(dateVal));

        // 2 = sent, etc.
        int mtype = cursor.getInt(cursor.
                getColumnIndex("type"));
        String body="";

        Bitmap bitmap;

        String type = cursor.getString(cursor.
                getColumnIndex("ct"));
        if ("text/plain".equals(type)){
            String data = cursor.getString(cursor.
                    getColumnIndex("body"));
            if(data != null){
                body = getReceivedMmsText(mms_id);
            }
            else {
                body = cursor.getString(cursor.
                        getColumnIndex("text"));
                //body text is stored here
            }
        }
        else if("image/jpeg".equals(type) ||
                "image/bmp".equals(type) ||
                "image/gif".equals(type) ||
                "image/jpg".equals(type) ||
                "image/png".equals(type)){
            bitmap = getReceivedMmsImage(mms_id);
            //image is stored here
            //now we are storing on SDcard
        }

        str = "Sent MMS: \n phone is: " + phone;
        str +="\n MMS type is: "+mtype;
        str +="\n MMS time stamp is:"+date;
        str +="\n MMS body is: "+body;
        str +="\n id is : "+mms_id;


        LogUtility.v("Debug","sent MMS phone is: "+phone);
        LogUtility.v("Debug","MMS type is: "+mtype);
        LogUtility.v("Debug","MMS time stamp is:"+date);
        LogUtility.v("Debug","MMS body is: "+body);
        LogUtility.v("Debug","MMS id is: "+mms_id);
    }


    /* .......methods to get details about Sent MMS.... */
    private void getSentMMSinfo() {


        Uri uri = Uri.parse("content://mms/sent");
        String str = "";
        Cursor cursor = con.getContentResolver().query(uri,
                null,null,
                null, null);
        cursor.moveToNext();

        String mms_id= cursor.getString(cursor.
                getColumnIndex("_id"));
        String phone = cursor.getString(cursor.
                getColumnIndex("address"));
        String dateVal = cursor.getString(cursor.
                getColumnIndex("date"));
        Date date = new Date(Long.valueOf(dateVal));
        // 2 = sent, etc.
        int mtype = cursor.getInt(cursor.
                getColumnIndex("type"));
        String body="";

        Bitmap bitmap;

        String type = cursor.getString(cursor.
                getColumnIndex("ct"));
        if ("text/plain".equals(type)){
            String data = cursor.getString(cursor.
                    getColumnIndex("body"));
            if(data != null){
                body = getSentMmsText(mms_id);
            }
            else {
                body = cursor.getString(cursor.
                        getColumnIndex("text"));
                //body text is stored here
            }
        }
        else if("image/jpeg".equals(type) ||
                "image/bmp".equals(type) ||
                "image/gif".equals(type) ||
                "image/jpg".equals(type) ||
                "image/png".equals(type)){
            bitmap = getSentMmsImage(mms_id);
            //image is stored here
            //now we are storing on SDcard
        }

        str = "Sent MMS: \n phone is: " + phone;
        str +="\n MMS type is: "+mtype;
        str +="\n MMS time stamp is:"+date;
        str +="\n MMS body is: "+body;
        str +="\n id is : "+mms_id;


        LogUtility.v("Debug","sent MMS phone is: "+phone);
        LogUtility.v("Debug","MMS type is: "+mtype);
        LogUtility.v("Debug","MMS time stamp is:"+date);
        LogUtility.v("Debug","MMS body is: "+body);
        LogUtility.v("Debug","MMS id is: "+mms_id);


    }


    //method to get Text body from Received MMS.........
    private String getReceivedMmsText(String id) {
        Uri partURI = Uri.parse("content://mms/inbox" + id);
        InputStream is = null;
        StringBuilder sb = new StringBuilder();
        try {
            is = con.getContentResolver().openInputStream(partURI);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is,
                        "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String temp = reader.readLine();
                while (temp != null) {
                    sb.append(temp);
                    temp = reader.readLine();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    //method to get image from Received MMS..............
    private Bitmap getReceivedMmsImage(String id) {
        Uri partURI = Uri.parse("content://mms/inbox" + id);
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = con.getContentResolver().openInputStream(partURI);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;

    }

    //method to get Text body from Sent MMS............
    private String getSentMmsText(String id) {

        Uri partURI = Uri.parse("content://mms/sent" + id);
        InputStream is = null;
        StringBuilder sb = new StringBuilder();
        try {
            is = con.getContentResolver().openInputStream(partURI);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is,
                        "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String temp = reader.readLine();
                while (temp != null) {
                    sb.append(temp);
                    temp = reader.readLine();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();

    }

    //method to get image from sent MMS............
    private Bitmap getSentMmsImage(String id) {

        Uri partURI = Uri.parse("content://mms/sent" + id);
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = con.getContentResolver().
                    openInputStream(partURI);
            bitmap = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;

    }

}
