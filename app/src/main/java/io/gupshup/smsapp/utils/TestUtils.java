package io.gupshup.smsapp.utils;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.Author;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

/*
 * @author  Bhargav Kolla
 * @since   Apr 18, 2018
 */
public final class TestUtils {

    public static final RoomDatabase.Callback POPULATE_WITH_TEST_DATA =
        new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        int noOfConversations = 1000;
                        int noOfConversationsPerType = noOfConversations / 4;
                        int noOfMessagesPerConversation = 10;

                        GupshupPublicKey publicKey = new GupshupPublicKey("AnzPVAwFeMIYOBBP+OGNCIvgDRP0WYT4tuDFWHE0aqhd");
                        long currentMillis = new Date().getTime();

                        List<ConversationEntity> conversationEntities = new ArrayList<>();
                        List<MessageEntity> messageEntities = new ArrayList<>();
                        List<MessageEntity> lastMessageEntities = new ArrayList<>();

                        for (int i = 0; i < noOfConversations; i++) {
                            String mask = "MASK-" + (i + 1);
                            String display = "Display " + (i + 1);
                            ConversationType type = (
                                (i < noOfConversationsPerType) ? ConversationType.PERSONAL :
                                    (i < 2 * noOfConversationsPerType) ? ConversationType.TRANSACTIONAL :
                                        (i < 3 * noOfConversationsPerType) ? ConversationType.PROMOTIONAL : ConversationType.BLOCKED
                            );

                            ConversationEntity conversationEntity = new ConversationEntity();

                            conversationEntities.add(conversationEntity);
                            conversationEntity.setMask(mask);
                            conversationEntity.setChannels(Arrays.asList(MessageChannel.SMS, MessageChannel.DATA));
                            conversationEntity.setIpPublicKey(publicKey);
                            conversationEntity.setIpLastSyncedTimestamp(-1);
                            conversationEntity.setDisplay(display);
                            conversationEntity.setImage(null);
                            conversationEntity.setPbLastSyncedTimestamp(-1);
                            conversationEntity.setType(type);
                            conversationEntity.setTypeSetBy(Author.APP);
                            conversationEntity.setSubType(null);
                            conversationEntity.setDraftMessage(null);
                            conversationEntity.setLastMessageId(null);
                            conversationEntity.setLastMessageData(null);
                            conversationEntity.setLastMessageTimestamp(-1);
                            conversationEntity.setNoOfUnreadMessages(noOfMessagesPerConversation);
                            conversationEntity.setSendNotifications(false);
                            conversationEntity.setNotificationsSound(false);
                            conversationEntity.setNotificationsLight(false);
                            conversationEntity.setNotificationsPopup(false);

                            for (int j = 0; j < noOfMessagesPerConversation; j++) {
                                String messageId = UUID.randomUUID().toString();
                                GupshupMessage message = new GupshupTextMessage("This is a test message [" + (i + 1) + "] [" + (j + 1) + "]");

                                MessageEntity messageEntity = new MessageEntity();

                                messageEntities.add(messageEntity);
                                messageEntity.setMessageId(messageId);
                                messageEntity.setMask(mask);
                                messageEntity.setDirection(MessageDirection.INCOMING);
                                messageEntity.setContent(message);
                                messageEntity.setChannel(MessageChannel.SMS);
                                messageEntity.setDestination(mask);
                                messageEntity.setSentTimestamp(-1);
                                messageEntity.setMessageTimestamp(currentMillis);
                                messageEntity.setDeliveredTimestamp(currentMillis);
                                messageEntity.setReadTimestamp(-1);
                                messageEntity.setReadAcknowledged(false);
                                messageEntity.setMetadata(null);
                                messageEntity.setSearch(null);

                                if (j == noOfMessagesPerConversation - 1) {
                                    lastMessageEntities.add(messageEntity);
                                }

                                currentMillis++;
                            }
                        }

                        AppDatabase appDatabase = AppDatabase.getInstance();
                        ConversationDAO conversationDAO = appDatabase.conversationDAO();
                        MessageDAO messageDAO = appDatabase.messageDAO();

                        long[] conversationIds =
                            conversationDAO.insert(conversationEntities.toArray(new ConversationEntity[conversationEntities.size()]));

                        for (int x = 0; x < conversationIds.length; x++) {
                            ConversationEntity conversationEntity = conversationEntities.get(x);

                            conversationEntity.setConversationId(conversationIds[x]);

                            updateConversation(conversationEntity, lastMessageEntities.get(x));
                        }

                        messageDAO.insert(messageEntities.toArray(new MessageEntity[messageEntities.size()]));

                        conversationDAO.update(conversationEntities.toArray(new ConversationEntity[conversationEntities.size()]));
                    }

                    private void updateConversation(ConversationEntity conversationEntity, MessageEntity lastMessageEntity) {
                        conversationEntity.setLastMessageId(lastMessageEntity.getMessageId());
                        conversationEntity.setLastMessageData(lastMessageEntity);
                        conversationEntity.setLastMessageTimestamp(lastMessageEntity.getMessageTimestamp());
                    }
                });
            }
        };

}
