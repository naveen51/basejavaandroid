package io.gupshup.smsapp.enums;

/**
 * Created by Naveen BM on 6/12/2018.
 */
public enum RegistrationSimOneState {
    SIM_ONE_FIRST_LAUNCH_NOT_REGISTERED ,
    SIM_ONE_REGISTRATION_SUCCESS ,
    SIM_ONE_REGISTRATION_FAILURE ,
    SIM_ONE_SIX_MONTHS_EXCEED,
    SIM_ONE_STATE_CHANGED,
    SIM_ONE_MANUAL_REGISTRATION,
    SIM_ONE_AUTO_REGISTRATION,
    SIM_ONE_REGISTRATION_IN_PROGRESS;


    public static RegistrationSimOneState toEnum (String enumString) {
        try {
            return valueOf(enumString);
        } catch (Exception ex) {
            // For error cases
            return SIM_ONE_FIRST_LAUNCH_NOT_REGISTERED;
        }
    }
}
