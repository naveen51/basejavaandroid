package io.gupshup.smsapp.enums;

/**
 * Created by Ram Prakash Bhat on 4/5/18.
 */

public enum ActionModeType {

    PERSONAL, TRANSACTIONAL, PROMOTIONAL, BLOCKED, ARCHIVE, MUTE, MOVE, DELETE, ADD_TO_CONTACT,
    MARK_AS_READ,UN_BLOCK,UN_MUTE,SHARE,COPY,INFO,FORWARD,UN_ARCHIVE
}
