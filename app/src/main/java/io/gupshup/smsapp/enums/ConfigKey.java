package io.gupshup.smsapp.enums;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public enum ConfigKey {
    PHONE_NUMBER_SIM_1,
    PHONE_NUMBER_SIM_2,
    API_KEY_1,
    API_KEY_2,
    IP_PRIVATE_KEY_1,
    IP_PRIVATE_KEY_2,
    PN_TOKEN
}
