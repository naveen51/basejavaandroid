package io.gupshup.smsapp.enums;

public enum GupshupMessageStatus {
    MSG("msg"),
    READ("read"),
    DLVR("dlvr"),
    DACK("dack"),
    RACK("rack"),
    FAIL("fail");

    private String status;

    GupshupMessageStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
