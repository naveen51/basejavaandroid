package io.gupshup.smsapp.enums;

/*
 * @author  Bhargav Kolla
 * @since   Apr 10, 2018
 */
public enum MessageChannel {

    SMS, DATA

}
