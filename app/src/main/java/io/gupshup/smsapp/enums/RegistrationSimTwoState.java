package io.gupshup.smsapp.enums;

/**
 * Created by Naveen BM on 6/12/2018.
 */
public enum RegistrationSimTwoState {
    SIM_TWO_FIRST_LAUNCH_NOT_REGISTERED,
    SIM_TWO_REGISTRATION_SUCCESS,
    SIM_TWO_REGISTRATION_FAILURE,
    SIM_TWO_SIX_MONTHS_EXCEED,
    SIM_TWO_STATE_CHANGED,
    SIM_TWO_MANUAL_REGISTRATION,
    SIM_TWO_AUTO_REGISTRATION,
    SIM_TWO_REGISTRATION_IN_PROGRESS;


    public static RegistrationSimTwoState toEnum(String enumString) {
        try {
            return valueOf(enumString);
        } catch (Exception ex) {
            // For error cases
            return SIM_TWO_FIRST_LAUNCH_NOT_REGISTERED;
        }
    }
}
