package io.gupshup.smsapp.enums;

public enum MessageStatus {
    FAILED, DELIVERED, READ, SENT, SENDING, RECEIVED, QUEUED, DRAFT
}
