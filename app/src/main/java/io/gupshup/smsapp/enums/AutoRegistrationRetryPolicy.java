package io.gupshup.smsapp.enums;

/**
 * Created by Ram Prakash Bhat on 21/6/18.
 */

public enum AutoRegistrationRetryPolicy {
    ONE_TIME, TWO_TIME, DONE;

    public static AutoRegistrationRetryPolicy toEnum(String enumString) {
        try {
            return valueOf(enumString);
        } catch (Exception ex) {
            // For error cases
            return DONE;
        }
    }
}
