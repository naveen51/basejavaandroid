package io.gupshup.smsapp.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import io.gupshup.smsapp.dagger.AppComponent;
import io.gupshup.smsapp.dagger.AppModule;
import io.gupshup.smsapp.dagger.DaggerAppComponent;
import io.gupshup.smsapp.networking.NetworkModule;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * This is the application class ,
 * will initialize requiered components while launching the application
 */
public class MainApplication extends Application {
    private static final String TAG = "GS_SMS_APP";
    private static MainApplication instance;
    private AppComponent mAppComponent;
    private boolean mIsApplicationActive;
    private String mCurrentActiveChatId = AppConstants.EMPTY_TEXT;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        createDaggerComponent();
    }

    public void createDaggerComponent() {
        mAppComponent = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .networkModule(new NetworkModule(getContext()))
            .build();
    }

    public static Context getContext() {
        return instance;
    }

    public static MainApplication getInstance() {
        return instance;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public boolean isAppActive() {
        return mIsApplicationActive;
    }

    public void setIsApplicationActive(boolean isActive) {
        this.mIsApplicationActive = isActive;
    }

    public String getActiveChatId() {
        return mCurrentActiveChatId;
    }

    public void setCurrentActiveChatId(String chatId) {
        this.mCurrentActiveChatId = chatId;
    }
}
