package io.gupshup.smsapp.ui.compose.view;

import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import java.util.Map;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.databinding.LayoutShareContactActivityBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.events.ShareContactEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.ShareContactAdapter;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.compose.viewmodel.ShareContactViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.ContactSingleton;

/**
 * Created by Naveen BM on 8/21/2018.
 */
public class ShareContactActivity extends BaseActivity implements BaseHandlers, RxBusCallback {

    private static final String TAG = "SHARECONTACTACTIVITY";
    private LayoutShareContactActivityBinding mShareBinding;
    private RecyclerView mShareContactRecycler;
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;
    private ShareContactViewModel shareContactViewModel;

    String[] projection = new String[]{
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
        ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI,
        ContactsContract.CommonDataKinds.Email.CONTACT_ID,
        ContactsContract.CommonDataKinds.Email.ADDRESS,
        ContactsContract.CommonDataKinds.Phone.TYPE
        //plus any other properties you wish to query
    };

    Cursor mCursor = null;
    private ShareContactAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        mShareBinding = DataBindingUtil.setContentView(this, R.layout.layout_share_contact_activity);
        shareContactViewModel = new ShareContactViewModel(MainApplication.getInstance());
        mShareBinding.setShareContactViewModel(shareContactViewModel);
        mShareBinding.setHandlers(this);
        registerEvents();
        mShareContactRecycler = mShareBinding.shareRecycler;
        setToolBar();
    }


    @Override
    protected void onStart() {
        super.onStart();
        LinearLayoutManager manager = new LinearLayoutManager(this);
        try {
            mCursor = getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                    null, null,
                    ContactsContract.Contacts.SORT_KEY_PRIMARY + " ASC");
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        mAdapter = new ShareContactAdapter(this, mCursor);
        mShareContactRecycler.setLayoutManager(manager);
        mShareContactRecycler.setAdapter(mAdapter);
    }

    private void setToolBar() {
        setSupportActionBar(mShareBinding.toolbarLayout.commonToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolBarTitle(getString(R.string.choose_contacts));
    }

    private void setToolBarTitle(String toolbarTitle) {
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                String sharedContact = constructShareContact();
                rxBus.send(new RxEvent(AppConstants.EVENT_SHARE_CONTACT_CLICK, new ShareContactEvent(sharedContact)));
                onBackPressed();
                break;
        }
    }

    /**
     * method used to construct share contact format
     */
    private String constructShareContact() {
        StringBuilder builder = new StringBuilder();
        Map<String, Contact> contactList = ContactSingleton.getInstance().getSelectedSharedContacts();
        for (Map.Entry<String, Contact> pair : contactList.entrySet()) {
            Contact contactSingle = pair.getValue();
            if (contactSingle != null) {
                builder.append(getString(R.string.share_format, contactSingle.getContactName(), contactSingle.getContactPhoneNumber()));
            }
        }

        return builder.toString();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onEventTrigger(Object object) {
        RxEvent event = (RxEvent) object;
        if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_NOTIFY_ADAPTER)) {
            Contact contact = (Contact) event.getEventData();
            notifyAdapter(contact);
            updateToolBarTitle();
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_CONTACT_SEARCH_FILTER)) {
            String searchFilter = (String) event.getEventData();
            if (mAdapter != null) {
                if (!TextUtils.isEmpty(searchFilter)) {
                    Cursor cursor = null;
                    try {
                        String[] selectionArgs = {"%" + searchFilter + "%", "%" + searchFilter + "%"};
                        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE ? OR " + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ?", selectionArgs,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                    mAdapter.updateCursor(cursor);
                } else {
                    mAdapter.updateCursor(mCursor);
                }

            }
        }
    }

    private void updateToolBarTitle() {
        if (ContactSingleton.getInstance().getSelectedSharedContacts() != null) {
            int size = ContactSingleton.getInstance().getSelectedSharedContacts().values().size();
            if (size > 0) {
                shareContactViewModel.showShareDoneButton.set(true);
                String constructTitle = getString(R.string.share_selected, size);
                setToolBarTitle(constructTitle);
            } else {
                shareContactViewModel.showShareDoneButton.set(false);
                setToolBarTitle(getString(R.string.choose_contacts));
            }
        }
    }

    /**
     * Notify perticular adapter
     */
    private void notifyAdapter(Contact contact) {
        mAdapter.updateSingletonLocalContactList();
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    protected void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        //clear shared contacts
        ContactSingleton.getInstance().removeSelectedSharedContacts();
        super.onBackPressed();
    }
}
