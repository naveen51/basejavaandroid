package io.gupshup.smsapp.ui.reply.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.events.ForwardMessageEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 5/24/2018.
 */
public class ForwardMessageViewModel extends BaseFragmentViewModel {
    private static final String TAG = "FORWARDMESSAGEVIEWMODEL";
    @Inject
    public RxBus rxBus;

    public ForwardMessageViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void itemClickForForward(View view, MessageEntity entity, String mask) {
        GupshupTextMessage textMessage = (GupshupTextMessage) entity.getContent();
        LogUtility.d(TAG, "-- BODY -----" + textMessage.getText() + " ------- Phone -------" + mask);
        RxEvent event = new RxEvent(AppConstants.FORWARD_MESSAGE_EVENT, new ForwardMessageEvent(entity, mask));
        rxBus.send(event);
        onDialogCancelClicked(view);
    }


    public void newMessageClicked(View view, MessageEntity entity) {
        GupshupTextMessage textMessage = (GupshupTextMessage) entity.getContent();
        RxEvent event = new RxEvent(AppConstants.NEW_MESSAGE_CLICK_EVENT, textMessage.getText());
        rxBus.send(event);
    }


    public void onDialogCancelClicked(View view) {
        RxEvent event = new RxEvent(AppConstants.FORWARD_DIALOG_CANCEL_CLICKED, null);
        rxBus.send(event);
    }
}
