package io.gupshup.smsapp.ui.home.viewmodel;


import android.app.Application;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.smsapp.Config;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.networking.responses.AutoVerificationResponse;
import io.gupshup.smsapp.networking.responses.ContactPhoneResponse;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.service.RegistrationService;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.ui.home.view.HomePagerFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityViewModel extends BaseFragmentViewModel {

    @Inject
    public APIService mService;
    @Inject
    public RxBus rxBus;

    public ObservableBoolean mProgressBar = new ObservableBoolean();
    public ObservableBoolean mInstructionOverLayVisibility = new ObservableBoolean();

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void callAutoRegistrationAPI(final Context context, final int simSlot) {

        final String sequenceNumber = AppUtils.generateRandomNumber(4);
        final String apiKey = AppUtils.fetchApiKey(simSlot);
        String ipPublicKey = "";
        final GupshupKeyPair gupshupKeyPair = AppUtils.generateGupshupKeyPair();
        if (gupshupKeyPair != null) {
            ipPublicKey = gupshupKeyPair.getPublicKey().toString();
        }
        final String finalIpPublicKey = ipPublicKey;

        //String acccountId = Config.ACCOUNT_ID;
        String acccountId = PreferenceManager.getInstance(context).getAccountId();
        if (acccountId.equalsIgnoreCase("0")) {
            acccountId = Config.ACCOUNT_ID;
        }
        if (simSlot == 0) {
            //LogUtility.d("auto", "Calling autoRegisterSIM1 ");
            RegistrationService.getInstance().autoRegisterSimNo1(context, acccountId, ipPublicKey,
                apiKey, simSlot, new Callback<ContactPhoneResponse>() {
                    @Override
                    public void onResponse(Call<ContactPhoneResponse> call,
                                           Response<ContactPhoneResponse> response) {
                        //final Handler handler = new Handler(Looper.getMainLooper());
                        AppUtils.saveSimIMSINoForParticularSlot(context, simSlot);
                        MainActivity.mHandler.postDelayed(new AutoRegistrationResponse(simSlot, finalIpPublicKey,
                            gupshupKeyPair.getPrivateKey().toString(),
                            context, apiKey, sequenceNumber), 45000);
                    }

                    @Override
                    public void onFailure(Call<ContactPhoneResponse> call, Throwable t) {
                        updatePreferenceDataOnFailure(simSlot, context, sequenceNumber);
                    }
                });
        } else {
            //LogUtility.d("auto", "Calling autoRegisterSIM2 ");
            RegistrationService.getInstance().autoRegisterSimNo2(context, acccountId, ipPublicKey,
                apiKey, simSlot, new Callback<ContactPhoneResponse>() {
                    @Override
                    public void onResponse(Call<ContactPhoneResponse> call,
                                           Response<ContactPhoneResponse> response) {
                        //final Handler handler = new Handler(Looper.getMainLooper());
                        // LogUtility.d("auto", "Response received from autoRegisterSIM2 with response "+response.isSuccessful());
                        AppUtils.saveSimIMSINoForParticularSlot(context, simSlot);
                        MainActivity.mHandler.postDelayed(new AutoRegistrationResponse(simSlot, finalIpPublicKey,
                            gupshupKeyPair.getPrivateKey().toString(),
                            context, apiKey, sequenceNumber), 45000);
                    }

                    @Override
                    public void onFailure(Call<ContactPhoneResponse> call, Throwable t) {
                        updatePreferenceDataOnFailure(simSlot, context, sequenceNumber);
                    }
                });
        }
    }

    class AutoRegistrationResponse implements Runnable {
        private int simSlot;
        private String publicKey;
        private String privateKey;
        private Context context;
        private String sequenceNumber;
        private String apiKey;

        public AutoRegistrationResponse(int simSlot, String publicKey, String privateKey, Context ctx, String apiKey, String sequenceNumber) {
            this.simSlot = simSlot;
            this.publicKey = publicKey;
            this.privateKey = privateKey;
            this.context = ctx;
            this.apiKey = apiKey;
            this.sequenceNumber = sequenceNumber;
        }

        @Override
        public void run() {
            //run code after 10 sec
            LogUtility.d("auto", "Phone no verification called ......for sim slot " + simSlot + "with public key : " + this.publicKey);
            RegistrationService.getInstance().getPhoneNumberVerificationStatus(this.publicKey, new Callback<AutoVerificationResponse>() {
                @Override
                public void onResponse(Call<AutoVerificationResponse> call, Response<AutoVerificationResponse> responseServe) {
                    LogUtility.d("auto", "onresponse of Phone Number Verification for sim slot " + simSlot);
                    AppUtils.logToFile(context, "onresponse of Phone Number Verification for sim slot " + simSlot);
                    RegistrationInfo requestItem = new RegistrationInfo();
                    PreferenceManager manager = PreferenceManager.getInstance(context);
                    AutoVerificationResponse response = responseServe.body();
                    if (response == null) {
                        LogUtility.d("auto", "Response is null");
                        return;
                    }
                    //count++;
                    if (AppConstants.VERIFIED.equalsIgnoreCase(response.getStatus())) {
                        //  manager.setSimPhoneNumber(sequenceNumber, response.getNationalNumber());
                        requestItem.isManualRegistration = AppConstants.RegistrationType.AUTO_REGISTRATION;
                        RegistrationService.getInstance().sendFirebaseTokenToServer(context, response.getCountryCode(), response.getNationalNumber(), apiKey);
                        if (simSlot == 0) {
                            LogUtility.d("auto", "AUTO SIM 1 verification succesful");
                            AppUtils.logToFile(context, "AUTO SIm 1 verification successful");
                            manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS.toString());
                            requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS;
                            PreferenceManager.getInstance(context).setSim1RegistrationDone(true);
                        } else {
                            LogUtility.d("auto", "AUTO SIM 2 verification succesful");
                            AppUtils.logToFile(context, "AUTO SIM 2 verification successful");
                            manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS.toString());
                            requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS;
                            PreferenceManager.getInstance(context).setSim2RegistrationDone(true);
                        }
                    } else {
                        LogUtility.d("auto", "AUTO verification failed...for sim slot " + simSlot);
                        AppUtils.logToFile(context, "Auto Verificaton failed for sim slot " + simSlot);
                        requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                        if (simSlot == 0) {
                            manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
                            requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                        } else {
                            manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
                            requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
                        }
                    }
                    requestItem.privateKey = privateKey;
                    requestItem.phoneNumber = response.getNationalNumber();
                    requestItem.countryCode = response.getCountryCode();
                    requestItem.fullNumberWithPlus = Utils.getMaskFromPhoneNumber(response.getNationalNumber());
                    requestItem.status = response.getStatus();
                    requestItem.simSlot = simSlot;
                    //requestItem.count = count;
                    requestItem.sequenceNumber = sequenceNumber;

                    requestItem.apiKey = apiKey;
                    AppUtils.setSimRequestItemData(context, requestItem);

                    RxEvent event = new RxEvent(AppConstants.RxEventName.AUTO_REGISTRATION_VERIFICATION_STATUS, requestItem);
                    rxBus.send(event);
                }

                @Override
                public void onFailure(Call<AutoVerificationResponse> call, Throwable t) {

                    LogUtility.d("auto", "auto verify failed in API onFailure for sim slot : " + simSlot + " throwable :" + t.getMessage());
                    updatePreferenceDataOnFailure(simSlot, context, sequenceNumber);

                    // requestItem.count = count;
                    // showManulaReg(context, simSlot, privateKey, sequenceNumber, apiKey);
                    //count++;
                }
            });

        }
    }

    /**
     * Update preference data in case of auto registration fails.
     *
     * @param simSlot
     * @param context
     * @param sequenceNumber
     */
    private void updatePreferenceDataOnFailure(int simSlot, Context context, String sequenceNumber) {

        AppUtils.saveSimIMSINoForParticularSlot(context, simSlot);
        RegistrationInfo requestItem = new RegistrationInfo();
        PreferenceManager manager = PreferenceManager.getInstance(context);
        if (simSlot == 0) {
            manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
            requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
        } else {
            manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
            requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
        }
        requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
        requestItem.status = AppConstants.ApiResponseStatus.FAILURE;
        requestItem.simSlot = simSlot;
        requestItem.sequenceNumber = sequenceNumber;
        AppUtils.setSimRequestItemData(context, requestItem);

        RxEvent event = new RxEvent(AppConstants.RxEventName.AUTO_REGISTRATION_VERIFICATION_STATUS, requestItem);
        rxBus.send(event);
    }


    /**
     * method used to handle the click of the overlay
     *
     * @param view
     */
    public void onOverlayClick(View view) {
        mInstructionOverLayVisibility.set(false);
        PreferenceManager.getInstance(view.getContext()).setHomeOverlayShow(true);
        if (!HomePagerFragment.isImportMessagesLoadingComplete)
            HomePagerFragment.progDialog.show();
    }
}
