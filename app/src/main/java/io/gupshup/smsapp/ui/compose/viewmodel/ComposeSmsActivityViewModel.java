package io.gupshup.smsapp.ui.compose.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Naveen BM on 4/12/2018.
 */
public class ComposeSmsActivityViewModel {
    public ComposeSmsActivityViewModel() {
    }

    public Contact getIntentContact(Intent intent, Context ctx) {
        Contact contact = null;
        String scheme = intent.getData().getScheme();
        String phoneNumber = null;
        if ("smsto".equals(scheme)
            || "sms".equals(scheme)) {
            phoneNumber = intent.getData().getSchemeSpecificPart();
        }
        if(phoneNumber == null || phoneNumber.equals("") || phoneNumber.equals(" ")){
            return null;
        }
        Uri phoneUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cursor = null;

        try {
            cursor = ctx.getContentResolver().query(phoneUri, projection, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contact = new Contact();
                String contactName = cursor.getString(0);
                contact.setmContactName(contactName);
                contact.setmContactPhoneNumber(phoneNumber);
            }
            cursor.close();
        }
        return contact;
    }
}
