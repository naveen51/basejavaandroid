package io.gupshup.smsapp.ui.base.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.utils.ContactChangeObserver;

public class BaseActivity extends AppCompatActivity {

    @Inject
    public APIService mService;
    public static int DEFAULT_SMS_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainApplication) getApplicationContext()).getAppComponent().inject(this);
        MainApplication.getInstance().setIsApplicationActive(true);
    }

    @Override
    protected void onResume() {
        MainApplication.getInstance().setIsApplicationActive(true);
        super.onResume();
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //clearReferences();
        super.onDestroy();
    }
    protected void clearReferences() {
        MainApplication.getInstance().setIsApplicationActive(false);
    }

    ContactChangeObserver mContactChangeObserver = new ContactChangeObserver(new Handler(), this);

    protected void registerContactChangeObserver() {

        getContentResolver().registerContentObserver(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, false, mContactChangeObserver);
    }

    protected void unRegisterContactChangeObserver() {

        getContentResolver().unregisterContentObserver(mContactChangeObserver);
    }

    protected void setupToolBar(boolean showBackBtn) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(showBackBtn);
            getSupportActionBar().setDisplayShowHomeEnabled(showBackBtn);
        }
        //setSupportActionBar(mBinding.toolbar);
    }

    protected void showDefaultSmsDialog() {
        final String packageName = getPackageName();
        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, packageName);
        startActivityForResult(intent, DEFAULT_SMS_REQUEST_CODE);
    }
}
