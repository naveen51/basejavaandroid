package io.gupshup.smsapp.ui.reply.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.MessageReplyBundleData;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.dao.SearchDAO;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.DialogAddToContactReplyScreenBinding;
import io.gupshup.smsapp.databinding.DialogMessageDetailBinding;
import io.gupshup.smsapp.databinding.DialogMessageMoveReplyBinding;
import io.gupshup.smsapp.databinding.MessageReplyFragmentBinding;
import io.gupshup.smsapp.databinding.MsgForwardLytBinding;
import io.gupshup.smsapp.enums.ActionModeType;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.ForwardContactAdapter;
import io.gupshup.smsapp.ui.adapter.ReplyAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.compose.view.ShareContactActivity;
import io.gupshup.smsapp.ui.earlierSMS.MMSDeepLinkUtility;
import io.gupshup.smsapp.ui.reply.viewmodel.ForwardMessageViewModel;
import io.gupshup.smsapp.ui.reply.viewmodel.MessageReplyViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.smsapp.utils.UiUtils;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

/**
 * Created by Naveen BM on 7/17/2018.
 */
public class MessageReplyFragment extends BaseFragment<MessageReplyFragmentBinding, MessageReplyViewModel> implements ReplyAdapter.onMessageItemClickListener, ActionMode.Callback, SearchView.OnQueryTextListener {

    private static final String TAG = MessageReplyFragment.class.getSimpleName();
    private RecyclerView mReplyRecycler;
    private String mMask, mDisplay, mRecepientThumbImage;
    private int mThemeColorFromTile;
    private ReplyAdapter mReplyAdapter;
    private boolean isScrollNeed;
    private String mSavedDraftMessage;
    private RecyclerView.SmoothScroller smoothScroller;
    private ConversationEntity mConversationEntity;
    private ActionMode mActionMode;
    private MessageEntity[] mDeletableMessage;
    private ForwardContactAdapter forwardAdapter;
    private Dialog forwardDialog = null;
    private ConversationEntity mDeletableConversation;
    private boolean isArchive;
    private int mSubscriptionID;
    private boolean mIsFromBlock;
    private SearchView mSearchView;
    private String mSearchQuery = AppConstants.EMPTY_TEXT;
    @Inject
    public RxBus rxBus;
    private List<SearchDAO.MessageEntityExtended> mMessagePositionList;
    private boolean mIsSendClicked;
    private List<SimInformation> mSimList;
    private int mPrevSelectedSimSlot = 0;
    private String mCarierName;
    private MessageEntity mLastMessageData;
    private RxBusHelper rxBusHelper;
    private String mForwardingMessage;
    private MessageReplyBundleData parcelData;
    private long mDraftMessageCreationTime = -1;
    private ConversationType conversationType;
    private boolean mIsVisibleToUser;

    @Override
    public int getBindingVariable() {
        return BR.messageRepViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.message_reply_fragment;
    }

    @Override
    public MessageReplyViewModel getViewModel() {
        return ViewModelProviders.of(this).get(MessageReplyViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        setHasOptionsMenu(true);
        getBundleData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getActivity().invalidateOptionsMenu();
        mBinding.sendEditTxt.setFocusable(false);
        mBinding.sendEditTxt.setFocusableInTouchMode(true);
        initializeViewComponents();
        getConversation();
        setDataToViews();
        getDataFromMask();
        handleTypeArea();
        setForwardOrDraft();
        getDataFromDataBase();
        ((SimpleItemAnimator) mReplyRecycler.getItemAnimator()).setSupportsChangeAnimations(false);

        smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        return mBinding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.setHandler(this);
    }

    /**
     * method used to set forward or draft message
     */
    private void setForwardOrDraft() {
        if (getCurrentViewModel() != null) {
            boolean enable = AppUtils.getSimInfoList().size() > 0;
            if (!TextUtils.isEmpty(mForwardingMessage)) {
                //when forward and draft both are there give priority to forward message
                getCurrentViewModel().editTextVal.set(mForwardingMessage);
                getCurrentViewModel().mShowSendImage.set(enable);
            } else if (!TextUtils.isEmpty(mSavedDraftMessage)) {
                getCurrentViewModel().editTextVal.set(mSavedDraftMessage);
                getCurrentViewModel().mShowSendImage.set(enable);
            }

        }
    }


    private void setDataToViews() {
        if (AppUtils.checkSimCardExistence(getActivity())) {
            //if sim card exist
            getCurrentViewModel().mSendViewClickable.set(true);
            mSimList = AppUtils.getSimInfoList();
            getCurrentViewModel().setColor(mSimList, getActivity());
            setPreviouslySelectedSimData();
            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancelAll();
            }
        } else {
            //if sim card doesnt exist then block the click
            getCurrentViewModel().mSendViewClickable.set(false);
        }
    }


    /**
     * method used to set the sim no on which last message
     */
    private void setPreviouslySelectedSimData() {
        if (mPrevSelectedSimSlot == 0) {
            getCurrentViewModel().mSimSlotNo.set(getString(R.string.sim_slot_one));
            getCurrentViewModel().mIsFirstSim.set(true);
        } else {
            getCurrentViewModel().mSimSlotNo.set(getString(R.string.sim_slot_two));
            getCurrentViewModel().mIsFirstSim.set(false);
        }
        Pair<String, Integer> info = AppUtils.getSimSubscriptionInfo(getActivity(), mPrevSelectedSimSlot);
        if (info != null) {
            mCarierName = info.first;
            try {
                mSubscriptionID = info.second;
            } catch (NullPointerException e) {
                mSubscriptionID = 0;
            }
        }
    }


    /**
     * method used to get the conversation for given mask ,
     * to get last message Id and last message Data
     */
    private void getConversation() {
        mConversationEntity = AppDatabase.getInstance(getActivity()).conversationDAO().getConversationFromMask(mMask);
        String lastMessageId = mConversationEntity.getLastMessageId();
        mLastMessageData = getCurrentViewModel().getLastMessageData(lastMessageId);
        if (mLastMessageData != null) {
            mPrevSelectedSimSlot = mLastMessageData.getIncomingSIMID();
        }
    }


    private void getDataFromMask() {

        Pair<String, String> userInfo = MessageUtils.getContactUserDetail(getActivity(), mMask);
        if (userInfo != null) {
            mDisplay = userInfo.first;
            mRecepientThumbImage = userInfo.second;
            TypedArray mColors = getResources().obtainTypedArray(R.array.letter_tile_colors);
            mThemeColorFromTile = AppUtils.pickColor(TextUtils.isEmpty(mDisplay) ? mMask : mDisplay, mColors);
            getCurrentViewModel().themeColor.set(mThemeColorFromTile);
        }
        mSavedDraftMessage = mConversationEntity.getDraftMessage();
        mDraftMessageCreationTime = mConversationEntity.getDraftCreationTime();
        if (TextUtils.isEmpty(mSavedDraftMessage)) {
            mSavedDraftMessage = AppConstants.EMPTY_TEXT;
        }
    }

    private void getBundleData() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AppConstants.BundleKey.PARCEL)) {
                parcelData = bundle.getParcelable(AppConstants.BundleKey.PARCEL);
                mIsFromBlock = parcelData.isFromBlock;
                mSearchQuery = parcelData.searchQuery;
                conversationType = parcelData.conversationType;
                //make search query empty for rest of the items
            }

            if (bundle.containsKey(AppConstants.BundleKey.FORWARD_MESSAGE)) {
                mForwardingMessage = bundle.getString(AppConstants.BundleKey.FORWARD_MESSAGE);
            }

            mMask = bundle.getString(AppConstants.BundleKey.MASK);

        }
    }


    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.send_view:

                if (!AppUtils.isDefaultSmsPackage(getActivity(), getActivity().getPackageName())) {
                    mIsSendClicked = true;
                    showDefaultSmsDialog();
                    return;
                }
                callSendMessage();
                break;

            case R.id.first_sim_lyt:
                handleToolbarOnOverlayShow(false);
                getCurrentViewModel().mShowSimListLyt.set(false);
                //simlayout visibility false to handle activity backpress
                clearReferences();
                PreferenceManager.getInstance(getActivity()).setSimLayoutVisibility(false);
                mSimList = AppUtils.getSimInfoList();
                if (mSimList != null && mSimList.size() > 0) {
                    mSubscriptionID = mSimList.get(0).getmSubscriptionID();
                    mCarierName = mSimList.get(0).getmSimName();
                    mPrevSelectedSimSlot = mSimList.get(0).getSimSlotIndex();
                    getCurrentViewModel().mIsFirstSim.set(true);
                    getCurrentViewModel().mSimSlotNo.set(getString(R.string.sim_slot_one));
                }
                break;

            case R.id.second_sim_lyt:
                handleToolbarOnOverlayShow(false);
                getCurrentViewModel().mShowSimListLyt.set(false);
                //simlayout visibility false to handle activity backpress
                clearReferences();
                if (mSimList != null && mSimList.size() > 1) {
                    mSubscriptionID = mSimList.get(1).getmSubscriptionID();
                    mCarierName = mSimList.get(1).getmSimName();
                    mPrevSelectedSimSlot = mSimList.get(1).getSimSlotIndex();
                    getCurrentViewModel().mSimSlotNo.set(getString(R.string.sim_slot_two));
                    getCurrentViewModel().mIsFirstSim.set(false);
                }
                break;

            case R.id.sim_number_img:
                handleToolbarOnOverlayShow(true);
                DialogUtils.hideKeyboard(getActivity());
                showSimSelectionLayout();
                break;

            case R.id.count_decrease_arrow:
                DialogUtils.hideKeyboard(getActivity());
                LogUtility.d("SEARCHCOUNT", "----------- DECREASE CLICKED ----------");
                int messagePositionListSize = mMessagePositionList.size();
                if (messagePositionListSize > 1) {
                    getCurrentViewModel().increaseSearchCount();
                    int curPosDec = getCurrentViewModel().getCurrentPosition();
                    setScrollPosition(messagePositionListSize - curPosDec, messagePositionListSize - curPosDec + 1);

                }

                break;

            case R.id.count_increase_arrow:
                DialogUtils.hideKeyboard(getActivity());
                LogUtility.d("SEARCHCOUNT", "----------- INCREASE CLICKED ----------");
                int messagePositionListSizeInc = mMessagePositionList.size();
                if (messagePositionListSizeInc > 1) {
                    getCurrentViewModel().decreaseSearchCount();
                    int curPos = getCurrentViewModel().getCurrentPosition();
                    setScrollPosition(messagePositionListSizeInc - curPos, messagePositionListSizeInc - curPos - 1);
                }
                break;

            case R.id.sim_overlay:
                if (getCurrentViewModel().getSimLayoutVisibility()) {
                    getCurrentViewModel().mShowSimListLyt.set(false);
                    handleToolbarOnOverlayShow(false);
                    clearReferences();

                }
                break;

            case R.id.floating_button_container:
                //launch share contact activity
                Intent intent = new Intent(getActivity(), ShareContactActivity.class);
                startActivity(intent);
                break;

        }

    }

    /**
     * Method used to initialize the view components
     */
    private void initializeViewComponents() {
        mReplyRecycler = mBinding.replyRecycler;
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setReverseLayout(true);
        mReplyRecycler.setLayoutManager(manager);
    }


    private void getDataFromDataBase() {

        if (TextUtils.isEmpty(mMask)) {
            Toast.makeText(getActivity(), getString(R.string.no_contact_exists), Toast.LENGTH_LONG).show();
            return;
        }
        getCurrentViewModel().getMessageForSelectedContact(mMask);
        mReplyAdapter = new ReplyAdapter(mRecepientThumbImage, this, this, TextUtils.isEmpty(mDisplay) ? mMask : mDisplay, mThemeColorFromTile, getActivity());
        mReplyRecycler.setAdapter(mReplyAdapter);
        getCurrentViewModel().observeData().observe(this, new Observer<PagedList<MessageEntity>>() {
            @Override
            public void onChanged(@Nullable PagedList<MessageEntity> messageEntities) {
                if (messageEntities != null && messageEntities.size() > 0) {
                    MessageEntity lastMessage = messageEntities.get(0);
                    if (lastMessage != null && lastMessage.getDirection() == MessageDirection.INCOMING) {
                        isScrollNeed = true;
                    }

                    if (messageEntities.size() > 0
                        && mIsVisibleToUser) {
                        MessagingService.getInstance(SMSService.class).readConversation(mMask, DateUtils.currentTimestamp());
                    }
                    LogUtility.d(TAG, " -------------- getDataFromDataBase Called -----------");
                    mReplyAdapter.submitList(messageEntities);
                  /*  final MessageDAO.CountData countData = MessagingService
                        .getInstance(SMSService.class).getTotalMessageCount(mMask);*/
                    if (isScrollNeed) {

                        mReplyRecycler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mReplyRecycler.smoothScrollToPosition(0);
                                isScrollNeed = false;
                            }
                        }, 300);
                    }
                }
            }
        });

    }


    /**
     * method used to scroll recycler to particular position
     * and highlight and de-highlight particular text
     *
     * @param curPos
     * @param prevPos
     */
    private void setScrollPosition(int curPos, int prevPos) {
        //get the item position from the list
        if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
            //item position in the list
            int curItemPosition = mMessagePositionList.get(curPos).getPosition();
            int prevItemPosition = -1;
            if (prevPos > 0) {
                prevItemPosition = mMessagePositionList.get(prevPos).getPosition();
            }
            mReplyAdapter.updateSearchItem(curItemPosition, prevItemPosition);
            smoothScroll(curItemPosition);
        }
    }


    public void smoothScroll(int pos) {
        smoothScroller.setTargetPosition(pos);
        mReplyRecycler.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    @Override
    public void onItemMessageClickSender(View view, int position, MessageEntity entity) {
        int count = mReplyAdapter.getSelectedItemCount();
        if (entity.getMessageStatus() == MessageStatus.FAILED && count == 0) {
            String failedCarrierName = "";
            int failedSubscriptionID = 0;
            //resend message
            Pair<String, Integer> info = AppUtils.getSimSubscriptionInfo(getActivity(), entity.getIncomingSIMID());
            if (info != null) {
                failedCarrierName = info.first;
                try {
                    failedSubscriptionID = info.second;
                } catch (NullPointerException e) {
                    failedSubscriptionID = 0;
                }
            }

            MessageDAO dao = AppDatabase.getInstance(getActivity()).messageDAO();
            entity.setCarierName(failedCarrierName);
            entity.setMessageStatus(MessageStatus.SENDING);
            entity.setSentTimestamp(DateUtils.currentTimestamp());
            dao.update(entity);

            MessageUtils.sendSMS(getActivity(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(),
                failedSubscriptionID);
        } else {
            if (count > 0) {
                enableActionMode(position, entity);
            } else {
                //showMessageDetailForSender(entity);
            }
        }
    }

    @Override
    public void onItemMessageClickReciepient(View view, int position, MessageEntity entity) {
        int count = mReplyAdapter.getSelectedItemCount();
        if (count > 0) {
            enableActionMode(position, entity);
        } else {
            if (entity.getMessageStatus() == MessageStatus.FAILED && entity.getDirection() == MessageDirection.OUTGOING) {

                //TODO: Whther we should try data route while resending...???
                //resend message
                MessageUtils.sendSMS(getActivity(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(), mSubscriptionID);
            }
        }

    }


    /**
     * Method used to handle the visibility of type are based on Mask
     * Allow only for Personal
     */
    private void handleTypeArea() {
        ConversationType type = Utils.getConversationType(mMask, ((GupshupTextMessage) mLastMessageData.getContent()).getText());
        if (ConversationType.PERSONAL == type
            || ConversationType.PROMOTIONAL == type) {
            getCurrentViewModel().mTypeAreaVisibility.set(true);
        } else {
            getCurrentViewModel().mTypeAreaVisibility.set(false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void enableActionMode(int pos, MessageEntity msgEntity) {
        if (mActionMode == null) {
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this);
        }

        if (mReplyAdapter != null) {

            mReplyAdapter.setBackgroundForSelection(pos, msgEntity);

        }
        actionModeSetup();
    }


    /**
     * method used to set up the action mode
     */
    public void actionModeSetup() {
        if (mActionMode == null) {
            return;
        }
        Menu menu = mActionMode.getMenu();
        handleTopBarIconVisibility(menu);
        showSelectAction(menu);
        int count = mReplyAdapter.getSelectedItemCount();
        if (count == 0) {
            mActionMode.finish();
        } else {
            if (getCurrentParentFragment() != null) {
                getCurrentParentFragment().setViewPagerSwipeAble(false);
            }
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_message_actions, menu);
        getSubTypeFromConversation();
        hideOrShowActionButtons(menu);
        return true;
    }

    private void hideOrShowActionButtons(Menu menu) {
        menu.findItem(R.id.action_share).setVisible(!isArchive);
        menu.findItem(R.id.action_copy).setVisible(!isArchive);
        menu.findItem(R.id.action_forward).setVisible(!isArchive);

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        PagedList<MessageEntity> listMessageEntity = getCurrentViewModel().observeData().getValue();
        List<MessageEntity> selectedMessages = mReplyAdapter.getSelectedMessages();
        switch (item.getItemId()) {
            case R.id.action_share:
                LogUtility.d("MESSAGEACTIONS", "-------- SHARE --------");
                updateMessageEntity(ActionModeType.SHARE, listMessageEntity, selectedMessages);
                mode.finish();
                return true;

            case R.id.action_copy:
                LogUtility.d("MESSAGEACTIONS", "-------- COPY --------");
                updateMessageEntity(ActionModeType.COPY, listMessageEntity, selectedMessages);
                mode.finish();
                return true;

            case R.id.action_info:
                LogUtility.d("MESSAGEACTIONS", "-------- INFO --------");
                updateMessageEntity(ActionModeType.INFO, listMessageEntity, selectedMessages);
                mode.finish();
                return true;

            case R.id.action_forward:
                LogUtility.d("MESSAGEACTIONS", "-------- FORWARD --------");
                updateMessageEntity(ActionModeType.FORWARD, listMessageEntity, selectedMessages);
                mode.finish();
                return true;

            case R.id.action_delete:
                LogUtility.d("MESSAGEACTIONS", "-------- DELETE --------");
                if (listMessageEntity.size() == 1 || listMessageEntity.size() == selectedMessages.size()) {
                    updateConversation(ActionModeType.DELETE, mMask);
                } else {
                    updateMessageEntity(ActionModeType.DELETE, listMessageEntity, selectedMessages);
                }

                // mode.finish();
                return true;

            case R.id.action_select_all:
                selectAllMessages();
                break;

            case R.id.action_deselect_all:
                deselectAllMessagesInReply();
                break;


        }
        return false;
    }

    /**
     * Method used to select all messages
     */
    private void selectAllMessages() {
        mReplyAdapter.addMessageEntityList(getCurrentViewModel().observeData().getValue());
        actionModeSetup();
    }

    /**
     * Method used to deselect all messages
     */
    private void deselectAllMessagesInReply() {
        mReplyAdapter.removeMessageEntityFromList();
        actionModeSetup();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = null;
        if (getCurrentParentFragment() != null) {
            getCurrentParentFragment().setViewPagerSwipeAble(true);
        }
        if (mReplyAdapter != null) {
            mReplyAdapter.notifySelectionClear();
        }
    }


    /**
     * Method used to handle top bar icon visibility in Action View
     *
     * @param menu
     */
    private void handleTopBarIconVisibility(Menu menu) {
        if (mReplyAdapter != null && mReplyAdapter.getLongItemCount() > 1) {
            menu.findItem(R.id.action_share).setVisible(false);
            menu.findItem(R.id.action_copy).setVisible(false);
            menu.findItem(R.id.action_info).setVisible(false);
            menu.findItem(R.id.action_forward).setVisible(false);
            menu.findItem(R.id.action_delete).setVisible(true);
        } else {
            hideOrShowActionButtons(menu);
            menu.findItem(R.id.action_info).setVisible(true);
            menu.findItem(R.id.action_delete).setVisible(true);
        }
    }


    /**
     * Method used to show select or deselect option at the action mode
     *
     * @param menu
     */
    public void showSelectAction(Menu menu) {
        if (mReplyAdapter != null) {
            if (mReplyAdapter.getLongItemCount() == getCurrentViewModel().observeData().getValue().size()) {
                menu.findItem(R.id.action_select_all).setVisible(false);
                menu.findItem(R.id.action_deselect_all).setVisible(true);
            } else {
                menu.findItem(R.id.action_select_all).setVisible(true);
                menu.findItem(R.id.action_deselect_all).setVisible(false);
            }
        }
    }


    protected void updateMessageEntity(ActionModeType actionModeType, PagedList<MessageEntity> messageList, List<MessageEntity> selectedItem) {
        MessageEntity[] message = new MessageEntity[selectedItem.size()];
        mDeletableMessage = message;
        message = selectedItem.toArray(message);
        switch (actionModeType) {
            case DELETE:

                String confirmationMessage = getResources().getQuantityString(R.plurals.message_delete_confirmation, message.length, message.length);
                DialogUtils.showConfirmPopUp(getActivity(), confirmationMessage, getString(R.string.delete_dialog_title),
                    getString(R.string.confirm), getString(R.string.cancel_btn), this);

                break;

            case SHARE:
                MessageEntity entityShare = selectedItem.get(0);
                if (entityShare != null) {
                    GupshupTextMessage txtMessage = (GupshupTextMessage) entityShare.getContent();
                    Utils.shareMessage(getActivity(), txtMessage.getText());
                }
                break;

            case INFO:
                MessageEntity entity = selectedItem.get(0);
                if (entity != null) {
                    if (entity.getDirection() == MessageDirection.OUTGOING) {
                        showMessageDetailForSender(entity);
                    } else {
                        showMessageDetailForRecipient(entity);
                    }
                }
                break;

            case COPY:
                MessageEntity entityCopy = selectedItem.get(0);
                if (entityCopy != null) {
                    GupshupTextMessage txtMessage = (GupshupTextMessage) entityCopy.getContent();
                    Utils.copyToClipBoard(getActivity(), txtMessage.getText());
                }

                break;

            case FORWARD:
                MessageEntity entityCopyForward = selectedItem.get(0);
                popupForwardDialog(entityCopyForward);
                break;
        }
    }

    /**
     * This method is to show individual message detail.
     */
    public void showMessageDetailForSender(MessageEntity message) {
        DialogMessageDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_detail,
            null, false);
        binding.setMessageEntity(message);
        // showReceiveTime.set(false);
        binding.status.setVisibility(View.VISIBLE);
        final Dialog dialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }


    /**
     * This method is to show individual message detail.
     */
    public void showMessageDetailForRecipient(MessageEntity message) {
        DialogMessageDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_detail,
            null, false);
        binding.setMessageEntity(message);
        //showReceiveTime.set(true);
        binding.status.setVisibility(View.GONE);
        final Dialog dialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }


    /**
     * Method used to show the forward Dialog
     *
     * @param entityCopyForward
     */
    private void popupForwardDialog(MessageEntity entityCopyForward) {
        MsgForwardLytBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.msg_forward_lyt,
            null, false);
        // showReceiveTime.set(false);
        binding.setSelectedMessageEntity(entityCopyForward);
        binding.setForwardMessageViewModel(new ForwardMessageViewModel(MainApplication.getInstance()));
        RecyclerView forwardRecycle = binding.forwardContactRecycler;
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        forwardRecycle.setLayoutManager(manager);
        getCurrentViewModel().callPersonalList();
        forwardAdapter = new ForwardContactAdapter(entityCopyForward);
        getCurrentViewModel().observeContact().observe(this, new Observer<PagedList<ContactModel>>() {
            @Override
            public void onChanged(@Nullable PagedList<ContactModel> contactMaskList) {
                // LogUtility.d("FORWARD", "---------------- CONTACT FORWARD ------------"+messageEntities.size());
                forwardAdapter.submitList(contactMaskList);

            }
        });
        forwardRecycle.setAdapter(forwardAdapter);

        if (forwardDialog == null) {
            forwardDialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        }
        forwardDialog.setCancelable(true);
        forwardDialog.setCanceledOnTouchOutside(false);
        forwardDialog.show();
    }


    protected void updateConversation(ActionModeType actionModeType, String mask) {
        ConversationEntity conversation = null;
        String movedToText = AppConstants.EMPTY_TEXT;
        ConversationDAO conversationDAO = AppDatabase.getInstance(getActivity()).conversationDAO();
        List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (conversationEntity != null && conversationEntity.size() > 0) {
            conversation = conversationEntity.get(0);
            if (conversation != null) {
                switch (actionModeType) {
                    case BLOCKED:
                        conversation.setLastConversationType(conversation.getType());
                        conversation.setType(ConversationType.BLOCKED);
                        break;
                    case MUTE:
                        conversation.setSendNotifications(false);
                        conversation.setNotificationsSound(false);
                        conversation.setNotificationsLight(false);
                        conversation.setNotificationsPopup(false);
                        break;
                    case UN_MUTE:
                        conversation.setSendNotifications(true);
                        conversation.setNotificationsSound(true);
                        conversation.setNotificationsLight(true);
                        conversation.setNotificationsPopup(true);
                        break;
                    case ARCHIVE:
                        conversation.setSubType(ConversationSubType.ARCHIVE);
                        break;
                    case UN_ARCHIVE:
                        conversation.setSubType(null);
                        break;
                    case UN_BLOCK:
                        conversation.setType(conversation.getLastConversationType());
                        break;
                }
            }
        }


        switch (actionModeType) {
            case BLOCKED:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                clearSelectionAndActionMode();
                movedToText = getString(R.string.moved_to_tab, getString(R.string.block_tab));
                break;
            case UN_BLOCK:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                clearSelectionAndActionMode();
                movedToText = getString(R.string.unblocked_successfully);
                break;
            case ARCHIVE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.moved_to_tab, getString(R.string.archive_block));
                break;
            case UN_ARCHIVE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.un_archived_message);
                break;
            case MUTE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                break;
            case MOVE:
                showMessageDetail(conversation);
                break;
            case DELETE:
                mDeletableMessage = null;
                mDeletableConversation = conversation;
                String confirmationMessage = getResources().getQuantityString(R.plurals.thread_delete_confirmation, 1, 1);
                DialogUtils.showConfirmPopUp(getActivity(), confirmationMessage, getString(R.string.delete_dialog_title),
                    getString(R.string.confirm), getString(R.string.cancel_btn), this);

                break;
            case ADD_TO_CONTACT:
                //Utils.addNewContact(this, mMask, null, false);
                if (Utils.getConversationType(conversation.getMask(), Utils.getLastMessageInConversation(conversation)) == ConversationType.PERSONAL) {
                    int phoneContactID = canAddToContact(conversation);
                    if (phoneContactID < 0) {
                        showAddToContactDialog(conversation);
                    }
                }
                break;

        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
    }


    protected int canAddToContact(ConversationEntity entity) {

        int contactId = -1;
        if (entity != null) {
            contactId = MessageUtils.getContactIDFromNumber(entity.getMask(), getActivity());
        }
        return contactId;
    }

    /**
     * This method is to show individual message detail.
     *
     * @param item
     */

    public void showAddToContactDialog(ConversationEntity item) {
        DialogAddToContactReplyScreenBinding newContactBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_add_to_contact_reply_screen, null, false);
        mAddToContactDialog = DialogUtils.getCustomDialog(getActivity(), newContactBinding.getRoot());
        newContactBinding.setMessageReplyFragment(this);
        newContactBinding.setConversationEntity(item);
        mAddToContactDialog.setCancelable(true);
        mAddToContactDialog.setCanceledOnTouchOutside(true);
        mAddToContactDialog.show();
    }


    public void addToContactClicked(View view, ConversationEntity item) {

        switch (view.getId()) {
            case R.id.add_to_existing_contact:
                Utils.addNewContact(getActivity(), item.getMask(), this, true);
                break;
            case R.id.add_to_new_contact:
                Utils.addNewContact(getActivity(), item.getMask(), this, false);
                break;
        }
        if (mAddToContactDialog != null) {
            mAddToContactDialog.dismiss();
        }
    }

    /**
     * This method is to show individual message detail.
     */

    public void showMessageDetail(ConversationEntity conversationEntity) {

        getCurrentViewModel().showTransactionalFolder.set(conversationEntity.getType() != ConversationType.TRANSACTIONAL);
        getCurrentViewModel().showPromoFolder.set(conversationEntity.getType() != ConversationType.PROMOTIONAL);
        getCurrentViewModel().showPersonalFolder.set(conversationEntity.getType() != ConversationType.PERSONAL);
        getCurrentViewModel().showBlockOption.set(true);


        DialogMessageMoveReplyBinding dialogMessageMoveBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_move_reply,
            null, false);


        if (ConversationType.BLOCKED == conversationEntity.getType()) {
            getCurrentViewModel().blocText.set(getString(R.string.unblock));
            dialogMessageMoveBinding.typeBlock.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.action_unblock_dark), null, null, null);
        } else {
            getCurrentViewModel().blocText.set(getString(R.string.block));
            dialogMessageMoveBinding.typeBlock.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_to_block), null, null, null);
        }

        if (ConversationSubType.ARCHIVE == conversationEntity.getSubType()) {
            getCurrentViewModel().archiveText.set(getString(R.string.action_un_archive));
            dialogMessageMoveBinding.typeArchive.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_unarchive), null, null, null);
        } else {
            getCurrentViewModel().archiveText.set(getString(R.string.archive));
            dialogMessageMoveBinding.typeArchive.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_to_archive), null, null, null);
        }

        mMessageSwitchDialog = DialogUtils.getCustomDialog(getActivity(), dialogMessageMoveBinding.getRoot());
        dialogMessageMoveBinding.setReplyActivity(this);
        dialogMessageMoveBinding.setConversationEntity(conversationEntity);
        dialogMessageMoveBinding.setReplyViewModel(getCurrentViewModel());
        mMessageSwitchDialog.setCancelable(true);
        mMessageSwitchDialog.setCanceledOnTouchOutside(true);
        mMessageSwitchDialog.show();
    }


    public void onMoveToItemClicked(View view, ConversationEntity conversation) {
        String movedToText = AppConstants.EMPTY_TEXT;
        if (conversation != null) {
            switch (view.getId()) {
                case R.id.type_personal:
                    conversation.setType(ConversationType.PERSONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.personal_tab));
                    break;

                case R.id.type_transactional:
                    conversation.setType(ConversationType.TRANSACTIONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.trans_tab));
                    break;

                case R.id.type_promotional:
                    conversation.setType(ConversationType.PROMOTIONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.promo_tab));
                    break;

                case R.id.type_block:

                    movedToText = markAsBlockOrUnBlock(conversation);
                    break;

                case R.id.type_archive:
                    movedToText = markAsArchiveOrUnArchive(conversation);
                    break;


            }
        }
        MessagingService.getInstance(SMSService.class).updateConversation(conversation);
        clearSelectionAndActionMode();
        if (mMessageSwitchDialog != null && mMessageSwitchDialog.isShowing()) {
            mMessageSwitchDialog.dismiss();
        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
        getCurrentParentFragment().updatePagerPosition();

    }


    private String markAsArchiveOrUnArchive(ConversationEntity conversation) {

        if (ConversationSubType.ARCHIVE == conversation.getSubType()) {
            conversation.setSubType(null);
            if (mMenu != null) {
                isArchive = false;
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
            }
            return getString(R.string.un_archived_message);
        } else {
            conversation.setSubType(ConversationSubType.ARCHIVE);
            if (mMenu != null) {
                isArchive = true;
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
            }
            return getString(R.string.moved_to_tab, getString(R.string.archive_block));
        }
    }

    private String markAsBlockOrUnBlock(ConversationEntity conversation) {

        if (ConversationType.BLOCKED == conversation.getType()) {
            ConversationType conversationType = Utils.getConversationType(conversation.getMask(), Utils.getLastMessageInConversation(conversation));
            conversation.setType(conversationType);
            blockUnblockMenuHandle(conversationType);
            return getString(R.string.unblocked_successfully);//MessageUtils.getTabTitle(this, conversationType);
        } else {
            conversation.setType(ConversationType.BLOCKED);
            blockUnblockMenuHandle(ConversationType.BLOCKED);
            return getString(R.string.moved_to_tab, getString(R.string.block_tab));
        }
    }

    private void blockUnblockMenuHandle(ConversationType type) {


        if (mMenu != null) {
            if (ConversationType.BLOCKED == type) {
                mMenu.findItem(R.id.action_block).setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
            } else {
                mMenu.findItem(R.id.action_block).setVisible(true);
                mMenu.findItem(R.id.action_un_block).setVisible(false);
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        MessageEntity msgEntity = (MessageEntity) view.getTag();
        String messageType = msgEntity.getMetadata();
        if (messageType == null || messageType == "") {
            return;
        }
        GupshupTextMessage msg = (GupshupTextMessage) msgEntity.getContent();
        MMSDeepLinkUtility mmsUtility = new MMSDeepLinkUtility(getActivity(), msg.getText(), messageType);
    }


    @Override
    public void onItemLongClick(View view, int position) {
        MessageEntity msgEntity = (MessageEntity) view.getTag();
        GupshupTextMessage msg = (GupshupTextMessage) msgEntity.getContent();
        LogUtility.d("LONGCLICK", "-------- On Message Long Clicked ------- Message ID : " + msgEntity.getMessageId() + " -------Message TimeStamp : " + msgEntity.getMessageId() + "----- Message Body : " + msg.getText());
        enableActionMode(position, msgEntity);
    }

    @Override
    public void onButtonClicked(DialogInterface dialog, int which) {

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (mDeletableMessage != null) {
                    MessagingService.getInstance(SMSService.class).deleteMessage(mDeletableMessage);
                    AppUtils.deleteSMSFromDefaultDB(getActivity(), Telephony.Sms.CONTENT_URI, mDeletableMessage);
                    getCurrentViewModel().upDateLastMessageIdOnDelete(getActivity(), mMask);
                    mActionMode.finish();
                } else {
                    MessagingService.getInstance(SMSService.class).deleteConversation(mDeletableConversation);
                    //getCurrentParentFragment().updatePagerPosition(); According to AMA-989
                    dialog.dismiss();
                    goBackToHomeScreen();
                }
                dialog.dismiss();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
            case DialogInterface.BUTTON_NEUTRAL:
                dialog.dismiss();
                break;
        }
    }

    private void goBackToHomeScreen() {

        if (getActivity() != null) {
            getActivity().finish();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_reply_conversation, menu);
        handleCallOption(menu);
        if (TextUtils.isEmpty(mDisplay)
            && Utils.getConversationType(mMask, ((GupshupTextMessage) mLastMessageData.getContent()).getText()) == ConversationType.PERSONAL) {
            menu.findItem(R.id.action_add_contact).setVisible(true);
        } else {
            menu.findItem(R.id.action_add_contact).setVisible(false);
        }
        if (mIsFromBlock) {
            menu.findItem(R.id.action_block).setVisible(false);
            menu.findItem(R.id.action_un_block).setVisible(true);
        } else {
            menu.findItem(R.id.action_block).setVisible(true);
            menu.findItem(R.id.action_un_block).setVisible(false);
        }
        //TODO: showing under move dialog
        getSubTypeFromConversation();
        if (isArchive) {
            menu.findItem(R.id.action_move).setVisible(false);
            menu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
        } else {
            menu.findItem(R.id.action_move).setVisible(true);
            menu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
        }
        final MenuItem menuItem = menu.findItem(R.id.conversation_action_search);
        mSearchView = (SearchView) menu.findItem(R.id.conversation_action_search).getActionView();
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setQueryHint(getString(R.string.message_search_hint));
        if (parcelData != null) {
            mSearchQuery = parcelData.searchQuery;
            parcelData.searchQuery = "";
        }
        if (!TextUtils.isEmpty(mSearchQuery) && mSearchView != null) {

            String titleName = mDisplay != null ? mDisplay : mMask;

            if ((titleName != null && !titleName.toLowerCase().contains(mSearchQuery.toLowerCase()) &&
                (mMask != null && !mMask.toLowerCase().contains(mSearchQuery.toLowerCase())))) {
                menuItem.expandActionView();
                mSearchView.setQuery(mSearchQuery, true);
                mSearchView.post(new Runnable() {
                    @Override
                    public void run() {
                        menuItem.expandActionView();
                        mSearchView.setQuery(mSearchQuery, true);
                    }
                });
            }
        }

        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                disableThreeDots(false, true);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                disableThreeDots(true, true);
                return true;
            }
        });

        mMenu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void handleCallOption(Menu menu) {
        //handle call option for promotional
        if (conversationType == ConversationType.PROMOTIONAL || conversationType == ConversationType.TRANSACTIONAL) {
            //promotional type
            menu.findItem(R.id.call_action).setVisible(false);
        } else {
            //other type
            menu.findItem(R.id.call_action).setVisible(true);
        }
    }


    /**
     * Method used to get the sub type from the mask
     */
    private void getSubTypeFromConversation() {

        List<ConversationEntity> conv = MessagingService.getInstance(SMSService.class).getConversationByMask(mMask);
        if (conv != null && conv.size() > 0) {
            ConversationEntity convEntity = conv.get(0);
            if (convEntity.getSubType() == ConversationSubType.ARCHIVE) {
                isArchive = true;
            } else {
                isArchive = false;
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public boolean onQueryTextChange(String query) {
        callSearch(query);
        return true;
    }


    @SuppressLint("StaticFieldLeak")
    private void callSearch(String query) {

        if (mReplyAdapter != null) {
            if (!TextUtils.isEmpty(query)) {
                query = query.trim();
            }
            final String finalQuery = query;

            new AsyncTask<Void, Void, List<SearchDAO.MessageEntityExtended>>() {
                @Override
                protected List<SearchDAO.MessageEntityExtended> doInBackground(Void... voids) {
                    mMessagePositionList = MessagingService.getInstance(SMSService.class).searchWithinThread(mMask, finalQuery);
                    Collections.reverse(mMessagePositionList);
                    return mMessagePositionList;
                }

                @Override
                protected void onPostExecute(List<SearchDAO.MessageEntityExtended> messageEntityExtendeds) {
                    super.onPostExecute(messageEntityExtendeds);
                    int count = 0;
                    if (!TextUtils.isEmpty(finalQuery)) {
                        //count = messageDAO.messageCountByText(newText);
                        for (int i = 0; i < mMessagePositionList.size(); i++) {
                            LogUtility.d("MESSAGEPOS", "----------- " + mMessagePositionList.get(i).getPosition());
                        }

                        if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
                            count = mMessagePositionList.size();
                            //scroll to first search text occurence
                            smoothScroll(mMessagePositionList.get(count - 1).getPosition());
                            getCurrentViewModel().mSearchPosition.set(1);
                        }

                    } else {
                        count = 0;
                    }

                    getCurrentViewModel().handleSearchCountVisibility(finalQuery, count);
                    getCurrentViewModel().setTotalMatchCount(count);
                    if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
                        if (count > 0) {
                            mReplyAdapter.updateWithQueryText(finalQuery, mMessagePositionList.get(count - 1).getPosition());
                        } else {
                            mReplyAdapter.updateWithQueryText(finalQuery, mMessagePositionList.get(count).getPosition());
                        }

                    } else {
                        mReplyAdapter.updateWithQueryText(finalQuery, -1);
                    }

                }
            }.execute();

        }
    }


    public void disableThreeDots(boolean isShow, boolean isFromSerchExpand) {
        if (isFromSerchExpand) {
            mMenu.findItem(R.id.call_action).setVisible(conversationType == ConversationType.PERSONAL && isShow);
        }
        if (isShow) {
            if (TextUtils.isEmpty(mDisplay)
                && Utils.getConversationType(mMask, ((GupshupTextMessage) mLastMessageData.getContent()).getText()) == ConversationType.PERSONAL) {
                mMenu.findItem(R.id.action_add_contact).setVisible(true);
            } else {
                mMenu.findItem(R.id.action_add_contact).setVisible(false);
            }

            if (mIsFromBlock) {
                mMenu.findItem(R.id.action_block).setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
            } else {
                mMenu.findItem(R.id.action_block).setVisible(true);
                mMenu.findItem(R.id.action_un_block).setVisible(false);
            }

            if (isArchive) {
                mMenu.findItem(R.id.action_move).setVisible(false);
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
            } else {
                mMenu.findItem(R.id.action_move).setVisible(true);
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
            }
            mMenu.findItem(R.id.action_archive).setVisible(isShow);
            mMenu.findItem(R.id.action_delete).setVisible(isShow);


        } else {
            mMenu.findItem(R.id.action_block).setVisible(isShow);
            mMenu.findItem(R.id.action_un_block).setVisible(isShow);
            mMenu.findItem(R.id.action_add_contact).setVisible(isShow);
            mMenu.findItem(R.id.action_archive).setVisible(isShow);
            mMenu.findItem(R.id.action_delete).setVisible(isShow);
            mMenu.findItem(R.id.action_move).setVisible(isShow);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_block:
                updateConversation(ActionModeType.BLOCKED, mMask);
                item.setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
                mIsFromBlock = true;
                getCurrentParentFragment().updatePagerPosition();
                break;

            case R.id.action_un_block:
                updateConversation(ActionModeType.UN_BLOCK, mMask);
                item.setVisible(false);
                mMenu.findItem(R.id.action_block).setVisible(true);
                mIsFromBlock = false;
                getCurrentParentFragment().updatePagerPosition();
                break;

            case R.id.action_archive:
                if (isArchive) {
                    updateConversation(ActionModeType.UN_ARCHIVE, mMask);
                    isArchive = false;
                    mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
                    mMenu.findItem(R.id.action_move).setVisible(true);
                } else {
                    updateConversation(ActionModeType.ARCHIVE, mMask);
                    isArchive = true;
                    mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
                    mMenu.findItem(R.id.action_move).setVisible(false);
                }
                getCurrentParentFragment().updatePagerPosition();
                break;

            case R.id.action_mute:
                updateConversation(ActionModeType.MUTE, mMask);
                break;

            case R.id.action_delete:
                updateConversation(ActionModeType.DELETE, mMask);
                break;

            case R.id.action_add_contact:
                updateConversation(ActionModeType.ADD_TO_CONTACT, mMask);
                break;

            case R.id.action_move:
                updateConversation(ActionModeType.MOVE, mMask);
                break;

            case R.id.call_action:
                AppUtils.callToPhone(mMask, getActivity());
                break;

            case android.R.id.home:
                LogUtility.d(TAG, "------- OnBack Pressed inFragment ------");
                if (getCurrentViewModel().getSimLayoutVisibility()) {
                    //disable three dots button , search and call button
                    return true;
                } else {
                    handleBackPress();
                    getActivity().onBackPressed();
                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void handleToolbarOnOverlayShow(boolean isDisable) {
        disableThreeDots(!isDisable, false);
        //handle call and search button
        if (getCurrentParentFragment() != null) {
            getCurrentParentFragment().setViewPagerSwipeAble(!isDisable);
        }
        mMenu.findItem(R.id.call_action).setEnabled(!isDisable);
        mMenu.findItem(R.id.conversation_action_search).setEnabled(!isDisable);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEFAULT_SMS_REQUEST_CODE) {
            switch (resultCode) {

                case Activity.RESULT_OK:
                    // Write your logic here
                    if (mIsSendClicked) {
                        callSendMessage();
                        mIsSendClicked = false;
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    // Write your logic here
                    getActivity().onBackPressed();
                    break;
            }
        } else if (requestCode == AppConstants.ADD_CONTACT_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                Pair<String, String> userInfo = MessageUtils.getContactUserDetail(getActivity(), mMask);
                if (userInfo != null) {
                    mDisplay = userInfo.first;
                }
                ToolbarInfo info = new ToolbarInfo();
                info.toolBarColor = -1;
                info.toolbarTitle = TextUtils.isEmpty(mDisplay) ? mMask : mDisplay;
                info.isVissible = true;
                //rx event to change the toolbar title and color
                RxEvent event = new RxEvent(AppConstants.RxEventName.MESSAGE_REPLY_TOOLBAR_INFO, info);
                rxBus.send(event);
            }
        }
    }

    /**
     * Method used to save message as as draft
     */
    public void handleBackPress() {
        String messageTobeSend = mBinding.sendEditTxt.getText().toString().trim();
        if (!TextUtils.isEmpty(mSavedDraftMessage) || !TextUtils.isEmpty(messageTobeSend)) {
            //Insert or Update draft message
            if (mSavedDraftMessage != null && !mSavedDraftMessage.equalsIgnoreCase(messageTobeSend)) {
                getCurrentViewModel().insertOrUpdateDraft(mMask, messageTobeSend, DateUtils.currentTimestamp());
            } else if (mSavedDraftMessage == null && !TextUtils.isEmpty(messageTobeSend)) {
                getCurrentViewModel().insertOrUpdateDraft(mMask, messageTobeSend, DateUtils.currentTimestamp());
            }
        }
        //get last message of the conversation
        if (!TextUtils.isEmpty(mSavedDraftMessage) && TextUtils.isEmpty(messageTobeSend)) {
            List<MessageEntity> messageEntityList = MessagingService.getInstance(SMSService.class).getLastMessageForAMask(mMask);
            if (messageEntityList != null && messageEntityList.size() > 0) {
                MessageEntity entityMsg = messageEntityList.get(0);
                getCurrentViewModel().insertOrUpdateDraft(mMask, messageTobeSend, entityMsg.getMessageTimestamp());
            }
            PagedList<MessageEntity> listMessageEntity = getCurrentViewModel().observeData().getValue();
            if (listMessageEntity != null
                && listMessageEntity.size() == 0) {
                updateConversation(ActionModeType.DELETE, mMask);
            }
        }
        DialogUtils.dismissKeyboard(getActivity(), mBinding.sendEditTxt);
        //

    }


    private void callSendMessage() {

        mSimList = AppUtils.getSimInfoList();
        String messageTobeSend = mBinding.sendEditTxt.getText().toString().trim();
        //when there is draft message and send button clicked need to clear draft message
        if (!TextUtils.isEmpty(messageTobeSend)) {
            isScrollNeed = true;
            setCarrierInfo();
            sendMessageWithSubId(mSubscriptionID, mDraftMessageCreationTime);
        }
        if (!TextUtils.isEmpty(mSavedDraftMessage)) {
            getCurrentViewModel().insertOrUpdateDraft(mMask, AppConstants.EMPTY_TEXT, DateUtils.currentTimestamp());
            mSavedDraftMessage = AppConstants.EMPTY_TEXT;
            mDraftMessageCreationTime = -1;
        }
    }


    private void sendMessageWithSubId(int mSubscriptionID, long draftMessageCreationTime) {
        String messageTobeSend = mBinding.sendEditTxt.getText().toString().trim();
        Utils.trySendingDataMessage(getActivity(), new GupshupTextMessage(messageTobeSend), mMask,
            mPrevSelectedSimSlot, draftMessageCreationTime, false);
        mBinding.sendEditTxt.setText("");
    }


    private void setCarrierInfo() {

        Pair<String, Integer> info = AppUtils.getSimSubscriptionInfo(getActivity(), mPrevSelectedSimSlot);
        if (info != null) {
            mCarierName = info.first;
            try {
                mSubscriptionID = info.second;
            } catch (NullPointerException e) {
                mSubscriptionID = 0;
            }
        }
    }

    private void showSimSelectionLayout() {
        getCurrentViewModel().showSimListLayout(mSimList, getActivity());
    }

    @Override
    public void onDestroy() {
        clearReferences();
        super.onDestroy();
    }


    @Override
    public void onPause() {
        clearReferences();
        super.onPause();
    }

    protected void clearReferences() {
        getCurrentViewModel().setSimVisibilityInPreference(getActivity(), false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser
            && mMask != null) {
            MessagingService.getInstance(SMSService.class).readConversation(mMask, DateUtils.currentTimestamp());
        }
    }

    public void cancelForwardDialog() {
        //cancel forward dialog
        if (forwardDialog != null) {
            forwardDialog.dismiss();
            forwardDialog = null;
        }
    }

    public void showHideSimLayout() {
        if (getCurrentViewModel().getSimLayoutVisibility()) {
            getCurrentViewModel().handleSimLayoutVisibility();
            getCurrentViewModel().setSimVisibilityInPreference(getActivity(), false);
            if (getCurrentParentFragment() != null) {
                getCurrentParentFragment().setViewPagerSwipeAble(true);
            }
            handleToolbarOnOverlayShow(false);
        } else {
            handleBackPress();
        }
    }

    /**
     * This method will return current parent fragment of this child
     *
     * @return
     */
    public MessageReplyPagerFragment getCurrentParentFragment() {

        Fragment fragment = getParentFragment();
        try {
            return (MessageReplyPagerFragment) fragment;
        } catch (Exception e) {
            throw new ClassCastException(fragment.getClass().getSimpleName() + "Cannot cast to"
                + MessageReplyPagerFragment.class.getSimpleName());
        }
    }

    public void setSharedContact(String shareContacts) {
        String previousText = mBinding.sendEditTxt.getText().toString();
        if (!TextUtils.isEmpty(previousText)
            && !previousText.endsWith("\n\n")) {
            previousText = previousText.concat("\n\n");
        }
        mBinding.sendEditTxt.setText(AppUtils.getFormatedShareContact(previousText, shareContacts));
        mBinding.sendEditTxt.setSelection(mBinding.sendEditTxt.getText().length());
    }
}
