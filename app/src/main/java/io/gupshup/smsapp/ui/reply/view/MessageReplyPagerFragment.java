package io.gupshup.smsapp.ui.reply.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.MessageReplyBundleData;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.databinding.MessageReplyPagerFragNewBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.events.ShareContactEvent;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.MessageReplyAdapterNew;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.reply.viewmodel.MessageReplyPagerFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Naveen BM on 7/17/2018.
 */

public class MessageReplyPagerFragment extends BaseFragment<MessageReplyPagerFragNewBinding, MessageReplyPagerFragmentViewModel> implements RxBusCallback, ViewPager.OnPageChangeListener {
    private MessageReplyBundleData parcelableData;
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;
    private String TAG = MessageReplyPagerFragment.class.getSimpleName();
    private MessageReplyAdapterNew messageReplyAdapterNew;
    private String mMask, mConversationSubType;
    private List<String> mMasksList;


    @Override
    public int getBindingVariable() {
        return BR.replyPagerFrag;
    }

    @Override
    public int getLayoutId() {
        return R.layout.message_reply_pager_frag_new;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        getBundleData();
    }

    @Override
    public MessageReplyPagerFragmentViewModel getViewModel() {
        return ViewModelProviders.of(this).get(MessageReplyPagerFragmentViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getBundleData() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey(AppConstants.BundleKey.PARCEL)) {
                parcelableData = bundle.getParcelable(AppConstants.BundleKey.PARCEL);
            }

            if (bundle.containsKey(AppConstants.BundleKey.MASK)) {
                mMask = (String) bundle.get(AppConstants.BundleKey.MASK);
            }
            if (bundle.containsKey(AppConstants.BundleKey.CONVERSATION_SUB_TYPE)) {
                mConversationSubType = (String) bundle.get(AppConstants.BundleKey.CONVERSATION_SUB_TYPE);
            }
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getConversationEntity();
        messageReplyAdapterNew = new MessageReplyAdapterNew(getChildFragmentManager(), parcelableData, mMasksList);
        mBinding.fragmentPager.setAdapter(messageReplyAdapterNew);
        // mBinding.fragmentPager.setOffscreenPageLimit(3);
        mBinding.fragmentPager.addOnPageChangeListener(this);
        setPagerPosition(mMasksList.indexOf(mMask));
    }


    /**
     * Method used to get the list of conversation entity ,
     * which will be used to construct the viewpager
     */
    private void getConversationEntity() {

        mMasksList = MessagingService.getInstance(SMSService.class)
            .getAllConversationsForType(parcelableData.conversationType.name(), mConversationSubType);
    }

    /**
     * method used to swipe to perticular position of the viewpager
     * @param position
     */
    private void setPagerPosition(int position) {
        mBinding.fragmentPager.setCurrentItem(position);
        onPageSelected(position);
    }


    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }

    @Override
    public void onEventTrigger(Object object) {
        RxEvent event = (RxEvent) object;
        if (event.getEventName().equalsIgnoreCase(AppConstants.FORWARD_DIALOG_CANCEL_CLICKED)) {
            //dismiss the forward dialog
            getCurrentVisibleFragment().cancelForwardDialog();
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.RxEventName.SHO_HIDE_SIM_LAY_OUT)) {
            //handle system back press
            //dismiss the forward dialog
            getCurrentVisibleFragment().showHideSimLayout();

        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_SHARE_CONTACT_CLICK)) {
            ShareContactEvent sharedContactEvent = (ShareContactEvent) event.getEventData();
            String shareContacts = sharedContactEvent.getFormatedShareContact();
            getCurrentVisibleFragment().setSharedContact(shareContacts);
        }else if(event.getEventName().equalsIgnoreCase(AppConstants.RxEventName.ACTION_MODE_MESSAGE_COUNT_UPDATE)){
            getCurrentVisibleFragment().actionModeSetup();
        }
    }


    /**
     * This method will return current active fragment from viewpager.
     *
     * @return
     */
    public MessageReplyFragment getCurrentVisibleFragment() {

        Fragment fragment = (Fragment) messageReplyAdapterNew.instantiateItem(mBinding.fragmentPager,
            mBinding.fragmentPager.getCurrentItem());
        try {
            return ((MessageReplyFragment) fragment);
        } catch (Exception e) {
            throw new ClassCastException(fragment.getClass().getSimpleName() + "Cannot cast to"
                + MessageReplyFragment.class.getSimpleName());
        }
    }

    /**
     * To enable/disable viewpager swipe feature.
     *
     * @param canSwipe
     */
    public void setViewPagerSwipeAble(boolean canSwipe) {
        mBinding.fragmentPager.setViewPagerSwipeAble(canSwipe);
    }


    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (mMasksList != null && mMasksList.size() > 0) {
            if (position < 0 || position == mMasksList.size()) {
                return;
            }
            List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mMasksList.get(position));
            int themeColorFromTile = AppUtils.getThemeColor(getActivity(), conversationEntity.get(0));
            ToolbarInfo info = new ToolbarInfo();
            info.toolBarColor = themeColorFromTile;
            info.toolbarTitle = AppUtils.getDisplayName(conversationEntity.get(0));
            info.isVissible = true;
            //rx event to change the toolbar title and color
            RxEvent event = new RxEvent(AppConstants.RxEventName.MESSAGE_REPLY_TOOLBAR_INFO, info);
            rxBus.send(event);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public void updatePagerPosition() {
        //when conversation move operation happens we need to scroll to other position
        int position = mBinding.fragmentPager.getCurrentItem();
        if (mMasksList.size() == 1 && getActivity() != null) {
            getActivity().onBackPressed();
        }
        mMasksList.remove(position);
        if (messageReplyAdapterNew != null) {
            messageReplyAdapterNew.updateMaskList(mMasksList);
            onPageSelected(position);
        }
    }

}
