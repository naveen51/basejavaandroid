package io.gupshup.smsapp.ui.search.view;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.databinding.FragmentMessageSearchResultBinding;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.ui.adapter.MessageSearchResultAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.search.viewmodel.MessageSearchViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Ram Prakash Bhat on 31/5/18.
 */

public class MessageSearchResultFragment extends BaseFragment<FragmentMessageSearchResultBinding, MessageSearchViewModel> {


    private String mSearchQuery = AppConstants.EMPTY_TEXT;
    private MessageSearchResultAdapter mMessageSearchResultAdapter;
    private String TAG = MessageSearchResultFragment.class.getSimpleName();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mSearchQuery = bundle.getString(AppConstants.BundleKey.SEARCH_QUERY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMessageSearchResultAdapter = new MessageSearchResultAdapter(this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
            false);
        mBinding.recyclerView.setLayoutManager(manager);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mBinding.recyclerView.setAdapter(mMessageSearchResultAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        setSearchQuery(mSearchQuery);
    }

    @Override
    public int getBindingVariable() {
        return BR.messageSearchViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_message_search_result;
    }

    @Override
    public MessageSearchViewModel getViewModel() {
        return ViewModelProviders.of(this).get(MessageSearchViewModel.class);
    }

    public void setSearchQuery(String searchQuery) {
        mSearchQuery = searchQuery;
        //removeObserver();
        PagedList<GlobalSearchEntity> pagedList = getCurrentViewModel().getSearchedData(searchQuery, getActivity());
        getCurrentViewModel().showHideEmptyView(pagedList.size() <= 0,
            getString(R.string.no_result_found, mSearchQuery));
        mMessageSearchResultAdapter.submitList(pagedList);
    }

    @Override
    public void onItemClick(View view, int position) {

        GlobalSearchEntity item = (GlobalSearchEntity) view.getTag();
        switch (view.getId()) {
            case R.id.item_container:
                MainActivity.hasCalledAnotherActivity = true;
                LogUtility.d(TAG, "---- Item Clicked ---------");
                AppUtils.launchMessageReplyActivityForSearch(view.getContext(), item, mSearchQuery);
                getActivity().overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
                break;
            case R.id.icon_un_selected:

                if (Utils.getConversationType(item.getMask(),item.getLastMessageData()) == ConversationType.PERSONAL) {
                    int phoneContactID = canAddToContact(item.getMask());
                    if (phoneContactID < 0) {
                        showAddToContactDialog(item.getMask());
                    } else if (Utils.getConversationType(item.getMask(),item.getLastMessageData())
                        == ConversationType.PERSONAL) {
                        Utils.showContactDetail(getActivity(), item.getMask(), phoneContactID);
                    }
                }
                break;
        }
        super.onItemClick(view, position);
    }
}
