package io.gupshup.smsapp.ui.home.view;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.databinding.FragmentSplashBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Ram Prakash Bhat on 5/4/18.
 */

public class SplashScreenFragment extends BaseFragment<FragmentSplashBinding, BaseFragmentViewModel> {

    @Inject
    public RxBus rxBus;

    private static int DEFAULT_SMS_REQUEST_CODE = 101;

    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.DialogFragment#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(STYLE_NO_FRAME, R.style.SplashScreenDialogTheme);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    public BaseFragmentViewModel getViewModel() {
        return ViewModelProviders.of(this).get(BaseFragmentViewModel.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBinding.setHandlers(this);
        //showConfigChangeDialog();
        hideToolBar();
    }

    private void hideToolBar() {
        ToolbarInfo info = new ToolbarInfo();
        info.isVissible = false;
        info.toolbarTitle = getString(R.string.message);
        RxEvent event = new RxEvent(AppConstants.RxEventName.TOOLBAR_INFO, info);
        rxBus.send(event);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEFAULT_SMS_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                    rxBus.send(event);
                    break;
                case Activity.RESULT_CANCELED:
                    if (!PreferenceManager.getInstance(getContext()).isRegistrationDone()) {
                        if (!PreferenceManager.getInstance(getContext()).isManualRegistrationTried()) {
                            RxEvent skipEvent = new RxEvent(AppConstants.RxEventName.SHOW_SKIP_SCREEN, null);
                            rxBus.send(skipEvent);
                        } else {
                            RxEvent homeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                            rxBus.send(homeEvent);
                        }
                    } else {
                        RxEvent homeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                        rxBus.send(homeEvent);
                    }
                    break;
            }
        }
    }

    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.splash_next_btn:
                showDefaultSmsDialog();
                break;
            case R.id.splash_skip_btn:
                PreferenceManager.getInstance(getContext()).setManualRegistrationTried(true);
                RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_SKIP_SCREEN, null);
                rxBus.send(event);
                break;
        }
    }
}
