package io.gupshup.smsapp.ui.base.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Ram Prakash Bhat on 6/4/18.
 */

public class BaseFragmentViewModel extends AndroidViewModel {

    protected static final int INITIAL_LOAD_KEY = 60;
    protected static final int PAGE_SIZE = 20;
    protected static final int PREFETCH_DISTANCE = 60;

    public ObservableBoolean showProgressBar = new ObservableBoolean(false);
    public ObservableBoolean showEmptyView = new ObservableBoolean(false);
    public ObservableField<String> emptyViewText = new ObservableField<>();
    public ObservableField<String> mTimerValue = new ObservableField<>(AppConstants.EMPTY_TEXT);
    public ObservableBoolean showTimer = new ObservableBoolean(false);
    public ObservableField<String> messageCounter = new ObservableField<>();
    public ObservableBoolean messageCounterVisibility = new ObservableBoolean(false);


    public BaseFragmentViewModel(@NonNull Application application) {
        super(application);
    }

    public void showProgressBar() {
        showProgressBar.set(true);
    }

    public void hideProgressBar() {
        showProgressBar.set(false);
    }

    public void showHideEmptyView(boolean showHide, String emptyText) {
        showEmptyView.set(showHide);
        if (showHide) {
            emptyViewText.set(emptyText);
            emptyViewText.notifyChange();
        } else {
            emptyViewText.set("");
            emptyViewText.notifyChange();
        }
    }




}
