package io.gupshup.smsapp.ui.compose.view;

import android.content.ContentResolver;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.property.FormattedName;
import ezvcard.property.Telephone;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.databinding.ComposeSmsActivityBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.compose.viewmodel.ComposeSmsActivityViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.FragmentUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Ram Prakash Bhat on 5/4/18.
 */

public class ComposeSmsActivity extends BaseActivity implements BaseHandlers, RxBusCallback {
    private static final String TAG = ComposeSmsActivity.class.getSimpleName();
    private ComposeSmsActivityBinding mBinding;
    private ComposeSmsActivityViewModel mViewModel;
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;
    private String mForwardingMessage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.compose_sms_activity);
        //removing selected contacts when entere the activity
        ContactSingleton.getInstance().removeSelectedContacts();
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        mViewModel = new ComposeSmsActivityViewModel();
        mBinding.setHandlers(this);
        setSupportActionBar(mBinding.toolbarLayout.commonToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolBarTitle(getString(R.string.new_conversation), false);
        checkIntentData();
    }

    /**
     * method used to check control came from the outside contact book
     */
    public void checkIntentData() {
        Intent intent = getIntent();

        // Get intent, action and MIME type
        String action = intent.getAction();
        String type = intent.getType();

        LogUtility.d(TAG,"INETNT DATA "+AppUtils.intentToString(intent));

        if (Intent.ACTION_SEND.equals(action)
            || Intent.ACTION_SENDTO.equals(action)
            || Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            if (type != null) {
                if (AppConstants.MimeTypeType.MIME_TYPE_TEXT_PLAIN.equalsIgnoreCase(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (AppConstants.MimeTypeType.MIME_TYPE_TEXT_VCARD.equalsIgnoreCase(type)
                    || AppConstants.MimeTypeType.MIME_TYPE_TEXT_X_VCARD.equalsIgnoreCase(type)) {
                    if (intent.getExtras() == null) {
                        Toast.makeText(getBaseContext(), R.string.content_not_found, Toast.LENGTH_SHORT).show();
					return;
					}
                    extractContactInfo(Uri.parse(intent.getExtras()
                        .get(Intent.EXTRA_STREAM).toString()));// Handle contact being sent
                } else {
                    Toast.makeText(getBaseContext(), R.string.content_not_supported, Toast.LENGTH_LONG).show();
                }
            } else {
                handleSendText(intent);
            }
        }
        if (intent.getData() != null) {
            //Launch MessageReplyActivity if conversation exist else go to new conversation
            // contact fragment and select particular contact.
            Contact contact = mViewModel.getIntentContact(intent, this);
            if (contact != null) {
                ContactSingleton.getInstance().addContact(contact);
                changeFragmentToConversationFrag();
            } else {
                if (getIntent() != null && getIntent().hasExtra(AppConstants.KEY_FORWARD_MESSAGE)) {
                    mForwardingMessage = getIntent().getStringExtra(AppConstants.KEY_FORWARD_MESSAGE);
                }
                changeFragmentToContactFrag();
            }

        } else {
            if (getIntent() != null && getIntent().hasExtra(AppConstants.KEY_FORWARD_MESSAGE)) {
                mForwardingMessage = getIntent().getStringExtra(AppConstants.KEY_FORWARD_MESSAGE);
            }
            changeFragmentToContactFrag();
        }
    }

    /**
     * This method will extract contact info from contact book(Direct share from contact book).
     * This will take vcard file uri and using Ezvcard,
     * this will parse file content and return formatted contact info.
     *
     * @param vcfUri Vcf file uri from contact intent.
     */
    private void extractContactInfo(Uri vcfUri) {

		if(vcfUri == null){
            return;
        }
        ContentResolver cr = getContentResolver();
        InputStream stream = null;
        try {
            stream = cr.openInputStream(vcfUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<VCard> vCardList;
        try {
			if(stream != null) {
                vCardList = Ezvcard.parse(stream).all();
                mForwardingMessage = AppConstants.EMPTY_TEXT;
                for (VCard vCard : vCardList) {
                    constructShareContact(vCard.getFormattedNames(), vCard.getTelephoneNumbers());
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This method will construct contact info in to sharable format.
     *
     * @param numberList which contains list of numbers
     * @param nameList which contains list of names
     */
    private void constructShareContact(List<FormattedName> nameList, List<Telephone> numberList) {

        StringBuilder contactName = new StringBuilder();
        StringBuilder contactNumber = new StringBuilder();
        if(nameList != null) {
            for (FormattedName name : nameList) {
                if (!contactNumber.toString().contains(name.getValue())) {      //de-duplication based on contact no.
                    contactName.append(getString(R.string.share_contact_name_format, name.getValue()));
                }
            }
        }
        if(numberList != null) {
            for (Telephone number : numberList) {
                if (!contactNumber.toString().contains(number.getText())) {
                    contactNumber.append(getString(R.string.share_contact_number_format, number.getText()));
                }
            }
        }
        contactNumber.append("\n");
        if(contactName != null) {
            mForwardingMessage = mForwardingMessage.concat(contactName.toString())
                    .concat(contactNumber.toString());
        }else{
            mForwardingMessage = AppConstants.EMPTY_TEXT;
        }

    }


    /**
     * This method will handle forwarded message from other thread screen(message reply screen)
     *
     * @param intent intent which contains data
     */
    private void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            mForwardingMessage = sharedText;
            // Update UI to reflect text being shared
        }
        if (intent.getExtras() != null) {
            String data = intent.getExtras().getString("sms_body");
            if (data != null) {
                mForwardingMessage = data;
            }
        }
    }

    private void changeFragmentToContactFrag() {
        FragmentUtils.replaceFragment(this, new ConversationContactFragment(),
            R.id.compose_fragment_container);
    }


    /**
     * Method used to replace fragment to Conversation fragment
     */
    private void changeFragmentToConversationFrag() {
        ConversationFragment fragment = new ConversationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.KEY_FORWARD_MESSAGE, mForwardingMessage);
        fragment.setArguments(bundle);
        FragmentUtils.replaceFragment(this, fragment,
            R.id.compose_fragment_container);
    }

    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }
    }


    private void setToolBarTitle(String toolbarTitle, boolean arrowVisibility) {
        getSupportActionBar().setTitle(toolbarTitle);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        Fragment frag = FragmentUtils.getCurrentFragment(this, R.id.compose_fragment_container);
        DialogUtils.hideKeyboard(this);
        if (frag instanceof ConversationFragment) {
            ConversationFragment convFrag = (ConversationFragment) frag;
            boolean simLytVisibiity = convFrag.handleSimlytVisible();
            if (simLytVisibiity) {
                convFrag.saveDraftMessageToDB();
                ContactSingleton.getInstance().removeSelectedContacts();
                PreferenceManager.getInstance(this).removeCoposedText();
                super.onBackPressed();
                overridePendingTransition(R.anim.enter_anim_left_to_right, R.anim.exit_anim_right_to_left);
            }
        } else {
            ContactSingleton.getInstance().removeSelectedContacts();
            PreferenceManager.getInstance(this).removeCoposedText();
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_anim_left_to_right, R.anim.exit_anim_right_to_left);
        }

    }


    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        onBackPressed();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }

    @Override
    public void onEventTrigger(Object object) {
        RxEvent event = (RxEvent) object;
        if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_CONTACT_SELECTED)) {
            Contact contact = (Contact) event.getEventData();
            if (ContactSingleton.getInstance().getSelectedContacts().size() == 1 || contact.isFromPlus()) {
                setToolBarTitle(getString(R.string.new_conversation), false);
                changeFragmentToConversationFrag();
            }

        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_ADDCONTACT)) {
            //change the tool bar title and put arrow at the end
            if (ContactSingleton.getInstance().getSelectedContacts().size() > 0) {
                setToolBarTitle(getString(R.string.add_people), true);
            } else {
                setToolBarTitle(getString(R.string.new_conversation), false);
            }
            changeToolBarTheme(ContextCompat.getColor(this, R.color.colorPrimary));
            changeFragmentToContactFrag();
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_DEFAULT_APP_PERMISSION_DENIED) || event.getEventName().equalsIgnoreCase(AppConstants.ACTIVITY_EXIT_EVENT)) {
            //default app permission denied
            onBackPressed();
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_UPDATE_TITLE)) {
            String tittleName = (String) event.getEventData();
            setToolBarTitle(tittleName, false);
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.HANDLE_TOOLBAR_CONTENT_EVENT)) {
            //this event recieved when contact size is zero, hide the right arrow and change the title
            setToolBarTitle(getString(R.string.new_conversation), false);
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_CONVERSATION_THEME_CHANGE)) {
            int themeColor = (int) event.getEventData();
            changeToolBarTheme(themeColor);
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_NAVIGATION_CLICKED)) {
            setToolBarTitle(getString(R.string.new_conversation), false);
            changeFragmentToConversationFrag();
        }
    }


    public void changeToolBarTheme(int themeColor) {
        mBinding.toolbarLayout.commonToolbar.setBackgroundColor(themeColor);
        AppUtils.darkenStatusBar(this, themeColor);
    }

}
