package io.gupshup.smsapp.ui.compose.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.dao.SearchDAO;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.ConversationFragBinding;
import io.gupshup.smsapp.databinding.DialogAddToContactComposeBinding;
import io.gupshup.smsapp.databinding.DialogMessageDetailBinding;
import io.gupshup.smsapp.databinding.DialogMessageMoveConversationBinding;
import io.gupshup.smsapp.databinding.MsgForwardLytBinding;
import io.gupshup.smsapp.enums.ActionModeType;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.events.ForwardMessageEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.events.ShareContactEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.ForwardContactAdapter;
import io.gupshup.smsapp.ui.adapter.ReplyAdapter;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.compose.viewmodel.ConversationFragmentViewModel;
import io.gupshup.smsapp.ui.reply.viewmodel.ForwardMessageViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.smsapp.utils.UiUtils;
import io.gupshup.soip.sdk.message.GupshupTextMessage;


/**
 * Created by Naveen BM on 4/10/2018.
 */
public class ConversationFragment extends Fragment implements BaseHandlers, RxBusCallback, RecyclerViewPageListAdapter.onItemClickListener, ReplyAdapter.onMessageItemClickListener, ActionMode.Callback, SearchView.OnQueryTextListener, DialogUtils.DialogClickListener {
    private static final String TAG = "CONVERSATIONCONTACTFRAGMENT";
    @Inject
    public RxBus rxBus;
    private ConversationFragBinding mBinding;
    private LinearLayout mContainerLinearLyt, mTileContainerLyt;
    private ConversationFragmentViewModel mViewModel;
    private RecyclerView mComposeRecyclerView;
    private boolean mIsNewConversation = true;
    // private ComposeAdapter adapter;
    private ReplyAdapter adapter;
    private ArrayList<String> mContactNumber = new ArrayList<>();
    private String mMask;
    private RxBusHelper rxBusHelper;
    private List<SimInformation> mSimList;
    private int mSubscriptionID;
    private String mCarierName, mRecepientThumbNail;
    private String mTitleContactName;
    private static int DEFAULT_SMS_REQUEST_CODE = 101;
    private int mPrevSelectedSimSlot = 0;
    private String mDraftMessage;
    private String mForwardMessage;
    private boolean isSimPresent;
    private boolean isScrollNeed;
    private boolean mIsSendClicked;
    private ActionMode mActionMode;
    public Dialog mMessageSwitchDialog;
    private ForwardContactAdapter forwardAdapter;
    private Dialog forwardDialog = null;
    public Dialog mAddToContactDialog;
    private Menu mMenu;
    private boolean isArchive;
    private int mThemeColorFromTile;
    private String mDisplayName;
    private MessageEntity[] mDeletableMessage;
    private boolean mIsFromBlock;
    private ConversationEntity mDeletableConversation;
    private List<SearchDAO.MessageEntityExtended> mMessagePositionList;
    private RecyclerView.SmoothScroller smoothScroller;
    private long mDraftMessageCreationTime = -1;
    private boolean mSendTapped;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.conversation_frag, container, false);
        mViewModel = ViewModelProviders.of(this).get(ConversationFragmentViewModel.class);
        mBinding.setConversationViewModel(mViewModel);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        getBundleData();
        initializeComponentViews();
        isSimPresent = AppUtils.checkSimCardExistence(getActivity());
        setThemeColor();
        getDataFromDB();
        mBinding.setHandlers(this);
        setComposedText();
        LogUtility.d(TAG, "------ Conversation Fragment ------");

        smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        return mBinding.getRoot();
    }

    private void setThemeColor() {
        mDisplayName = mViewModel.getContactName();
        getThemeColor(mDisplayName);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_reply_conversation, menu);
        mMenu = menu;
        disableThreeDots(false, false);


        final MenuItem menuItem = mMenu.findItem(R.id.conversation_action_search);
        SearchView searchView = (SearchView) mMenu.findItem(R.id.conversation_action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.message_search_hint));


        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                disableThreeDots(false, false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                disableThreeDots(true, false);
                return true;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_block:
                updateConversation(ActionModeType.BLOCKED, mMask);
                item.setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
                mIsFromBlock = true;
                break;

            case R.id.action_un_block:
                updateConversation(ActionModeType.UN_BLOCK, mMask);
                item.setVisible(false);
                mMenu.findItem(R.id.action_block).setVisible(true);
                mIsFromBlock = false;
                break;

            case R.id.action_archive:
                if (isArchive) {
                    updateConversation(ActionModeType.UN_ARCHIVE, mMask);
                    isArchive = false;
                    mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
                    mMenu.findItem(R.id.action_move).setVisible(true);
                } else {
                    updateConversation(ActionModeType.ARCHIVE, mMask);
                    isArchive = true;
                    mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
                    mMenu.findItem(R.id.action_move).setVisible(false);
                }
                break;

            case R.id.action_mute:
                updateConversation(ActionModeType.MUTE, mMask);
                break;

            case R.id.action_delete:
                updateConversation(ActionModeType.DELETE, mMask);
                break;

            case R.id.action_add_contact:
                updateConversation(ActionModeType.ADD_TO_CONTACT, mMask);
                break;

            case R.id.action_move:
                updateConversation(ActionModeType.MOVE, mMask);
                break;

            case R.id.call_action:
                AppUtils.callToPhone(mMask, getActivity());
                break;

            case android.R.id.home:
                LogUtility.d(TAG, "------- OnBack Pressed inFragment ------");
                if (mViewModel.getSimLayoutVisibility()) {
                    //disable three dots button , search and call button
                    return true;
                } else {
                    getActivity().onBackPressed();
                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method used to get the sub type from the mask
     */
    private void getSubTypeFromConversation() {
        List<ConversationEntity> conv = MessagingService.getInstance(SMSService.class).getConversationByMask(mMask);
        if (conv != null && conv.size() > 0) {
            ConversationEntity convEntity = conv.get(0);
            if (convEntity.getSubType() == ConversationSubType.ARCHIVE) {
                isArchive = true;
            } else {
                isArchive = false;
            }
        }
    }

    /**
     * method used to set previosly composed text to the edit text while adding more contacts
     */
    private void setComposedText() {
        String savedConv = PreferenceManager.getInstance(getActivity()).getComposedText();
        mViewModel.mEditText.set(savedConv);
    }


    /**
     * method used to get the bundle data
     */
    private void getBundleData() {
        if (getArguments() != null) {
            mForwardMessage = getArguments().getString(AppConstants.KEY_FORWARD_MESSAGE);
        }
    }

    private void initPage() {
        mSimList = AppUtils.getSimInfoList();
        if (mSimList != null && mSimList.size() > 1) {
            mViewModel.mSimVisibility.set(true);
            mViewModel.mFirstSimColor.set(mSimList.get(0).getIconTint());
            mViewModel.mSecondSimColor.set(mSimList.get(1).getIconTint());
        } else {
            //make sim visibility gone
            mViewModel.mSimVisibility.set(false);
        }

    }

    private void getDataFromDB() {
        ArrayList<String> contactNumber = mViewModel.getContactNumber();
        if (contactNumber != null && contactNumber.size() == 1) {
            //for single contact showing thumbnail
            mRecepientThumbNail = mViewModel.getRecepientThumbNail(getActivity(), contactNumber.get(0));
        }

        //handling broadcast text visibility
        mViewModel.handleBroadcastTextVisibility();

        mViewModel.getMaskByPhone(contactNumber);
        mViewModel.observeMaskData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String mask) {
                if (mask != null && !TextUtils.isEmpty(mask)) {
                    LogUtility.d("CONVMASK", "----- MASK --------" + mask);
                    mMask = mask;
                    getDBDataOnMask(mask);
                    getConversationOnMask(mask);
                    //To control notification visibility;
                    MainApplication.getInstance().setIsApplicationActive(true);
                }
            }
        });

    }

    /**
     * Method used to get the random color for thread
     *
     * @param contactNumber which can be display name or number
     */
    private void getThemeColor(String contactNumber) {
        TypedArray colors = getResources().obtainTypedArray(R.array.letter_tile_colors);
        mThemeColorFromTile = AppUtils.pickColor(contactNumber, colors);
        mViewModel.themeColor.set(mThemeColorFromTile);
        rxBus.send(new RxEvent(AppConstants.EVENT_CONVERSATION_THEME_CHANGE, mThemeColorFromTile));

    }

    /**
     * method used to get the Conversation entity to check it has draft messages or not
     * This is only for single contact , Multiple contact support is not there
     *
     * @param mask mask is phone number appended with country code
     */
    private void getConversationOnMask(String mask) {
        ArrayList<Contact> contactList = new ArrayList<>();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
        if (contactList.size() == 1) {
            //call conversation entity APi
            ConversationEntity conversationEntity = mViewModel.getConversationEntityFromDB(mask);
            //check if there is already existing conversation then set that sim else first sim as default sim
            if (conversationEntity != null && conversationEntity.getLastMessageId() != null) {
                String lastMessageID = conversationEntity.getLastMessageId();
                MessageEntity lastMessageData = mViewModel.getLastMessageData(lastMessageID);
                if (lastMessageData != null) {
                    mPrevSelectedSimSlot = lastMessageData.getIncomingSIMID();
                } else {
                    mPrevSelectedSimSlot = AppUtils.getDefaultSelectedSim(getActivity());
                }
            } else {
                mPrevSelectedSimSlot = AppUtils.getDefaultSelectedSim(getActivity());
            }
            setForwardOrDraft(conversationEntity);
        } else {
            //dont call conversation entity API
            if (!TextUtils.isEmpty(mForwardMessage)) {
                //when forward and draft bboth are there give priority to forward message
                mViewModel.mEditText.set(mForwardMessage);
            } else {
                mViewModel.mEditText.set("");
                setComposedText();
            }
        }
        if (isSimPresent) {
            setPreviouslySelectedSimData();
        }
    }

    /**
     * Method used to set forward or draft messages
     *
     * @param conversationEntity
     */
    private void setForwardOrDraft(ConversationEntity conversationEntity) {
        boolean enable = AppUtils.getSimInfoList().size() > 0;
        if (conversationEntity != null && !TextUtils.isEmpty(conversationEntity.getDraftMessage())) {
            mDraftMessage = conversationEntity.getDraftMessage();
            mDraftMessageCreationTime = conversationEntity.getDraftCreationTime();
            if (!TextUtils.isEmpty(mForwardMessage)
                && mForwardMessage.trim().length() > 0) {
                //when forward and draft both are there give priority to forward message
                mViewModel.mEditText.set(mForwardMessage);
                mViewModel.mShowSendImage.set(enable);
            } else if (!TextUtils.isEmpty(mDraftMessage)) {
                mViewModel.mEditText.set(mDraftMessage);
                mViewModel.mShowSendImage.set(enable);
            }
        } else {
            if (!TextUtils.isEmpty(mForwardMessage)
                && mForwardMessage.trim().length() > 0) {
                //when forward and draft bboth are there give priority to forward message
                mViewModel.mEditText.set(mForwardMessage);
                mViewModel.mShowSendImage.set(enable);
            }
        }
    }

    /**
     * Method used to previously selected sim
     */
    private void setPreviouslySelectedSimData() {

        if (mPrevSelectedSimSlot == 0) {
            mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_one));
            mViewModel.mIsFirstSim.set(true);
        } else {
            mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_two));
            mViewModel.mIsFirstSim.set(false);
        }
        Pair<String, Integer> info = AppUtils.getSimSubscriptionInfo(getActivity(), mPrevSelectedSimSlot);
        if (info != null) {
            mCarierName = info.first;
            try {
                mSubscriptionID = info.second;
            } catch (NullPointerException e) {
                mSubscriptionID = 0;
            }
        }
    }


    /**
     * Method used to get the message entities based on the mask
     *
     * @param mask mask is phone number appended with country code
     */
    public void getDBDataOnMask(final String mask) {
        mViewModel.getMessageForSelectedContact(mask);
        // adapter = new ComposeAdapter(mRecepientThumbNail);
        adapter = new ReplyAdapter(mRecepientThumbNail, this, this, TextUtils.isEmpty(mDisplayName) ? mask : mDisplayName, mThemeColorFromTile, getActivity());
        mComposeRecyclerView.setAdapter(adapter);
        mViewModel.observeData().observe(this, new Observer<PagedList<MessageEntity>>() {
            @Override
            public void onChanged(@Nullable PagedList<MessageEntity> messageEntities) {
                if (messageEntities != null && messageEntities.size() > 0) {
                    mIsNewConversation = false;
                    adapter.submitList(messageEntities);
                    mViewModel.mIsShowOldConv.set(true);
                    mViewModel.mIsShowFreshConv.set(false);

                    //handle the scroll of recieved message
                    MessageEntity lastMessage = messageEntities.get(0);
                    if (lastMessage != null
                        && lastMessage.getDirection() == MessageDirection.INCOMING) {
                        isScrollNeed = true;
                    }
                    if (isScrollNeed) {
                        mComposeRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mComposeRecyclerView.smoothScrollToPosition(0);
                                isScrollNeed = false;
                            }
                        }, 300);
                    }
                    addContactToContainer();
                } else {
                    //New Conversation  show new Conversation Lyt
                    mIsNewConversation = true;
                    addContactToContainer();
                    mViewModel.mIsShowFreshConv.set(true);

                }

            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initializeComponentViews() {
        mContainerLinearLyt = mBinding.contactContainer;
        mComposeRecyclerView = mBinding.composeRecycler;
        mTileContainerLyt = mBinding.tileContainer;
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        ((SimpleItemAnimator) mComposeRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        manager.setReverseLayout(true);
        mComposeRecyclerView.setLayoutManager(manager);

        mBinding.scrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return false;
            }
        });


    }

    final GestureDetector detector = new GestureDetector(getActivity(), new GestureDetector.OnGestureListener() {

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            addConversationToPref();
            rxBus.send(new RxEvent(AppConstants.EVENT_ADDCONTACT, null));
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            return false;
        }

        // Note that there are more methods which will appear here
        // (which you probably don't need).
    });


    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.add_contact_img:
            case R.id.selected_type_lyt:
            case R.id.selected_innner_lyt:
                addConversationToPref();
                rxBus.send(new RxEvent(AppConstants.EVENT_ADDCONTACT, null));
                break;


            case R.id.send_view:
            case R.id.send_view_container:
                if (getActivity() != null
                    && !AppUtils.isDefaultSmsPackage(getActivity(), getActivity().getPackageName())) {
                    mIsSendClicked = true;
                    showDefaultSmsDialog();
                    return;
                }
                callSendMessage();
                break;

            case R.id.first_sim_lyt:
                handleToolbarOnOverlayShow(false);
                mViewModel.mShowSimLayout.set(false);
                if (mSimList != null && mSimList.size() > 0) {
                    mSubscriptionID = mSimList.get(0).getmSubscriptionID();
                    mPrevSelectedSimSlot = mSimList.get(0).getSimSlotIndex();
                    mCarierName = mSimList.get(0).getmSimName();
                    mPrevSelectedSimSlot = mSimList.get(0).getSimSlotIndex();
                    mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_one));
                    mViewModel.mIsFirstSim.set(true);
                    PreferenceManager.getInstance(getActivity()).setSelectedSubscriptionID(mSubscriptionID);
                    PreferenceManager.getInstance(getActivity()).setSelectedCarier(mCarierName, getString(R.string.sim_slot_one));
                }
                break;

            case R.id.second_sim_lyt:
                handleToolbarOnOverlayShow(false);
                mViewModel.mShowSimLayout.set(false);
                if (mSimList != null && mSimList.size() > 0) {
                    mSubscriptionID = mSimList.get(1).getmSubscriptionID();
                    mCarierName = mSimList.get(1).getmSimName();
                    mPrevSelectedSimSlot = mSimList.get(1).getSimSlotIndex();
                    mPrevSelectedSimSlot = mSimList.get(1).getSimSlotIndex();
                    mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_two));
                    mViewModel.mIsFirstSim.set(false);
                    PreferenceManager.getInstance(getActivity()).setSelectedSubscriptionID(mSubscriptionID);
                    PreferenceManager.getInstance(getActivity()).setSelectedCarier(mCarierName, getString(R.string.sim_slot_two));
                }
                break;

            case R.id.sim_number_img:
                handleToolbarOnOverlayShow(true);
                DialogUtils.hideKeyboard(getActivity());
                showSimSelectionLayout();
                break;

            case R.id.count_decrease_arrow:
                DialogUtils.hideKeyboard(getActivity());
                LogUtility.d("SEARCHCOUNT", "----------- DECREASE CLICKED ----------");
                int messagePositionListSize = mMessagePositionList.size();
                if (messagePositionListSize > 1) {
                    mViewModel.increaseSearchCount();
                    int curPosDec = mViewModel.getCurrentPosition();
                    setScrollPosition(messagePositionListSize - curPosDec, messagePositionListSize - curPosDec + 1);

                }

                break;

            case R.id.count_increase_arrow:
                DialogUtils.hideKeyboard(getActivity());
                LogUtility.d("SEARCHCOUNT", "----------- INCREASE CLICKED ----------");
                int messagePositionListSizeInc = mMessagePositionList.size();
                if (messagePositionListSizeInc > 1) {
                    mViewModel.decreaseSearchCount();
                    int curPos = mViewModel.getCurrentPosition();
                    setScrollPosition(messagePositionListSizeInc - curPos, messagePositionListSizeInc - curPos - 1);
                }
                break;

            case R.id.sim_overlay:
                if (mViewModel.getSimLayoutVisibility()) {
                    mViewModel.mShowSimLayout.set(false);
                    if (mMenu != null) {
                        //to check the menu has been already inflated, then only handle options visibility
                        if (mMenu.findItem(R.id.call_action).isVisible()) {
                            handleToolbarOnOverlayShow(false);
                        }
                    }

                }
                break;

            case R.id.floating_button_container:
                //launch contact picker activity
                Intent intent = new Intent(getActivity(), ShareContactActivity.class);
                startActivity(intent);
                break;

        }

    }

    /**
     * Method used to scroll the recycler view to the particular position
     * and highlight and de-highlight message text
     *
     * @param curPos  current position
     * @param prevPos previous position
     */
    private void setScrollPosition(int curPos, int prevPos) {
        //get the item position from the list
        if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
            //item position in the list
            int curItemPosition = mMessagePositionList.get(curPos).getPosition();
            int prevItemPosition = -1;
            if (prevPos > 0) {
                prevItemPosition = mMessagePositionList.get(prevPos).getPosition();
            }
            adapter.updateSearchItem(curItemPosition, prevItemPosition);
            smoothScroll(curItemPosition);
        }
    }


    /**
     * Method used to save the conversation while adding multiple contacts
     */
    private void addConversationToPref() {
        PreferenceManager.getInstance(getActivity()).removeCoposedText();
        String conv = mBinding.editTxt.getText().toString().trim();
        PreferenceManager.getInstance(getActivity()).setComposedText(conv);
    }

    private void callSendMessage() {
        mSendTapped = true;
        String conv = mBinding.editTxt.getText().toString().trim();


        if (!TextUtils.isEmpty(conv)) {
            mContactNumber = mViewModel.getContactsNumbersToSend();
            boolean isValid;
            isValid = mContactNumber.size() != 1 || Utils.isValidPhoneNumber(mContactNumber.get(0));
            if (isValid) {
                if (!TextUtils.isEmpty(conv)) {
                    isScrollNeed = true;
                }
                setCarrierInfo();
                sendMessageWithSubId(mSubscriptionID, mDraftMessageCreationTime);
                mViewModel.mConversationStarted.set(true);
                rxBus.send(new RxEvent(AppConstants.EVENT_UPDATE_TITLE, mTitleContactName));
                if (mContactNumber != null && mContactNumber.size() == 1) {
                    disableThreeDots(true, false);
                }

            } else {
                DialogUtils.showAlertDialog(getActivity(), getString(R.string.no_valid_recipient), getString(R.string.unable_to_send_title), getString(R.string.ok_btn), null);
            }
        }
        if (!TextUtils.isEmpty(mDraftMessage)) {
            mViewModel.insertOrUpdateDraft(mMask, "");
            mDraftMessage = "";
            mDraftMessageCreationTime = -1;
        }
    }

    private void setCarrierInfo() {

        Pair<String, Integer> info = AppUtils.getChanelName(getActivity(), mPrevSelectedSimSlot);
        if (info != null) {
            mCarierName = info.first;
            try {
                mSubscriptionID = info.second;
            } catch (NullPointerException e) {
                mSubscriptionID = 0;
            }
        }
    }

    private void sendMessageWithSubId(int mSubscriptionID, long draftMessageCreationTime) {
        String msgTxt = mBinding.editTxt.getText().toString().trim();
        mBinding.editTxt.setText("");
        if (mIsNewConversation) {
            if (mViewModel != null) {
                //Its a new conversation so pass Mask as null
                mViewModel.sendMessage(getActivity(), mMask, mContactNumber, msgTxt, mIsNewConversation,
                    mSubscriptionID, mPrevSelectedSimSlot, draftMessageCreationTime);
            }
        } else {
            if (mViewModel != null) {
                //its a existing conversation so pass phone as null
                mViewModel.sendMessage(getActivity(), mMask, null, msgTxt, mIsNewConversation,
                    mSubscriptionID, mPrevSelectedSimSlot, draftMessageCreationTime);
            }
        }

        if (mIsNewConversation) {
            mViewModel.mIsShowOldConv.set(true);
            mViewModel.mIsShowFreshConv.set(false);
        }

    }


    private void showSimSelectionLayout() {
        mViewModel.showSimListLayout(mSimList, getActivity());
    }


    /**
     * Method used to add the contacts at the Top bar with Buble as background
     */
    public void addContactToContainer() {
        mContainerLinearLyt.removeAllViews();
        mTileContainerLyt.removeAllViews();
        ArrayList<Contact> contactList = new ArrayList<>();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
        StringBuilder builder = new StringBuilder();
        if (contactList.size() > 0) {

            for (int i = 0; i < contactList.size(); i++) {
                Contact contact = contactList.get(i);
                if (contact != null) {
                    View bubleView = null;
                    if (contact.getContactName() != null && !TextUtils.isEmpty(contact.getContactName())) {
                        if (builder != null && builder.toString().isEmpty()) {
                            builder.append(contact.getContactName());
                        } else {
                            builder.append(", ").append(contact.getContactName());
                        }
                        bubleView = AppUtils.createContactTextView(contact, getActivity(), mContainerLinearLyt);
                        ImageView imageView = AppUtils.createTile(getActivity(), mTileContainerLyt, contact.getContactName());
                        if (imageView != null) {
                            mTileContainerLyt.addView(imageView);
                        }

                    } else if (contact.getContactPhoneNumber() != null && !TextUtils.isEmpty(contact.getContactPhoneNumber())) {
                        if (builder != null && builder.toString().isEmpty()) {
                            builder.append(contact.getContactPhoneNumber());
                        } else {
                            builder.append(", ").append(contact.getContactPhoneNumber());
                        }
                        bubleView = AppUtils.createContactTextView(contact, getActivity(), mContainerLinearLyt);
                    }
                    mViewModel.constructConversationWith(builder.toString());
                    mTitleContactName = builder.toString();
                    if (bubleView != null) {
                        mContainerLinearLyt.addView(bubleView);
                    }
                }
            }
        } else {
            //back to contact selection screen
            rxBus.send(new RxEvent(AppConstants.EVENT_ADDCONTACT, null));
        }

    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }

    @Override
    public void onEventTrigger(Object event) {
        LogUtility.d(TAG, "-------------- EVENT TRIGGERED -------------" + event.toString());
        if (event instanceof RxEvent) {
            RxEvent eve = (RxEvent) event;
            if (eve.getEventName().equalsIgnoreCase(AppConstants.DELETE_CONTACT)) {
                Contact contact = (Contact) eve.getEventData();
                Map<String, Contact> contactList = ContactSingleton.getInstance().getSelectedContacts();
                if (contact != null) {
                    Iterator<Map.Entry<String, Contact>> it = contactList.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry<String, Contact> pair = it.next();
                        Contact contactSingle = pair.getValue();
                        if (contactSingle != null) {
                            if ((contact.getContactPhoneNumber() != null &&
                                contact.getContactPhoneNumber().toLowerCase().equalsIgnoreCase(contactSingle.getContactPhoneNumber()))) {
                                it.remove();
                            }
                        }
                    }

                    //handle Broadcast text visibility
                    mViewModel.handleBroadcastTextVisibility();
                    addContactToContainer();
                    getDataFromDB();
                }
            } else if (eve.getEventName().equalsIgnoreCase(AppConstants.FORWARD_MESSAGE_EVENT)) {
                ForwardMessageEvent forwardMessageEvent = (ForwardMessageEvent) eve.getEventData();
                MessageEntity msgEntity = forwardMessageEvent.getMessageEntity();
                String mask = forwardMessageEvent.getMask();
                if (msgEntity != null && !TextUtils.isEmpty(mask)) {
                    launchActivityForForward(msgEntity, mask);
                }
            } else if (eve.getEventName().equalsIgnoreCase(AppConstants.NEW_MESSAGE_CLICK_EVENT)) {
                String forwardMessage = (String) eve.getEventData();
                launchComposeActivity(forwardMessage);
            } else if (eve.getEventName().equalsIgnoreCase(AppConstants.FORWARD_DIALOG_CANCEL_CLICKED)) {
                //cancel forward dialog
                if (forwardDialog != null) {
                    forwardDialog.dismiss();
                    forwardDialog = null;
                }
            } else if (eve.getEventName().equalsIgnoreCase(AppConstants.EVENT_SHARE_CONTACT_CLICK)) {
                ShareContactEvent sharedContactEvent = (ShareContactEvent) eve.getEventData();
                String shareContacts = sharedContactEvent.getFormatedShareContact();
                String previousText = mBinding.editTxt.getText().toString();
                if (!TextUtils.isEmpty(previousText)
                    && !previousText.endsWith("\n\n")) {
                    previousText = previousText.concat("\n\n");
                }
                mBinding.editTxt.setText(AppUtils.getFormatedShareContact(previousText, shareContacts));
                mBinding.editTxt.setSelection(mBinding.editTxt.getText().length());
            } else if (eve.getEventName().equalsIgnoreCase(AppConstants.RxEventName.ACTION_MODE_MESSAGE_COUNT_UPDATE)) {
                actionModeSetup();
            }
        }
    }


    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    public boolean handleSimlytVisible() {
        boolean simLyTVisibility = mViewModel.getSimLayoutVisibility();
        if (simLyTVisibility) {
            mViewModel.handleSimLayoutVisibility();
            handleToolbarOnOverlayShow(false);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        if (isSimPresent) {
            //if sim present
            mViewModel.mSendViewClickable.set(true);
            initPage();
        } else {
            //if sim not presend block the click
            mViewModel.mSendViewClickable.set(false);
        }
        mBinding.editTxt.setFocusable(false);
        mBinding.editTxt.setFocusableInTouchMode(true);
        super.onResume();
    }

    private void showDefaultSmsDialog() {
        if (getActivity() != null) {
            final String packageName = getActivity().getPackageName();
            Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, packageName);
            startActivityForResult(intent, DEFAULT_SMS_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEFAULT_SMS_REQUEST_CODE) {
            switch (resultCode) {

                case Activity.RESULT_OK:
                    //nothing will happen
                    if (mIsSendClicked) {
                        callSendMessage();
                        mIsSendClicked = false;
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    //redirect to home page , event is triggered to click back press as user not given Default APP permission
                    rxBus.send(new RxEvent(AppConstants.EVENT_DEFAULT_APP_PERMISSION_DENIED, null));
                    break;
            }
        }
    }

    /**
     * Method used to save draft messages to DB when back pressed
     */
    public void saveDraftMessageToDB() {
        ArrayList<Contact> contactList = new ArrayList<>();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
        if (mIsNewConversation) {
            final String conv = mBinding.editTxt.getText().toString().trim();
            mContactNumber = mViewModel.getContactsNumbersToSend();
            mViewModel.getMaskByPhone(mContactNumber);
            mViewModel.observeMaskData().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String mask) {
                    if (mask != null && !TextUtils.isEmpty(mask)) {
                        if (!TextUtils.isEmpty(mDraftMessage) || !TextUtils.isEmpty(conv)) {
                            if (mDraftMessage != null && conv != null && !mDraftMessage.equalsIgnoreCase(conv)) {
                                mViewModel.createNewConversationAndUpdateDraft(mask, mContactNumber, MessageChannel.SMS, conv);
                            } else if (mDraftMessage == null && !TextUtils.isEmpty(conv)) {
                                mViewModel.createNewConversationAndUpdateDraft(mask, mContactNumber, MessageChannel.SMS, conv);
                            }
                        }
                    }
                }
            });

        } else {
            if (contactList != null && contactList.size() == 1) {
                //save or update message in draft
                String conv = mBinding.editTxt.getText().toString().trim();
                if (!TextUtils.isEmpty(mDraftMessage) || !TextUtils.isEmpty(conv)) {
                    //Insert or Update draft message
                    if (mDraftMessage != null && conv != null && !mDraftMessage.equalsIgnoreCase(conv)) {
                        mViewModel.insertOrUpdateDraft(mMask, conv);
                    } else if (mDraftMessage == null && !TextUtils.isEmpty(conv)) {
                        mViewModel.insertOrUpdateDraft(mMask, conv);
                    }

                }

            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onSelectAllAndScroll() {

    }

    @Override
    public void onItemLongClick(View view, int position) {
        MessageEntity msgEntity = (MessageEntity) view.getTag();
        enableActionMode(position, msgEntity);
    }

    @Override
    public void onItemMessageClickSender(View view, int position, final MessageEntity entity) {
        int count = adapter.getSelectedItemCount();
        if (entity.getMessageStatus() == MessageStatus.FAILED && count == 0) {
            String failedCarrierName = "";
            int failedSubscriptionID = 0;
            //resend message
            Pair<String, Integer> info = AppUtils.getSimSubscriptionInfo(getActivity(), entity.getIncomingSIMID());
            if (info != null) {
                failedCarrierName = info.first;
                try {
                    failedSubscriptionID = info.second;
                } catch (NullPointerException e) {
                    failedSubscriptionID = 0;
                }
            }

            final int finalFailedSubscriptionId = failedSubscriptionID;
            MessageDAO dao = AppDatabase.getInstance(getActivity()).messageDAO();
            entity.setCarierName(failedCarrierName);
            entity.setMessageStatus(MessageStatus.SENDING);
            entity.setSentTimestamp(DateUtils.currentTimestamp());
            dao.update(entity);


            /*final DataService dataService = (DataService) MessagingService.getInstance(DataService.class);
            final String messageId = dataService.preSending(mMask, mMask, entity.getContent(), failedCarrierName, MessageStatus.SENDING, entity.getIncomingSIMID() - 1, "");
            //sim slot is zero indexed and sim id is 1 start...
            dataService.fetchPhoneNumberDetails(entity.getIncomingSIMID() - 1, mMask, new Callback<PhoneNumberDetailsResponse>() {
                @Override
                public void onResponse(Call<PhoneNumberDetailsResponse> call, Response<PhoneNumberDetailsResponse> response) {
                    if (response.isSuccessful()) {
                        try {
                            dataService.sendMessage(messageId, mMask, mMask, ((GupshupTextMessage) entity.getContent()), entity.getIncomingSIMID() - 1);
                        } catch (Exception ex) {
                            //TODO: handle exception based on type...
                        }

                    } else {
                        MessagingService.getInstance(SMSService.class)
                                .postSending(entity.getMessageId(),
                                        DateUtils.currentTimestamp(), -1, MessageStatus.SENDING);
                        MessageUtils.sendSMS(getContext(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(),
                                finalFailedSubscriptionId);
                    }
                }

                @Override
                public void onFailure(Call<PhoneNumberDetailsResponse> call, Throwable t) {
                    MessagingService.getInstance(SMSService.class)
                            .postSending(entity.getMessageId(),
                                    DateUtils.currentTimestamp(), -1, MessageStatus.SENDING);
                    MessageUtils.sendSMS(getContext(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(),
                            finalFailedSubscriptionId);
                }
            });*/

            MessageUtils.sendSMS(getActivity(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(),
                failedSubscriptionID);
        } else {
            if (count > 0) {
                enableActionMode(position, entity);
            } else {
                //showMessageDetailForSender(entity);
            }
        }
    }

    @Override
    public void onItemMessageClickReciepient(View view, int position, MessageEntity entity) {
        int count = adapter.getSelectedItemCount();
        if (count > 0) {
            enableActionMode(position, entity);
        } else {
            if (entity.getMessageStatus() == MessageStatus.FAILED && entity.getDirection() == MessageDirection.OUTGOING) {

                //TODO: Whther we should try data route while resending...???
                //resend message
                MessageUtils.sendSMS(getActivity(), mMask, ((GupshupTextMessage) entity.getContent()).getText(), entity.getMessageId(), mSubscriptionID);
            }
        }
    }


    private void enableActionMode(int pos, MessageEntity entity) {
        if (mActionMode == null) {
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this);
        }
        if (adapter != null) {

            adapter.setBackgroundForSelection(pos, entity);

        }
        actionModeSetup();
    }

    private void actionModeSetup() {
        if (mActionMode == null) {
            return;
        }
        Menu menu = mActionMode.getMenu();
        handleTopBarIconVisibility(menu);
        showSelectAction(menu);
        int count = adapter.getSelectedItemCount();
        if (count == 0) {
            mActionMode.finish();
        } else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }


    /**
     * Method used to handle top bar icon visibility in Action View
     *
     * @param menu Menu which contains options
     */
    private void handleTopBarIconVisibility(Menu menu) {
        if (adapter != null && adapter.getLongItemCount() > 1) {
            menu.findItem(R.id.action_share).setVisible(false);
            menu.findItem(R.id.action_copy).setVisible(false);
            menu.findItem(R.id.action_info).setVisible(false);
            menu.findItem(R.id.action_forward).setVisible(false);
            menu.findItem(R.id.action_delete).setVisible(true);
        } else {
            hideOrShowActionButtons(menu);
            menu.findItem(R.id.action_info).setVisible(true);
            menu.findItem(R.id.action_delete).setVisible(true);
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_message_actions, menu);
        getSubTypeFromConversation();
        hideOrShowActionButtons(menu);
        return true;
    }

    private void hideOrShowActionButtons(Menu menu) {
        menu.findItem(R.id.action_share).setVisible(!isArchive);
        menu.findItem(R.id.action_copy).setVisible(!isArchive);
        menu.findItem(R.id.action_forward).setVisible(!isArchive);

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        PagedList<MessageEntity> listMessageEntity = mViewModel.observeData().getValue();
        List<MessageEntity> selectedMessageEntity = adapter.getSelectedMessages();
        switch (item.getItemId()) {
            case R.id.action_share:
                LogUtility.d("MESSAGEACTIONS", "-------- SHARE --------");
                updateMessageEntity(ActionModeType.SHARE, listMessageEntity, selectedMessageEntity);
                mode.finish();
                return true;

            case R.id.action_copy:
                LogUtility.d("MESSAGEACTIONS", "-------- COPY --------");
                updateMessageEntity(ActionModeType.COPY, listMessageEntity, selectedMessageEntity);
                mode.finish();
                return true;

            case R.id.action_info:
                LogUtility.d("MESSAGEACTIONS", "-------- INFO --------");
                updateMessageEntity(ActionModeType.INFO, listMessageEntity, selectedMessageEntity);
                mode.finish();
                return true;

            case R.id.action_forward:
                LogUtility.d("MESSAGEACTIONS", "-------- FORWARD --------");
                updateMessageEntity(ActionModeType.FORWARD, listMessageEntity, selectedMessageEntity);
                mode.finish();
                return true;

            case R.id.action_delete:
                LogUtility.d("MESSAGEACTIONS", "-------- DELETE --------");
                if (listMessageEntity.size() == 1 || listMessageEntity.size() == selectedMessageEntity.size()) {
                    updateConversation(ActionModeType.DELETE, mMask);
                } else {
                    updateMessageEntity(ActionModeType.DELETE, listMessageEntity, selectedMessageEntity);
                }

                // mode.finish();
                return true;

            case R.id.action_select_all:
                selectAllMessages();
                break;

            case R.id.action_deselect_all:
                deselectAllMessages();
                break;

        }
        return false;
    }


    /**
     * Method used to deselect all messages
     */
    private void deselectAllMessages() {
        adapter.removeMessageEntityFromList();
        actionModeSetup();
    }

    /**
     * Method used to select all messages
     */
    private void selectAllMessages() {
        adapter.addMessageEntityList(mViewModel.observeData().getValue());
        actionModeSetup();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = null;
        if (adapter != null) {
            adapter.notifySelectionClear();
        }
    }


    protected void updateMessageEntity(ActionModeType actionModeType, PagedList<MessageEntity> messageList, List<MessageEntity> selectedItem) {
        MessageEntity[] message = new MessageEntity[selectedItem.size()];
        mDeletableMessage = message;
        message = selectedItem.toArray(message);
        switch (actionModeType) {
            case DELETE:
                String confirmationMessage = getResources().getQuantityString(R.plurals.message_delete_confirmation, message.length, message.length);
                DialogUtils.showConfirmPopUp(getActivity(), confirmationMessage, getString(R.string.delete_dialog_title),
                    getString(R.string.confirm), getString(R.string.cancel_btn), this);
                break;

            case SHARE:
                MessageEntity entityShare = selectedItem.get(0);
                if (entityShare != null) {
                    GupshupTextMessage txtMessage = (GupshupTextMessage) entityShare.getContent();
                    Utils.shareMessage(getActivity(), txtMessage.getText());
                }
                break;

            case INFO:
                MessageEntity entity = selectedItem.get(0);
                if (entity != null) {
                    if (entity.getDirection() == MessageDirection.OUTGOING) {
                        showMessageDetailForSender(entity);
                    } else {
                        showMessageDetailForRecipient(entity);
                    }
                }
                break;

            case COPY:
                MessageEntity entityCopy = selectedItem.get(0);
                if (entityCopy != null) {
                    GupshupTextMessage txtMessage = (GupshupTextMessage) entityCopy.getContent();
                    Utils.copyToClipBoard(getActivity(), txtMessage.getText());
                }

                break;

            case FORWARD:
                MessageEntity entityCopyForward = selectedItem.get(0);
                popupForwardDialog(entityCopyForward);
                break;
        }
    }


    protected void updateConversation(ActionModeType actionModeType, String mask) {
        ConversationEntity conversation = null;
        String movedToText = AppConstants.EMPTY_TEXT;
        ConversationDAO conversationDAO = AppDatabase.getInstance(getActivity()).conversationDAO();
        List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (conversationEntity != null && conversationEntity.size() > 0) {
            conversation = conversationEntity.get(0);
            if (conversation != null) {
                switch (actionModeType) {
                    case BLOCKED:
                        conversation.setLastConversationType(conversation.getType());
                        conversation.setType(ConversationType.BLOCKED);
                        break;
                    case MUTE:
                        conversation.setSendNotifications(false);
                        conversation.setNotificationsSound(false);
                        conversation.setNotificationsLight(false);
                        conversation.setNotificationsPopup(false);
                        break;
                    case UN_MUTE:
                        conversation.setSendNotifications(true);
                        conversation.setNotificationsSound(true);
                        conversation.setNotificationsLight(true);
                        conversation.setNotificationsPopup(true);
                        break;
                    case ARCHIVE:
                        conversation.setSubType(ConversationSubType.ARCHIVE);
                        break;
                    case UN_ARCHIVE:
                        conversation.setSubType(null);
                        break;
                    case UN_BLOCK:
                        conversation.setType(conversation.getLastConversationType());
                        //conversation.setType(Utils.getConversationTypeFromMask(conversation.getMask()));
                        break;
                }
            }
        }


        switch (actionModeType) {
            case BLOCKED:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.moved_to_tab, getString(R.string.block_tab));
                break;
            case UN_BLOCK:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.unblocked_successfully);
                break;
            case ARCHIVE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.moved_to_tab, getString(R.string.archive_block));
                break;
            case UN_ARCHIVE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                movedToText = getString(R.string.un_archived_message);
                break;
            case MUTE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                break;
            case MOVE:
                showMessageDetail(conversation);
                break;
            case DELETE:
                mDeletableMessage = null;
                mDeletableConversation = conversation;
                String confirmationMessage = getResources().getQuantityString(R.plurals.thread_delete_confirmation, 1, 1);
                DialogUtils.showConfirmPopUp(getActivity(), confirmationMessage, getString(R.string.delete_dialog_title),
                    getString(R.string.confirm), getString(R.string.cancel_btn), this);

                break;
            case ADD_TO_CONTACT:
                if (Utils.getConversationType(conversation.getMask(), Utils.getLastMessageInConversation(conversation)) == ConversationType.PERSONAL) {
                    int phoneContactID = canAddToContact(conversation);
                    if (phoneContactID < 0) {
                        showAddToContactDialog(conversation);
                    }
                }
                break;

        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
    }


    /**
     * This method is to show individual message detail.
     */
    public void showMessageDetailForSender(MessageEntity message) {
        DialogMessageDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_detail,
            null, false);
        binding.setMessageEntity(message);
        binding.status.setVisibility(View.VISIBLE);
        final Dialog dialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }


    /**
     * This method is to show individual message detail.
     */
    public void showMessageDetailForRecipient(MessageEntity message) {
        DialogMessageDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_detail,
            null, false);
        binding.setMessageEntity(message);
        binding.status.setVisibility(View.GONE);
        final Dialog dialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }

    /**
     * This method is to show individual message detail.
     */
    public void showMessageDetail(ConversationEntity conversationEntity) {

        mViewModel.showTransactionalFolder.set(conversationEntity.getType() != ConversationType.TRANSACTIONAL);
        mViewModel.showPromoFolder.set(conversationEntity.getType() != ConversationType.PROMOTIONAL);
        mViewModel.showPersonalFolder.set(conversationEntity.getType() != ConversationType.PERSONAL);
        mViewModel.showBlockOption.set(true);


        DialogMessageMoveConversationBinding dialogMessageMoveBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_move_conversation,
            null, false);

        if (ConversationType.BLOCKED == conversationEntity.getType()) {
            mViewModel.blocText.set(getString(R.string.unblock));
            dialogMessageMoveBinding.typeBlock.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.action_unblock_dark), null, null, null);
        } else {
            mViewModel.blocText.set(getString(R.string.block));
            dialogMessageMoveBinding.typeBlock.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_to_block), null, null, null);
        }


        if (ConversationSubType.ARCHIVE == conversationEntity.getSubType()) {
            mViewModel.archiveText.set(getString(R.string.action_un_archive));
            dialogMessageMoveBinding.typeArchive.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_unarchive), null, null, null);
        } else {
            mViewModel.archiveText.set(getString(R.string.archive));
            dialogMessageMoveBinding.typeArchive.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(getActivity(), R.drawable.icn_move_to_archive), null, null, null);
        }

        mMessageSwitchDialog = DialogUtils.getCustomDialog(getActivity(), dialogMessageMoveBinding.getRoot());
        dialogMessageMoveBinding.setConversationFrag(this);
        dialogMessageMoveBinding.setConversationEntity(conversationEntity);
        dialogMessageMoveBinding.setConvFragViewModel(mViewModel);
        mMessageSwitchDialog.setCancelable(true);
        mMessageSwitchDialog.setCanceledOnTouchOutside(true);
        mMessageSwitchDialog.show();
    }


    protected int canAddToContact(ConversationEntity entity) {

        int contactId = -1;
        if (entity != null) {
            contactId = MessageUtils.getContactIDFromNumber(entity.getMask(), getActivity());
        }
        return contactId;
    }


    /**
     * Method used to show the forward Dialog
     *
     * @param entityCopyForward selected message entity for forward
     */
    private void popupForwardDialog(MessageEntity entityCopyForward) {
        MsgForwardLytBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.msg_forward_lyt,
            null, false);
        // showReceiveTime.set(false);
        binding.setSelectedMessageEntity(entityCopyForward);
        binding.setForwardMessageViewModel(new ForwardMessageViewModel(MainApplication.getInstance()));
        RecyclerView forwardRecycle = binding.forwardContactRecycler;
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        forwardRecycle.setLayoutManager(manager);
        mViewModel.callPersonalList();
        forwardAdapter = new ForwardContactAdapter(entityCopyForward);
        mViewModel.observeContact().observe(this, new Observer<PagedList<ContactModel>>() {
            @Override
            public void onChanged(@Nullable PagedList<ContactModel> contactMaskList) {
                // LogUtility.d("FORWARD", "---------------- CONTACT FORWARD ------------"+messageEntities.size());
                forwardAdapter.submitList(contactMaskList);

            }
        });
        forwardRecycle.setAdapter(forwardAdapter);

        if (forwardDialog == null) {
            forwardDialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        }
        forwardDialog.setCancelable(true);
        forwardDialog.setCanceledOnTouchOutside(false);
        forwardDialog.show();
    }


    /**
     * This method is to show individual message detail.
     *
     * @param item is a conversation entity which contains details to add contact
     */

    public void showAddToContactDialog(ConversationEntity item) {

        DialogAddToContactComposeBinding contactBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_add_to_contact_compose,
            null, false);
        mAddToContactDialog = DialogUtils.getCustomDialog(getActivity(), contactBinding.getRoot());
        contactBinding.setConversationAddToContactFrag(this);
        contactBinding.setConversationEntity(item);
        mAddToContactDialog.setCancelable(true);
        mAddToContactDialog.setCanceledOnTouchOutside(true);
        mAddToContactDialog.show();
    }


    public void onMoveToItemClicked(View view, ConversationEntity conversation) {

        String movedToText = AppConstants.EMPTY_TEXT;
        ConversationDAO conversationDAO = AppDatabase.getInstance(view.getContext()).conversationDAO();
        if (conversation != null) {
            switch (view.getId()) {
                case R.id.type_personal:
                    conversation.setType(ConversationType.PERSONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.personal_tab));
                    break;

                case R.id.type_transactional:
                    conversation.setType(ConversationType.TRANSACTIONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.trans_tab));
                    break;

                case R.id.type_promotional:
                    conversation.setType(ConversationType.PROMOTIONAL);
                    movedToText = getString(R.string.moved_to_tab, getString(R.string.promo_tab));
                    break;

                case R.id.type_block:

                    movedToText = markAsBlockOrUnBlock(conversation);
                    break;
              /*  case R.id.type_unblock:

                    ConversationType conversationType = Utils.getConversationTypeFromMask(conversation.getMask());
                    conversation.setType(conversationType);
                    movedToText = MessageUtils.getTabTitle(this, conversationType);
                    break;*/
              /*  case R.id.type_delete:
                    MessagingService.getInstance(SMSService.class).deleteConversation(conversation);
                    break;*/
                case R.id.type_archive:
                    movedToText = markAsArchiveOrUnArchive(conversation);
                    break;


            }
        }
        MessagingService.getInstance(SMSService.class).updateConversation(conversation);
        if (mMessageSwitchDialog != null && mMessageSwitchDialog.isShowing()) {
            mMessageSwitchDialog.dismiss();
        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
    }


    private String markAsBlockOrUnBlock(ConversationEntity conversation) {

        if (ConversationType.BLOCKED == conversation.getType()) {
            ConversationType conversationType = Utils.getConversationType(conversation.getMask(), Utils.getLastMessageInConversation(conversation));
            conversation.setType(conversationType);
            blockUnblockMenuHandle(conversationType);
            return getString(R.string.unblocked_successfully);//MessageUtils.getTabTitle(this, conversationType);
        } else {
            conversation.setType(ConversationType.BLOCKED);
            blockUnblockMenuHandle(ConversationType.BLOCKED);
            return getString(R.string.moved_to_tab, getString(R.string.block_tab));
        }
    }


    private void blockUnblockMenuHandle(ConversationType type) {


        if (mMenu != null) {
            if (ConversationType.BLOCKED == type) {
                mMenu.findItem(R.id.action_block).setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
            } else {
                mMenu.findItem(R.id.action_block).setVisible(true);
                mMenu.findItem(R.id.action_un_block).setVisible(false);
            }
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        callSearch(newText);
        return true;
    }


    @SuppressLint("StaticFieldLeak")
    private void callSearch(String query) {

        if (adapter != null) {
            if (!TextUtils.isEmpty(query)) {
                query = query.trim();
            }
            final String finalQuery = query;

            new AsyncTask<Void, Void, List<SearchDAO.MessageEntityExtended>>() {
                @Override
                protected List<SearchDAO.MessageEntityExtended> doInBackground(Void... voids) {
                    mMessagePositionList = MessagingService.getInstance(SMSService.class).searchWithinThread(mMask, finalQuery);
                    Collections.reverse(mMessagePositionList);
                    return mMessagePositionList;
                }

                @Override
                protected void onPostExecute(List<SearchDAO.MessageEntityExtended> messageEntityExtendeds) {
                    super.onPostExecute(messageEntityExtendeds);
                    int count = 0;
                    if (!TextUtils.isEmpty(finalQuery)) {
                        //count = messageDAO.messageCountByText(newText);
                        for (int i = 0; i < mMessagePositionList.size(); i++) {
                            LogUtility.d("MESSAGEPOS", "----------- " + mMessagePositionList.get(i).getPosition());
                        }

                        if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
                            count = mMessagePositionList.size();
                            //scroll to first search text occurence
                            smoothScroll(mMessagePositionList.get(count - 1).getPosition());
                            mViewModel.mSearchPosition.set(1);
                        }

                    } else {
                        count = 0;
                    }

                    mViewModel.handleSearchCountVisibility(finalQuery, count);
                    mViewModel.setTotalMatchCount(count);
                    if (mMessagePositionList != null && mMessagePositionList.size() > 0) {
                        if (count > 0) {
                            adapter.updateWithQueryText(finalQuery, mMessagePositionList.get(count - 1).getPosition());
                        } else {
                            adapter.updateWithQueryText(finalQuery, mMessagePositionList.get(count).getPosition());
                        }

                    } else {
                        adapter.updateWithQueryText(finalQuery, -1);
                    }

                }
            }.execute();

        }
    }

    /**
     * Method used to smooth scroll to particular position
     *
     * @param pos target position where scroll should go
     */
    public void smoothScroll(int pos) {
        smoothScroller.setTargetPosition(pos);
        mComposeRecyclerView.getLayoutManager().startSmoothScroll(smoothScroller);
    }


    private String markAsArchiveOrUnArchive(ConversationEntity conversation) {

        if (ConversationSubType.ARCHIVE == conversation.getSubType()) {
            conversation.setSubType(null);
            if (mMenu != null) {
                isArchive = false;
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
            }
            return getString(R.string.un_archived_message);
        } else {
            conversation.setSubType(ConversationSubType.ARCHIVE);
            if (mMenu != null) {
                isArchive = true;
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
            }
            return getString(R.string.moved_to_tab, getString(R.string.archive_block));
        }
    }


    public void addToContactClicked(View view, ConversationEntity item) {

        switch (view.getId()) {
            case R.id.add_to_existing_contact:
                Utils.addNewContact(getActivity(), item.getMask(), null, true);
                break;
            case R.id.add_to_new_contact:
                Utils.addNewContact(getActivity(), item.getMask(), null, false);
                break;
        }
        if (mAddToContactDialog != null) {
            mAddToContactDialog.dismiss();
        }
    }

    /**
     * Method used to launch the activity for forwarding message
     */
    private void launchActivityForForward(MessageEntity messageEntity, String mask) {

        ConversationEntity entity = mViewModel.getConversationByMask(mask, getActivity());
        AppUtils.launchMessageActivityForForward(getActivity(), entity, messageEntity);
        getActivity().overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
        getActivity().finish();
    }

    /**
     * If user selects new message in Message forwarding screen then launch compose sms activity
     */
    private void launchComposeActivity(String forwardMessage) {
        Intent intent = new Intent(getActivity(), ComposeSmsActivity.class);
        intent.putExtra(AppConstants.KEY_FORWARD_MESSAGE, forwardMessage);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
        getActivity().finish();
    }


    private String setMoveToMessage(Context ctx, int threadCount, String tabType) {
        return ctx.getResources()
            .getQuantityString(R.plurals.move_to_tab_msgs, threadCount, tabType);
    }


    /**
     * Method used to show select or deselect option at the action mode
     *
     * @param menu Menu which contains options
     */
    public void showSelectAction(Menu menu) {
        if (adapter != null) {
            if (adapter.getLongItemCount() == mViewModel.observeData().getValue().size()) {
                menu.findItem(R.id.action_select_all).setVisible(false);
                menu.findItem(R.id.action_deselect_all).setVisible(true);
            } else {
                menu.findItem(R.id.action_select_all).setVisible(true);
                menu.findItem(R.id.action_deselect_all).setVisible(false);
            }
        }
    }


    @Override
    public void onButtonClicked(DialogInterface dialog, int which) {

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (mDeletableMessage != null) {
                    MessagingService.getInstance(SMSService.class).deleteMessage(mDeletableMessage);
                    AppUtils.deleteSMSFromDefaultDB(getActivity(), Telephony.Sms.CONTENT_URI, mDeletableMessage);
                    mViewModel.upDateLastMessageIdOnDelete(getActivity(), mMask);
                    mActionMode.finish();
                    dialog.dismiss();
                } else {
                    MessagingService.getInstance(SMSService.class).deleteConversation(mDeletableConversation);
                    dialog.dismiss();
                    getActivity().finish();
                }

                break;
            case DialogInterface.BUTTON_NEGATIVE:
            case DialogInterface.BUTTON_NEUTRAL:
                dialog.dismiss();
                break;
        }
    }

    /**
     * Method used to disable three dots button at the top corner
     *
     * @param isShow          is a boolean to show three dots should show or not
     * @param isFromSimLayout is a boolean to it is from sim layout or not
     */
    public void disableThreeDots(boolean isShow, boolean isFromSimLayout) {
        List<ConversationEntity> conversationEntityList = MessagingService.getInstance(SMSService.class).getConversationByMask(mMask);
        ConversationType type = null;
        ConversationEntity conversationEntity = null;

        if (conversationEntityList == null || conversationEntityList.size() <= 0) {
            isShow = false;
        } else {
            conversationEntity = conversationEntityList.get(0);
            type = Utils.getConversationType(mMask, Utils.getLastMessageInConversation(conversationEntity));
        }

        if (!isFromSimLayout) {
            // handle call option for PROMOTIONAL TYPE
            if (type != null && !type.name().equalsIgnoreCase(ConversationType.PROMOTIONAL.name())) {
                mMenu.findItem(R.id.call_action).setVisible(isShow);
            } else {
                // if its not promotional  jus hide the call option
                mMenu.findItem(R.id.call_action).setVisible(false);
            }
            mMenu.findItem(R.id.conversation_action_search).setVisible(isShow);
        }


        // ConversationType type = Utils.getConversationTypeFromMask(mMask);
        if (isShow && mSendTapped) {

            if (TextUtils.isEmpty(conversationEntity.getDisplay())
                && type == ConversationType.PERSONAL) {
                mMenu.findItem(R.id.action_add_contact).setVisible(true);
            } else {
                mMenu.findItem(R.id.action_add_contact).setVisible(false);
            }

            //is from block
            if (conversationEntity != null && conversationEntity.getType() == ConversationType.BLOCKED) {
                mIsFromBlock = true;
                mMenu.findItem(R.id.action_block).setVisible(false);
                mMenu.findItem(R.id.action_un_block).setVisible(true);
            } else {
                mIsFromBlock = false;
                mMenu.findItem(R.id.action_block).setVisible(true);
                mMenu.findItem(R.id.action_un_block).setVisible(false);
            }
            getSubTypeFromConversation();
            if (isArchive) {
                mMenu.findItem(R.id.action_move).setVisible(false);
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.action_un_archive));
            } else {
                mMenu.findItem(R.id.action_move).setVisible(true);
                mMenu.findItem(R.id.action_archive).setTitle(getString(R.string.archive));
            }
            mMenu.findItem(R.id.action_archive).setVisible(isShow);
            mMenu.findItem(R.id.action_delete).setVisible(isShow);

        } else {
            mMenu.findItem(R.id.action_block).setVisible(false);
            mMenu.findItem(R.id.action_un_block).setVisible(false);
            mMenu.findItem(R.id.action_add_contact).setVisible(false);
            mMenu.findItem(R.id.action_archive).setVisible(false);
            mMenu.findItem(R.id.action_delete).setVisible(false);
            mMenu.findItem(R.id.action_move).setVisible(false);
        }


    }

    public void handleToolbarOnOverlayShow(boolean isDisable) {
        if (mMenu != null) {
            disableThreeDots(!isDisable, true);
            //handle call and search button
            mMenu.findItem(R.id.call_action).setEnabled(!isDisable);
            mMenu.findItem(R.id.conversation_action_search).setEnabled(!isDisable);
        }

    }

}
