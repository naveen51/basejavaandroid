package io.gupshup.smsapp.ui.earlierSMS;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Telephony;
import android.support.v4.util.Pair;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.Author;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.ui.home.view.HomePagerFragment;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import io.gupshup.smsapp.utils.Triple;
import io.gupshup.soip.sdk.message.GupshupTextMessage;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class FetchEarlierSMS {

    private Context context;
    private Cursor smsInboxCursor, mmsInboxCursor;
    private long lastSyncTime;

    public FetchEarlierSMS(Context ctx) {
        this.context = ctx;
        lastSyncTime=Long.parseLong(PreferenceManager.getInstance(ctx).getLastSyncTime());
        LogUtility.d("GDC", "the last sync time found from prefs is : "+lastSyncTime);
        AppUtils.logToFile(ctx, "the last sync time found from prefs is : "+lastSyncTime);
        //LogUtility.d("GDC", "Start time ..."+System.currentTimeMillis());
    }

    public void importEarlierSMSForInitialLoad(int limit){
        //new PopulateDBWithEarlierSMSForInitialLoad(limit).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new PopulateDBWithEarlierSMSForInitialLoad(limit).execute();
    }

    public void importEarlierSmsAndMms(boolean isFirst) {
        //new PopulateDBWithEarlierSMS().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new PopulateDBWithEarlierSMS(isFirst).execute();
        //new PopulateDBWithEarlierMMS().execute();
        }

    public void importCurrentMMS(){
        new PopulateDBWithEarlierMMS().execute();
    }


    public class PopulateDBWithEarlierSMSForInitialLoad extends AsyncTask<String, String, String> {
        int limit=0;
        public PopulateDBWithEarlierSMSForInitialLoad(int lim)
        {
            limit=lim;
        }

        @Override
        protected String doInBackground(String... arg) {

           ContentResolver contentResolver = context.getContentResolver();
           smsInboxCursor= contentResolver.query(Telephony.Sms.CONTENT_URI, null, "date > ?", new String[] {String.valueOf(lastSyncTime)}, Telephony.Sms.DEFAULT_SORT_ORDER + " LIMIT "+limit);
            LogUtility.d("Cursor","the init loader count :"+smsInboxCursor.getCount() );
           AppUtils.logToFile(context,"the init loader count :"+smsInboxCursor.getCount() );
           populateDB();
           return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(HomePagerFragment.progDialog != null)
                HomePagerFragment.progDialog.dismiss();
        }
    }

    public class PopulateDBWithEarlierSMS extends AsyncTask<String, String, String> {
        boolean isFirstTimerun=false;
        public PopulateDBWithEarlierSMS(boolean isFirst)
        {
            isFirstTimerun=isFirst;
        }
        @Override
        protected String doInBackground(String... arg) {
            LogUtility.d("Cursor count", "Exec do in back..");
            if(context == null){
                return null;
            }
            ContentResolver contentResolver = context.getContentResolver();
            if(PreferenceManager.getInstance(context).isFirstLaunch()){
                LogUtility.d("Cursor", "first time launch..");
                AppUtils.logToFile(context, "FetchearlierSMS - populateDBWithearlierSMS - first time launch..");
                smsInboxCursor= contentResolver.query(Telephony.Sms.CONTENT_URI, null, "date > ?", new String[] {String.valueOf(0)}, null);
                HomePagerFragment.firstTimeRun=false;
                isFirstTimerun = false;
            }else{
                LogUtility.d("Cursor", "NOT first time launch..");
                AppUtils.logToFile(context, "FetchearlierSMS - populateDBWithearlierSMS - NOT first time launch..");
                smsInboxCursor= contentResolver.query(Telephony.Sms.CONTENT_URI, null, "date > ?", new String[] {String.valueOf(lastSyncTime)}, null);
            }
            LogUtility.d("Cursor","the main loader count :"+smsInboxCursor.getCount() );
            populateDB();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            HomePagerFragment.isImportMessagesLoadingComplete = true;
            if(HomePagerFragment.progDialog != null)
                HomePagerFragment.progDialog.dismiss();
        }
    }

    public class PopulateDBWithEarlierMMS extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... arg) {
            if(context==null){
                LogUtility.e("FetchEarlierSMS", "Context is null here..");
                return null;
            }
            ContentResolver contentResolver = context.getContentResolver();
            //smsInboxCursor= contentResolver.query(Uri.parse("content://sms/"), null, null, null, null);
            lastSyncTime=0;
            mmsInboxCursor= contentResolver.query(Telephony.Mms.CONTENT_URI, null, "date > ?", new String[] {String.valueOf(lastSyncTime)}, null);
            //LogUtility.d("mms", "the cursor size is "+mmsInboxCursor.getCount());
            populateDBforMMS();

            return null;
        }
    }

    public void populateDB()
    {
        //DatabaseUtils.dumpCursor(smsInboxCursor);
        try {
            reSyncMessages();
        }catch(Exception ex){
            //ex.printStackTrace();
        }
        int indexBody = smsInboxCursor.getColumnIndex("body");
        if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
        do {
            MessageEntity messageEntity = new MessageEntity();
            final String messageId = smsInboxCursor.getString(smsInboxCursor.getColumnIndex("_id"));
            if(messageId==null){
                return;
            }
            messageEntity.setMessageId((messageId != null && !messageId.isEmpty()) ? messageId : Utils.generateMessageId());
            messageEntity.setMask(Utils.getMaskFromPhoneNumber(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("address"))));
            messageEntity.setContent(new GupshupTextMessage(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("body"))));
            LogUtility.d("Cursor", "message body is :"+smsInboxCursor.getString(smsInboxCursor.getColumnIndex("body")));
            messageEntity.setChannel(MessageChannel.SMS);

            final String messageData = smsInboxCursor.getString(smsInboxCursor.getColumnIndex("body"));
            int smsDirectionFieldIndex = smsInboxCursor.getColumnIndex("type");
            int read_status = smsInboxCursor.getInt(smsInboxCursor.getColumnIndex("read"));
            if(smsInboxCursor.getColumnIndex("sim_id") != -1) {
                int simIndex = smsInboxCursor.getInt(smsInboxCursor.getColumnIndex("sim_id"));
                LogUtility.d("dump", "The SIM index is : " + simIndex);
                messageEntity.setIncomingSIMID(simIndex);
            }

            if(smsInboxCursor.getColumnIndex("index_on_icc") != -1) {
                int simIndex = smsInboxCursor.getInt(smsInboxCursor.getColumnIndex("index_on_icc"));
                LogUtility.d("dump", "The SIM index is : " + simIndex);
                messageEntity.setIncomingSIMID(simIndex);
            }


            LogUtility.d("Cursor", "The message dir val is :"+smsInboxCursor.getString(smsDirectionFieldIndex));
            if(smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("0")){
                messageEntity.setDirection(MessageDirection.INCOMING);
                if(read_status==0) {
                    messageEntity.setMessageStatus(MessageStatus.READ);
                }else{
                    messageEntity.setMessageStatus(MessageStatus.RECEIVED);
                }
            } else if (smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("1")) {
                messageEntity.setDirection(MessageDirection.INCOMING);
                if(read_status==0) {
                    messageEntity.setMessageStatus(MessageStatus.READ);
                }else{
                    messageEntity.setMessageStatus(MessageStatus.RECEIVED);
                }
            } else if (smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("2")) {
                messageEntity.setDirection(MessageDirection.OUTGOING);
                messageEntity.setMessageStatus(MessageStatus.DELIVERED);
            } else if(smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("3")){
                messageEntity.setDirection(MessageDirection.OUTGOING);
                messageEntity.setMessageStatus(MessageStatus.DRAFT);
            }else if(smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("4")){
                messageEntity.setDirection(MessageDirection.OUTGOING);
                messageEntity.setMessageStatus(MessageStatus.SENT);
            }else if(smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("5")){
                messageEntity.setDirection(MessageDirection.INCOMING);
                messageEntity.setMessageStatus(MessageStatus.FAILED);
            }else if(smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("6")){
                messageEntity.setDirection(MessageDirection.OUTGOING);
                messageEntity.setMessageStatus(MessageStatus.QUEUED);
            }
            messageEntity.setDestination(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("address")));
            messageEntity.setSentTimestamp(messageEntity.
                    getDirection() == MessageDirection.OUTGOING ? Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date_sent"))) : -1L);
            long messageDate=0L;
            try {
                messageDate=Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date")));
            }catch(Exception e){
                messageDate=0L;
            }

            //if(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date")) != "" && smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"))!=null) {
            //messageDate = Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date")));
            //}
            messageEntity.setMessageTimestamp(messageDate);
            messageEntity.setCreationTime(messageDate);
            if(messageDate > lastSyncTime){
                lastSyncTime=messageDate;
            }

            messageEntity.setDeliveredTimestamp(messageEntity.getDirection() == MessageDirection.INCOMING ? DateUtils.currentTimestamp() : -1);
            try {
                messageEntity.setReadTimestamp(Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"))));
                messageEntity.setDeliveredTimestamp(messageEntity.getDirection() == MessageDirection.INCOMING ? DateUtils.currentTimestamp() : Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"))));
            }catch(Exception e){
                messageEntity.setReadTimestamp(0L);
            }
            messageEntity.setReadAcknowledged(false);
            messageEntity.setMetadata(null);
            messageEntity.setSearch(null);
            messageEntity.setCarierName("");
            final MessageEntity me = messageEntity;
            AppDatabase appDatabase = AppDatabase.getInstance(context);

            if (appDatabase != null) {
                ConversationEntity conversationEntity = null;
                if (messageId != null) {
                    try {

                        conversationEntity = Utils.getNewConversation(me.getMask(), smsInboxCursor.getString(smsInboxCursor.getColumnIndex("address")), me.getChannel(), ((GupshupTextMessage) me.getContent()).getText());
                        long[] conversationIds = MessagingService.getInstance(SMSService.class).insertConversation(conversationEntity);
                        conversationEntity.setConversationId(conversationIds[0]);


                        if(Utils.getConversationType(conversationEntity.getMask(),((GupshupTextMessage)messageEntity.getContent()).getText())== ConversationType.PERSONAL){
                            /*Pair<String, String> userInfo = MessageUtils.getContactUserDetail(context, conversationEntity.getMask());
                            String name = userInfo.first;
                            String image = userInfo.second;
                            conversationEntity.setImage(image);
                            conversationEntity.setDisplay(name);*/

                            Triple<String, String, String> userInfo = MessageUtils.getContactUserDetails(context, conversationEntity.getMask());
                           //LogUtility.d("triple", "name :"+userInfo.getFirst()+" image : "+userInfo.getSecond()+" user contact id : "+userInfo.getThird());
                            conversationEntity.setDisplay(userInfo.getFirst());
                            conversationEntity.setImage(userInfo.getSecond());
                            conversationEntity.setContactId(userInfo.getThird());
                        }else{
                            conversationEntity.setDisplay(conversationEntity.getDisplay().toUpperCase());
                            //LogUtility.d("GDC", "Conversation type is not personal ...its type is : "+Utils.getConversationTypeFromMask(conversationEntity.getMask()));
                        }


                        if(read_status==0){
                            /*MessagingService.getInstance(SMSService.class).
                                    postReceiving(messageEntity.getMask(), smsInboxCursor.getString(smsInboxCursor.getColumnIndex("address")), messageEntity.getMessageId(), messageEntity.getContent(), messageEntity.getDestination(), messageEntity.getMessageTimestamp(), "", conversationEntity.getDisplay(),
                                    conversationEntity.getImage(), 0);*/
                            conversationEntity.setSubType(null); // Will be unarchived in case the conversation is archived
                            conversationEntity.setNoOfUnreadMessages(conversationEntity.getNoOfUnreadMessages() + 1);
                            conversationEntity.setUnread(true);
                            conversationEntity.setPNRequired(true);
                        }

                    } catch (Exception e) {
                       //LogUtility.d("Cursor", "Exc in conv entity creation:"+e.getMessage(), e);


                        List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class)
                            .getConversationByMask(me.getMask());
                        if (conversationEntities.size() > 0) {
                                conversationEntity = conversationEntities.get(0);
                        }

                        try {
                            if(read_status==0){
                                conversationEntity.setSubType(null); // Will be unarchived in case the conversation is archived
                                conversationEntity.setNoOfUnreadMessages(conversationEntity.getNoOfUnreadMessages() + 1);
                                conversationEntity.setUnread(true);
                                conversationEntity.setPNRequired(true);
                            }
                        }catch(Exception ex){
                           //LogUtility.d("Cursor", "Exception in read status.."+ex.getMessage());
                        }
                    }


                    try {
                       //LogUtility.d("GDC", "The conversation entity mask before msg is "+conversationEntity.getMask());
                        long[] messageIds = appDatabase.messageDAO().insert(me);
                       //LogUtility.d("Cursor", "message inserted id : "+messageIds[0]);
                        ConversationEntity tempConversationEntity = updateConversation(conversationEntity, me);
                        if(tempConversationEntity != null) {
                            //TODO : Conversation table not getting updated......Message table getting updated..
                            int noOfRowsUpdated = appDatabase.conversationDAO().update(tempConversationEntity);
                           //LogUtility.d("Cursor", "The no of rows updated are :" + noOfRowsUpdated);
                        }else{
                            //LogUtility.d("GDC", "Not updating as conv more..");
                        }
                    } catch (Exception e) {
                        LogUtility.d("Cursor","exc in insert message for message id : "+messageEntity.getMessageId() +" is   "+e.getMessage());
                        e.printStackTrace();

                    }
                    /*try {
                        ConversationEntity tempConversationEntity = updateConversation(conversationEntity, me);
                        if(tempConversationEntity != null) {
                            //TODO : Conversation table not getting updated......Message table getting updated..
                            int noOfRowsUpdated = appDatabase.conversationDAO().update(tempConversationEntity);
                            LogUtility.d("Cursor", "The no of rows updated are :" + noOfRowsUpdated);
                        }else{
                            //LogUtility.d("GDC", "Not updating as conv more..");
                        }
                    }catch (Exception ex){
                       //LogUtility.d("Cursor","exc in con update is "+ex.getMessage());
                        ex.printStackTrace();
                    }*/
                }
            } else {
                //TODO: handle db handle null
                LogUtility.d("Cursor", "App database is null");

            }
        } while (smsInboxCursor.moveToNext());
        PreferenceManager.getInstance(context).setLastSyncTime(String.valueOf(lastSyncTime));
        LogUtility.d("sync", "Setting last sync time as : "+lastSyncTime);
    }

    public void populateDBforMMS()
    {
        LogUtility.d("ms", "Dumping mms first table");
        //DatabaseUtils.dumpCursor(mmsInboxCursor);
        ScanMMS();
    }

    public void ScanMMS() {
        if (!mmsInboxCursor.moveToFirst()) return;
            do {
                MessageEntity messageEntity = new MessageEntity();
                String messageId = mmsInboxCursor.getString(mmsInboxCursor.getColumnIndex("_id"));
                messageEntity.setMessageId((messageId != null && !messageId.isEmpty()) ? messageId : Utils.generateMessageId());
                //messageEntity.setMask(Utils.getMaskFromPhoneNumber(mmsInboxCursor.getString(mmsInboxCursor.getColumnIndex("address"))));
                //messageEntity.setContent(new GupshupTextMessage(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("body"))));
                messageEntity.setChannel(MessageChannel.SMS);
                //Msg msg = new Msg(c.getString(c.getColumnIndex("_id")));
                //[]]msg.setThread(c.getString(c.getColumnIndex("thread_id")));
                messageEntity.setReadTimestamp(Long.parseLong(mmsInboxCursor.getString(mmsInboxCursor.getColumnIndex("date"))));
                messageEntity.setMask(getMmsAddr(messageEntity.getMessageId()));
                LogUtility.d("mms", "the address of mms is :"+getMmsAddr(messageEntity.getMessageId()));


                //final String messageData = smsInboxCursor.getString(smsInboxCursor.getColumnIndex("body"));
                //int smsDirectionFieldIndex = smsInboxCursor.getColumnIndex("type");

                //LogUtility.d("GauravC", "The message dir val is :"+smsInboxCursor.getString(smsDirectionFieldIndex));
                /*if (smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("1")) {
                    messageEntity.setDirection(MessageDirection.INCOMING);
                } else if (smsInboxCursor.getString(smsDirectionFieldIndex).equalsIgnoreCase("2")) {
                    messageEntity.setDirection(MessageDirection.OUTGOING);
                }*/
                if(mmsInboxCursor.getColumnIndex("type")!=-1) {
                    int type = mmsInboxCursor.getInt(mmsInboxCursor.getColumnIndex("type"));
                    if (type == 1) {
                        messageEntity.setDirection(MessageDirection.INCOMING);
                    } else if (type == 2) {
                        messageEntity.setDirection(MessageDirection.OUTGOING);
                    } else {
                        messageEntity.setDirection(MessageDirection.OUTGOING);
                    }
                }else{
                    messageEntity.setDirection(MessageDirection.OUTGOING);
                }
                messageEntity.setDestination(getMmsAddr(messageId));
                //messageEntity.setSentTimestamp(messageEntity.getDirection() == MessageDirection.OUTGOING ? Long.parseLong(smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date_sent"))) : -1L);
                long messageDate = Long.parseLong(mmsInboxCursor.getString(mmsInboxCursor.getColumnIndex("date")));
                messageEntity.setMessageTimestamp(messageDate);
                messageEntity.setCreationTime(messageDate);
                if(messageDate > lastSyncTime){
                    lastSyncTime=messageDate;
                }

                messageEntity.setDeliveredTimestamp(messageEntity.getDirection() == MessageDirection.INCOMING ? DateUtils.currentTimestamp() : -1);
                messageEntity.setReadTimestamp(Long.parseLong(mmsInboxCursor.getString(mmsInboxCursor.getColumnIndex("date"))));
                messageEntity.setReadAcknowledged(false);
                messageEntity.setMetadata(null);
                messageEntity.setSearch(null);
                messageEntity.setCarierName("");


                MessageEntity me = ParseMMS(messageEntity);
                AppDatabase appDatabase = AppDatabase.getInstance();
                if (appDatabase != null) {
                    ConversationEntity conversationEntity = new ConversationEntity();
                    if (conversationEntity != null) {
                        try {
                            //conversationEntity = getNewConversation(me.getMask(), me.getChannel(), me, messageId, me.getMessageTimestamp());
                            conversationEntity = Utils.getNewConversation(me.getMask(), getMmsAddr(me.getMessageId()), me.getChannel(), ((GupshupTextMessage) me.getContent()).getText());
                            long[] conversationIds = MessagingService.getInstance(SMSService.class).insertConversation(conversationEntity);
                            conversationEntity.setConversationId(conversationIds[0]);
                            LogUtility.d("mms", "Conversation id is : "+conversationIds[0]);
                        } catch (Exception e) {
                            LogUtility.d("mms", "exception in mms conv insert : "+e.getMessage());
                            List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class)
                                .getConversationByMask(me.getMask());
                            if (conversationEntities.size() > 0) {
                                conversationEntity = conversationEntities.get(0);
                            }
                        }
                        try {
                            long messageIds[]=appDatabase.messageDAO().insert(me);
                            LogUtility.d("mms", "Message id returned "+messageIds[0]);
                            ConversationEntity tempConversationEntity = updateConversation(conversationEntity, me);
                            if(tempConversationEntity!=null) {
                                int noOfRowsUpdated = appDatabase.conversationDAO().update(tempConversationEntity);
                                LogUtility.d("mms", "No of rows updated :" + noOfRowsUpdated);
                            }
                        } catch (Exception e) {
                            LogUtility.d("mms", "exception in mms conv update : "+e.getMessage());
                        }
                    }
                } else {
                    //TODO: handle db handle null
                }
                //System.out.println(msg);
            } while (mmsInboxCursor.moveToNext());
    }

    public void getPidOfMMS(MessageEntity messageEntity){
        Uri uri = Uri.parse("content://mms/part");
        String mmsId = "mid = " + messageEntity.getMessageId();
        Cursor c = context.getContentResolver().query(uri, null, mmsId, null, null);
        LogUtility.d("ms", "Dumping mms part table");
        //DatabaseUtils.dumpCursor(c);
        while(c.moveToNext()) {
            String pid = c.getString(c.getColumnIndex("_id"));
            String type = c.getString(c.getColumnIndex("ct"));
            Pair<String, String> mmsInfo = new Pair<>(pid, type);
        }
    }

    public MessageEntity ParseMMS(MessageEntity msg) {
        Uri uri = Uri.parse("content://mms/part");
        String mmsId = "mid = " + msg.getMessageId();
        Cursor c = context.getContentResolver().query(uri, null, mmsId, null, null);
        LogUtility.d("ms", "Dumping mms part table");
        //DatabaseUtils.dumpCursor(c);
        //c.moveToFirst();
        while(c.moveToNext()) {
            String pid = c.getString(c.getColumnIndex("_id"));
            String type = c.getString(c.getColumnIndex("ct"));
            LogUtility.d("GDC", "pid and type is found.....");
            String data = c.getString(c.getColumnIndex("_data"));
            /*if(data !=null){
                LogUtility.d("ms", "data is :"+data);
                /*Uri fileUri = Uri.parse(data);
                Intent intentView = new Intent(Intent.ACTION_VIEW, fileUri);
                intentView.setDataAndType(fileUri, "image/jpeg");
                context.startActivity(intentView);*/

                /*Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(data)), "image/*");
                context.startActivity(intent);
            }*/
            if ("text/plain".equals(type)) {
                msg.setMetadata("text");
            } else if (type.contains("image")) {
                //msg.setImg(getMmsImg(pid));
                msg.setMetadata("image");
                //msg.setContent(new GupshupTextMessage("Message contains an image. Click on the following link to view it : io.gupshup.mmsImage/" + pid));
                msg.setContent(new GupshupTextMessage(pid));
            }else if(type.contains("video")){
                msg.setMetadata("video");
                msg.setContent(new GupshupTextMessage(pid));
            }
            else if(type.contains("audio")){
                msg.setMetadata("audio");
                msg.setContent(new GupshupTextMessage(pid));
            }else{
                msg.setMetadata("Unknown type");
                msg.setContent(new GupshupTextMessage("Contains something unparsed: "));
            }
        }
        c.close();
        return msg;
    }

    public String getMmsAddr(String id) {
        String sel = "msg_id=" + id;
        String uriString = MessageFormat.format("content://mms/{0}/addr", id);
        LogUtility.d("uri", "The uri is : "+uriString);
        Uri uri = Uri.parse(uriString);
        Cursor c = context.getContentResolver().query(uri, null, sel, null, null);
        LogUtility.d("ms", "Dumping mms addr table");
        //DatabaseUtils.dumpCursor(c);
        StringBuilder name = new StringBuilder();
        while (c.moveToNext()) {
            String t = c.getString(c.getColumnIndex("address"));
            if(!(t.contains("insert")))
                name.append(t).append(" ");
        }
        c.close();
        return name.toString();
    }


    private ConversationEntity updateConversation(ConversationEntity conversationEntity, MessageEntity lastMessageEntity) {
        //LogUtility.d("GDC", "Exec updateConversation before");

        if(conversationEntity.getLastMessageTimestamp()>lastMessageEntity.getMessageTimestamp()){
            return null;
        }

        conversationEntity.setLastMessageId(lastMessageEntity.getMessageId());
        conversationEntity.setLastMessageTimestamp(lastMessageEntity.getMessageTimestamp());
        conversationEntity.setLastMessageData(lastMessageEntity);

        if(conversationEntity.getLastMessageId()==null){
            MessagingService.getInstance(SMSService.class).deleteConversation(conversationEntity);
            return null;
        }

        return conversationEntity;
    }

    private ConversationEntity getNewConversation(String mask, MessageChannel channel, MessageEntity messageEntityData, String messageId, long messageTimestamp) {
        ConversationEntity conversationEntity = new ConversationEntity();
        conversationEntity.setMask(mask);
        conversationEntity.setChannels(Collections.singletonList(channel));
        conversationEntity.setIpPublicKey(null);
        conversationEntity.setIpLastSyncedTimestamp(-1);
        conversationEntity.setDisplay(mask);
        conversationEntity.setImage(null);
        conversationEntity.setPbLastSyncedTimestamp(-1);
        conversationEntity.setType(Utils.getConversationType(mask ,Utils.getLastMessageInConversation(conversationEntity)));
        conversationEntity.setTypeSetBy(Author.APP);
        conversationEntity.setSubType(null);
        conversationEntity.setDraftMessage(null);
        conversationEntity.setLastMessageId(null);
        conversationEntity.setLastMessageData(null);
        conversationEntity.setLastMessageTimestamp(0L);
        conversationEntity.setNoOfUnreadMessages(0);
        conversationEntity.setSendNotifications(true);
        conversationEntity.setNotificationsSound(true);
        conversationEntity.setNotificationsLight(true);
        conversationEntity.setNotificationsPopup(true);
        return conversationEntity;
    }

    /**
     * This method will update conversation table, if messages deleted from other SMS app.
     */
    private void reSyncMessages() throws NumberParseException {

        List<String> listOfOriginalContactNumber = MessagingService.getInstance(SMSService.class).getOriginalContactNumberList();
        ContentResolver contentResolver = context.getContentResolver();
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(context);
        long nationalNumber;
        for (String contactNumber : listOfOriginalContactNumber) {
            String[] address = new String[]{String.valueOf(contactNumber)};
            boolean isDeleted;
            Cursor cursor1 = getCursor(address, contentResolver);
            if (cursor1 != null && cursor1.getCount() <= 0) {
                Phonenumber.PhoneNumber phoneNumberWithCountryCode = phoneUtil.parse(contactNumber,
                    context.getResources().getConfiguration().locale.getCountry());
                nationalNumber = phoneNumberWithCountryCode.getNationalNumber();
                address = new String[]{String.valueOf(nationalNumber)};
                Cursor cursor2 = getCursor(address, contentResolver); // To double confirm
                isDeleted = cursor2 != null && cursor2.getCount() <= 0;
                if (cursor2 != null) {
                    cursor2.close();
                }
            } else {
                isDeleted = false;
            }
            if (cursor1 != null) {
                cursor1.close();
            }
            deleteConversation(isDeleted, contactNumber);
        }
    }

    private Cursor getCursor(String[] phoneNumber, ContentResolver contentResolver) {

        return contentResolver.query(Telephony.Sms.CONTENT_URI,
            new String[]{"_id", "thread_id", "address", "person", "date", "body", "type"},
            "address=?", phoneNumber, "date asc");
    }

    /**
     * Delete conversation from conversation table.
     * @param isDeleted
     * @param contactNumber
     */
    private void deleteConversation(boolean isDeleted, String contactNumber) {
        if (isDeleted) {
            ConversationEntity conversation;
            List<ConversationEntity> conversationEntity =
                MessagingService.getInstance(SMSService.class).getConversationByMask(Utils.getMaskFromPhoneNumber(contactNumber));
            if (conversationEntity != null && conversationEntity.size() > 0) {
                conversation = conversationEntity.get(0);
                if (conversation != null) {
                    MessagingService.getInstance(SMSService.class).deleteConversation(conversation);
                }
            }
        }
    }
}
