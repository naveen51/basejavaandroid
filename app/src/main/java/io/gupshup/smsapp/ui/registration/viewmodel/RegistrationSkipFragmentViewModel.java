package io.gupshup.smsapp.ui.registration.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.StatusResponse;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ManualRegistrationEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.service.RegistrationService;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Naveen BM on 6/14/2018.
 */
public class RegistrationSkipFragmentViewModel extends BaseFragmentViewModel {

    private static final String TAG = RegistrationSkipFragmentViewModel.class.getSimpleName();
    public ObservableField<String> mSimOneEditTextPhoneNumber = new ObservableField<>("");
    public ObservableField<String> mSimTwoEditTextPhoneNumber = new ObservableField<>("");
    public ObservableBoolean mSimOneContainerVisibility = new ObservableBoolean(true);
    public ObservableBoolean mSimTwoContainerVisibility = new ObservableBoolean(true);
    public ObservableBoolean mDividerVisibility = new ObservableBoolean(true);
    public ObservableBoolean mNextButtonEnable = new ObservableBoolean(false);
    public ObservableField<Float> mNextButtonAlpha = new ObservableField<>(0.4f);

    @Inject
    public RxBus rxBus;
    private CountDownTimer mCountDownTimerSimOne;
    private CountDownTimer mCountDownTimerSimTwo;

    public RegistrationSkipFragmentViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void nextClicked(View view) {
        String mSimOnePhone = mSimOneEditTextPhoneNumber.get();
        String mSimTwoPhone = mSimTwoEditTextPhoneNumber.get();
        DialogUtils.hideKeyboard(view.getContext());

        LogUtility.d(TAG, "---------- SIM ONE---------" + mSimOnePhone + " ------ SIM TWO ---------" + mSimTwoPhone);
        ManualRegistrationEvent manualRegEvent = new ManualRegistrationEvent(mSimOnePhone, mSimTwoPhone);
        RxEvent event = new RxEvent(AppConstants.SKIP_MANULA_REGISTRATION_NEXT_CLICKED, manualRegEvent);
        rxBus.send(event);

    }


    public void skipClicked(View view) {
        RxEvent event = new RxEvent(AppConstants.SKIP_MANUAL_SKIP_CLICKED, AppConstants.SKIP_MANUAL_SKIP_CLICKED);
        rxBus.send(event);
    }

    /**
     * Method used to call the registration API
     */
    public void manualRegistration(final Context ctx, final String countryCode, final String mobileNum,
                                   final String fullNumberWithPlus, final int simSlot) {

        LogUtility.d("auto", "manual regis called");
        final String sequenceNumber = AppUtils.generateRandomNumber(4);
        final String apiKey = AppUtils.fetchApiKey(simSlot);
        LogUtility.d("auto", "the api key is :"+apiKey);
        String ipPublicKey = "", ipPrivateKey = "";
        final GupshupKeyPair gupshupKeyPair = AppUtils.generateGupshupKeyPair();
        if (gupshupKeyPair != null) {
            ipPublicKey = gupshupKeyPair.getPublicKey().toString();
            ipPrivateKey = gupshupKeyPair.getPrivateKey().toString();
        }
        final RegistrationInfo requestItem = saveManualRegistrationRequestItem(ctx, simSlot, apiKey,
                ipPrivateKey, mobileNum, countryCode, sequenceNumber, fullNumberWithPlus);

        LogUtility.d("auto", "manual reg called for sim slot " + simSlot);
        RegistrationService.getInstance().manualRegistration(ctx, countryCode, mobileNum, fullNumberWithPlus,
                sequenceNumber, ipPublicKey, apiKey, simSlot, new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        StatusResponse model = response.body();

                        LogUtility.d("auto", "manual reg response recvd as :"+response.body());
                        if (model != null &&
                                AppConstants.ApiResponseStatus.SUCCESS.equalsIgnoreCase(model.getmStatus())) {
                            requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            AppUtils.setSimRequestItemData(ctx, requestItem);
                            startTimer(ctx, sequenceNumber, simSlot);
                            //PreferenceManager.getInstance(ctx).setRegistrationDone(true);
                        } else {
                            requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            if (simSlot == 0) {
                                requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                            } else {
                                requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
                            }
                            cancelTimer(simSlot);
                            updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);

                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        LogUtility.d("auto", "manual reg on Failure with "+t.getMessage());
                        //Toast.makeText(ctx, ctx.getString(R.string.registration_error), Toast.LENGTH_SHORT).show();
                        cancelTimer(simSlot);
                        //showMrTwoScreen(ctx, simSlot);
                        //Toast.makeText(ctx, ctx.getString(R.string.registration_error), Toast.LENGTH_SHORT).show();
                        updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);

                    }
                });
    }

    /**
     * method used to call the API to verify the Phone Number
     */
    public void verifyManualRegistration(final Context ctx, final RegistrationInfo request, String otp,
                                         final int simSlot, final String sequenceNumber) {

        LogUtility.d("auto", "verify manual reg called for sim slot "+simSlot);
        RegistrationService.getInstance().verifyOtpForManualRegistration(request.countryCode,
                request.phoneNumber, otp, request.apiKey, request.sequenceNumber, new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        LogUtility.d("auto", "onresponse of verify called here for simslot " + simSlot);
                        StatusResponse model = response.body();
                        cancelTimer(simSlot);
                        PreferenceManager manager = PreferenceManager.getInstance(ctx);
                        request.sequenceNumber = sequenceNumber;
                        if (model != null) {

                            if (AppConstants.ApiResponseStatus.SUCCESS.equalsIgnoreCase(model.getmStatus())) {
                                //LogUtility.d("auto", "Success for manual reg..for simslot " + simSlot);
                                if (simSlot == 0) {
                                    manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS.toString());
                                    request.status = AppConstants.VERIFIED;
                                    request.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS;
                                } else {
                                    manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS.toString());
                                    request.status = AppConstants.VERIFIED;
                                    request.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS;
                                }
                                //LogUtility.d("auto", "Calling event MR 2 verification...");
                                AppUtils.setSimRequestItemData(ctx, request);
                                RegistrationService.getInstance().sendFirebaseTokenToServer(ctx, request.countryCode, request.phoneNumber, request.apiKey);
                                RxEvent event = new RxEvent(AppConstants.RxEventName.MANUAL_REGISTRATION_MR_TWO_VERIFICATION_STATUS, request);
                                rxBus.send(event);
                            } else {
                                if (simSlot == 0) {
                                    manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
                                    request.status = model.getmStatus();
                                    request.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                                } else {
                                    manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
                                    request.status = model.getmStatus();
                                    request.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;

                                }
                                updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                            }
                        } else {
                            updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        LogUtility.d("auto", "onfailure of verify called here for simslot " + simSlot+ "with throwable "+t.getMessage());
                        //Toast.makeText(ctx, ctx.getString(R.string.verification_error), Toast.LENGTH_SHORT).show();
                        cancelTimer(simSlot);
                        updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                    }
                });
    }


    /**
     * To update request item data while manual registration.
     *
     * @param ctx
     * @param simSlot
     * @param apiKey
     * @param ipPrivateKey
     */
    private RegistrationInfo saveManualRegistrationRequestItem(Context ctx, int simSlot, String apiKey,
                                                               String ipPrivateKey, String mobileNum,
                                                               String countryCode, String sequenceNumber, String fullNumber) {

        RegistrationInfo requestItem = new RegistrationInfo();
        requestItem.apiKey = apiKey;
        requestItem.privateKey = ipPrivateKey;
        requestItem.phoneNumber = mobileNum;
        requestItem.countryCode = countryCode;
        requestItem.sequenceNumber = sequenceNumber;
        requestItem.simSlot = simSlot;
        requestItem.fullNumberWithPlus = fullNumber;
        AppUtils.setSimRequestItemData(ctx, requestItem);
        return requestItem;
    }

    /**
     * Update preference data in case of auto registration fails.
     *
     * @param simSlot
     * @param context
     * @param sequenceNumber
     */
    private void updatePreferenceDataOnFailure(int simSlot, Context context, String sequenceNumber) {

        LogUtility.d("auto", "Updating on failure..");
        RegistrationInfo requestItem = new RegistrationInfo();
        PreferenceManager manager = PreferenceManager.getInstance(context);
        if (simSlot == 0) {
            manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
            requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
        } else {
            manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
            requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
        }
        requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
        requestItem.status = AppConstants.ApiResponseStatus.FAILURE;
        requestItem.simSlot = simSlot;
        requestItem.sequenceNumber = sequenceNumber;
        AppUtils.setSimRequestItemData(context, requestItem);

        LogUtility.d("auto", "calling registration verification mr2 ");
        RxEvent event = new RxEvent(AppConstants.RxEventName.MANUAL_REGISTRATION_MR_TWO_VERIFICATION_STATUS, requestItem);
        rxBus.send(event);
    }


    public void cancelTimerSimOne() {
        if (mCountDownTimerSimOne != null) {
            mCountDownTimerSimOne.cancel();
            mCountDownTimerSimOne = null;
        }
    }

    public void cancelTimerSimTwo() {

        if (mCountDownTimerSimTwo != null) {
            mCountDownTimerSimTwo.cancel();
            mCountDownTimerSimTwo = null;
        }
    }


    public void countDownTimerSimOne(final Context ctx, final int simSlot, final String sequenceNumber) {

        if (mCountDownTimerSimOne == null) {
            mCountDownTimerSimOne = new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    hideProgressBar();
                    updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                }
            }.start();
        } else {
            mCountDownTimerSimOne.start();
        }
    }

    public void countDownTimerSimTwo(final Context ctx, final int simSlot, final String sequenceNumber) {

        if (mCountDownTimerSimTwo == null) {

            mCountDownTimerSimTwo = new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    hideProgressBar();
                    updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                }
            }.start();
        } else {
            mCountDownTimerSimTwo.start();
        }
    }

    public void startTimer(Context ctx, String sequenceNumber, int simSlot) {

        if (simSlot == 0) {
            countDownTimerSimOne(ctx, simSlot, sequenceNumber);
        } else {
            countDownTimerSimTwo(ctx, simSlot, sequenceNumber);
        }

    }

    public void cancelTimer(int simSlot) {

        if (simSlot == 0) {
            cancelTimerSimOne();
        } else {
            cancelTimerSimTwo();
        }
    }

    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        if (charSequence.length() >= 10) {
            mNextButtonAlpha.set(1.0f);
            mNextButtonEnable.set(true);
        } else {
            mNextButtonAlpha.set(0.4f);
            mNextButtonEnable.set(false);
        }

    }
    public void clearEditText() {
        mSimOneEditTextPhoneNumber.set(AppConstants.EMPTY_TEXT);
        mSimTwoEditTextPhoneNumber.set(AppConstants.EMPTY_TEXT);
    }

}
