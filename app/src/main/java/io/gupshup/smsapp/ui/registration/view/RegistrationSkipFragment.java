package io.gupshup.smsapp.ui.registration.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.databinding.RegSkipFragmentBinding;
import io.gupshup.smsapp.databinding.TimerDialogCommonBinding;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ConfigUpdateEvent;
import io.gupshup.smsapp.events.ManualRegistrationEvent;
import io.gupshup.smsapp.events.OtpContent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.home.view.HomePagerFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.home.viewmodel.HomeFragmentViewModel;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationSkipFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Naveen BM on 6/14/2018.
 */
public class RegistrationSkipFragment extends Fragment implements RxBusCallback {
    private static final String TAG = RegistrationSkipFragment.class.getSimpleName();
    private RegSkipFragmentBinding mRegSkipFragBinding;
    @Inject
    public RxBus rxBus;
    private RxBusHelper rxBusHelper;
    private RegistrationSkipFragmentViewModel mRegistrationSkipFragViewModel;
    private int mManualRegistrationApiCallCount = 0;
    private AlertDialog mTimerDialog;
    private CountDownTimer mCountDownTimer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRegSkipFragBinding = DataBindingUtil.inflate(inflater, R.layout.reg_skip_fragment, container, false);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        mRegistrationSkipFragViewModel = ViewModelProviders.of(this).get(RegistrationSkipFragmentViewModel.class);
        mRegSkipFragBinding.setSkipViewModel(mRegistrationSkipFragViewModel);
        checkSimVerificationStatus();
        return mRegSkipFragBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onEventTrigger(Object object) {
        if (object instanceof RxEvent) {
            handleRxEvents((RxEvent) object);
        }
    }

    private void handleRxEvents(RxEvent event) {
        switch (event.getEventName()) {

            case AppConstants.RxEventName.ALREADY_REGISTERED:

                mRegistrationSkipFragViewModel.cancelTimer(0);
                mRegistrationSkipFragViewModel.cancelTimer(1);
                //UiUtils.showToast(getActivity(), getString(R.string.already_registered, phoneNumber));
                mRegistrationSkipFragViewModel.clearEditText();
                showHideProgressBar(false);
                break;
            case AppConstants.SKIP_MANULA_REGISTRATION_NEXT_CLICKED:
                //Next click handle manual registration flow
                LogUtility.d("auto", "manual registration next clicked");
                ManualRegistrationEvent manualRegEvent = (ManualRegistrationEvent) event.getEventData();
                String simOneNumber = manualRegEvent.getmSimOneManualNumber();
                String simTwoNumber = manualRegEvent.getmSimTwoManualNumber();
                String simOneCountryCode = mRegSkipFragBinding.ccpOne.getSelectedCountryCode();
                String simTwoCountryCode = mRegSkipFragBinding.ccpTwo.getSelectedCountryCode();

                mRegSkipFragBinding.ccpOne.registerCarrierNumberEditText(mRegSkipFragBinding.mobileNumberOne);
                mRegSkipFragBinding.ccpTwo.registerCarrierNumberEditText(mRegSkipFragBinding.mobileNumberTwo);

                if (!TextUtils.isEmpty(simOneNumber)) {
                    if (mRegSkipFragBinding.ccpOne.isValidFullNumber()) {
                        showHideProgressBar(true);
                        //LogUtility.d("auto", "Manual registration called for sim one after validation");
                        mRegistrationSkipFragViewModel.manualRegistration(getActivity(), simOneCountryCode, simOneNumber, Utils.getMaskFromPhoneNumber(simOneNumber), 0);

                    } else {
                        showHideProgressBar(false);
                        mRegSkipFragBinding.mobileNumberOne.setError(getString(R.string.enter_valid_mobile_number));
                        return;
                    }
                } else {
                    //LogUtility.d("auto", "Ignoring SIM 1 reg.....");
                    HomePagerFragment.isIgnoreSIM1Registration = true;
                }
                if (!TextUtils.isEmpty(simTwoNumber)) {
                    if (mRegSkipFragBinding.ccpTwo.isValidFullNumber()) {
                        showHideProgressBar(true);
                        //LogUtility.d("auto", "Manual registration called for sim two after validation... ");
                        mRegistrationSkipFragViewModel.manualRegistration(getActivity(), simTwoCountryCode, simTwoNumber, Utils.getMaskFromPhoneNumber(simTwoNumber), 1);
                    } else {
                        showHideProgressBar(false);
                        mRegSkipFragBinding.mobileNumberTwo.setError(getString(R.string.enter_valid_mobile_number));
                        return;
                    }
                } else {
                    //LogUtility.d("auto", "Ignoring SIM 2 reg.....");
                    HomePagerFragment.isIgnoreSIM2Registration = true;
                }

                if (TextUtils.isEmpty(simOneNumber)
                    || TextUtils.isEmpty(simTwoNumber)) {
                    mManualRegistrationApiCallCount = 1;
                }
                showCountDownTimerDialog();
                LogUtility.d(TAG, "----- SKIP MANUAL ---" + simOneCountryCode + "" + simOneNumber + ", " + simTwoCountryCode + "" + simTwoNumber);
                break;
            case AppConstants.SKIP_MANUAL_SKIP_CLICKED:
                RegistrationInfo simOneItem = AppUtils.getSimOneData(getActivity());
                RegistrationInfo simTwoItem = AppUtils.getSimTwoData(getActivity());
                PreferenceManager.getInstance(getContext()).setManualRegistrationTried(true);
                if (simOneItem != null) {
                    simOneItem.isSkipClicked = true;
                    AppUtils.setSimRequestItemData(getActivity(), simOneItem);
                }
                if (simTwoItem != null) {
                    simTwoItem.isSkipClicked = true;
                    AppUtils.setSimRequestItemData(getActivity(), simTwoItem);
                }
                MainActivity.disregardSimChange = true;
                RxEvent skipEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                rxBus.send(skipEvent);
                break;
            case AppConstants.RxEventName.OTP_MESSAGE:

                LogUtility.d("auto", "OTP message handled in RegSkipFrag");
                OtpContent otpContent = (OtpContent) event.getEventData();
                RegistrationInfo simOnePrefData = AppUtils.getSimOneData(getActivity());
                RegistrationInfo simTwoPrefData = AppUtils.getSimTwoData(getActivity());
                LogUtility.d("auto", "Call verify manual for sim slot " + otpContent.slot);
                if (otpContent.slot == 0) {
                    simOnePrefData.simSlot = otpContent.slot;
                    mRegistrationSkipFragViewModel.verifyManualRegistration(getActivity(), simOnePrefData, otpContent.otp, otpContent.slot, otpContent.sequenceNumber);
                } else {
                    simTwoPrefData.simSlot = otpContent.slot;
                    mRegistrationSkipFragViewModel.verifyManualRegistration(getActivity(), simTwoPrefData, otpContent.otp, otpContent.slot, otpContent.sequenceNumber);
                }
                HomeFragmentViewModel.isManualRegistrationInProcess = false;
                break;

            case AppConstants.RxEventName.MANUAL_REGISTRATION_MR_TWO_VERIFICATION_STATUS:

                LogUtility.d("auto", "MANUAL_REGISTRATION_MR_TWO_VERIFICATION_STATUS called");
                RegistrationInfo simData = (RegistrationInfo) event.getEventData();
                RegistrationInfo simOneRequest = AppUtils.getSimOneData(getActivity());
                RegistrationInfo simTwoRequest = AppUtils.getSimTwoData(getActivity());
                PreferenceManager manager = PreferenceManager.getInstance(getActivity());
                List<SimInformation> simList = AppUtils.getSimInfoList();
                AppUtils.saveSimInfoToPreference(simList, manager);
                showHideProgressBar(false);
                if (simList.size() > 1) {
                    if (simOneRequest != null && simTwoRequest != null) {
                        if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState
                            && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState) {
                            //Both success.
                            LogUtility.d("auto", "manual registration for both sim success");
                            mRegistrationSkipFragViewModel.cancelTimer(simOneRequest.simSlot);
                            mRegistrationSkipFragViewModel.cancelTimer(simTwoRequest.simSlot);
                            AppUtils.saveSimInfoToPreference(simList, manager);
                            //UiUtils.showToast(getActivity(), getString(R.string.registration_success));
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneRequest, simTwoRequest);
                            RxEvent configEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(configEvent);
                            MainActivity.disregardSimChange = true;
                            RxEvent takeToHomeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                            rxBus.send(takeToHomeEvent);
                        } else if ((RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState && HomePagerFragment.isIgnoreSIM2Registration) || (RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState && HomePagerFragment.isIgnoreSIM1Registration)) {
                            //same as both success
                            HomePagerFragment.isIgnoreSIM2Registration = false;
                            HomePagerFragment.isIgnoreSIM1Registration = false;
                            LogUtility.d("auto","Success ignore case matched....");
                            mRegistrationSkipFragViewModel.cancelTimer(simOneRequest.simSlot);
                            mRegistrationSkipFragViewModel.cancelTimer(simTwoRequest.simSlot);
                            AppUtils.saveSimInfoToPreference(simList, manager);
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneRequest, simTwoRequest);
                            RxEvent configEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(configEvent);
                            MainActivity.disregardSimChange = true;
                            RxEvent simTwoUpdateEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                            rxBus.send(simTwoUpdateEvent);
                        } else {
                            //Either of one sim manual registration failed
                            manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                        }
                    } else {
                        //Either of one sim manual registration failed
                        manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                    }
                } else if (simList.size() == 1) {
                    Log.d("auto", "Single sim manual registration with simOneState as :"+simOneRequest.simOneState);
                    //manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                    if(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState) {
                        PreferenceManager.getInstance(getContext()).setSim1RegistrationDone(true);
                        ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneRequest, null);
                        RxEvent simOneUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                        rxBus.send(simOneUpdateEvent);
                        MainActivity.disregardSimChange = true;
                        RxEvent manualRegSuccessEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                        rxBus.send(manualRegSuccessEvent);
                    }else{
                        manualRegistrationHandlingBasedOnStatus(simOneRequest, null, simOneRequest.simSlot);
                    }
                }
                break;
        }
    }

    private void showHideProgressBar(boolean show) {
        mRegistrationSkipFragViewModel.showProgressBar.set(show);
        mRegSkipFragBinding.ccpTwo.setCcpClickable(!show);
        mRegSkipFragBinding.ccpOne.setCcpClickable(!show);
        if (show) {
            showCountdownTimer();
        } else {
            cancelTimer();
        }
    }


    /**
     * Any of one or both sim Manual registration failed.
     *
     * @param simOneData
     * @param simTwoData
     */
    private void manualRegistrationHandlingBasedOnStatus(RegistrationInfo simOneData, RegistrationInfo simTwoData, int simSlot) {

        // ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
        mManualRegistrationApiCallCount++;
        if (simSlot == 0) {
            if (simOneData != null && simOneData.simOneState != null) {
                switch (simOneData.simOneState) {
                    case SIM_ONE_REGISTRATION_SUCCESS:
                        LogUtility.d("auto", "sim 1 manual reg success....");
                        if (!HomePagerFragment.isIgnoreSIM2Registration) {
                            mRegistrationSkipFragViewModel.cancelTimer(simOneData.simSlot);
                            //LogUtility.d("auto", "val of full phone number "+simOneData.fullNumberWithPlus);
                            LogUtility.d("auto", "ignore sim 2 reg is false here");
                            PreferenceManager.getInstance(getContext()).setSim1RegistrationDone(true);
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            RxEvent simOneUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(simOneUpdateEvent);
                        } else {
                            HomePagerFragment.isIgnoreSIM2Registration = false;
                            mRegistrationSkipFragViewModel.cancelTimer(simOneData.simSlot);
                            //LogUtility.d("auto", "Going to home");
                            MainActivity.disregardSimChange = true;
                            RxEvent takeToHomeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                            rxBus.send(takeToHomeEvent);
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            RxEvent simOneUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(simOneUpdateEvent);
                        }
                        break;
                    case SIM_ONE_REGISTRATION_FAILURE:
                        //Sim one auto failed
                        mRegistrationSkipFragViewModel.cancelTimer(0);
                        mRegistrationSkipFragViewModel.cancelTimer(1);
                        simOneData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                        AppUtils.setSimRequestItemData(getActivity(), simOneData);
                        //Fire event for manual registration.
                        showMrScreen();
                        break;
                }
            }
        } else if (simSlot == 1) {
            if (simTwoData != null && simTwoData.simTwoState != null) {
                switch (simTwoData.simTwoState) {
                    case SIM_TWO_REGISTRATION_SUCCESS:
                        LogUtility.d("auto", "sim 2 manual reg success....");
                        if (!HomePagerFragment.isIgnoreSIM1Registration) {
                            mRegistrationSkipFragViewModel.cancelTimer(simTwoData.simSlot);
                            //LogUtility.d("auto", "val of full phone number "+simTwoData.fullNumberWithPlus);
                            LogUtility.d("auto", "ignore sim 1 reg is false here");
                            PreferenceManager.getInstance(getContext()).setSim2RegistrationDone(true);
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            RxEvent simTwoUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(simTwoUpdateEvent);
                        } else {
                            HomePagerFragment.isIgnoreSIM1Registration = false;
                            mRegistrationSkipFragViewModel.cancelTimer(simTwoData.simSlot);
                            //LogUtility.d("auto", "Going to home");
                            MainActivity.disregardSimChange = true;
                            RxEvent takeToHomeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                            rxBus.send(takeToHomeEvent);
                            ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            RxEvent simOneUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                            rxBus.send(simOneUpdateEvent);
                        }
                        break;
                    case SIM_TWO_REGISTRATION_FAILURE:
                        //Sim two auto failed
                        mRegistrationSkipFragViewModel.cancelTimer(0);
                        mRegistrationSkipFragViewModel.cancelTimer(1);
                        simTwoData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                        AppUtils.setSimRequestItemData(getActivity(), simTwoData);
                        //Fire event for manual registration.
                        showMrScreen();
                        break;

                }
            }
        }
        checkSimVerificationStatus();
    }

    private void showMrScreen() {
        int simListSize = AppUtils.getSimInfoList().size();
        if (simListSize > 1) {
            if (mManualRegistrationApiCallCount == 2) {
                RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_MR_TWO_SKIP_SCREEN, null);
                rxBus.send(event);
            }
        } else {
            RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_MR_TWO_SKIP_SCREEN, null);
            rxBus.send(event);
        }
    }

    @Override
    public void onDetach() {
        cancelTimer();
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        cancelTimer();
        super.onDestroy();
    }

    private void checkSimVerificationStatus() {

        RegistrationInfo simOneRequest = AppUtils.getSimOneData(getActivity());
        RegistrationInfo simTwoRequest = AppUtils.getSimTwoData(getActivity());
        List<SimInformation> simList = AppUtils.getSimInfoList();
        if (simList.size() > 1) {
            if (simOneRequest != null && simTwoRequest != null) {
                if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState
                    && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState) {
                    mRegistrationSkipFragViewModel.mSimOneContainerVisibility.set(false);
                    mRegistrationSkipFragViewModel.mSimTwoContainerVisibility.set(false);
                    RxEvent skipEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                    rxBus.send(skipEvent);
                } else {
                    if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState) {
                        mRegistrationSkipFragViewModel.mSimOneContainerVisibility.set(false);
                    } else {
                        //Sim1 not verified
                        mRegistrationSkipFragViewModel.mSimOneContainerVisibility.set(true);
                    }

                    if (RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState) {
                        mRegistrationSkipFragViewModel.mSimTwoContainerVisibility.set(false);
                    } else {
                        mRegistrationSkipFragViewModel.mSimTwoContainerVisibility.set(true);
                    }
                    handleViewVisibility(simOneRequest, simTwoRequest);
                }
            } else {
                handleViewVisibility(simOneRequest, simTwoRequest);
            }
        } else if (simList.size() == 1) {
            SimInformation information = simList.get(0);
            if (information.getSimSlotIndex() == 0) {
                handleViewVisibility(simOneRequest, null);
            } else {
                handleViewVisibility(null, simTwoRequest);
            }
        }
    }

    private void handleViewVisibility(RegistrationInfo simOneRequest, RegistrationInfo simTwoRequest) {

        boolean canShowDivider = true;
        if (simOneRequest != null) {
            if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState) {
                mRegistrationSkipFragViewModel.mSimOneContainerVisibility.set(false);
                canShowDivider = false;
            } else {
                //Sim1 not verified
                mRegistrationSkipFragViewModel.mSimOneContainerVisibility.set(true);
            }
        }
        if (simTwoRequest != null) {
            if (RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState) {
                mRegistrationSkipFragViewModel.mSimTwoContainerVisibility.set(false);
                canShowDivider = false;
            } else {
                mRegistrationSkipFragViewModel.mSimTwoContainerVisibility.set(true);
            }
        }
        mRegistrationSkipFragViewModel.mDividerVisibility.set(canShowDivider);
    }

    private void showCountdownTimer() {

        mCountDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                mRegistrationSkipFragViewModel.showTimer.set(true);
                mRegistrationSkipFragViewModel.mTimerValue.set(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                mRegistrationSkipFragViewModel.showTimer.set(false);
                hideTimerDialog();
            }
        }.start();
    }

    private void cancelTimer() {
        mRegistrationSkipFragViewModel.showTimer.set(false);
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        hideTimerDialog();
    }

    private void showCountDownTimerDialog() {

        if (getActivity() == null) return;
        TimerDialogCommonBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.timer_dialog_common, null, false);
        binding.setViewModel(mRegistrationSkipFragViewModel);
        mTimerDialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        mTimerDialog.setCancelable(false);
        mTimerDialog.setCanceledOnTouchOutside(false);
        mTimerDialog.show();
    }

    private void hideTimerDialog() {

        if (mTimerDialog != null) {
            mTimerDialog.dismiss();
            mTimerDialog = null;
        }
    }
}
