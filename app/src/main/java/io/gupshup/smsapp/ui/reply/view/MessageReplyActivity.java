package io.gupshup.smsapp.ui.reply.view;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.MessageReplyBundleData;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.MessageReplyActivityNewBinding;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.events.ForwardMessageEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.compose.view.ComposeSmsActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.FragmentUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Naveen BM on 4/20/2018.
 */
public class MessageReplyActivity extends BaseActivity implements BaseHandlers, RxBusCallback {
    private static final String TAG = MessageReplyActivity.class.getSimpleName();
    private String mMask;
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;
    private boolean mIsFromBlock;
    private String mForwardingMessage;
    private MessageReplyActivityNewBinding mNewBinding;
    private int mPagerPosition;
    private String mSearchQuery = AppConstants.EMPTY_TEXT;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNewBinding = DataBindingUtil.setContentView(this, R.layout.message_reply_activity_new);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        initToolBar();
        loadMessageFrag();
    }

    /**
     * method used to get all the data to load MessageReply fragment,
     * and to load the fragment
     */
    private void loadMessageFrag() {

        ConversationType conversationType = ConversationType.PERSONAL;
        String conversationSubType = null;
        if (getIntent() != null) {
            LogUtility.d(TAG,"MESSAGE REPLY ACTIVITY INETNT DATA "+AppUtils.intentToString(getIntent()));
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mMask = bundle.getString(AppConstants.BundleKey.MASK);
                mSearchQuery = bundle.getString(AppConstants.BundleKey.SEARCH_QUERY, AppConstants.EMPTY_TEXT);
                mIsFromBlock = bundle.getBoolean(AppConstants.IS_FROM_BLOCK_SCREEN, false);
                mPagerPosition = bundle.getInt(AppConstants.BundleKey.PAGER_POSITION, 0);
                if (bundle.containsKey(AppConstants.KEY_FORWARD_MESSAGE)) {
                    mForwardingMessage = bundle.getString(AppConstants.KEY_FORWARD_MESSAGE);
                }
                if (bundle.containsKey(AppConstants.BundleKey.CONVERSATION_TYPE)) {
                    conversationType = (ConversationType) (bundle
                        .getSerializable(AppConstants.BundleKey.CONVERSATION_TYPE));
                }
                if (bundle.containsKey(AppConstants.BundleKey.CONVERSATION_SUB_TYPE)) {
                    conversationSubType = bundle.getString(AppConstants.BundleKey.CONVERSATION_SUB_TYPE);
                }
            }
        }


        MessageReplyBundleData parcelableData = new MessageReplyBundleData(mSearchQuery, mIsFromBlock,
            mForwardingMessage, mPagerPosition, conversationType);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BundleKey.MASK, mMask);
        bundle.putParcelable(AppConstants.BundleKey.PARCEL, parcelableData);

        if (!TextUtils.isEmpty(mForwardingMessage)) {
            //control came from forward
            List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mMask);
            int themeColorFromTile = AppUtils.getThemeColor(this, conversationEntity.get(0));
            setToolBarTitle(AppUtils.getDisplayName(conversationEntity.get(0)), themeColorFromTile);

            MessageReplyFragment fragItem = new MessageReplyFragment();
            bundle.putString(AppConstants.BundleKey.FORWARD_MESSAGE, mForwardingMessage);
            fragItem.setArguments(bundle);
            FragmentUtils.replaceFragment(this, fragItem, R.id.fragment_container);

        } else {
            //create bundle
            MessageReplyPagerFragment fragment = new MessageReplyPagerFragment();
            bundle.putString(AppConstants.BundleKey.CONVERSATION_SUB_TYPE, conversationSubType);
            fragment.setArguments(bundle);
            FragmentUtils.replaceFragment(this, fragment, R.id.fragment_container);
        }


    }


    private void setToolBarTitle(String displayName, int themeColor) {
        getSupportActionBar().setTitle(displayName);
        if (themeColor != -1) {
            mNewBinding.toolbarLayout.commonToolbar.setBackgroundColor(themeColor);
            AppUtils.darkenStatusBar(this, themeColor);
        }
    }

    private void initToolBar() {
        setSupportActionBar(mNewBinding.toolbarLayout.commonToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void onViewClick(View view) {

    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        boolean simVisibility = PreferenceManager.getInstance(this).getSimLayoutVisibility();
        if (simVisibility) {
            RxEvent simLayoutEvent = new RxEvent(AppConstants.RxEventName.SHO_HIDE_SIM_LAY_OUT, null);
            rxBus.send(simLayoutEvent);
        } else {
            overridePendingTransition(R.anim.enter_anim_left_to_right, R.anim.exit_anim_right_to_left);
            super.onBackPressed();
        }
    }

    public static void deleteSMSFromDefaultDB(Context context, Uri contentUri, MessageEntity[] messageEntities) {
        int i;
        for (i = 0; i < messageEntities.length; i++) {
            try {
                // Delete the SMS
                String uri = contentUri + "/" + messageEntities[i].getMessageId();
                int count = context.getContentResolver().delete(Uri.parse(uri), null, null);
                LogUtility.d("Cursor count", "Deleted :" + count);
            } catch (Exception e) {
                LogUtility.d("Exception del", "exc in delete  with val :" + e.getMessage());
            }
        }
    }

    public static void deleteSMSFromDefaultDB(Context context, Uri contentUri, String messageId) {
            try {
                // Delete the SMS
                String uri = contentUri + "/" + messageId;
                int count = context.getContentResolver().delete(Uri.parse(uri), null, null);
            } catch (Exception e) {
                //LogUtility.d("Exception del", "exc in delete  with val :" + e.getMessage());
            }
    }

    public static void updateSMSInDefaultDB(Context context, Uri contentUri, MessageEntity[] messageEntities) {

        int i;
        for (i = 0; i < messageEntities.length; i++) {
            try {
                String uri = contentUri + "/" + messageEntities[i].getMessageId();
                ContentValues values = new ContentValues();
                values.put("read", 1);
                int countUpdated = context.getContentResolver().update(Uri.parse(uri), values, "read = 0", null);
                //LogUtility.d("auto2", "Cursor count for read updated is :" + countUpdated);
            } catch (Exception e) {
                //LogUtility.d("auto2", "exc in update  with val :" + e.getMessage());
            }
        }
    }

    public static void updateSMSAsUnreadInDefaultDB(Context context, Uri contentUri, MessageEntity[] messageEntities) {

        int i;
        for (i = 0; i < messageEntities.length; i++) {
            try {
                String uri = contentUri + "/" + messageEntities[i].getMessageId();
                ContentValues values = new ContentValues();
                values.put("read", 0);
                int countUpdated = context.getContentResolver().update(Uri.parse(uri), values, null, null);
                //LogUtility.d("auto2", "Cursor count for read updated is :" + countUpdated);
            } catch (Exception e) {
                //LogUtility.d("auto2", "exc in update  with val :" + e.getMessage());
            }
        }
        //LogUtility.d("auto2", "the value after is :"+printvaluesFromSmstable(context, null));
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onEventTrigger(Object object) {
        RxEvent event = (RxEvent) object;
        if (event.getEventName().equalsIgnoreCase(AppConstants.FORWARD_MESSAGE_EVENT)) {
            ForwardMessageEvent forwardMessageEvent = (ForwardMessageEvent) event.getEventData();
            MessageEntity msgEntity = forwardMessageEvent.getMessageEntity();
            String mask = forwardMessageEvent.getMask();
            if (msgEntity != null && !TextUtils.isEmpty(mask)) {
                launchActivityForForward(msgEntity, mask);
            }
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.NEW_MESSAGE_CLICK_EVENT)) {
            String forwardMessage = (String) event.getEventData();
            launchComposeActivity(forwardMessage);
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.RxEventName.MESSAGE_REPLY_TOOLBAR_INFO)) {
            ToolbarInfo toolbarInfo = (ToolbarInfo) event.getEventData();
            setToolBarTitle(toolbarInfo.toolbarTitle, toolbarInfo.toolBarColor);
        }
    }

    /**
     * Method used to launch the activity for forwarding message
     */
    private void launchActivityForForward(MessageEntity messageEntity, String mask) {
        ConversationEntity entity = getConversationByMask(mask, this);
        AppUtils.launchMessageActivityForForward(this, entity, messageEntity);
        overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
        finish();
    }

    /**
     * If user selects new message in Message forwarding screen then launch compose sms activity
     */
    private void launchComposeActivity(String forwardMessage) {
        Intent intent = new Intent(this, ComposeSmsActivity.class);
        intent.putExtra(AppConstants.KEY_FORWARD_MESSAGE, forwardMessage);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
        finish();
    }

    /**
     * method used to handle the call option in the toolbar
     *
     * @param phoneNum
     */
    private void callToPhone(String phoneNum) {
        if (TextUtils.isEmpty(phoneNum))
            return;

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.CALL_PHONE},
                AppConstants.PermissionRequest.REQUEST_CALL_PHONE);
        } else {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phoneNum));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case AppConstants.PermissionRequest.REQUEST_CALL_PHONE:
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callToPhone(mMask);
                }
                break;
        }
    }

    /**
     * method used to get the conversation for a mask
     *
     * @param mask (Mobile number added with country code)
     * @param ctx
     * @return
     */
    public ConversationEntity getConversationByMask(String mask, Context ctx) {
        List<ConversationEntity> conList = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (conList != null && conList.size() > 0) {
            return conList.get(0);
        }
        return null;
    }
}
