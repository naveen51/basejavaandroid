package io.gupshup.smsapp.ui.settings.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class WebViewViewModel  extends BaseFragmentViewModel {

    public WebViewViewModel(@NonNull Application application) {
        super(application);
    }
}
