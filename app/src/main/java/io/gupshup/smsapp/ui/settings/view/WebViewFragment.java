package io.gupshup.smsapp.ui.settings.view;

import android.annotation.TargetApi;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.databinding.WebViewFragmentBinding;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.settings.viewmodel.WebViewViewModel;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class WebViewFragment extends BaseFragment<WebViewFragmentBinding,
    WebViewViewModel> {

    private String webURL;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(AppConstants.BundleKey.WEB_URL)) {
            webURL = bundle.getString(AppConstants.BundleKey.WEB_URL);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        loadWebURL(webURL);
    }

    private void initView() {
        mBinding.webView.setWebViewClient(new WebClient());
    }

    /**
     * Method used to load the Web URL
     *
     * @param
     */
    private void loadWebURL(String url) {
        if (!TextUtils.isEmpty(url)) {
            mBinding.webView.loadUrl(url);
        }
    }

    @Override
    public int getBindingVariable() {
        return BR.webViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.web_view_fragment;
    }

    @Override
    public WebViewViewModel getViewModel() {
        return ViewModelProviders.of(this).get(WebViewViewModel.class);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }


    private class WebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            super.onPageStarted(view, url, favicon);
            getCurrentViewModel().showProgressBar.set(true);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            // This javascript will remove the download link in the web page.
            // This is a requirement where, the download link should not be displayed in tyhe web page, if the page is displayed via the native mobile web view.
            getCurrentViewModel().showProgressBar.set(false);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

        }
    }

}
