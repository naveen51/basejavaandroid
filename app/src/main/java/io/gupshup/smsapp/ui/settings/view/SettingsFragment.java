package io.gupshup.smsapp.ui.settings.view;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.databinding.SettingsFragmentBinding;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.settings.viewmodel.SettingsFragViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class SettingsFragment extends BaseFragment<SettingsFragmentBinding,
    SettingsFragViewModel> implements RxBusCallback {

    private static final String TAG = SettingsFragment.class.getSimpleName();
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.setVariable(BR.handler, this);
        registerEvents();
        setUpProfileData();
        MainActivity.hasCalledAnotherActivity = true;
    }

    @Override
    public int getBindingVariable() {
        return BR.settingsFragViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.settings_fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        setCurrentDefaultSMSAppName();
        String packageName = getActivity().getPackageName();
        mBinding.defaultSmsNameContainer.setEnabled(!AppUtils.isDefaultSmsPackage(getActivity(), packageName));
    }

    private void setUpProfileData() {

        List<SimInformation> simList = AppUtils.getSimInfoList();
        if (simList.size() > 0) {
            for (SimInformation simInformation : simList) {
                setSimInfoToUi(simInformation, simList);
            }
        }
    }

    private void setSimInfoToUi(SimInformation simInformation, List<SimInformation> simList) {

        RegistrationInfo registrationInfo = AppUtils.getSimDataBasedOnSlot(getActivity(),
            String.valueOf(simInformation.getSimSlotIndex()));
        if (registrationInfo != null) {
            if (registrationInfo.simSlot == 0) {
                setUpSimOneRegistrationUI(registrationInfo, simList.size());
            } else if (registrationInfo.simSlot == 1) {
                setUpSimTwoRegistrationUI(registrationInfo, simList.size());
            }
        } else if (simInformation.getSimSlotIndex() == 0) {
            setUpSimOneRegistrationUI(null, simList.size());
        } else if (simInformation.getSimSlotIndex() == 1) {
            setUpSimTwoRegistrationUI(null, simList.size());
        }
    }


    private void setUpSimOneRegistrationUI(RegistrationInfo registrationInfo, int size) {

        if (registrationInfo != null) {
            if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == registrationInfo.simOneState) {
                getCurrentViewModel().mSimOneMobileNumber.set(AppUtils.replaceLastFourNumber(getActivity()
                    , registrationInfo.phoneNumber));
                getCurrentViewModel().mSimOneRightButtonText.set(getString(R.string.re_register_text));
                getCurrentViewModel().mSimOneButtonBg.set(ContextCompat.getDrawable(getActivity(), R.drawable.bg_re_register_round));
            } else if (RegistrationSimOneState.SIM_ONE_AUTO_REGISTRATION == registrationInfo.simOneState
                || RegistrationSimOneState.SIM_ONE_MANUAL_REGISTRATION == registrationInfo.simOneState) {
                setupRegistrationInProgressUI(registrationInfo.simSlot);
            } else {
                setSimOneRegisterNowView();
            }
            getCurrentViewModel().mSimOneVisibility.set(true);
        } else {
            setSimOneRegisterNowView();
        }
        Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.roboto_regular);
        mBinding.simOneBtn.setTypeface(typeface);
        mBinding.simOneBtn.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        mBinding.simOneBtn.setEnabled(true);
        if (size == 1) {
            getCurrentViewModel().mSimTwoVisibility.set(false);
        }
        mBinding.simOneBtn.setTag(registrationInfo);
    }

    private void setSimOneRegisterNowView() {

        getCurrentViewModel().mSimOneRightButtonText.set(getString(R.string.setting_register_now));
        getCurrentViewModel().mSimOneButtonBg.set(ContextCompat.getDrawable(getActivity(), R.drawable.bg_register_now_ronded));
        getCurrentViewModel().mSimOneVisibility.set(true);
        getCurrentViewModel().mSimOneMobileNumber.set(AppConstants.EMPTY_TEXT);
    }


    private void setUpSimTwoRegistrationUI(RegistrationInfo registrationInfo, int size) {

        if (registrationInfo != null) {
            if (RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == registrationInfo.simTwoState) {
                getCurrentViewModel().mSimTwoMobileNumber.set(AppUtils.replaceLastFourNumber(getActivity()
                    , registrationInfo.phoneNumber));
                getCurrentViewModel().mSimTwoVisibility.set(true);
                getCurrentViewModel().mSimTwoRightButtonText.set(getString(R.string.re_register_text));
                getCurrentViewModel().mSimTwoButtonBg.set(ContextCompat.getDrawable(getActivity(), R.drawable.bg_re_register_round));
            } else if (RegistrationSimTwoState.SIM_TWO_AUTO_REGISTRATION == registrationInfo.simTwoState
                || RegistrationSimTwoState.SIM_TWO_MANUAL_REGISTRATION == registrationInfo.simTwoState) {
                setupRegistrationInProgressUI(registrationInfo.simSlot);
            } else {
                setSimTwoRegisterNowView();
            }
            getCurrentViewModel().mSimTwoVisibility.set(true);
        } else {
            setSimTwoRegisterNowView();
        }
        Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.roboto_regular);
        mBinding.simTwoBtn.setTypeface(typeface);
        mBinding.simTwoBtn.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        mBinding.simTwoBtn.setEnabled(true);
        if (size == 1) {
            getCurrentViewModel().mSimOneVisibility.set(false);
        }
        mBinding.simTwoBtn.setTag(registrationInfo);
    }

    private void setSimTwoRegisterNowView() {

        getCurrentViewModel().mSimTwoRightButtonText.set(getString(R.string.setting_register_now));
        getCurrentViewModel().mSimTwoButtonBg.set(ContextCompat.getDrawable(getActivity(), R.drawable.bg_register_now_ronded));
        getCurrentViewModel().mSimTwoVisibility.set(true);
        getCurrentViewModel().mSimTwoMobileNumber.set(AppConstants.EMPTY_TEXT);
    }

    private void setupRegistrationInProgressUI(int simSlot) {

        if (simSlot == 0) {
            getCurrentViewModel().mSimOneRightButtonText.set(getString(R.string.registration_in_progress));
            Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.roboto_medium_italic);
            mBinding.simOneBtn.setTypeface(typeface);
            mBinding.simOneBtn.setTextColor(ContextCompat.getColor(getActivity(), R.color.manatee));
            getCurrentViewModel().mSimOneButtonBg.set(null);
        } else if (simSlot == 1) {
            getCurrentViewModel().mSimTwoRightButtonText.set(getString(R.string.registration_in_progress));
            Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.roboto_medium_italic);
            mBinding.simTwoBtn.setTypeface(typeface);
            mBinding.simTwoBtn.setTextColor(ContextCompat.getColor(getActivity(), R.color.manatee));
            getCurrentViewModel().mSimTwoButtonBg.set(null);
        }
    }


    private void setCurrentDefaultSMSAppName() {
        getCurrentViewModel().mDefaultSmsAppName.set(AppUtils.getCurrentDefaultSMSAppName(getActivity()));
    }

    @Override
    public SettingsFragViewModel getViewModel() {
        return ViewModelProviders.of(this).get(SettingsFragViewModel.class);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }

    @Override
    public void onViewClick(View view) {
        super.onViewClick(view);
        switch (view.getId()) {
            case R.id.default_sms_name_container:
                showDefaultSmsDialog();
                break;
            case R.id.sim_one_btn:
                RxEvent eventOneBtn = new RxEvent(AppConstants.RxEventName.RE_REGISTRATION_FRAGMENT_EVENT, null);
                rxBus.send(eventOneBtn);
              /*  view.setEnabled(false);
                RegistrationInfo infoSimOne = (RegistrationInfo) view.getTag();
                if (infoSimOne != null) {
                    setupRegistrationInProgressUI(infoSimOne.simSlot);
                } else {
                    setupRegistrationInProgressUI(0);
                }
                ReRegistrationEvent simOneEvent = new ReRegistrationEvent();
                simOneEvent.registrationInfo = infoSimOne;
                simOneEvent.simSlot = 0;
                RxEvent eventSimOne = new RxEvent(AppConstants.RxEventName.SETTING_RE_REGISTRATION, simOneEvent);
                rxBus.send(eventSimOne);*/
                break;
            case R.id.sim_two_btn:
                RxEvent eventTwoBtn = new RxEvent(AppConstants.RxEventName.RE_REGISTRATION_FRAGMENT_EVENT, null);
                rxBus.send(eventTwoBtn);
               /* view.setEnabled(false);
                RegistrationInfo infoSimTwo = (RegistrationInfo) view.getTag();
                if (infoSimTwo != null) {
                    setupRegistrationInProgressUI(infoSimTwo.simSlot);
                } else {
                    setupRegistrationInProgressUI(1);
                }
                ReRegistrationEvent simTwoEvent = new ReRegistrationEvent();
                simTwoEvent.registrationInfo = infoSimTwo;
                simTwoEvent.simSlot = 1;
                RxEvent eventSimTwo = new RxEvent(AppConstants.RxEventName.SETTING_RE_REGISTRATION, simTwoEvent);
                rxBus.send(eventSimTwo);*/
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    setCurrentDefaultSMSAppName();
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onEventTrigger(Object event) {

        if (event instanceof RxEvent) {
            handleRxEvents((RxEvent) event);
        }
    }

    private void handleRxEvents(RxEvent event) {

        switch (event.getEventName()) {
            case AppConstants.RxEventName.AUTO_REGISTRATION_VERIFICATION_STATUS:
                updateUI(event);
                //exit Re-Registration Screen Event
                RxEvent exitReRegistration= new RxEvent(AppConstants.RxEventName.EXIT_RE_REGISTRATION_SCREEN_EVENT, event.getEventData());
                rxBus.send(exitReRegistration);
                break;
            case AppConstants.RxEventName.UPDATE_SETTINGS_UI_ON_MANUAL_REGISTRATION_SUCCES:
                 updateUI(event);
                break;

        }
    }

    private void updateUI(RxEvent event) {
        List<SimInformation> simList = AppUtils.getSimInfoList();
        RegistrationInfo simData = (RegistrationInfo) event.getEventData();
        if (simData != null) {
            if (simData.simSlot == 0) {
                setUpSimOneRegistrationUI(simData, simList.size());
            } else if (simData.simSlot == 1) {
                setUpSimTwoRegistrationUI(simData, simList.size());
            }
        }
    }
}
