package io.gupshup.smsapp.ui.base.common;

/**
 * Created by Ram Prakash Bhat on 8/5/18.
 */

public interface SwipeHandler {

    void onItemSwipedLeft(int position);

    void onItemSwipedRight(int position);
}
