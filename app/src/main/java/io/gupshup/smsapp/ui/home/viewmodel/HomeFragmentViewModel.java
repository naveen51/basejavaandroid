package io.gupshup.smsapp.ui.home.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.StatusResponse;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.entities.ConfigEntity;
import io.gupshup.smsapp.enums.ConfigKey;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ConfigUpdateEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.service.RegistrationService;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ram Prakash Bhat on 6/4/18.
 */

public class HomeFragmentViewModel extends BaseFragmentViewModel {

    private CountDownTimer mCountDownTimerSimOne;
    private CountDownTimer mCountDownTimerSimTwo;
    @Inject
    public RxBus rxBus;
    public static boolean isManualRegistrationInProcess;

    public HomeFragmentViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public LiveData<Integer> getPersonalTabUnreadCount(Context context, ConversationType personal) {

        ConversationDAO conversationDAO = AppDatabase.getInstance(context).conversationDAO();
        return conversationDAO.getTabCount(personal);
    }

    public LiveData<Integer> getTranslTabUnreadCount(Context context, ConversationType [] types) {

        ConversationDAO conversationDAO = AppDatabase.getInstance(context).conversationDAO();
        return conversationDAO.getTabCount(types);
    }

    public LiveData<Integer> getPromoTabUnreadCount(Context context, ConversationType promo) {

        ConversationDAO conversationDAO = AppDatabase.getInstance(context).conversationDAO();
        return conversationDAO.getTabCount(promo);
    }

    public LiveData<Integer> getBlockedTabUnreadCount(Context context, ConversationType blocked) {

        ConversationDAO conversationDAO = AppDatabase.getInstance(context).conversationDAO();
        return conversationDAO.getTabCount(blocked);
    }


    /**
     * Method used to call the registration API
     */
    public void manualRegistration(final Context ctx, final String countryCode, final String mobileNum,
                                   final String fullNumberWithPlus, final int simSlot) {

        final String sequenceNumber = AppUtils.generateRandomNumber(4);
        final String apiKey = AppUtils.fetchApiKey(simSlot);
        String ipPublicKey = "";
        final GupshupKeyPair gupshupKeyPair = AppUtils.generateGupshupKeyPair();
        if (gupshupKeyPair != null) {
            ipPublicKey = gupshupKeyPair.getPublicKey().toString();
        }
        final RegistrationInfo requestItem = saveManualRegistrationRequestItem(ctx, simSlot, apiKey,
                gupshupKeyPair.getPrivateKey().toString(), mobileNum, countryCode, sequenceNumber);
        // setPreferenceValues(ctx, simSlot, apiKey, ipPublicKey, sequenceNumber, mobileNum);
        isManualRegistrationInProcess = true;
        RegistrationService.getInstance().manualRegistration(ctx, countryCode, mobileNum, fullNumberWithPlus,
                sequenceNumber, ipPublicKey, apiKey, simSlot, new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        StatusResponse model = response.body();
                        PreferenceManager.getInstance(ctx).setManualRegistrationTried(true);
                        if (model != null &&
                                AppConstants.ApiResponseStatus.SUCCESS.equalsIgnoreCase(model.getmStatus())) {
                            requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            AppUtils.setSimRequestItemData(ctx, requestItem);
                            startTimer(ctx, sequenceNumber, simSlot);
                            PreferenceManager.getInstance(ctx).setRegistrationDone(true);
                            LogUtility.d("reg", "Manual registration successful for sim slot "+simSlot);
                            AppUtils.logToFile(ctx, "Manual registration successful for sim slot "+simSlot);
                            RegistrationInfo simOneData = AppUtils.getSimOneData(ctx);
                            RegistrationInfo simTwoData = AppUtils.getSimTwoData(ctx);
                            //simOneData.
                           if(simSlot == 0){
                               simOneData.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS);
                           }else if(simSlot == 1){
                               simOneData.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS);
                           }
                            ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            updateConfigData(updateEvent, ctx);

                        } else {
                            LogUtility.d("auto", "manual registration failed for sim : "+simSlot);
                            AppUtils.logToFile(ctx, "manual registration failed for sim : "+simSlot);
                            requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            if (simSlot == 0) {
                                requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                            } else {
                                requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
                            }
                            cancelTimer(simSlot);
                            //Toast.makeText(ctx, ctx.getString(R.string.registration_error), Toast.LENGTH_SHORT).show();
                            updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        //Toast.makeText(ctx, ctx.getString(R.string.registration_error), Toast.LENGTH_SHORT).show();
                        LogUtility.d("auto", "manual registration API onFailure for sim : "+simSlot);
                        AppUtils.logToFile(ctx, "manual registration API onfailure for sim : "+simSlot);
                        cancelTimer(simSlot);
                        updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                        PreferenceManager.getInstance(ctx).setManualRegistrationTried(true);
                        isManualRegistrationInProcess=false;
                    }
                });
    }


    private void updateConfigData(ConfigUpdateEvent item, Context ctx) {

        LogUtility.d("auto", "updating config..");
        AppUtils.logToFile(ctx, "updating config..");
        if (item != null) {
            RegistrationInfo itemOne = item.getSimOneData();
            RegistrationInfo itemTwo = item.getSimTwoData();

            if (itemOne != null) {
                final ConfigEntity apiKeyEntity = new ConfigEntity();
                apiKeyEntity.setKey(ConfigKey.API_KEY_1);
                apiKeyEntity.setValue(itemOne.apiKey);

                final ConfigEntity privateKeyOneEntity = new ConfigEntity();
                privateKeyOneEntity.setKey(ConfigKey.IP_PRIVATE_KEY_1);
                privateKeyOneEntity.setValue(itemOne.privateKey);

                final ConfigEntity phoneNumberEntity = new ConfigEntity();
                phoneNumberEntity.setKey(ConfigKey.PHONE_NUMBER_SIM_1);
                phoneNumberEntity.setValue(itemOne.phoneNumber);

                new MainActivity.InsertConfigTask(ctx, apiKeyEntity, privateKeyOneEntity, phoneNumberEntity).execute();
            }
            if (itemTwo != null) {
                final ConfigEntity apiKeyEntity = new ConfigEntity();
                apiKeyEntity.setKey(ConfigKey.API_KEY_2);
                apiKeyEntity.setValue(itemTwo.apiKey);

                final ConfigEntity privateKeyOneEntity = new ConfigEntity();
                privateKeyOneEntity.setKey(ConfigKey.IP_PRIVATE_KEY_2);
                privateKeyOneEntity.setValue(itemTwo.privateKey);

                final ConfigEntity phoneNumberEntity = new ConfigEntity();
                phoneNumberEntity.setKey(ConfigKey.PHONE_NUMBER_SIM_2);
                phoneNumberEntity.setValue(itemTwo.phoneNumber);

                new MainActivity.InsertConfigTask(ctx, apiKeyEntity, privateKeyOneEntity, phoneNumberEntity).execute();
            }
        }
    }
    /**
     * method used to call the API to verify the Phone Number
     */
    public void verifyManualRegistration(final Context ctx, final RegistrationInfo request, String otp,
                                         final int simSlot, final String sequenceNumber) {
        //showProgressBar();

        RegistrationService.getInstance().verifyOtpForManualRegistration(Utils.verifyCountryCode(request.countryCode),
                request.phoneNumber, otp, request.apiKey, request.sequenceNumber, new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {

                        StatusResponse model = response.body();
                        cancelTimer(simSlot);
                        PreferenceManager manager = PreferenceManager.getInstance(ctx);
                        //manager.setSimPhoneNumber(sequenceNumber,  request.phoneNumber);
                        request.sequenceNumber = sequenceNumber;
                        if (model != null) {

                            if (AppConstants.ApiResponseStatus.SUCCESS.equalsIgnoreCase(model.getmStatus())) {
                                if (simSlot == 0) {
                                    manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS.toString());
                                    request.status = AppConstants.VERIFIED;
                                    request.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS;
                                } else {
                                    manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS.toString());
                                    request.status = AppConstants.VERIFIED;
                                    request.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS;
                                }
                                AppUtils.setSimRequestItemData(ctx, request);
                                RegistrationService.getInstance().sendFirebaseTokenToServer(ctx, request.countryCode, request.phoneNumber, request.apiKey);
                                RxEvent event = new RxEvent(AppConstants.RxEventName.MANUAL_REGISTRATION_VERIFICATION_STATUS, request);
                                rxBus.send(event);

                                RxEvent eventReRegister = new RxEvent(AppConstants.RxEventName.UPDATE_SETTINGS_UI_ON_MANUAL_REGISTRATION_SUCCES, request);
                                rxBus.send(eventReRegister);

                            } else {
                                if (simSlot == 0) {
                                    manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
                                    request.status = model.getmStatus();
                                    request.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                                } else {
                                    manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
                                    request.status = model.getmStatus();
                                    request.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;

                                }
                                updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                            }
                            //==============
                        } else {
                            //Toast.makeText(ctx, ctx.getString(R.string.verification_error), Toast.LENGTH_SHORT).show();
                            updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        //Toast.makeText(ctx, ctx.getString(R.string.verification_error), Toast.LENGTH_SHORT).show();
                        cancelTimer(simSlot);
                        updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                    }
                });
    }


    /**
     * To update request item data while manual registration.
     *
     * @param ctx
     * @param simSlot
     * @param apiKey
     * @param ipPublicKey
     */
    private RegistrationInfo saveManualRegistrationRequestItem(Context ctx, int simSlot, String apiKey,
                                                               String ipPublicKey, String mobileNum,
                                                               String countryCode, String sequenceNumber) {

        RegistrationInfo requestItem = new RegistrationInfo();
        requestItem.apiKey = apiKey;
        requestItem.privateKey = ipPublicKey;
        requestItem.phoneNumber = mobileNum;
        requestItem.countryCode = countryCode;
        requestItem.sequenceNumber = sequenceNumber;
        requestItem.simSlot = simSlot;
        requestItem.fullNumberWithPlus = Utils.getMaskFromPhoneNumber(mobileNum);
        AppUtils.setSimRequestItemData(ctx, requestItem);
        return requestItem;
    }

    /**
     * Update preference data in case of auto registration fails.
     *
     * @param simSlot
     * @param context
     * @param sequenceNumber
     */
    private void updatePreferenceDataOnFailure(int simSlot, Context context, String sequenceNumber) {

        LogUtility.d("auto", "updating preferences on failure of manual registration....");
        AppUtils.logToFile(context, "updating preferences on failure of manual registration....");
        RegistrationInfo requestItem = new RegistrationInfo();
        PreferenceManager manager = PreferenceManager.getInstance(context);
        if (simSlot == 0) {
            manager.setSimOneState(RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE.toString());
            requestItem.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
        } else {
            manager.setSimTwoState(RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE.toString());
            requestItem.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
        }
        requestItem.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
        requestItem.status = AppConstants.ApiResponseStatus.FAILURE;
        requestItem.simSlot = simSlot;
        requestItem.sequenceNumber = sequenceNumber;
        AppUtils.setSimRequestItemData(context, requestItem);

        RxEvent event = new RxEvent(AppConstants.RxEventName.MANUAL_REGISTRATION_VERIFICATION_STATUS, requestItem);
        rxBus.send(event);
    }


    public void cancelTimerSimOne() {
        if (mCountDownTimerSimOne != null) {
            mCountDownTimerSimOne.cancel();
            mCountDownTimerSimOne = null;
        }
    }

    public void cancelTimerSimTwo() {

        if (mCountDownTimerSimTwo != null) {
            mCountDownTimerSimTwo.cancel();
            mCountDownTimerSimTwo = null;
        }
    }


    public void countDownTimerSimOne(final Context ctx, final int simSlot, final String sequenceNumber) {

        if (mCountDownTimerSimOne == null) {
            mCountDownTimerSimOne = new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    hideProgressBar();
                    updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                }
            }.start();
        } else {
            mCountDownTimerSimOne.start();
        }
    }

    private void countDownTimerSimTwo(final Context ctx, final int simSlot, final String sequenceNumber) {

        if (mCountDownTimerSimTwo == null) {

            mCountDownTimerSimTwo = new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    hideProgressBar();
                    updatePreferenceDataOnFailure(simSlot, ctx, sequenceNumber);
                    isManualRegistrationInProcess = false;
                }
            }.start();
        } else {
            mCountDownTimerSimTwo.start();
        }
    }

    private void startTimer(Context ctx, String sequenceNumber, int simSlot) {

        isManualRegistrationInProcess = true;
        if (simSlot == 0) {
            countDownTimerSimOne(ctx, simSlot, sequenceNumber);
        } else {
            countDownTimerSimTwo(ctx, simSlot, sequenceNumber);
        }

    }

    public void cancelTimer(int simSlot) {

        if (simSlot == 0) {
            cancelTimerSimOne();
        } else {
            cancelTimerSimTwo();
        }
    }

}
