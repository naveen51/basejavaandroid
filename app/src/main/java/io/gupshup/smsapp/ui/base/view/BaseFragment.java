package io.gupshup.smsapp.ui.base.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.DialogAddToContactBinding;
import io.gupshup.smsapp.databinding.DialogMessageMoveBinding;
import io.gupshup.smsapp.enums.ActionModeType;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.ui.adapter.MessageListAdapter;
import io.gupshup.smsapp.ui.archive.view.ArchivedMessageFragment;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.ui.home.viewmodel.MessageViewModel;
import io.gupshup.smsapp.ui.reply.view.MessageReplyActivity;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.DialogUtils.DialogClickListener;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.SwipeItemTouchHelperCallback;
import io.gupshup.smsapp.utils.UiUtils;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Ram Prakash Bhat on 6/4/18.
 */

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseFragmentViewModel>
    extends Fragment implements BaseHandlers, RecyclerViewPageListAdapter.onItemClickListener
    , DialogClickListener/* , SearchView.OnQueryTextListener*//*, RadioGroup.OnCheckedChangeListener*/ {

    protected ActionBar mActionBar;
    protected T mBinding;
    private V mViewModel;
    public Dialog mMessageSwitchDialog, mAddToContactDialog;
    protected SwipeItemTouchHelperCallback swipeCallback;
    protected Menu mMenu;
    protected int mUnreadMessageCount = 0;
    protected boolean mSelectAllClicked;
    protected ConversationSubType mConversationSubType;
    protected ConversationType mConversationType;
    private ConversationEntity[] mDeletableconversation;
    public static int DEFAULT_SMS_REQUEST_CODE = 101;
    //protected Dialog mConfigDetailDialog;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();


    @Override
    public void onDetach() {
        mActionBar = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = getViewModel();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.setVariable(getBindingVariable(), mViewModel);
        mBinding.executePendingBindings();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public V getCurrentViewModel() {
        return mViewModel;
    }

    public interface ActionBar {

        void setActionBarVisibility(int visibility);

        void setToolBarTitle(String title);
    }


    @Override
    public void onViewClick(View view) {
        //TODO: common click handle here
    }

    /*public void onItemClick(View view) {
        //TODO: common click handle here
    }

    public boolean onLongClick(View view) {
        //TODO: common click handle here
        return true;
    }*/

    @Override
    public void onItemClick(View view, int position) {
        switch (view.getId()) {

        }

    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    @Override
    public void onSelectAllAndScroll() {

    }

    protected void updateConversation(ActionModeType actionModeType, PagedList<ConversationEntity> messageList, List<ConversationEntity> selectedItem) {

        String movedToText = AppConstants.EMPTY_TEXT;
        List<ConversationEntity> conversationEntities = new ArrayList<>();
        for (int position = selectedItem.size() - 1; position >= 0; position--) {
            if (messageList != null && position < messageList.size()) {
                ConversationEntity conversation = selectedItem.get(position);//messageList.get(selectedItem.get(position));
                if (conversation != null) {
                    switch (actionModeType) {
                        case BLOCKED:
                            conversation.setLastConversationType(conversation.getType());
                            conversation.setType(ConversationType.BLOCKED);
                            break;
                        case MUTE:
                            conversation.setSendNotifications(false);
                            conversation.setNotificationsSound(false);
                            conversation.setNotificationsLight(false);
                            conversation.setNotificationsPopup(false);
                            break;
                        case UN_MUTE:
                            conversation.setSendNotifications(true);
                            conversation.setNotificationsSound(true);
                            conversation.setNotificationsLight(true);
                            conversation.setNotificationsPopup(true);
                            break;
                        case ARCHIVE:
                            conversation.setSubType(ConversationSubType.ARCHIVE);
                            break;
                        case UN_ARCHIVE:
                            conversation.setSubType(null);
                            break;
                        case UN_BLOCK:
                            //conversation.setType(Utils.getConversationTypeFromMask(conversation.getMask()));
                            if (conversation.getLastConversationType() != null) {
                                conversation.setType(conversation.getLastConversationType());
                            } else {
                                conversation.setType(Utils.getConversationType(conversation.getMask(),Utils.getLastMessageInConversation(conversation)));
                            }
                            break;
                    }
                    conversationEntities.add(conversation);
                }
            }
        }
        ConversationEntity[] conversation = new ConversationEntity[conversationEntities.size()];
        conversation = conversationEntities.toArray(conversation);
        switch (actionModeType) {
            case BLOCKED:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                if (this instanceof ArchivedMessageFragment) {
                    notifySelectionClear();
                } else {
                    clearSelectionAndActionMode();
                }
                movedToText = setMoveToMessage(getActivity(), conversationEntities.size(), getString(R.string.block_tab));
                break;
            case UN_BLOCK:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                if (this instanceof ArchivedMessageFragment) {
                    notifySelectionClear();
                } else {
                    clearSelectionAndActionMode();
                }
                movedToText = getString(R.string.unblocked_successfully);
                break;
            case MOVE:
                showMessageDetail(conversationEntities);
                break;
            case DELETE:
                mDeletableconversation = conversation;
                String confirmationMessage = getResources().getQuantityString(R.plurals.thread_delete_confirmation, mDeletableconversation.length, mDeletableconversation.length);
                DialogUtils.showConfirmPopUp(getActivity(), confirmationMessage, getString(R.string.delete_dialog_title),
                    getString(R.string.confirm), getString(R.string.cancel_btn), this);
                break;
            case ADD_TO_CONTACT:
                Utils.addNewContact(getActivity(), conversation[0].getMask(), this, false);
                notifySelectionClear();
                break;
            case MARK_AS_READ:
                for (ConversationEntity entity : conversation) {
                    if (entity.getNoOfUnreadMessages() > 0
                        || entity.isUnread()) {
                        MessagingService.getInstance(SMSService.class).
                            readConversation(entity.getMask(),
                                DateUtils.currentTimestamp());
                    }
                }
                notifySelectionClear();
                break;
            case MUTE:
            case UN_MUTE:
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                notifySelectionClear();
                break;
            case ARCHIVE:
            case UN_ARCHIVE:
                if (this instanceof ArchivedMessageFragment) {
                    movedToText = getString(R.string.un_archived_message);
                } else {
                    movedToText = setMoveToMessage(getActivity(), conversationEntities.size(), getString(R.string.archive_block));
                }
                MessagingService.getInstance(SMSService.class).updateConversation(conversation);
                notifySelectionClear();
                break;
        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
    }

    private void deleteConversationFromDefaultSMSDb(ConversationEntity[] conversations) {

        MessageDAO messageDAO = AppDatabase.getInstance(getContext()).messageDAO();

        String masks[] = new String[conversations.length];
        for (int i = 0; i < conversations.length; i++) {
            masks[i] = conversations[i].getMask();
        }

        LogUtility.d("delete", "The mask size is " + masks.length + " mask contents :" + masks[0]);
        //int messageIdList[] = new int[masks.length];
        for (String mask : masks) {
            List<MessageEntity> messageEntityList = messageDAO.getMessagesForMask(mask);
            LogUtility.d("delete", "The messages for mask " + mask + " has nos. " + messageEntityList.size());
            MessageEntity[] message = new MessageEntity[messageEntityList.size()];
            message = messageEntityList.toArray(message);
            MessageReplyActivity.deleteSMSFromDefaultDB(getContext(), Telephony.Sms.CONTENT_URI, message);
        }
    }


    public void showMessageDetail(List<ConversationEntity> conversationEntityList) {

        DialogMessageMoveBinding dialogMessageMoveBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_message_move,
            null, false);
        mMessageSwitchDialog = DialogUtils.getCustomDialog(getActivity(), dialogMessageMoveBinding.getRoot());
        if (this instanceof ArchivedMessageFragment) {
            dialogMessageMoveBinding.typeArchiveImage.setImageResource(R.drawable.icn_move_unarchive);
        }
        ConversationType type = getConversationTypeFromBundle();
        if (type != null && type == ConversationType.BLOCKED) {
            dialogMessageMoveBinding.typeBlockImage.setImageResource(R.drawable.action_unblock_dark);
        }
        dialogMessageMoveBinding.setBaseFragment(this);
        dialogMessageMoveBinding.setConversationList(conversationEntityList);
        dialogMessageMoveBinding.setMessageViewModel(getCurrentViewModel() instanceof
            MessageViewModel ? (MessageViewModel) getCurrentViewModel() : null);
        mMessageSwitchDialog.setCancelable(true);
        mMessageSwitchDialog.setCanceledOnTouchOutside(true);
        mMessageSwitchDialog.show();
    }

    public void onMoveToItemClicked(View view, List<ConversationEntity> conversationList) {

        List<ConversationEntity> conversationEntities = new ArrayList<>();
        String movedToText = AppConstants.EMPTY_TEXT;
        for (int position = 0; position < conversationList.size(); position++) {
            if (position < conversationList.size()) {
                ConversationEntity conversation = conversationList.get(position);
                if (conversation != null) {
                    if (this instanceof ArchivedMessageFragment) {
                        conversation.setSubType(null);
                    }
                    switch (view.getId()) {
                        case R.id.type_personal_container:
                            conversation.setType(ConversationType.PERSONAL);
                            movedToText = setMoveToMessage(getActivity(), conversationList.size(), getString(R.string.personal_tab));
                            break;

                        case R.id.transactional_container:
                            conversation.setType(ConversationType.TRANSACTIONAL);
                            movedToText = setMoveToMessage(getActivity(), conversationList.size(), getString(R.string.trans_tab));
                            break;

                        case R.id.type_promotional_container:
                            conversation.setType(ConversationType.PROMOTIONAL);
                            movedToText = setMoveToMessage(getActivity(), conversationList.size(), getString(R.string.promo_tab));
                            break;

                        case R.id.type_block_container:
                            ConversationType type = getConversationTypeFromBundle();
                            if ((this instanceof ArchivedMessageFragment
                                || type != null && type == ConversationType.BLOCKED)  //AMA-549 this instanceof BlockedMessageListFragment
                                && conversation.getType() == ConversationType.BLOCKED) {
                                MessagingService.getInstance(SMSService.class).unBlockMessage(conversation);
                                movedToText = getString(R.string.unblocked_successfully);
                            } else {
                                conversation.setLastConversationType(conversation.getType());
                                conversation.setType(ConversationType.BLOCKED);
                                movedToText = setMoveToMessage(getActivity(), conversationList.size(), getString(R.string.block_tab));
                            }
                            break;
                        /*case R.id.type_unblock:
                            //From Blocked tab
                            ConversationType conversationType = Utils.getConversationTypeFromMask(conversation.getMask());
                            conversation.setType(conversationType);
                            //movedToText = MessageUtils.getTabTitle(getActivity(), conversationType);
                            movedToText = getString(R.string.unblocked_successfully);
                            break;
                        case R.id.type_delete:
                            MessagingService.getInstance(SMSService.class).deleteConversation(conversation);
                            break;*/
                        case R.id.type_archive_container:
                            if (this instanceof ArchivedMessageFragment) {
                                movedToText = getString(R.string.un_archived_message);//getTabTitle(actualType);
                            } else {
                                conversation.setSubType(ConversationSubType.ARCHIVE);
                                movedToText = setMoveToMessage(getActivity(), conversationList.size(), getString(R.string.archive_block));
                            }
                            break;
                    }
                    conversationEntities.add(conversation);
                }
            }
        }
        ConversationEntity[] conversation = new ConversationEntity[conversationEntities.size()];
        conversation = conversationEntities.toArray(conversation);
        MessagingService.getInstance(SMSService.class).updateConversation(conversation);
        clearSelectionAndActionMode();
        if (mMessageSwitchDialog != null && mMessageSwitchDialog.isShowing()) {
            mMessageSwitchDialog.dismiss();
        }
        if (!TextUtils.isEmpty(movedToText)) {
            UiUtils.showToast(getActivity(), movedToText);
        }
    }

    protected void clearSelectionAndActionMode() {

    }

    protected void notifySelectionClear() {

    }

    protected void handleActionMenuForMultiItem(int position, int count, Menu menu,
                                                ConversationSubType conversationSubType,
                                                ConversationEntity entity, boolean isSelected) {


        //int unreadMessageCount = 0/*, muteCount = 0, unMuteCount = 0, blockCount = 0, unBlockCount = 0*/;

        if (mSelectAllClicked) {
            if (menu.findItem(R.id.action_add_contact) != null) {
                menu.findItem(R.id.action_add_contact).setVisible(false);
            }
        } else {
            boolean canAddToContact;
            canAddToContact = canAddToContact(entity.getMask()) < 0
                && Utils.getConversationType(entity.getMask(),Utils.getLastMessageInConversation(entity)) == ConversationType.PERSONAL;
            enableMenuItemVisibility(entity, isSelected);
            if (menu.findItem(R.id.action_add_contact) != null) {
                if (count == 1 && canAddToContact) {
                    menu.findItem(R.id.action_add_contact).setVisible(true);
                } else {
                    menu.findItem(R.id.action_add_contact).setVisible(false);
                }
            }
            if (mUnreadMessageCount > 0) {
                menu.findItem(R.id.action_mark_as_read).setVisible(true);
                menu.findItem(R.id.action_mark_as_read).setTitle(getString(R.string.action_mark_as_read));
            } else {
                menu.findItem(R.id.action_mark_as_read).setVisible(false);
            }
        }
        if (ConversationSubType.ARCHIVE == conversationSubType) {
            menu.findItem(R.id.action_archive).setVisible(false);
            menu.findItem(R.id.action_un_archive).setVisible(true);
        }
    }

    /**
     * Method used to show select or deselect option at the action mode
     *
     * @param menu
     */
    public void showSelectAction(Menu menu, int count, int totalMessage) {
        if (totalMessage > 1) {
            if (count == totalMessage) {
                menu.findItem(R.id.action_select_all).setVisible(false);
                menu.findItem(R.id.action_deselect_all).setVisible(true);
            } else if (count > 0) {
                menu.findItem(R.id.action_select_all).setVisible(true);
                menu.findItem(R.id.action_deselect_all).setVisible(false);
            }
        } else {
            menu.findItem(R.id.action_select_all).setVisible(false);
            menu.findItem(R.id.action_deselect_all).setVisible(false);
        }
    }

    private void enableMenuItemVisibility(ConversationEntity entity, boolean isSelected) {

        if (entity.getNoOfUnreadMessages() >= 1
            || entity.isUnread()) {
            if (isSelected) {
                mUnreadMessageCount++;
            } else {
                if (mUnreadMessageCount > 0) {
                    mUnreadMessageCount--;
                }
            }
        }
    }

    protected int canAddToContact(String mask) {

        int contactId = -1;
        if (mask != null) {
            contactId = MessageUtils.getContactIDFromNumber(mask, getActivity());
        }
        return contactId;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAddToContactDialog = null;
        mMessageSwitchDialog = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            /*case R.id.action_settings:
                //TODO: load setting screen
                return true;*/

           /* case R.id.action_archive:
                //TODO: load archive screen
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is to show individual message detail.
     *
     * @param mask
     */

    public void showAddToContactDialog(String mask) {

        DialogAddToContactBinding contactBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_add_to_contact,
            null, false);
        mAddToContactDialog = DialogUtils.getCustomDialog(getActivity(), contactBinding.getRoot());
        contactBinding.setBaseFragment(this);
        contactBinding.setMask(mask);
        mAddToContactDialog.setCancelable(true);
        mAddToContactDialog.setCanceledOnTouchOutside(true);
        mAddToContactDialog.show();
    }

    public void addToContactClicked(View view, String mask) {

        switch (view.getId()) {
            case R.id.add_to_existing_contact:
                Utils.addNewContact(getActivity(), mask, this, true);
                break;
            case R.id.add_to_new_contact:
                Utils.addNewContact(getActivity(), mask, this, false);
                break;
        }
        if (mAddToContactDialog != null) {
            mAddToContactDialog.dismiss();
        }
    }

    @SuppressLint("RestrictedApi")
    protected void setMarkAsReadVisible() {

        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }

    }


    private String setMoveToMessage(Context ctx, int threadCount, String tabType) {
        return ctx.getResources()
            .getQuantityString(R.plurals.move_to_tab_msgs, threadCount, tabType);
    }

    protected void actionModeSetup(int count, ActionMode actionMode, List<ConversationEntity> messageList) {

        if (actionMode == null) return;
        Menu menu = actionMode.getMenu();

        if (mSelectAllClicked) {
            if (menu.findItem(R.id.action_add_contact) != null) {
                menu.findItem(R.id.action_add_contact).setVisible(false);
            }
            if (mConversationType != null) {
                int unReadCount = MessagingService.getInstance(SMSService.class).getUnreadConversationCount(mConversationType);
                if (menu.findItem(R.id.action_mark_as_read) != null) {
                    if (unReadCount > 0) {
                        menu.findItem(R.id.action_mark_as_read).setVisible(true);
                    } else {
                        menu.findItem(R.id.action_mark_as_read).setVisible(false);
                    }
                }
            }
        }
        showSelectAction(menu, count, messageList.size());
        if (count > 0) {
            actionMode.setTitle(String.valueOf(count));
        }
        actionMode.invalidate();
    }

    /**
     * Method used to select all conversation
     */
    protected void selectAllMessages(MessageListAdapter adapter, List<ConversationEntity> messageList,
                                     ActionMode actionMode, ConversationType conversationType) {
        mConversationType = conversationType;
        mSelectAllClicked = true;
        adapter.addConversationToList(messageList);
        actionModeSetup(adapter.getSelectedItemCount(), actionMode, messageList);
    }

    /**
     * Method used to deselect all conversation
     */
    protected void deselectAllMessages() {
        mSelectAllClicked = false;
        notifySelectionClear();
    }

    @Override
    public void onButtonClicked(DialogInterface dialog, int which) {

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                deleteConversationFromDefaultSMSDb(mDeletableconversation);
                MessagingService.getInstance(SMSService.class).deleteConversation(mDeletableconversation);
                clearSelectionAndActionMode();
                closeNotification();
                dialog.dismiss();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
            case DialogInterface.BUTTON_NEUTRAL:
                closeNotification();
                dialog.dismiss();
                break;
        }

    }

    private void closeNotification() {

        if (getActivity() != null) {
            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancelAll();
            }
        }
    }

    protected void showDefaultSmsDialog() {
        if (getActivity() != null) {
            final String packageName = getActivity().getPackageName();
            Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, packageName);
            startActivityForResult(intent, DEFAULT_SMS_REQUEST_CODE);
        }
    }


   /* *//**
     * This method is to show config detail.
     *//*

    protected void showConfigChangeDialog() {


        DialogConfigSelectionBinding configSelectionBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.dialog_config_selection,
            null, false);
        mConfigDetailDialog = DialogUtils.getCustomDialog(getActivity(), configSelectionBinding.getRoot());
        configSelectionBinding.setBaseFragment(this);
        mConfigDetailDialog.setCancelable(false);
        mConfigDetailDialog.setCanceledOnTouchOutside(false);
        mConfigDetailDialog.show();
    }


    protected void dismissDialog() {

        if (mConfigDetailDialog != null) {
            mConfigDetailDialog.dismiss();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        PreferenceManager manager = PreferenceManager.getInstance(group.getContext());
        if (checkedId == R.id.production_radio_btn) {
            manager.setConfigEnvironment(Config.BASE_URL);
        } else if (checkedId == R.id.dev_radio_btn) {
            manager.setConfigEnvironment(Config.BASE_URL);
        } else if (checkedId == R.id.staging_radio_btn) {
            manager.setConfigEnvironment(Config.BASE_URL);
        }
        dismissDialog();
    }*/

    protected ConversationType getConversationTypeFromBundle() {
        return (ConversationType) (getArguments() != null ? getArguments()
            .getSerializable(AppConstants.BundleKey.CONVERSATION_TYPE) : ConversationType.PERSONAL);
    }
}
