package io.gupshup.smsapp.ui.compose.viewmodel;

import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 5/4/2018.
 */
public class ContactBubleViewModel {

    @Inject
    RxBus rxBus;

    public ContactBubleViewModel() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void onDeleteClick(View view, Contact contact) {
        if(contact != null){
            LogUtility.d("Delete", "--- Contact Deleted --------");
            RxEvent event = new RxEvent(AppConstants.DELETE_CONTACT, contact);
            rxBus.send(event);
        }
    }
}
