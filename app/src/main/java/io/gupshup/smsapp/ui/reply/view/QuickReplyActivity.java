package io.gupshup.smsapp.ui.reply.view;

import android.app.Activity;
import android.app.NotificationManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.ActivityQuickReplyBinding;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.reply.viewmodel.MessageReplyViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PermissionResultCallback;
import io.gupshup.smsapp.utils.PermissionUtils;
import io.gupshup.soip.sdk.message.GupshupTextMessage;

import static io.gupshup.smsapp.utils.AppConstants.PermissionRequest.SMS_PERMISSION_REQUEST;
import static io.gupshup.smsapp.utils.AppConstants.PermissionRequestCode.SMS_PERMISSION_REQUEST_CODE;

/**
 * Created by Ram Prakash Bhat on 11/7/18.
 */
public class QuickReplyActivity extends BaseActivity implements BaseHandlers,
    ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback {

    private static final String TAG = QuickReplyActivity.class.getSimpleName();
    private ActivityQuickReplyBinding mBinding;
    private MessageReplyViewModel mViewModel;
    private ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;
    private String mMask;
    private ConversationType mConversationType;
    private List<SimInformation> mSimList;

    private int mPrevSelectedSimSlot = 0;
    private boolean mIsSendClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {//, PersistableBundle persistentState
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quick_reply);
        mViewModel = ViewModelProviders.of(this).get(MessageReplyViewModel.class);
        mBinding.setHandler(this);
        mBinding.setMessageRepViewModel(mViewModel);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
        onNewIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDataToViews();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtility.d(TAG,"QUICK REPLY ACTIVITY INETNT DATA "+AppUtils.intentToString(intent));
        permissions.addAll(Arrays.asList(SMS_PERMISSION_REQUEST));
        permissionUtils = new PermissionUtils(this);
        permissionUtils.checkPermission(permissions, getString(R.string.permission_explanation_message), SMS_PERMISSION_REQUEST_CODE);
        initReplayView();

    }

    private void initReplayView() {


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }


        if (extras.containsKey(AppConstants.BundleKey.MOBILE_NUMBER)) {
            String mobileNumber;
            mobileNumber = TextUtils.isEmpty(extras.getString(AppConstants.BundleKey.MOBILE_NUMBER))
                ? AppConstants.EMPTY_TEXT : extras.getString(AppConstants.BundleKey.MOBILE_NUMBER);
            mMask = Utils.getMaskFromPhoneNumber(mobileNumber);
            mConversationType = (ConversationType) extras.getSerializable(AppConstants.BundleKey.CONVERSATION_TYPE);
        }
        if (extras.containsKey(AppConstants.BundleKey.CONTACT_NAME)) {
            String contactName = extras.getString(AppConstants.BundleKey.CONTACT_NAME);
            mBinding.contactNameOrNumber.setText(getString(R.string.reply_to, contactName));
        }
        if (extras.containsKey(AppConstants.BundleKey.LAST_MESSAGE_ID)) {
            MessageEntity lastMessageData = mViewModel.getLastMessageData(extras.getString(AppConstants.BundleKey.LAST_MESSAGE_ID));
            if (lastMessageData != null) {
                mPrevSelectedSimSlot = lastMessageData.getIncomingSIMID();
            }
        }
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setTitle("");
        setupToolBar(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void PermissionGranted(int requestCode) {
        //TODO:
    }

    @Override
    public void PartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions) {
        finish();
    }

    @Override
    public void PermissionDenied(int requestCode) {
        finish();
    }

    @Override
    public void NeverAskAgain(int requestCode) {

        String message = getString(R.string.sms_permission_message);
        String dialogTitle = getString(R.string.permission_dialog_title);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(dialogTitle);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.grant_btn),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    DialogUtils.goToAppSettings(QuickReplyActivity.this);
                }
            });
        builder.setNegativeButton(getString(R.string.cancel_btn),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    dialog.cancel();
                }
            });
        builder.show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {

        boolean simLyTVisibility = mViewModel.getSimLayoutVisibility();
        if (simLyTVisibility) {
            mViewModel.handleSimLayoutVisibility();
            return;
        } else {
            //save or update message in draft
            String conversation = mBinding.replyEditBar.getText().toString().trim();
            if (!TextUtils.isEmpty(conversation)) {
                //Insert or Update draft message
                mViewModel.insertOrUpdateDraft(mMask, conversation, DateUtils.currentTimestamp());
            }
        }
        super.onBackPressed();
    }

    @Override
    public void onViewClick(View view) {

        switch (view.getId()) {
            case R.id.open_conversation:
                launchMainActivity();
                break;
            case R.id.sim_selection_btn:
            case R.id.sim_number_img:
            case R.id.sim_bg:
                DialogUtils.dismissKeyboard(this, mBinding.replyEditBar);
                showSimSelectionLayout();
                break;


            case R.id.first_sim_lyt:
                mViewModel.mShowSimListLyt.set(false);
                mSimList = AppUtils.getSimInfoList();
                if (mSimList != null && mSimList.size() > 0) {
                    mPrevSelectedSimSlot = mSimList.get(0).getSimSlotIndex();
                    mViewModel.mIsFirstSim.set(true);
                    mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_one));
                }
                break;

            case R.id.second_sim_lyt:
                mViewModel.mShowSimListLyt.set(false);
                if (mSimList != null && mSimList.size() > 1) {
                    mPrevSelectedSimSlot = mSimList.get(1).getSimSlotIndex();
                    mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_two));
                    mViewModel.mIsFirstSim.set(false);
                }
                break;

            case R.id.send_view:
            case R.id.send_view_container:
                if (!AppUtils.isDefaultSmsPackage(this, getPackageName())) {
                    mIsSendClicked = true;
                    showDefaultSmsDialog();
                    return;
                }
                callSendMessage();
                break;

            case R.id.sim_overlay:
                if (mViewModel.getSimLayoutVisibility()) {
                    mViewModel.mShowSimListLyt.set(false);

                }
                break;
        }
    }

    private void showSimSelectionLayout() {
        mViewModel.showSimListLayout(mSimList, this);
    }

    private void launchMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);//MainActivity
        intent.putExtra(AppConstants.BundleKey.MOBILE_NUMBER, mMask);
        intent.putExtra(AppConstants.BundleKey.CONVERSATION_TYPE, mConversationType == null
            ? ConversationType.PERSONAL : mConversationType);
        startActivity(intent);
        this.finish();
    }


    private void setDataToViews() {
        if (AppUtils.checkSimCardExistence(this)) {
            //if sim card exist
            mViewModel.mSendViewClickable.set(true);
            mSimList = AppUtils.getSimInfoList();
            mViewModel.setColor(mSimList, this);
            setPreviouslySelectedSimData();
        } else {
            //if sim card doesn't exist then block the click
            mViewModel.mSendViewClickable.set(false);
        }
    }

    private void setPreviouslySelectedSimData() {
        if (mPrevSelectedSimSlot == 0) {
            mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_one));
            mViewModel.mIsFirstSim.set(true);
        } else {
            mViewModel.mSimSlotNo.set(getString(R.string.sim_slot_two));
            mViewModel.mIsFirstSim.set(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEFAULT_SMS_REQUEST_CODE) {
            switch (resultCode) {

                case Activity.RESULT_OK:
                    // Write your logic here
                    if (mIsSendClicked) {
                        callSendMessage();
                        mIsSendClicked = false;
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    // Write your logic here
                    onBackPressed();
                    break;
            }
        }
    }

    private void callSendMessage() {

        mSimList = AppUtils.getSimInfoList();
        String messageTobeSend = mBinding.replyEditBar.getText().toString().trim();
        //when there is draft message and send button clicked need to clear draft message

        if (!TextUtils.isEmpty(messageTobeSend)) {
            Utils.trySendingDataMessage(this, new GupshupTextMessage(messageTobeSend), mMask,
                mPrevSelectedSimSlot, -1, false);
            mBinding.replyEditBar.setText("");
            this.finish();

        }
    }
}
