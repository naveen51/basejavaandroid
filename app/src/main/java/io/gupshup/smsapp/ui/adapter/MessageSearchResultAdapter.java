package io.gupshup.smsapp.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.databinding.AdapterSearchResultItemBinding;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.search.viewmodel.MessageSearchViewModel;
import io.gupshup.smsapp.utils.MessageUtils;

/**
 * Created by Ram Prakash Bhat on 1/6/18.
 */

public class MessageSearchResultAdapter extends RecyclerViewPageListAdapter<GlobalSearchEntity, MessageSearchResultAdapter.SearchResultViewHolder> {


    private final BaseFragment mBaseFragment;

    public MessageSearchResultAdapter(BaseFragment fragment) {
        super(MessageUtils.GLOBAL_SEARCH_ENTITY_DIFF_CALLBACK);
        mBaseFragment = fragment;
        mOnItemClickListener = fragment;
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AdapterSearchResultItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.adapter_search_result_item, parent, false);
        return new SearchResultViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {

        GlobalSearchEntity searchedItem = getItem(position);
        MessageSearchViewModel searchViewModel = new MessageSearchViewModel(MainApplication.getInstance());
        ((AdapterSearchResultItemBinding) holder.getBinding()).setMessageSearchViewModel(searchViewModel);
        ((AdapterSearchResultItemBinding) holder.getBinding()).setSearchItem(searchedItem);
        ((AdapterSearchResultItemBinding) holder.getBinding()).setBaseFragment(mBaseFragment);
        String displayName = "", displayThumb = "";

        if (searchedItem != null) {
            if (searchedItem.getDisplay() != null) {
                displayName = searchedItem.getDisplay();
            } else {
                displayName = searchedItem.getMask();
            }
            displayThumb = searchedItem.getImage();
        }
        searchViewModel.displayName.set(displayName);
        searchViewModel.displayPic.set(displayThumb);
        super.onBindViewHolder(holder, position);
    }

    @Override
    public void onDestroy() {

    }

    public class SearchResultViewHolder extends RecyclerViewPageListAdapter.RecyclerViewHolder {

        public SearchResultViewHolder(AdapterSearchResultItemBinding viewBinding) {
            super(viewBinding);


            viewBinding.itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setTag(getItem(getAdapterPosition()));
                    mOnItemClickListener.onItemClick(view, getAdapterPosition());
                }
            });
            viewBinding.iconUnSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setTag(getItem(getAdapterPosition()));
                    mOnItemClickListener.onItemClick(view, getAdapterPosition());
                }
            });
        }
    }
}
