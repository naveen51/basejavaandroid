package io.gupshup.smsapp.ui.earlierSMS;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import io.gupshup.smsapp.utils.LogUtility;

public class MMSDeepLinkUtility {

    private static String EXTRA_MMS_ID = "mms_id";
    private static String EXTRA_MMS_TYPE = "mms_type";
    private Context context; Activity activity;


    public MMSDeepLinkUtility(Activity act, String id, String dataType){
        this.context = act.getBaseContext();
        this.EXTRA_MMS_ID = id;
        this.EXTRA_MMS_TYPE = dataType;
        this.activity=act;

        //if() - check for type of message from metdata...
        if(dataType.equals("text")){
            getAndSaveMmsText(EXTRA_MMS_ID);
        }else if(dataType.equals("image")) {
            getAndSaveMmsImage(EXTRA_MMS_ID);
        }else if (dataType.equals("video")){
            getAndSaveMmsVideo(EXTRA_MMS_ID);
        }else if(dataType.equals("audio")){
            getAndSaveMmsAudio(EXTRA_MMS_ID);
        }
        else{
            LogUtility.d("GDC", "Input type is not recognized..");
        }
    }


    public void getAndSaveMmsImage(String id) {
        LogUtility.d("GDC", "The id is :"+id);
        Uri uri = Uri.parse("content://mms/part/" + id);

        InputStream in = null;
        Bitmap bitmap = null; File file=null;
        StringBuilder total = new StringBuilder();
        FileOutputStream out = null;
        try {
            in =context.getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(in);

            //file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "mmsData");
            file = new File(context.getApplicationContext().getCacheDir(), "mmsData");
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
             // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try{
            //opening it for current view.
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            //File fileWritten = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "mmsData");
            File fileWritten = new File(context.getApplicationContext().getCacheDir(), "mmsData");
            LogUtility.d("GDC", "File size is : "+fileWritten.length());
            Uri accessUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", fileWritten);
            intent.setDataAndType(accessUri,"image/*");
            activity.startActivity(intent);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }


    private void getAndSaveMmsVideo(String id) {

        LogUtility.d("GDC", "The id is :"+id);
        Uri videoUri = Uri.parse("content://mms/part/" + id);
        Bitmap finalBitmap = null;
        File file=null;
        FileOutputStream outputStream;
        try {
            finalBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), videoUri);
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            file = new File(context.getCacheDir(), "MyCache"+n);
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION|Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.setDataAndType(Uri.fromFile(file), "video/*");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getAndSaveMmsText(String id) {

        LogUtility.d("GDC", "The id is :"+id);
        Uri textUri = Uri.parse("content://mms/part/" + id);

        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(textUri);
            FileOutputStream out = null;
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            File file = new File(context.getCacheDir(), "MyCache"+n);
            FileOutputStream output = new FileOutputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder outString = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                outString.append(line);
            }
            System.out.println(out.toString());   //Prints the string content read from input stream
            out.write(out.toString().getBytes());
            out.close();
            reader.close();

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "text/*");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAndSaveMmsAudio(String id) {

        LogUtility.d("test", "The id is :"+id);
        Uri textUri = Uri.parse("content://mms/part/" + id);

        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(textUri);
            FileOutputStream out = null;
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            File file = new File(context.getCacheDir(), "MyCache"+n);
            FileOutputStream output = new FileOutputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder outString = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                outString.append(line);
            }
            System.out.println(out.toString());   //Prints the string content read from input stream
            out.write(out.toString().getBytes());
            out.close();
            reader.close();

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "audio/*");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
