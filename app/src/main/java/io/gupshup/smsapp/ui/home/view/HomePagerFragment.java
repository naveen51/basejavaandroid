package io.gupshup.smsapp.ui.home.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.TabItem;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.databinding.FragmentHomePagerBinding;
import io.gupshup.smsapp.databinding.LayoutCustomTabBinding;
import io.gupshup.smsapp.databinding.RegDialogCommonBinding;
import io.gupshup.smsapp.databinding.TimerDialogCommonBinding;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ConfigUpdateEvent;
import io.gupshup.smsapp.events.ManualRegistrationEvent;
import io.gupshup.smsapp.events.OtpContent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.HomePagerAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.compose.view.ComposeSmsActivity;
import io.gupshup.smsapp.ui.earlierSMS.FetchEarlierSMS;
import io.gupshup.smsapp.ui.home.viewmodel.CustomTabViewModel;
import io.gupshup.smsapp.ui.home.viewmodel.HomeFragmentViewModel;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationDialogViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Ram Prakash Bhat on 6/4/18.
 */

public class HomePagerFragment extends BaseFragment<FragmentHomePagerBinding, HomeFragmentViewModel>
    implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener, RxBusCallback {

    private static final String TAG = HomePagerFragment.class.getSimpleName();

    public ProgressDialog progressDlg;
    public static ProgressDialog progDialog;
    public static boolean isImportMessagesLoadingComplete = false;
    @Inject
    public RxBus rxBus;
    @Inject
    public RxBusHelper rxBusHelper;
    private LayoutCustomTabBinding mCustomTabBinding;
    public static boolean firstTimeRun = true;
    private RegistrationDialogViewModel mRegistrationDialogViewModel;
    private RegDialogCommonBinding mRegistrationCommonBinding;
    private AlertDialog mRegistrationDialog, mTimerDialog;
    private int mManualRegistrationApiCallCount = 0;
    public static boolean isIgnoreSIM1Registration = false;
    public static boolean isIgnoreSIM2Registration = false;
    private ConversationType mConversationTypeFromNotification = ConversationType.PERSONAL;
    public CountDownTimer mCountDownTimer;
    private HomePagerAdapter mHomePagerAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home_pager;
    }

    @Override
    public HomeFragmentViewModel getViewModel() {
        return ViewModelProviders.of(this).get(HomeFragmentViewModel.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.homeViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRegistrationDialogViewModel = ViewModelProviders.of(this).get(RegistrationDialogViewModel.class);
        mBinding.tabLayout.addOnTabSelectedListener(this);
        mBinding.fragmentPager.addOnPageChangeListener(this);
        mHomePagerAdapter = new HomePagerAdapter(getChildFragmentManager(),
            Arrays.asList(getResources().getStringArray(R.array.tab_title)));
        mBinding.fragmentPager.setAdapter(mHomePagerAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.fragmentPager);
        mBinding.setHandlers(this);
        mBinding.fragmentPager.setOffscreenPageLimit(3);
        if (mConversationTypeFromNotification != null) {
            mBinding.fragmentPager.setCurrentItem(mConversationTypeFromNotification.ordinal());
        }
        setUpCustomTabs();
        if (!PreferenceManager.getInstance(this.getActivity()).isManualRegistrationTried()) {
            initManualRegistration();
        }

        if (!PreferenceManager.getInstance(view.getContext()).isHomeOverlayShow()) {
            RxEvent event = new RxEvent(AppConstants.RxEventName.INSTRUCTION_OVERLAY, null);
            rxBus.send(event);
        }

    }

    /**
     * method used to get the bandle data
     */
    private void getBundleData() {
        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.containsKey(AppConstants.BundleKey.NOTIFICATION_CONVERSATION_TYPE)) {
            mConversationTypeFromNotification = (ConversationType) bundle
                .get(AppConstants.BundleKey.NOTIFICATION_CONVERSATION_TYPE);
        }
    }

    private void initManualRegistration() {

        RegistrationInfo simOneData = AppUtils.getSimOneData(getActivity());
        RegistrationInfo simTwoData = AppUtils.getSimTwoData(getActivity());
        if (simOneData != null && simTwoData != null) {
            if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneData.simOneState
                && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoData.simTwoState) {
                if (mRegistrationDialog != null) {
                    mRegistrationDialog.dismiss();
                }
                return;
            }

        }
        List<SimInformation> simList = AppUtils.getSimInfoList();
        if (simList.size() == 0) return;

        if (simList.size() > 1) {
            simOneRelatedUiChanges(simOneData);
            simTwoRelatedUiChanges(simTwoData);
            if (simOneData != null
                && !simOneData.isSkipClicked
                && RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS != simOneData.simOneState && RegistrationSimOneState.SIM_ONE_REGISTRATION_IN_PROGRESS != simOneData.simOneState) {  //&& RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS != simOneData.simOneState
                showRegistrationDialog();
            }
            if (simTwoData != null
                && !simTwoData.isSkipClicked
                && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS != simTwoData.simTwoState && RegistrationSimTwoState.SIM_TWO_REGISTRATION_IN_PROGRESS != simTwoData.simTwoState) { // && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS != simTwoData.simTwoState
                showRegistrationDialog();
            }

        } else if (simList.size() == 1) {
            simOneRelatedUiChanges(simOneData);
            if (simOneData != null
                && !simOneData.isSkipClicked
                && RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS != simOneData.simOneState && RegistrationSimOneState.SIM_ONE_REGISTRATION_IN_PROGRESS != simOneData.simOneState) {  //&& RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS != simOneData.simOneState
                showRegistrationDialog();
            }

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        getBundleData();
        if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.READ_SMS)
            != PackageManager.PERMISSION_GRANTED) {
            getPermissionToReadSMS();
        } else {
            LogUtility.d("Gaurav", "permission granted....");
            long lastSyncTime = Long.parseLong(PreferenceManager.getInstance(getContext()).getLastSyncTime());
            LogUtility.d("sync", "in home pager sunc val :" + lastSyncTime);
            FetchEarlierSMS fetchSms = new FetchEarlierSMS(this.getActivity().getBaseContext());
            progressDlg = new ProgressDialog(this.getActivity());
            progressDlg.setMessage("Loading messages..");
            progDialog = progressDlg;
            //progressDlg.show();
            fetchSms.importEarlierSMSForInitialLoad(300);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        FetchEarlierSMS fetchSms = new FetchEarlierSMS(getContext());
                        fetchSms.importEarlierSmsAndMms(firstTimeRun);
                    } catch (Exception ex) {
                        //TODO: Handle exc if context is not available...
                        LogUtility.d("Cursor count", "exc is : " + ex.getMessage());
                    }
                }
            }, 3000);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDlg.isShowing()) {
                    progressDlg.dismiss();
                }
            }
        }, 10000);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerEvents();
        ToolbarInfo info = new ToolbarInfo();
        info.isVissible = true;
        info.toolbarTitle = getString(R.string.message);
        RxEvent event = new RxEvent(AppConstants.RxEventName.TOOLBAR_INFO, info);
        rxBus.send(event);

    }


    @SuppressLint("StaticFieldLeak")
    private void setUpCustomTabs() {

        for (int tabPos = 0; tabPos < mBinding.tabLayout.getTabCount(); tabPos++) {
            mCustomTabBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_custom_tab,
                null, false);
            CustomTabViewModel model = ViewModelProviders.of(this).get(CustomTabViewModel.class);//new CustomTabViewModel(MainApplication.getInstance());
            mCustomTabBinding.setTabItem(model.getTabItem(getActivity(), tabPos));
            mCustomTabBinding.setCustomViewModel(model);
            mBinding.tabLayout.getTabAt(tabPos).setCustomView(mCustomTabBinding.getRoot());
            if (tabPos == 0) {
                onTabSelected(mBinding.tabLayout.getTabAt(tabPos));
            }
            //To divide tab width to equal width
            ViewGroup slidingTabStrip = (ViewGroup) mBinding.tabLayout.getChildAt(0);
            View tab = slidingTabStrip.getChildAt(tabPos);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);
        }
        getCurrentViewModel().getPersonalTabUnreadCount(getActivity(),
            ConversationType.PERSONAL).observe(HomePagerFragment.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer count) {
                updateSpecificTabCount(count, 0);
            }
        });
        //TODO: ConversationType OTP and TRANSACTIONAL are consider as same.
        ConversationType type[] = {ConversationType.TRANSACTIONAL, ConversationType.OTP};
        getCurrentViewModel().getTranslTabUnreadCount(getActivity(), type
        ).observe(HomePagerFragment.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer count) {
                updateSpecificTabCount(count, 1);

            }
        });
        getCurrentViewModel().getPromoTabUnreadCount(getActivity(),
            ConversationType.PROMOTIONAL).observe(HomePagerFragment.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer count) {
                updateSpecificTabCount(count, 2);
            }
        });
        getCurrentViewModel().getBlockedTabUnreadCount(getActivity(),
            ConversationType.BLOCKED).observe(HomePagerFragment.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer count) {
                updateSpecificTabCount(count, 3);
            }
        });


    }

    private void updateSpecificTabCount(Integer count, int tabPosition) {

        CustomTabViewModel model = ViewModelProviders.of(HomePagerFragment.this).get(CustomTabViewModel.class);
        TabItem tabItem = model.getTabItem(getActivity(), tabPosition);
        tabItem.tabMsgCount = count == null ? 0 : count;
        TabLayout.Tab tab = mBinding.tabLayout.getTabAt(tabPosition);
        if (tab != null && tab.getCustomView() != null) {
            TextView txt = tab.getCustomView().findViewById(R.id.tab_text);
            if (tabItem.tabMsgCount > 0) {
                txt.setText(getString(R.string.message_count, tabItem.tabTitle, tabItem.tabMsgCount));
            } else {
                txt.setText(tabItem.tabTitle);
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (tab.getCustomView() != null) {
            TextView txt = tab.getCustomView().findViewById(R.id.tab_text);
            txt.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            ImageView imgView = tab.getCustomView().findViewById(R.id.tab_image);
            imgView.setColorFilter(ContextCompat.getColor(getActivity(),
                R.color.white), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        if (tab.getCustomView() != null) {
            TextView txt = tab.getCustomView().findViewById(R.id.tab_text);
            txt.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_opacity_70));
            ImageView imgView = tab.getCustomView().findViewById(R.id.tab_image);
            imgView.setColorFilter(ContextCompat.getColor(getActivity(),
                R.color.white_opacity_70), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    public void onDetach() {
        super.onDetach();
        unregisterEvents();
        cancelTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterEvents();
        cancelTimer();
        dismissProgressDialog();
    }

    private void dismissProgressDialog() {
        if (progressDlg != null && progressDlg.isShowing()) {
            progressDlg.dismiss();
        }
    }

    @Override
    public void onViewClick(View view) {
        super.onViewClick(view);
        switch (view.getId()) {
            case R.id.fab:
                //MainActivity.hasCalledAnotherActivity = true;
                getCurrentVisibleFragment().mIsScrollNeeded = false;
                Intent intent = new Intent(view.getContext(), ComposeSmsActivity.class);
                view.getContext().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void registerEvents() {
        rxBusHelper.registerEvents(rxBus, TAG, this);
    }

    private void unregisterEvents() {

        rxBusHelper.unSubScribe();
    }

    @Override
    public void onEventTrigger(Object event) {

        if (event instanceof RxEvent) {
            handleRxEvents((RxEvent) event);
        }
    }

    private void handleRxEvents(RxEvent event) {
        switch (event.getEventName()) {

            case AppConstants.RxEventName.ALREADY_REGISTERED:
                String phoneNumber = (String) event.getEventData();
                if (TextUtils.isEmpty(phoneNumber)) {
                    phoneNumber = AppConstants.EMPTY_TEXT;
                }
                getCurrentViewModel().cancelTimer(0);
                getCurrentViewModel().cancelTimer(1);
                mRegistrationDialogViewModel.clearEditText();
                showHideProgressBar(false);
                break;
            case AppConstants.RxEventName.SHOW_HIDE_VIEW:
                mBinding.fab.setVisibility((Integer) event.getEventData());
                if (((Integer) event.getEventData()) == View.GONE) {
                    //Make it Gone
                    if (mBinding.tabLayout.getHeight() == 0) return;
                    slideUp(mBinding.tabLayout);
                } else {
                    //Make it Visible
                    slideDown(mBinding.tabLayout);
                }
                break;
            case AppConstants.RxEventName.SIM_ONE_REGISTRATION_FAILURE:
                LogUtility.d("reg", "SIM 1 reg failure landed in home act...");
                ConfigUpdateEvent configItemOne = (ConfigUpdateEvent) event.getEventData();
                RegistrationInfo simOneData = null;
                if (configItemOne != null) {
                    simOneData = configItemOne.getSimOneData();
                    simTwoRelatedUiChanges(configItemOne.getSimTwoData());
                }
                simOneRelatedUiChanges(simOneData);
                LogUtility.d("auto", "Showing manual registration dialog for sim 1");
                AppUtils.logToFile(getContext(), "Showing manual registration dialog for sim 1");
                showRegistrationDialog();
                showHideProgressBar(false);
                break;

            case AppConstants.RxEventName.SIM_TWO_REGISTRATION_FAILURE:
                LogUtility.d("auto", "SIM 2 Auto reg failed handling in home pager frag...");
                ConfigUpdateEvent configItemTwo = (ConfigUpdateEvent) event.getEventData();
                RegistrationInfo simTwoData = null;
                if (configItemTwo != null) {
                    simTwoData = configItemTwo.getSimTwoData();
                    simOneRelatedUiChanges(configItemTwo.getSimOneData());
                }
                simTwoRelatedUiChanges(simTwoData);
                LogUtility.d("auto", "Showing manual registration dialog for sim 2");
                AppUtils.logToFile(getContext(), "Showing manual registration dialog for sim 2");
                showRegistrationDialog();
                showHideProgressBar(false);
                break;

            case AppConstants.MANUAL_REGISTRATION_SUBMIT_CLICK:
                //Manual Registration
                LogUtility.d("reg", "Manual reg submit click....");

                PreferenceManager.getInstance(this.getActivity()).setManualRegistrationTried(true);
                ManualRegistrationEvent manualRegEvent = (ManualRegistrationEvent) event.getEventData();
                String simOneNumber = manualRegEvent.getmSimOneManualNumber();
                String simTwoNumber = manualRegEvent.getmSimTwoManualNumber();
                String simOneCountryCode = mRegistrationCommonBinding.ccpOne.getSelectedCountryCode();
                String simTwoCountryCode = mRegistrationCommonBinding.ccpTwo.getSelectedCountryCode();
                mRegistrationDialogViewModel.mSimOneCountryCode.set(simOneCountryCode);
                mRegistrationDialogViewModel.mSimTwoCountryCode.set(simTwoCountryCode);
                mRegistrationCommonBinding.ccpOne.registerCarrierNumberEditText(mRegistrationCommonBinding.mobileNumberOne);
                mRegistrationCommonBinding.ccpTwo.registerCarrierNumberEditText(mRegistrationCommonBinding.mobileNumberTwo);
                if (!TextUtils.isEmpty(simOneNumber)) {
                    if (mRegistrationCommonBinding.ccpOne.isValidFullNumber()) {
                        showHideProgressBar(true);
                        LogUtility.d("reg", "trigerring man reg for sim 1 from dialog....");
                        AppUtils.logToFile(getContext(), "trigerring man reg for sim 1 from dialog....");
                        getCurrentViewModel().manualRegistration(getActivity(), simOneCountryCode, simOneNumber, Utils.getMaskFromPhoneNumber(simOneNumber), 0);
                    } else {
                        showHideProgressBar(false);
                        mRegistrationCommonBinding.mobileNumberOne.setError(getString(R.string.enter_valid_mobile_number));
                        return;
                    }
                }
                if (!TextUtils.isEmpty(simTwoNumber)) {
                    if (mRegistrationCommonBinding.ccpTwo.isValidFullNumber()) {
                        showHideProgressBar(true);
                        LogUtility.d("reg", "trigerring man reg for sim 2 from dialog....");
                        AppUtils.logToFile(getContext(), "trigerring man reg for sim 2 from dialog....");
                        getCurrentViewModel().manualRegistration(getActivity(), simTwoCountryCode, simTwoNumber, Utils.getMaskFromPhoneNumber(simTwoNumber), 1);
                    } else {
                        showHideProgressBar(false);
                        mRegistrationCommonBinding.mobileNumberTwo.setError(getString(R.string.enter_valid_mobile_number));
                        return;
                    }
                }
                if (TextUtils.isEmpty(simOneNumber)
                    || TextUtils.isEmpty(simTwoNumber)) {
                    mManualRegistrationApiCallCount = 1;
                }
                showCountDownTimerDialog();
                break;

            case AppConstants.RxEventName.OTP_MESSAGE:

                try {
                    OtpContent otpContent = (OtpContent) event.getEventData();
                    LogUtility.d("auto", "OTP mess recv..for slot" + otpContent.slot);
                    RegistrationInfo simOnePrefData = AppUtils.getSimOneData(getActivity());
                    RegistrationInfo simTwoPrefData = AppUtils.getSimTwoData(getActivity());
                    if (otpContent.slot == 0) {
                        simOnePrefData.simSlot = otpContent.slot;
                        LogUtility.d("regs", "verify otp called for sim one");
                        getCurrentViewModel().verifyManualRegistration(getActivity(), simOnePrefData, otpContent.otp, otpContent.slot, otpContent.sequenceNumber);
                    } else {
                        LogUtility.d("regs", "verify otp called for sim one");
                        simTwoPrefData.simSlot = otpContent.slot;
                        getCurrentViewModel().verifyManualRegistration(getActivity(), simTwoPrefData, otpContent.otp, otpContent.slot, otpContent.sequenceNumber);
                    }
                } catch (Exception e) {
                    //TODO: Handle specific exceptions....
                }
                break;
            case AppConstants.RxEventName.HIDE_DIALOG:
                if (mRegistrationDialog != null) {
                    mRegistrationDialog.dismiss();
                }
                break;
            case AppConstants.RxEventName.MANUAL_REGISTRATION_VERIFICATION_STATUS:

                RegistrationInfo simData = (RegistrationInfo) event.getEventData();
                LogUtility.d("auto", "manual reg varification status event called :");
                AppUtils.logToFile(getContext(), "manual reg varification status called :");
                RegistrationInfo simOneRequest = AppUtils.getSimOneData(getActivity());
                RegistrationInfo simTwoRequest = AppUtils.getSimTwoData(getActivity());
                PreferenceManager manager = PreferenceManager.getInstance(getActivity());
                List<SimInformation> simList = AppUtils.getSimInfoList();
                AppUtils.saveSimInfoToPreference(simList, manager);
                showHideProgressBar(false);
                //Auto verification success.
                if (simList.size() > 1) {
                    if (simOneRequest != null && simTwoRequest != null) {
                        if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState
                            && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoRequest.simTwoState) {
                            //Both success.
                            LogUtility.d("auto", "both sim manual reg success");
                            AppUtils.logToFile(getContext(), "both sim manual reg success");
                            getCurrentViewModel().cancelTimer(simOneRequest.simSlot);
                            getCurrentViewModel().cancelTimer(simTwoRequest.simSlot);
                            hideRegistrationDialog();
                        } else {
                            manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                            AppUtils.logToFile(getContext(), "manual reg failed...handling based on status for states : SIm 1 : " + simOneRequest.simOneState + " SIM 2 : " + simTwoRequest.simTwoState);
                        }
                    } else {
                        //Both sim manual registration failed
                        LogUtility.d("auto", "manual reg failed...handling based on status for states : SIm 1 : " + (simOneRequest != null ? simOneRequest.simOneState : null) + " SIM 2 : " + (simTwoRequest != null ? simTwoRequest.simTwoState : null));
                        AppUtils.logToFile(getContext(), "manual reg failed...handling based on status for states : SIm 1 : " + (simOneRequest != null ? simOneRequest.simOneState : null) + " SIM 2 : " + (simTwoRequest != null ? simTwoRequest.simTwoState : null));
                        manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                    }
                } else if (simList.size() == 1) {
                    LogUtility.d("auto", "Single sim manual reg...handling based on status");
                    if (simOneRequest != null
                        && RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneRequest.simOneState) {
                    }
                    manualRegistrationHandlingBasedOnStatus(simOneRequest, simTwoRequest, simData.simSlot);
                }
                break;
        }
    }

    private void showHideProgressBar(boolean show) {
        if (mRegistrationDialogViewModel != null) {
            mRegistrationDialogViewModel.showProgressBar.set(show);
        }
        if (show) {
            showCountdownTimer();
        } else {
            cancelTimer();
        }
        if (mRegistrationCommonBinding != null) {
            mRegistrationCommonBinding.ccpTwo.setCcpClickable(!show);
            mRegistrationCommonBinding.ccpOne.setCcpClickable(!show);
        }
    }

    private void hideRegistrationDialog() {

        if (mRegistrationDialog != null) {
            mRegistrationDialog.dismiss();
            mRegistrationDialog = null;
        }
    }

    private void showMrScreen() {
        LogUtility.d("auto", "showing mr screen....");
        AppUtils.logToFile(getContext(), "showing mr screen....");
        int simListSize = AppUtils.getSimInfoList().size();
        if (simListSize > 1) {
            if (mManualRegistrationApiCallCount == 2) {
                hideRegistrationDialog();
                LogUtility.d("auto", "MR2 skip screen called..");
                AppUtils.logToFile(getContext(), "MR2 skip screen called..");
                RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_MR_TWO_SKIP_SCREEN, null);
                rxBus.send(event);
            }
        } else {
            hideRegistrationDialog();
            LogUtility.d("auto", "MR2 skip screen called..");
            AppUtils.logToFile(getContext(), "MR2 skip screen called..");
            RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_MR_TWO_SKIP_SCREEN, null);
            rxBus.send(event);
        }
    }


    public static final int READ_SMS_PERMISSIONS_REQUEST = 1;

    public void getPermissionToReadSMS() {
        if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.READ_SMS)
            != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(
                Manifest.permission.READ_SMS)) {
                Toast.makeText(this.getActivity(), "Please allow permission!", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.READ_SMS},
                READ_SMS_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this.getActivity(), "Read SMS permission granted", Toast.LENGTH_SHORT).show();
                FetchEarlierSMS fetchSms = new FetchEarlierSMS(this.getActivity().getBaseContext());
                fetchSms.importEarlierSmsAndMms(false);
            } else {
                Toast.makeText(this.getActivity(), "Read SMS permission denied", Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    // slide the view from below itself to the current position
    public void slideDown(final View view) {
        //make it visible
        AppUtils.openTabLayout(view);
    }

    // slide the view from its current position to below itself
    public void slideUp(final View view) {
        AppUtils.collapseTabLayout(view);
    }

    //Manual registration related dialog
    private void showRegistrationDialog() {
        if (getActivity() == null) return;
        LogUtility.d("reg", "showing manual reg dialog...called from Home");
        RegistrationInfo simOneData = AppUtils.getSimOneData(getActivity());
        RegistrationInfo simTwoData = AppUtils.getSimTwoData(getActivity());

        if (simOneData != null && simTwoData != null) {
            if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS != simOneData.simOneState
                && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS != simTwoData.simTwoState) {
                updateHeaderTextValue(2);
                mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.auto_head_three));
                mRegistrationDialogViewModel.mTwoVerifiedText.set(false);
                mRegistrationDialogViewModel.handleSimTwoVisibility(true);
                mRegistrationDialogViewModel.handleSimOneVisibility(true);
            }
        }

        if (AppUtils.getSimInfoList().size() == 1) {
            SimInformation information = AppUtils.getSimInfoList().get(0);
            RegistrationInfo simData = AppUtils.getSimDataBasedOnSlot(getActivity(), String.valueOf(information.getSimSlotIndex()));
            if (information.getSimSlotIndex() == 0) {
                if (simData.simOneState == RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE) {
                    mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.auto_head_one));
                    updateHeaderTextValue(1);
                    simOneHandle();
                }
            } else {
                if (simData.simTwoState == RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE) {
                    mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.auto_head_one));
                    updateHeaderTextValue(1);
                    simOneHandle();
                }

            }
        }

        if (mRegistrationDialog != null && mRegistrationDialog.isShowing()) {
            return;
        }

        mRegistrationDialogViewModel.clearEditText();
        mRegistrationCommonBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.reg_dialog_common, null, false);
        mRegistrationCommonBinding.setRegDialogViewModel(mRegistrationDialogViewModel);
        mRegistrationDialog = DialogUtils.getCustomDialog(getActivity(), mRegistrationCommonBinding.getRoot());
        mRegistrationDialog.setCancelable(true);
        mRegistrationDialog.setCanceledOnTouchOutside(true);
        PreferenceManager.getInstance(getContext()).setManualRegistrationTried(true);
        mRegistrationDialog.show();
    }

    private void updateHeaderTextValue(int size) {
        if (PreferenceManager.getInstance(getActivity()).isMrFourScreenView()) {
            if (size == 1) {
                mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.manual_head_one));
            } else {
                mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.manual_head_two));
            }
        }
    }


    /**
     * Method used to handle the sim one visibility
     */
    public void simOneHandle() {
        //one sim failure handling
        mRegistrationDialogViewModel.handleSimTwoVisibility(false);
        mRegistrationDialogViewModel.handleSimOneVisibility(true);
        mRegistrationDialogViewModel.mTwoVerifiedText.set(false);
    }

    private void simOneRelatedUiChanges(RegistrationInfo simOneData) {

        mRegistrationDialogViewModel.mTwoVerifiedText.set(true);
        if (simOneData != null
            && RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneData.simOneState) {
            //Sim1 verified
            //one sim failure handling
            mRegistrationDialogViewModel.handleSimOneVisibility(false);
            //Set Text Verified
            mRegistrationDialogViewModel.mVerificationTextValue.set(getString(R.string.sim_one_title,
                getString(R.string.verified_phone_txt, simOneData.countryCode, simOneData.phoneNumber)));
            mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.auto_head_two));
        } else {
            //Sim1 not verified
            mRegistrationDialogViewModel.handleSimOneVisibility(true);
        }

    }


    private void simTwoRelatedUiChanges(RegistrationInfo simTwoData) {

        if (simTwoData != null
            && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoData.simTwoState) {
            //Sim2 verified
            mRegistrationDialogViewModel.handleSimTwoVisibility(false);
            //Set Text Verified
            mRegistrationDialogViewModel.mVerificationTextValue.set(getString(R.string.sim_two_title,
                getString(R.string.verified_phone_txt, simTwoData.countryCode, simTwoData.phoneNumber)));
            mRegistrationDialogViewModel.mHeaderTextValue.set(getString(R.string.auto_head_two));
        } else {
            //Sim2 not verified
            mRegistrationDialogViewModel.handleSimTwoVisibility(true);
        }

    }


    /**
     * Any of one or both sim Manual registration failed.
     *
     * @param simOneData
     * @param simTwoData
     */
    private void manualRegistrationHandlingBasedOnStatus(RegistrationInfo simOneData, RegistrationInfo simTwoData, int simSlotIndex) {

        // ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
        LogUtility.d("auto", "handling manual registration method..for sim slot : " + simSlotIndex);
        mManualRegistrationApiCallCount++;
        int size = AppUtils.getSimInfoList().size();

        if (simSlotIndex == 0) {
            if (simOneData != null && simOneData.simOneState != null) {

                switch (simOneData.simOneState) {
                    case SIM_ONE_REGISTRATION_SUCCESS:
                        getCurrentViewModel().cancelTimer(simOneData.simSlot);
                        LogUtility.d("auto", "manual sim1 registration success..");
                        AppUtils.logToFile(getContext(), "manual sim 1 registration success");
                        PreferenceManager.getInstance(getContext()).setSim1RegistrationDone(true);
                        ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(simOneData, null);
                        RxEvent simOneUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                        rxBus.send(simOneUpdateEvent);
                        if (size == 1) {
                            hideRegistrationDialog();
                        } else {
                            simOneRelatedUiChanges(simOneData);
                            simTwoRelatedUiChanges(simTwoData);
                        }
                        break;
                    case SIM_ONE_REGISTRATION_FAILURE:
                        //Sim one auto failed
                        LogUtility.d("auto", "manual sim1 registration failed..");
                        AppUtils.logToFile(getContext(), "manual sim 1 registration failed");
                        getCurrentViewModel().cancelTimer(0);
                        getCurrentViewModel().cancelTimer(1);
                        simOneData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                        AppUtils.setSimRequestItemData(getActivity(), simOneData);
                        //Fire event for manual registration.
                        LogUtility.d("auto", "calling MR screen from sim 1 manual registration failure");
                        showMrScreen();
                        break;
                }
            }
        } else if (simSlotIndex == 1) {
            if (simTwoData != null && simTwoData.simTwoState != null) {
                switch (simTwoData.simTwoState) {
                    case SIM_TWO_REGISTRATION_SUCCESS:
                        LogUtility.d("auto", "manual sim 2 registration success..");
                        AppUtils.logToFile(getContext(), "manual sim 2 registration success");
                        getCurrentViewModel().cancelTimer(simTwoData.simSlot);
                        PreferenceManager.getInstance(getContext()).setSim2RegistrationDone(true);
                        ConfigUpdateEvent configUpdateEvent = new ConfigUpdateEvent(null, simTwoData);
                        RxEvent simTwoUpdateEvent = new RxEvent(AppConstants.RxEventName.UPDATE_CONFIG_DATA, configUpdateEvent);
                        rxBus.send(simTwoUpdateEvent);
                        if (size == 1) {
                            hideRegistrationDialog();
                        } else {
                            simOneRelatedUiChanges(simOneData);
                            simTwoRelatedUiChanges(simTwoData);
                        }


                        break;
                    case SIM_TWO_REGISTRATION_FAILURE:
                        //Sim two auto failed
                        getCurrentViewModel().cancelTimer(0);
                        getCurrentViewModel().cancelTimer(1);
                        LogUtility.d("auto ", "manual sim 2 registration failure ");
                        AppUtils.logToFile(getContext(), "manual sim 2 registration failure ");
                        simTwoData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                        AppUtils.setSimRequestItemData(getActivity(), simTwoData);
                        LogUtility.d("auto", "calling MR screen from sim 2 manual registration failure");
                        AppUtils.logToFile(getContext(), "calling MR screen from sim 2 manual registration failure");
                        //Fire event for manual registration.
                        showMrScreen();
                        break;

                }
            }
        }
    }

    @Override
    public void onStop() {
        if (mRegistrationDialog != null && mRegistrationDialog.isShowing()) {
            PreferenceManager.getInstance(getContext()).setManualRegistrationTried(false);
            LogUtility.d("auto", "reg dialog was showing...");
            AppUtils.logToFile(getContext(), "registration dialog was showing while on stop of home pager fragment");
        } else {
            LogUtility.d("auto", "reg dialog was NOT showing...");
            AppUtils.logToFile(getContext(), "registration dialog was NOT showing while on stop of home pager fragment");
        }
        super.onStop();
    }

    private void showCountdownTimer() {

        mCountDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                mRegistrationDialogViewModel.showTimer.set(true);
                mRegistrationDialogViewModel.mTimerValue.set(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                mRegistrationDialogViewModel.showTimer.set(false);
                //getCurrentViewModel().enableResendOtp.set(true);
                hideTimerDialog();
            }
        }.start();
    }

    private void cancelTimer() {
        mRegistrationDialogViewModel.showTimer.set(false);
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        hideTimerDialog();
    }

    private void showCountDownTimerDialog() {

        if (getActivity() == null) return;
        TimerDialogCommonBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
            R.layout.timer_dialog_common, null, false);
        binding.setViewModel(mRegistrationDialogViewModel);
        mTimerDialog = DialogUtils.getCustomDialog(getActivity(), binding.getRoot());
        mTimerDialog.setCancelable(false);
        mTimerDialog.setCanceledOnTouchOutside(false);
        mTimerDialog.show();
        hideRegistrationDialog();
    }

    private void hideTimerDialog() {

        if (mTimerDialog != null) {
            mTimerDialog.dismiss();
            mTimerDialog = null;
        }
    }


    /**
     * This method will return current active fragment from viewpager.
     *
     * @return
     */
    public MessageListFragment getCurrentVisibleFragment() {

        Fragment fragment = (Fragment) mHomePagerAdapter.instantiateItem(mBinding.fragmentPager,
            mBinding.fragmentPager.getCurrentItem());
        try {
            return ((MessageListFragment) fragment);
        } catch (Exception e) {
            throw new ClassCastException(fragment.getClass().getSimpleName() + "Cannot cast to"
                + MessageListFragment.class.getSimpleName());
        }
    }
}
