package io.gupshup.smsapp.ui.home.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.data.model.TabItem;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Ram Prakash Bhat on 9/4/18.
 */

public class CustomTabViewModel extends BaseFragmentViewModel {


    public CustomTabViewModel(@NonNull Application application) {
        super(application);
    }

    public TabItem getTabItem(Context ctx, int pos) {

        TabItem item = new TabItem();
        TypedArray icons = ctx.getResources().obtainTypedArray(R.array.tab_icons);
        List<String> tabTitle = Arrays.asList(ctx.getResources().getStringArray(R.array.tab_title));
        item.tabTitle = tabTitle.get(pos);
        item.tabIcon =  icons.getDrawable(pos);
        // recycle the array
        icons.recycle();
        return item;
    }

}
