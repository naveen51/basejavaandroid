package io.gupshup.smsapp.ui.registration.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.databinding.RegVerificationErrorFragLytBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.registration.viewmodel.RegistrationVerificationErrorFragViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 6/14/2018.
 */
public class RegistrationVerificationErrorFragment extends Fragment implements RxBusCallback {

    private static final String TAG = RegistrationVerificationErrorFragment.class.getSimpleName();
    private RxBusHelper rxBusHelper;
    @Inject
    public RxBus rxBus;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RegVerificationErrorFragLytBinding regVerificationErrFragLytBinding = DataBindingUtil.inflate(inflater, R.layout.reg_verification_error_frag_lyt, container, false);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        RegistrationVerificationErrorFragViewModel registrationVerificationErrorViewModel = ViewModelProviders.of(this).get(RegistrationVerificationErrorFragViewModel.class);
        regVerificationErrFragLytBinding.setRegErrorViewModel(registrationVerificationErrorViewModel);
        return regVerificationErrFragLytBinding.getRoot();
    }


    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onEventTrigger(Object object) {
        if (object instanceof RxEvent) {
            handleRxEvents((RxEvent) object);
        }
    }

    private void handleRxEvents(RxEvent object) {
        switch (object.getEventName()) {

            case AppConstants.SKIP_ERROR_LYT_CLICK:
                RegistrationInfo simOneItem = AppUtils.getSimOneData(getActivity());
                RegistrationInfo simTwoItem = AppUtils.getSimTwoData(getActivity());
                if (simOneItem != null) {
                    simOneItem.isSkipClicked = true;
                    AppUtils.setSimRequestItemData(getActivity(), simOneItem);
                }
                if (simTwoItem != null) {
                    simTwoItem.isSkipClicked = true;
                    AppUtils.setSimRequestItemData(getActivity(), simTwoItem);
                }
                MainActivity.disregardSimChange = true;
                RxEvent homeEvent = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
                rxBus.send(homeEvent);
                break;

            case AppConstants.SKIP_RETRY_CLICK:
                RxEvent retryEvent = new RxEvent(AppConstants.RxEventName.SHOW_SKIP_SCREEN_FOR_RETRY, null);
                rxBus.send(retryEvent);
                LogUtility.d(TAG, "-------- SKIP RETRY  RECIEVED --------");
                break;
        }
    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }
}
