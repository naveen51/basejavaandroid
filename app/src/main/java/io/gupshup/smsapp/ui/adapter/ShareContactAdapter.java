package io.gupshup.smsapp.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.databinding.AdapterShareContactItemBinding;
import io.gupshup.smsapp.databinding.ContactHeaderItemLetterBinding;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewAdapter;
import io.gupshup.smsapp.ui.base.view.ViewType;
import io.gupshup.smsapp.ui.compose.viewmodel.ContactViewModel;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 8/21/2018.
 */
public class ShareContactAdapter extends CursorRecyclerViewAdapter {

    private ArrayList<Contact> contactList = new ArrayList<>();

    public ShareContactAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        this.contactList.addAll(ContactSingleton.getInstance().getSelectedSharedContacts().values());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (ViewType.valueOf(viewType)) {
            case HEADER:
                ContactHeaderItemLetterBinding headerBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.contact_header_item_letter, parent, false);
                return new ShareContactAdapter.HeaderViewHolder(headerBinding);

            case ITEM:
                AdapterShareContactItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_share_contact_item, parent, false);
                return new ShareContactAdapter.ConversationContactViewHolder(itemBinding);

        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void updateCursor(Cursor cursor) {
        swapCursor(cursor);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor) {
        int prevPosition = cursor.getPosition();
        String prevLetter = null;
        String prevContactID = null;
        if (cursor.getPosition() >= 1 && cursor.moveToPosition(cursor.getPosition() - 1)) {
            prevLetter = String.valueOf(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)).charAt(0));
            prevContactID = String.valueOf(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)));
            if (!AppUtils.isEnglishLetterOrDigit(prevLetter.charAt(0))) {
                prevLetter = "#";
            }
        }
        cursor.moveToPosition(prevPosition);
        ShareContactAdapter.ConversationContactViewHolder viewHolder = (ShareContactAdapter.ConversationContactViewHolder) holder;
        ContactViewModel contactViewModel = new ContactViewModel();

        int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
        int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int indexOfThumbNailUri = cursor.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI);
        int indexOfContactID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID);
        int indexOfContactType = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);





        String displayName = cursor.getString(indexOfDisplayName);
        String displayNumber = cursor.getString(indexOfDisplayNumber);
        String displayThumbNail = cursor.getString(indexOfThumbNailUri);
        String contactID = cursor.getString(indexOfContactID);
        int type = cursor.getInt(indexOfContactType);
        contactViewModel.mContactType.set(getPhoneContactType(type,viewHolder.itemView.getContext()));


        LogUtility.d("CONTACTID", "---------------  Contact  = " + displayName + " , Contact Number = " + displayNumber + "   , ContactID =  ------------" + contactID);

        Contact contact = new Contact();
        contact.setmContactName(displayName);
        contact.setmContactPhoneNumber(displayNumber);
        contact.setmContactThumbNail(displayThumbNail);
        contact.setmItemPosition(cursor.getPosition());
        contact.setContactID(contactID);

        //handling header check and visibility
        if (AppUtils.isEnglishLetterOrDigit(displayName.charAt(0))) {
            //Goes under letter header
            if (prevLetter == null) {
                //for the first time control enter
                prevLetter = String.valueOf(displayName.charAt(0));
                contactViewModel.headerLayoutVisibility.set(true);
            } else {
                //for the second and consequence time control enters
                String currentLetter = String.valueOf(displayName.charAt(0));
                if (!prevLetter.equalsIgnoreCase(currentLetter)) {
                    contactViewModel.headerLayoutVisibility.set(true);
                    prevLetter = currentLetter;
                } else {
                    contactViewModel.headerLayoutVisibility.set(false);
                }
            }

        } else {
            //Goes under # header
            if (prevLetter == null) {
                prevLetter = "#";
                contactViewModel.headerLayoutVisibility.set(true);
            } else {

                if (!prevLetter.equalsIgnoreCase("#")) {
                    contactViewModel.headerLayoutVisibility.set(true);
                    prevLetter = "#";
                } else {
                    contactViewModel.headerLayoutVisibility.set(false);
                }

            }
        }
        //set header letter
        contactViewModel.contactHeaderLetter.set(prevLetter.toUpperCase());

        //select and unselct icon
        if (contactList.contains(contact)) {
            contactViewModel.isIconSelected.set(true);
            contact.setSelected(true);
        } else {
            contactViewModel.isIconSelected.set(false);
            contact.setSelected(false);
        }

        if (contact.getContactName() != null && contact.getContactPhoneNumber() != null) {
            if (contact.getContactName().equalsIgnoreCase(contact.getContactPhoneNumber())) {
                ((AdapterShareContactItemBinding) viewHolder.getBinding()).setIsDisplayNameEmpty(true);
            } else {
                ((AdapterShareContactItemBinding) viewHolder.getBinding()).setIsDisplayNameEmpty(false);
            }
        }


        if (contact.getContactID().equalsIgnoreCase(prevContactID)) {
            contactViewModel.isMultiple.set(true);
            ((AdapterShareContactItemBinding) viewHolder.getBinding()).setIsDisplayNameEmpty(true);
        } else {
            contactViewModel.isMultiple.set(false);
            ((AdapterShareContactItemBinding) viewHolder.getBinding()).setIsDisplayNameEmpty(false);
        }


        ((AdapterShareContactItemBinding) viewHolder.getBinding()).setContactItem(contact);
        ((AdapterShareContactItemBinding) viewHolder.getBinding()).setContactViewModel(contactViewModel);
        ((AdapterShareContactItemBinding) viewHolder.getBinding()).outerContainer.setTag(contact);
    }

    private String getPhoneContactType(int type, Context context) {

        switch (type) {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                return context.getString(R.string.type_home);
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                return context.getString(R.string.type_mobile);
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                return context.getString(R.string.type_work);
            default:
                return context.getString(R.string.type_other);
        }

    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    public class ConversationContactViewHolder extends RecyclerViewAdapter.RecyclerViewHolder {

        public ConversationContactViewHolder(AdapterShareContactItemBinding itemView) {
            super(itemView);
            itemView.iconSelectedRelLyt.setVisibility(View.GONE);
        }
    }

    public class HeaderViewHolder extends RecyclerViewAdapter.RecyclerViewHolder {

        public HeaderViewHolder(ContactHeaderItemLetterBinding itemView) {
            super(itemView);

        }

    }

    public void notifySelectedContactList(Contact contact) {
        contactList.clear();
        contactList.addAll(ContactSingleton.getInstance().getSelectedSharedContacts().values());
        notifyItemChanged(contact.getmItemPosition(), contact);

    }


    public void updateSingletonLocalContactList() {
        contactList.clear();
        contactList.addAll(ContactSingleton.getInstance().getSelectedSharedContacts().values());
    }
}
