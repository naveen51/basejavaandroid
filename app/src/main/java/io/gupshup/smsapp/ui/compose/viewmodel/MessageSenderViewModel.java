package io.gupshup.smsapp.ui.compose.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

/**
 * Created by Naveen BM on 4/16/2018.
 */
public class MessageSenderViewModel {

    public ObservableBoolean headerVisibility = new ObservableBoolean(false);
    public ObservableField<String> headerVal = new ObservableField<>();

    public MessageSenderViewModel() {

    }

}
