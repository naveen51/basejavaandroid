package io.gupshup.smsapp.ui.settings.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.databinding.AboutFragmentBinding;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.settings.viewmodel.AboutFragmentViewModel;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class AboutFragment extends BaseFragment<AboutFragmentBinding,
    AboutFragmentViewModel> {

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.tvAppVersion.setText(getString(R.string.version,
            AppUtils.getAppVersionName(getActivity())));
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getBindingVariable() {
        return BR.aboutViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.about_fragment;
    }

    @Override
    public AboutFragmentViewModel getViewModel() {
        return ViewModelProviders.of(this).get(AboutFragmentViewModel.class);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }
}
