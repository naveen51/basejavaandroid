package io.gupshup.smsapp.ui.widget;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.databinding.FlashAlertLytBinding;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Naveen BM on 5/18/2018.
 */
public class FlashDialogActivity extends AppCompatActivity {
    private String mMessage;
    private FlashAlertLytBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.setContentView(this, R.layout.flash_alert_lyt);
        getIntentData();
        initializeComponentViews();
    }

    private void initializeComponentViews() {
        AppCompatTextView desc = binding.description;
        desc.setText(mMessage);
        AppCompatTextView okBtn = binding.okButton;
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mMessage = getIntent().getStringExtra(AppConstants.FLASH_MESSAGE_BODY);
        }
    }
}
