package io.gupshup.smsapp.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.databinding.AppTourLayoutBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Ram Prakash Bhat on 26/6/18.
 */

public class WelcomeScreenPagerAdapter extends PagerAdapter implements BaseHandlers {

    @Inject
    public RxBus rxBus;

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        MainApplication.getInstance().getAppComponent().inject(this);
        AppTourLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.app_tour_layout, container,
            false);
        View view = binding.getRoot();
        binding.setHandlers(this);

        AppCompatImageView imageView = binding.tourImage;
        AppCompatTextView titleTextView = binding.tourTitle;
        AppCompatTextView subtitleTextView = binding.tourSubtitle;
        AppCompatTextView subtitleSecondTextView = binding.tourSubtitleSecond;
        AppCompatTextView smsChargeTextView = binding.tourConditionText;
        AppCompatTextView skipBtn = binding.skipBtn;
        skipBtn.setVisibility(View.GONE);
        switch (position) {
            case 0:
                imageView.setImageResource(R.drawable.img_first_slide);
                titleTextView.setText(R.string.make_conversation_fast);
                subtitleTextView.setText(R.string.message_app);
                subtitleSecondTextView.setText(R.string.spalsh_stay_connected_title);
                subtitleSecondTextView.setVisibility(View.VISIBLE);
                smsChargeTextView.setVisibility(View.GONE);

                break;
            case 2:
                imageView.setImageResource(R.drawable.img_second_slide);
                titleTextView.setText(R.string.organize_conversastion);
                subtitleTextView.setText(R.string.enjoy_messaging_experience);
                subtitleSecondTextView.setVisibility(View.GONE);
                break;
            case 1:
                Context context = imageView.getContext();
                final String packageName = context.getPackageName();
                subtitleSecondTextView.setVisibility(View.GONE);
                titleTextView.setText(R.string.enable_seamless_messaging);
                imageView.setImageResource(R.drawable.img_second_slide);
                if (!AppUtils.isDefaultSmsPackage(context, packageName)) {
                    skipBtn.setVisibility(View.GONE);
                    subtitleTextView.setText(R.string.auto_registration_clutter_next_click);
                } else {
                    skipBtn.setVisibility(View.VISIBLE);
                    subtitleTextView.setText(R.string.auto_registration_clutter);
                }
                smsChargeTextView.setVisibility(View.VISIBLE);
                smsChargeTextView.setText(R.string.standard_sms_charge);
                break;

            default:
                break;
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public void onViewClick(View view) {
        RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_HOME_SCREEN, null);
        rxBus.send(event);
    }
}