package io.gupshup.smsapp.ui.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.ContactForwardItemBinding;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.reply.viewmodel.ForwardMessageViewModel;
import io.gupshup.smsapp.utils.MessageUtils;

/**
 * Created by Naveen BM on 5/24/2018.
 */
public class ForwardContactAdapter extends RecyclerViewPageListAdapter<ContactModel, RecyclerViewPageListAdapter.RecyclerViewHolder> {
    private MessageEntity mSelectedEntity;

    public ForwardContactAdapter(MessageEntity selectedEntity) {
        super(MessageUtils.FORWARD_CONTACT_DIFF_UTIL);
        this.mSelectedEntity = selectedEntity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ContactForwardItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.contact_forward_item, parent, false);
        return new ForwardContactAdapter.ForwardContactViewHolder(itemBinding);

    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final ContactModel contact = getItem(position);

        //LogUtility.d("Cursor", "Position is "+position);

        holder.getBinding().getRoot().setTag(position);
        ((ContactForwardItemBinding) holder.getBinding()).setContactPhone(contact.getMask());
        ((ContactForwardItemBinding) holder.getBinding()).setContactThumb(contact.getImage());
        ((ContactForwardItemBinding) holder.getBinding()).setSelectedMessageEntity(mSelectedEntity);
        ((ContactForwardItemBinding) holder.getBinding()).setForwardModel(new ForwardMessageViewModel(MainApplication.getInstance()));


        String displayName = contact.getDisplay();

        if (TextUtils.isEmpty(displayName)) {
            ((ContactForwardItemBinding) holder.getBinding()).setIsDisplayNameEmpty(true);
            ((ContactForwardItemBinding) holder.getBinding()).setContactName(contact.getMask());
        } else {
            ((ContactForwardItemBinding) holder.getBinding()).setContactName(displayName);
            if (contact.getMask() != null && contact.getMask().equalsIgnoreCase(displayName)) {
                //display name and mash both are same then dont show name
                ((ContactForwardItemBinding) holder.getBinding()).setIsDisplayNameEmpty(true);

            } else {
                ((ContactForwardItemBinding) holder.getBinding()).setIsDisplayNameEmpty(false);
            }
        }

        super.onBindViewHolder(holder, position);
    }


    @Override
    public void onDestroy() {

    }

    public class ForwardContactViewHolder extends RecyclerViewPageListAdapter.RecyclerViewHolder {

        public ForwardContactViewHolder(ContactForwardItemBinding itemView) {
            super(itemView);

        }
    }
}
