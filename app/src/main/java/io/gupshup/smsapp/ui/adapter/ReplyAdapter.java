package io.gupshup.smsapp.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.databinding.ReplyReciepentItemBinding;
import io.gupshup.smsapp.databinding.ReplySenderItemLytBinding;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.enums.MessageDirection;
import io.gupshup.smsapp.enums.MessageStatus;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.reply.viewmodel.ReplyMessageReciepentViewModel;
import io.gupshup.smsapp.ui.reply.viewmodel.ReplyMessageSenderViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.MessageUtils;

/**
 * Created by Naveen BM on 4/20/2018.
 */
public class ReplyAdapter extends RecyclerViewPageListAdapter<MessageEntity, RecyclerViewPageListAdapter.RecyclerViewHolder> {

    private static final int MESSAGE_SENDER = 1;
    private static final int MESSAGE_RECIEPENT = 2;
    private String mRecepientThumbNail;
    private onItemClickListener mItemClickListener;
    private onMessageItemClickListener mListener;
    private String mQuerytext;
    private int highLightPosition, removeDarkHighLightAtPos;
    private String mDisplayName;
    private int mThemeColor;
    private Context mContext;
    private List<MessageEntity> mSelectedListMessageEntity;
    //To handle long press selected message status even after message entity status gets updated
   // private SparseIntArray mSelectedItemPosition;
    private boolean mSelectAllClicked;
    @Inject
    public RxBus rxBus;


    public ReplyAdapter(String thumbNailRecepient, onItemClickListener itemClickedListener, onMessageItemClickListener listener, String displayName, int themeColor, Context context) {
        super(MessageUtils.MESSAGE_ENTITY_DIFF_CALLBACK);
        MainApplication.getInstance().getAppComponent().inject(this);
        this.mRecepientThumbNail = thumbNailRecepient;
        this.mItemClickListener = itemClickedListener;
        this.mListener = listener;
        this.mDisplayName = displayName;
        this.mThemeColor = themeColor;
        this.mContext = context;
        mSelectedListMessageEntity = new ArrayList<>();
      //  mSelectedItemPosition = new SparseIntArray();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MESSAGE_SENDER:
                ReplySenderItemLytBinding senderBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.reply_sender_item_lyt, parent, false);
                return new ReplySenderViewHolder(senderBinding);

            case MESSAGE_RECIEPENT:
                ReplyReciepentItemBinding reciepentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.reply_reciepent_item, parent, false);
                return new ReplyReciepentViewHolder(reciepentBinding);

        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {

        MessageEntity item = getItem(position);
        if(item!=null) {
            switch (getItemViewType(position)) {

                case MESSAGE_SENDER:
                    ReplySenderViewHolder senderViewHolder = (ReplySenderViewHolder) holder;

                    ReplyMessageSenderViewModel model = new ReplyMessageSenderViewModel(MainApplication.getInstance());
                    long nextTimeStampSender = 0;
                    if(position < (getItemCount()-1)){
                        MessageEntity pm = getItem(position + 1);
                        nextTimeStampSender = pm.getMessageTimestamp();
                    }

                    if (MessageChannel.SMS == item.getChannel()) {
                        model.colorForBg.set(ContextCompat.getColor(senderViewHolder
                            .itemView.getContext(), R.color.color_eeeeee));
                    } else {
                        model.colorForBg.set(ContextCompat.getColor(senderViewHolder
                            .itemView.getContext(), R.color.default_sim_two_color));
                    }

                    //we are setting sentTimeStamp between the call of preSending and postSending call coz getMessageTimestamp will be -1 between these two calls
                    if (item.getMessageTimestamp() == -1) {
                        model.checkPreviousItemHeader(item.getSentTimestamp(), nextTimeStampSender);
                    } else {
                        model.checkPreviousItemHeader(item.getMessageTimestamp(), nextTimeStampSender);
                    }

                    /**
                     * To work select all functionality while pagination.
                     */
                    if (mSelectAllClicked
                        && !mSelectedListMessageEntity.contains(item)) {
                        mSelectedListMessageEntity.add(item);
                        sendRxEvent();
                    }

                    if (mSelectedListMessageEntity.contains(item)) {
                        model.itemBg.set(true);
                    } else {
                        model.itemBg.set(false);
                    }

                    if (position == highLightPosition) {
                        ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setHighLight(true);
                    } else {
                        ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setHighLight(false);
                    }

                    if (position == removeDarkHighLightAtPos) {
                        ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setHighLight(false);
                    }

                    ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setSearchQuerry(mQuerytext);
                    ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setPos(position);
                    ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setMessageSender(model);
                    ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).setMessageEntItem(item);
                    ((ReplySenderItemLytBinding) senderViewHolder.getBinding()).senderDetailContainer.setEnabled(true);
                    break;

                case MESSAGE_RECIEPENT:
                    ReplyReciepentViewHolder reciepentViewHolder = (ReplyReciepentViewHolder) holder;
                    ReplyMessageReciepentViewModel modelReciepent = new ReplyMessageReciepentViewModel(MainApplication.getInstance());
                    modelReciepent.themeColorForBg.set(mThemeColor);
                    long nextTimeStamp = 0;
                    if(position < (getItemCount()-1)){
                        MessageEntity pm = getItem(position + 1);
                        nextTimeStamp = pm.getMessageTimestamp();
                    }

                    /**
                     * To work select all functionality while pagination.
                     */
                    if (mSelectAllClicked
                        && !mSelectedListMessageEntity.contains(item)) {
                        mSelectedListMessageEntity.add(item);
                        sendRxEvent();
                    }

                    if (mSelectedListMessageEntity.contains(item)) {
                        modelReciepent.itemBg.set(true);
                    } else {
                        modelReciepent.itemBg.set(false);
                    }

                    if(mRecepientThumbNail != null) {
                    modelReciepent.displayPic.set(mRecepientThumbNail);
                	}
                    modelReciepent.displayName.set(mDisplayName);
                    if (position == highLightPosition) {
                        ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setHighLight(true);
                    } else {
                        ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setHighLight(false);
                    }

                    if (position == removeDarkHighLightAtPos) {
                        ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setHighLight(false);
                    }
                    modelReciepent.checkPreviousItemHeader(item.getMessageTimestamp(), nextTimeStamp);
                    ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setSearchQuerry(mQuerytext);
                    ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setPos(position);
                    ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setReciepentViewModel(modelReciepent);
                    ((ReplyReciepentItemBinding) reciepentViewHolder.getBinding()).setMessageEntItem(item);
                    break;

            }
        }

        super.onBindViewHolder(holder, position);

    }

    @Override
    public void onDestroy() {

    }


    public void setBackgroundForSelection(int pos, MessageEntity msgEntity) {
        /*if (mSelectedLongPressedItems.get(pos, false)) {
            mSelectedLongPressedItems.delete(pos);
        } else {
            mSelectedLongPressedItems.put(pos, true);
        }*/
        mSelectAllClicked = false;
        if (mSelectedListMessageEntity.contains(msgEntity)) {
            mSelectedListMessageEntity.remove(msgEntity);
        } else {
            mSelectedListMessageEntity.add(msgEntity);
        }
        notifyItemChanged(pos);
    }


    @Override
    public int getItemViewType(int position) {
        MessageEntity msgEntity = getItem(position);
        if (msgEntity != null && msgEntity.getDirection() == MessageDirection.INCOMING) {
            return MESSAGE_RECIEPENT;
        } else {
            return MESSAGE_SENDER;
        }

    }

    public int getSelectedItemCount() {
        // return mSelectedLongPressedItems.size();
        return mSelectedListMessageEntity.size();

    }

    /**
     * Method used to update dats set with query text
     *
     * @param newText
     */
    public void updateWithQueryText(String newText, int highLightPos) {
        highLightPosition = highLightPos;
        mQuerytext = newText;
        notifyDataSetChanged();
    }

    public class ReplySenderViewHolder extends RecyclerViewPageListAdapter.RecyclerViewHolder {

        long startTime;
        long longClickDelay = ViewConfiguration.getLongPressTimeout();

        @SuppressLint("ClickableViewAccessibility")
        public ReplySenderViewHolder(ReplySenderItemLytBinding viewBinding) {
            super(viewBinding);
            viewBinding.senderDetailContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mItemClickListener != null) {
                        view.setTag(getItem(getAdapterPosition()));
                        mItemClickListener.onItemLongClick(view, getAdapterPosition());
                    }
                    return true;
                }
            });

            viewBinding.senderDetailContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        MessageStatus status = getItem(getAdapterPosition()).getMessageStatus();
                        if (MessageStatus.FAILED == status) {
                            view.setEnabled(false);
                        }
                        mListener.onItemMessageClickSender(view, getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });


            viewBinding.msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemMessageClickSender(view, getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });

            viewBinding.msg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mItemClickListener != null) {
                        view.setTag(getItem(getAdapterPosition()));
                        mItemClickListener.onItemLongClick(view, getAdapterPosition());
                    }
                    return true;
                }
            });
            viewBinding.msg.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int action = motionEvent.getAction();

                    if (action == MotionEvent.ACTION_DOWN) {
                        startTime = System.currentTimeMillis();
                    }
                    if (action == MotionEvent.ACTION_UP) {
                        long currentTime = System.currentTimeMillis();
                        if ((currentTime - startTime) >= longClickDelay)
                            return true;
                    }
                    return false;
                }
            });

        }
    }


    public class ReplyReciepentViewHolder extends RecyclerViewPageListAdapter.RecyclerViewHolder {

        long startTime;
        long longClickDelay = ViewConfiguration.getLongPressTimeout();

        @SuppressLint("ClickableViewAccessibility")
        public ReplyReciepentViewHolder(ReplyReciepentItemBinding viewBinding) {
            super(viewBinding);
            viewBinding.recepientDetailContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mItemClickListener != null) {
                        view.setTag(getItem(getAdapterPosition()));
                        mItemClickListener.onItemLongClick(view, getAdapterPosition());
                    }
                    return true;
                }
            });


            viewBinding.recepientDetailContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemMessageClickReciepient(view, getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });


            viewBinding.msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemMessageClickReciepient(view, getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });

            viewBinding.msg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mItemClickListener != null) {
                        view.setTag(getItem(getAdapterPosition()));
                        mItemClickListener.onItemLongClick(view, getAdapterPosition());
                    }
                    return true;
                }
            });
            viewBinding.msg.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int action = motionEvent.getAction();

                    if (action == MotionEvent.ACTION_DOWN) {
                        startTime = System.currentTimeMillis();
                    }
                    if (action == MotionEvent.ACTION_UP) {
                        long currentTime = System.currentTimeMillis();
                        if ((currentTime - startTime) >= longClickDelay)
                            return true;
                    }
                    return false;
                }
            });

        }

    }

    public void notifySelectionClear() {
      //  mSelectedItemPosition.clear();
        mSelectAllClicked = false;
        for (int position = 0; position <= mSelectedListMessageEntity.size() - 1; position++) {
            int index = getCurrentList().indexOf(mSelectedListMessageEntity.get(position));
            notifyItemChanged(index);
        }
        mSelectedListMessageEntity.clear();
    }


    public int getLongItemCount() {
        return mSelectedListMessageEntity.size();
    }


    public interface onMessageItemClickListener {

        void onItemMessageClickSender(View view, int position, MessageEntity entity);

        void onItemMessageClickReciepient(View view, int position, MessageEntity entity);
    }

    public void updateSearchItem(int pos, int prevPos) {
        highLightPosition = pos;
        removeDarkHighLightAtPos = prevPos;
        notifyItemChanged(prevPos);
        notifyItemChanged(pos);
    }

    /**
     * Method used to add all the list message entities when select all button clicked
     */
    public void addMessageEntityList(List<MessageEntity> messageEntityList) {
        mSelectAllClicked = true;
        mSelectedListMessageEntity.clear();
      //  mSelectedItemPosition.clear();
        mSelectedListMessageEntity.addAll(messageEntityList);
        notifyDataSetChanged();
    }

    /**
     * method remove all messages from the selected list
     */
    public void removeMessageEntityFromList() {
        mSelectedListMessageEntity.clear();
       // mSelectedItemPosition.clear();
        mSelectAllClicked = false;
        notifyDataSetChanged();
    }


    public List<MessageEntity> getSelectedMessages() {
        return mSelectedListMessageEntity;
    }


    private void sendRxEvent(){
        //rx event to change the toolbar title and color
        RxEvent event = new RxEvent(AppConstants.RxEventName.ACTION_MODE_MESSAGE_COUNT_UPDATE, null);
        rxBus.send(event);
    }

}
