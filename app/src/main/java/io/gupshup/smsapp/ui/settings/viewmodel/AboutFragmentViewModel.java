package io.gupshup.smsapp.ui.settings.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.Config;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.events.WebViewURLEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class AboutFragmentViewModel extends BaseFragmentViewModel {
    @Inject
    public RxBus rxBus;

    public AboutFragmentViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void onPrivacyClicked(View view) {
        RxEvent event = new RxEvent(AppConstants.RxEventName.WEB_VIEW_FRAG_EVENT, new WebViewURLEvent(Config.WEB_BASE_URL+ Config.PRIVACY_WEB_URL,view.getContext().getString(R.string.privacy_title)));
        rxBus.send(event);
    }

    public void onTermsClicked(View view) {
        RxEvent event = new RxEvent(AppConstants.RxEventName.WEB_VIEW_FRAG_EVENT, new WebViewURLEvent(Config.WEB_BASE_URL+ Config.TERMS_WEB_URL,view.getContext().getString(R.string.terms_conditions)));
        rxBus.send(event);
    }

    public void onLicenceClicked(View view) {
        RxEvent event = new RxEvent(AppConstants.RxEventName.WEB_VIEW_FRAG_EVENT, new WebViewURLEvent(Config.WEB_BASE_URL+ Config.LICENCE_WEB_URL,view.getContext().getString(R.string.licence_title)));
        rxBus.send(event);
    }
}
