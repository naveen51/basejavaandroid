package io.gupshup.smsapp.ui.settings.view;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.databinding.NotificationFragmentBinding;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.home.view.MainActivity;
import io.gupshup.smsapp.ui.settings.viewmodel.NotificationFragmentViewModel;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.PreferenceManager;

import static io.gupshup.smsapp.utils.AppConstants.TONE_PICKER_REQUEST_CODE;

/**
 * Created by Naveen BM on 7/11/2018.
 */
public class NotificationSettingFragment extends BaseFragment<NotificationFragmentBinding, NotificationFragmentViewModel> implements CompoundButton.OnCheckedChangeListener {

    @Override
    public int getBindingVariable() {
        return BR.notificationViwModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.notification_fragment;
    }

    @Override
    public NotificationFragmentViewModel getViewModel() {
        return ViewModelProviders.of(this).get(NotificationFragmentViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        mBinding.setVariable(BR.notificationFragment, this);
        mBinding.setVariable(BR.handler, this);
        setUpNotificationSetting(getCurrentToneUri());
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * This method will return existing notification tone as Uri.
     *
     * @return
     */
    private Uri getCurrentToneUri() {

        PreferenceManager preferenceManager = PreferenceManager.getInstance(getActivity());
        Uri currentTone;
        if (TextUtils.isEmpty(preferenceManager.getNotificationTone())) {
            currentTone = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), RingtoneManager.TYPE_NOTIFICATION);
        } else {
            currentTone = Uri.parse(preferenceManager.getNotificationTone());
        }
        return currentTone;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TONE_PICKER_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Uri notificationToneUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    PreferenceManager.getInstance(getActivity()).setNotificationTone(notificationToneUri.toString());
                    setUpNotificationSetting(notificationToneUri);
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);


    }

    private void setUpNotificationSetting(Uri notificationToneUri) {

        PreferenceManager preferenceManager = PreferenceManager.getInstance(getActivity());
        String ringToneName = AppUtils.getSelectedRingToneTitle(getActivity(), notificationToneUri);
        getViewModel().mNotificationSoundName.set(ringToneName);
        getViewModel().handleSwitchesOnNotificationStatus(preferenceManager.isNotificationEnabled());
        getViewModel().setNotificationLightOnOff(preferenceManager.isNotificationLightEnabled());
        getViewModel().setVibrateOnOff(preferenceManager.isVibrationEnabled());
    }

    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.notification_sound_container:
                showNotificationRingToneDialog();
                break;
        }
        super.onViewClick(view);
    }

    private void showNotificationRingToneDialog() {

        MainActivity.hasCalledAnotherActivity = true;
        final Uri currentTone = getCurrentToneUri();
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getString(R.string.select_a_sound));
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, false);
        startActivityForResult(intent, TONE_PICKER_REQUEST_CODE);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        switch (compoundButton.getId()) {

            case R.id.notification_enable_switch:
                getViewModel().handleSwitchesOnNotificationStatus(isChecked);
                PreferenceManager.getInstance(compoundButton.getContext()).setIsNotificationEnabled(isChecked);
                break;

            case R.id.notification_vibrate_switch:
                getViewModel().setVibrateOnOff(isChecked);
                PreferenceManager.getInstance(compoundButton.getContext()).setIsVibrationEnabled(isChecked);
                break;

            case R.id.notification_light_switch:
                getViewModel().setNotificationLightOnOff(isChecked);
                PreferenceManager.getInstance(compoundButton.getContext()).setIsNotificationLightEnabled(isChecked);
                break;
        }

    }


}
