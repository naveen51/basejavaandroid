package io.gupshup.smsapp.ui.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.data.model.MessageReplyBundleData;
import io.gupshup.smsapp.ui.reply.view.MessageReplyFragment;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Naveen BM on 7/17/2018.
 */
public class MessageReplyAdapterNew extends FragmentStatePagerAdapter {
    private MessageReplyBundleData mParcelData;

    private String mMask;
    private List<String> mMasksList = new ArrayList<>();

    public MessageReplyAdapterNew(FragmentManager fm, MessageReplyBundleData parcelData, List<String> maskList) {
        super(fm);
        this.mParcelData = parcelData;
        mMasksList.addAll(maskList);
    }

    @Override
    public Fragment getItem(int position) {
        MessageReplyFragment fragItem = new MessageReplyFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.BundleKey.PARCEL, mParcelData);
        bundle.putString(AppConstants.BundleKey.MASK, mMasksList.get(position));
        fragItem.setArguments(bundle);
        return fragItem;
    }


    @Override
    public int getCount() {
        if (mMasksList != null && mMasksList.size() > 0) {
            return mMasksList.size();
        } else {
            return 0;
        }
    }


    public void updateMaskList(List<String> masks) {
        if (mMasksList != null) {
            mMasksList.clear();
            mMasksList.addAll(masks);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
