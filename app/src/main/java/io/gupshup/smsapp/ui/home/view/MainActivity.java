package io.gupshup.smsapp.ui.home.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.crashlytics.android.Crashlytics;

import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.lang.ref.WeakReference;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import io.gupshup.smsapp.Config;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.ToolbarInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.entities.ConfigEntity;
import io.gupshup.smsapp.database.model.SimStateChangeModel;
import io.gupshup.smsapp.databinding.ActivityMainBinding;
import io.gupshup.smsapp.enums.AutoRegistrationRetryPolicy;
import io.gupshup.smsapp.enums.ConfigKey;
import io.gupshup.smsapp.enums.ConversationSubType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ConfigUpdateEvent;
import io.gupshup.smsapp.events.ReRegistrationEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.events.WebViewURLEvent;
import io.gupshup.smsapp.networking.APIService;
import io.gupshup.smsapp.networking.responses.AppConfigurationAPIResponse;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.service.AppConfigurationAPI;
import io.gupshup.smsapp.service.DeregisterPhoneNumberApi;
import io.gupshup.smsapp.service.RegistrationService;
import io.gupshup.smsapp.ui.archive.view.ArchivedMessageFragment;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.base.view.BaseActivity;
import io.gupshup.smsapp.ui.compose.view.ComposeSmsActivity;
import io.gupshup.smsapp.ui.home.viewmodel.MainActivityViewModel;
import io.gupshup.smsapp.ui.registration.view.ReRegistrationFragment;
import io.gupshup.smsapp.ui.registration.view.RegistrationSkipFragment;
import io.gupshup.smsapp.ui.registration.view.RegistrationVerificationErrorFragment;
import io.gupshup.smsapp.ui.search.view.MessageSearchResultFragment;
import io.gupshup.smsapp.ui.settings.view.AboutFragment;
import io.gupshup.smsapp.ui.settings.view.NotificationSettingFragment;
import io.gupshup.smsapp.ui.settings.view.SettingsFragment;
import io.gupshup.smsapp.ui.settings.view.WebViewFragment;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.FragmentUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.PermissionResultCallback;
import io.gupshup.smsapp.utils.PermissionUtils;
import io.gupshup.smsapp.utils.PreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.gupshup.smsapp.utils.AppConstants.PermissionRequest.SMS_PERMISSION_REQUEST;
import static io.gupshup.smsapp.utils.AppConstants.PermissionRequestCode.SMS_PERMISSION_REQUEST_CODE;


public class MainActivity extends BaseActivity implements BaseHandlers, RxBusCallback,
    ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback,
    FragmentManager.OnBackStackChangedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    public MainActivityViewModel mMainActivityViewModel;
    @Inject
    public RxBus rxBus;
    @Inject
    protected APIService apiService;
    @Inject
    public ArchivedMessageFragment mArchivedMessageFragment;
    @Inject
    public MessageSearchResultFragment mMessageSearchResultFragment;

    ArrayList<String> permissions = new ArrayList<>();
    PermissionUtils permissionUtils;
    private ActivityMainBinding mBinding;
    private RxBusHelper rxBusHelper;
    private Menu mSearchMenu;
    private MenuItem mItemSearch;
    private ConversationType mConversationType = ConversationType.PERSONAL;
    public static Handler mHandler = null;
    public static boolean hasCalledAnotherActivity = false;
    public static boolean disregardSimChange = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        mHandler = new Handler();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        MainApplication.getInstance().getAppComponent().inject(this);
        registerEvents();
        if (PreferenceManager.getInstance(this).isSim1RegistrationDone() && PreferenceManager.getInstance(this).isSim2RegistrationDone()) {
            PreferenceManager.getInstance(this).setRegistrationDone(true);
        }
        Security.addProvider(new BouncyCastleProvider());
        setSearchController();
        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mBinding.setMainActivityViewModel(mMainActivityViewModel);
        mBinding.setHandlers(this);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        // showSplashScreen();
        onNewIntent(getIntent());
        AppUtils.saveSimInfoToPreference(AppUtils.getSimInfoList(), PreferenceManager.getInstance(this));
        //LogUtility.d("auto", "mainact oncreate.......");
        try {
            if (PreferenceManager.getInstance(getBaseContext()).getAccountId().equalsIgnoreCase("0")) {
                PreferenceManager.getInstance(getBaseContext()).setAccountId(Config.ACCOUNT_ID);
            }
        } catch (Exception e) {
            //TODO: Handle exceptions.
        }
    }

    /**
     * App is not default.
     * Show the "not currently set as the default SMS app" interface
     */
    private void showSpecificScreen() {
        if (getIntent() == null) {
            showHomeScreen();
            return;
        }
        //Handle deep link here
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            showHomeScreen();
            return;
        }
        if (extras.containsKey(AppConstants.BundleKey.MOBILE_NUMBER)) {
            mConversationType = (ConversationType) extras.getSerializable(AppConstants.BundleKey.CONVERSATION_TYPE);
            launchMessageReplyActivityForPushDeepLink(extras.getString(AppConstants.BundleKey.MOBILE_NUMBER),
                extras.getString(AppConstants.BundleKey.LAST_MESSAGE_ID), mConversationType);
            hasCalledAnotherActivity = true;

        }
        showHomeScreen();
    }


    private void showHomeScreen() {
        LogUtility.d("auto", "In showHomeScreen");
        //pair = AppUtils.getIsSimCardChanged(getBaseContext());
        //AppUtils.checkSimChangedStatus(this); //Sim change status detection.
        final RegistrationInfo simOneItem = AppUtils.getSimOneData(this);
        final RegistrationInfo simTwoItem = AppUtils.getSimTwoData(this);
        String packageName = getPackageName();
        //AMA-460
        PreferenceManager manager = PreferenceManager.getInstance(this);
        if (manager.isFirstLaunch()) {
            manager.setIsFirstLaunch(false);
            FragmentUtils.replaceFragment(this, new WelComeScreenFragment(),
                R.id.fragment_container);
        } else if (!AppUtils.isDefaultSmsPackage(this, packageName)) {
            FragmentUtils.replaceFragment(this, new SplashScreenFragment(),
                R.id.fragment_container);
        } else {
            loadHomeScreen();
            if (PreferenceManager.getInstance(getBaseContext()).getAppId().equals("0")) {
                //calling auto register
                LogUtility.d("auto", "App id is 0..calling auto-reg");
                callRegistration();
                return;
            }
            if (disregardSimChange) {
                disregardSimChange = false;
                return;
            } else {
            }
            checkAllCasesForRegistrationAndDeregistration(simOneItem, simTwoItem);
        }
    }

    /**
     * This method will check weather re-registration required on SIM (State) change.
     *
     * @param simOneItem
     * @param simTwoItem
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    private void checkAllCasesForRegistrationAndDeregistration(final RegistrationInfo simOneItem, final RegistrationInfo simTwoItem) {

        boolean simChangeExecuted = false;
        SimStateChangeModel simStateChangeModel = AppUtils.getIsSimCardStatusChange(this);
        if (simOneItem != null
            && simStateChangeModel != null) {
            //LogUtility.d("auto", "calling deregister for sim slot 0");
            if ((AppConstants.SimChangeStatus.ABSENT
                .equalsIgnoreCase(simStateChangeModel.getSimOneState())
                || AppConstants.SimChangeStatus.NOT_READY
                .equalsIgnoreCase(simStateChangeModel.getSimOneState())
                || simStateChangeModel.getSimOneState() == null)
                && simStateChangeModel.isSimOneChanged()) {
                simChangeExecuted = true;
                RegistrationService.isLongCode1CalledForSIM1 = false;
                RegistrationService.isLongCode2CalledForSIM1 = false;
                PreferenceManager.getInstance(getBaseContext()).setImsiNumber(0, AppConstants.EMPTY_TEXT);
                //deregister sim 1
                if (simOneItem.countryCode != null && simOneItem.phoneNumber != null) {
                    DeregisterPhoneNumberApi.getInstance().deregisterPhoneNumberApi(simOneItem
                        .countryCode, simOneItem.phoneNumber, simOneItem.apiKey, simOneItem.simSlot);
                    deRegisterFromConfigAndClearPrefsForSim(0);
                    //simOneItem.setSimOneState(RegistrationSimOneState.SIM_ONE_STATE_CHANGED);
                }
            }
            if ((AppConstants.SimChangeStatus.LOADED
                .equalsIgnoreCase(simStateChangeModel.getSimOneState())
                || simStateChangeModel.getSimOneState() == null
                || AppConstants.SimChangeStatus.DE_REGISTRATION_DONE
                .equalsIgnoreCase(simStateChangeModel.getSimOneState()))
                && simStateChangeModel.isSimOneChanged()) {
                simOneItem.simOneState = RegistrationSimOneState.SIM_ONE_STATE_CHANGED;
                AppUtils.setSimRequestItemData(this, simOneItem);
                PreferenceManager.getInstance(getBaseContext()).setSim1RegistrationDone(false);
                if (SubscriptionManager.from(getBaseContext()).getActiveSubscriptionInfoForSimSlotIndex(0) != null) {
                    AppUtils.saveSimIMSINoForParticularSlot(this, 0);
                    autoRegistrationLogic(simOneItem, 0);
                    simChangeExecuted = true;
                }
            }
        }
        if (simTwoItem != null
            && simStateChangeModel != null) {

            if ((AppConstants.SimChangeStatus.ABSENT
                .equalsIgnoreCase(simStateChangeModel.getSimTwoState())
                || AppConstants.SimChangeStatus.NOT_READY
                .equalsIgnoreCase(simStateChangeModel.getSimTwoState())
                || simStateChangeModel.getSimTwoState() == null)
                && simStateChangeModel.isSimTwoChanged()) {
                simChangeExecuted = true;
                RegistrationService.isLongCode1CalledForSIM2 = false;
                RegistrationService.isLongCode2CalledForSIM2 = false;
                //deregister sim 2
                PreferenceManager.getInstance(getBaseContext()).setImsiNumber(1, AppConstants.EMPTY_TEXT);
                if (simTwoItem.countryCode != null && simTwoItem.phoneNumber != null) {
                    DeregisterPhoneNumberApi.getInstance().deregisterPhoneNumberApi(simTwoItem
                        .countryCode, simTwoItem.phoneNumber, simTwoItem.apiKey, simTwoItem.simSlot);
                    deRegisterFromConfigAndClearPrefsForSim(1);
                }
            }
            if ((AppConstants.SimChangeStatus.LOADED.equalsIgnoreCase(simStateChangeModel.getSimTwoState())
                || simStateChangeModel.getSimTwoState() == null
                || AppConstants.SimChangeStatus.DE_REGISTRATION_DONE
                .equalsIgnoreCase(simStateChangeModel.getSimTwoState()))
                && simStateChangeModel.isSimTwoChanged()) {
                simTwoItem.simTwoState = RegistrationSimTwoState.SIM_TWO_STATE_CHANGED;
                AppUtils.setSimRequestItemData(this, simTwoItem);
                PreferenceManager.getInstance(getBaseContext()).setSim2RegistrationDone(false);
                if (SubscriptionManager.from(getBaseContext()).getActiveSubscriptionInfoForSimSlotIndex(1) != null) {
                    AppUtils.saveSimIMSINoForParticularSlot(this, 1);
                    autoRegistrationLogic(simTwoItem, 1);
                    simChangeExecuted = true;
                }
            }
        }

        if (simChangeExecuted) {
            return;
        }
        if (!allSimsRegistered() && !PreferenceManager.getInstance(this).isManualRegistrationTried()) {
            if (PreferenceManager.getInstance(getBaseContext()).getAppId().equals("0")) {
                AppConfigurationAPI.getInstance().getAppId(PreferenceManager.getInstance(getBaseContext()).getAccountId(), AppUtils.getBuildName(), AppUtils.getBuildNumber(), "1", "2", new Callback<AppConfigurationAPIResponse>() {
                    @Override
                    public void onResponse(Call<AppConfigurationAPIResponse> call, Response<AppConfigurationAPIResponse> response) {

                        if (response.isSuccessful()) {
                            PreferenceManager.getInstance(getBaseContext()).setAppId(response.body().getAppId());
                        }

                        List<SimInformation> simList = AppUtils.getSimInfoList();
                        if (simList.size() == 1) {
                            SimInformation info = simList.get(0);
                            autoRegistrationLogic(AppUtils.getSimDataBasedOnSlot(getBaseContext(),
                                String.valueOf(info.getSimSlotIndex())), info.getSimSlotIndex());
                        } else if (simList.size() == 2) {
                            if (!PreferenceManager.getInstance(getBaseContext()).isSim1RegistrationDone()) {
                                autoRegistrationLogic(simOneItem, 0);
                            }
                            if (!PreferenceManager.getInstance(getBaseContext()).isSim2RegistrationDone()) {
                                autoRegistrationLogic(simTwoItem, 1);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AppConfigurationAPIResponse> call, Throwable t) {
                    }
                });
            } else {
                List<SimInformation> simList = AppUtils.getSimInfoList();
                if (simList.size() == 1) {
                    SimInformation info = simList.get(0);
                    autoRegistrationLogic(AppUtils.getSimDataBasedOnSlot(this,
                        String.valueOf(info.getSimSlotIndex())), info.getSimSlotIndex());
                } else if (simList.size() == 2) {
                    if (!PreferenceManager.getInstance(this).isSim1RegistrationDone()) {
                        autoRegistrationLogic(simOneItem, 0);
                    }
                    if (!PreferenceManager.getInstance(this).isSim2RegistrationDone()) {
                        autoRegistrationLogic(simTwoItem, 1);
                    }
                }
            }
        }
    }


    private boolean allSimsRegistered() {

        List<SimInformation> simList = AppUtils.getSimInfoList();
        int count = simList.size();
        if (count == 1) {
            return PreferenceManager.getInstance(this).isSim1RegistrationDone();
        } else if (count == 2) {
            return PreferenceManager.getInstance(this).isSim1RegistrationDone() && PreferenceManager.getInstance(this).isSim2RegistrationDone();
        }
        return false;
    }

    private void callRegistration() {
        PreferenceManager.getInstance(getBaseContext()).setSim1RegistrationDone(false);
        PreferenceManager.getInstance(getBaseContext()).setSim2RegistrationDone(false);

        AppConfigurationAPI.getInstance().getAppId(PreferenceManager.getInstance(getBaseContext()).getAccountId(), AppUtils.getBuildName(), AppUtils.getBuildNumber(), "1", "2", new Callback<AppConfigurationAPIResponse>() {
            @Override
            public void onResponse(Call<AppConfigurationAPIResponse> call, Response<AppConfigurationAPIResponse> response) {

                if (response.isSuccessful()) {
                    PreferenceManager.getInstance(getBaseContext()).setAppId(response.body().getAppId());
                }
                List<SimInformation> simList = AppUtils.getSimInfoList();
                if (simList.size() == 1) {
                    SimInformation info = simList.get(0);
                    autoRegistrationLogic(AppUtils.getSimDataBasedOnSlot(getBaseContext(),
                        String.valueOf(info.getSimSlotIndex())), info.getSimSlotIndex());
                } else if (simList.size() == 2) {
                    autoRegistrationLogic(AppUtils.getSimOneData(getBaseContext()), 0);
                    autoRegistrationLogic(AppUtils.getSimTwoData(getBaseContext()), 1);
                }
            }

            @Override
            public void onFailure(Call<AppConfigurationAPIResponse> call, Throwable t) {
                LogUtility.d("auto", "onFailure of getAppId called...");
            }
        });
    }

    private void showHomeScreenAfterSkipFragment() {


        loadHomeScreen();
        if (PreferenceManager.getInstance(getBaseContext()).getAppId().equals("0")) {
            //calling auto register
            callRegistration();
            return;
        }

        if (disregardSimChange) {
            LogUtility.d("auto", "NOT checking sim change...");
            disregardSimChange = false;
            return;
        } else {
            LogUtility.d("auto", "Checking sim change....");
        }
        final RegistrationInfo simOneItem = AppUtils.getSimOneData(this);
        final RegistrationInfo simTwoItem = AppUtils.getSimTwoData(this);
        checkAllCasesForRegistrationAndDeregistration(simOneItem, simTwoItem);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtility.d(TAG, "MAIN ACTIVITY INTENT DATA " + AppUtils.intentToString(intent));
        permissions.addAll(Arrays.asList(SMS_PERMISSION_REQUEST));
        permissionUtils = new PermissionUtils(this);
        permissionUtils.checkPermission(permissions, getString(R.string.permission_explanation_message), SMS_PERMISSION_REQUEST_CODE);

    }

    private void launchMessageReplyActivityForPushDeepLink(String phoneNumber, String lastMessageId,
                                                           ConversationType conversationType) {

        AppUtils.launchMessageReplyActivityForPushDeepLink(this, phoneNumber, lastMessageId, conversationType);
        overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
    }

    private void loadHomeScreen() {

        setActionBarVisibility(View.VISIBLE);
        setToolBarTitle(getString(R.string.message));
        HomePagerFragment fragment = new HomePagerFragment();
        Bundle budle = new Bundle();
        budle.putSerializable(AppConstants.BundleKey.NOTIFICATION_CONVERSATION_TYPE, mConversationType);
        fragment.setArguments(budle);
        FragmentUtils.popBackStackAndReplace(this, fragment,
            R.id.fragment_container);
        updateConfigData(null);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
            == PackageManager.PERMISSION_GRANTED)) {
            registerContactChangeObserver();
        }
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void PermissionGranted(int requestCode) {
        showSpecificScreen();
        registerContactChangeObserver();
    }

    @Override
    public void PartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions) {
        finish();
    }

    @Override
    public void PermissionDenied(int requestCode) {
        finish();
    }

    @Override
    public void NeverAskAgain(int requestCode) {

        String message = getString(R.string.sms_permission_message);
        String dialogTitle = getString(R.string.permission_dialog_title);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(dialogTitle);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.grant_btn),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    DialogUtils.goToAppSettings(MainActivity.this);
                }
            });
        builder.setNegativeButton(getString(R.string.cancel_btn),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    dialog.cancel();
                }
            });
        builder.show();

    }

    private void loadSkipScreen() {
        mBinding.toolbar.setVisibility(View.VISIBLE);
        setToolBarTitle(getString(R.string.register_now));
        MainActivity.hasCalledAnotherActivity = true;
        RegistrationSkipFragment skipRegistration = new RegistrationSkipFragment();
        FragmentUtils.replaceFragmentWithAnimation(this, skipRegistration,
            R.id.fragment_container);
        PreferenceManager.getInstance(this).setManualRegistrationTried(true);
    }

    private void loadArchivedScreen() {

        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.BundleKey.CONVERSATION_SUB_TYPE, ConversationSubType.ARCHIVE.ordinal());
        mArchivedMessageFragment.setArguments(bundle);
        FragmentUtils.addFragmentWithAnimation(this, mArchivedMessageFragment,
            R.id.fragment_container);
    }


    @Override
    protected void onResume() {
        super.onResume();
        clearSearchView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_pager_settings:
                loadSettingsScreen();
                return true;
            case R.id.action_archive:
                loadArchivedScreen();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void loadSettingsScreen() {
        FragmentUtils.addFragmentWithAnimation(this, new SettingsFragment(),
            R.id.fragment_container);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment == null) return;
        //To update back button in archived screen.
        if (fragment instanceof ArchivedMessageFragment) {
            setToolBarTitle(getString(R.string.archived));
            setupToolBar(true);
        } else if (fragment instanceof SettingsFragment) {
            setToolBarTitle(getString(R.string.settings));
            setupToolBar(true);
        } else if (fragment instanceof AboutFragment) {
            setToolBarTitle(getString(R.string.about));
            setupToolBar(true);
        } else if (fragment instanceof WebViewFragment) {
            // setToolBarTitle(getString(R.string.webview));
            setupToolBar(true);
        } else if (fragment instanceof NotificationSettingFragment) {
            setToolBarTitle(getString(R.string.settings));
            setupToolBar(true);
        } else if (fragment instanceof RegistrationVerificationErrorFragment) {
            setToolBarTitle(getString(R.string.verification_error_toolbar_title));
            setupToolBar(true);
        } else if (fragment instanceof RegistrationSkipFragment) {
            setToolBarTitle(getString(R.string.register_now));
            setupToolBar(true);
        } else if (fragment instanceof ReRegistrationFragment) {
            setToolBarTitle(getString(R.string.re_registration));
            setupToolBar(true);
        } else {
            setupToolBar(false);
            setToolBarTitle(getString(R.string.message));
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            super.onBackPressed();
            return;
        }
        if (mSearchMenu != null
            && mItemSearch != null
            && mSearchMenu.findItem(R.id.action_search).isActionViewExpanded()) {
            mItemSearch.collapseActionView();
            return;
        }
        super.onBackPressed();
        //finish();
    }

    @Override
    public void onViewClick(View view) {
        LogUtility.d(TAG, "--------- View Clicked ---------" + view.getId());
        Intent intent = new Intent(this, ComposeSmsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    protected void onDestroy() {
        try {
            unregisterEvents();
            unRegisterContactChangeObserver();
        } catch (Exception e) {
            //TODO: handle exception cases.....
        }
        super.onDestroy();
    }

    @Override
    public void onEventTrigger(Object event) {
        LogUtility.d(TAG, "-------------- EVENT TRIGGERED -------------" + event.toString());
        if (event instanceof RxEvent) {
            handleRxEvents((RxEvent) event);
        }
    }

    private void deRegisterFromConfigAndClearPrefsForSim(int simslot) {
        RegistrationInfo simOneData = AppUtils.getSimOneData(this);
        RegistrationInfo simTwoData = AppUtils.getSimTwoData(this);
        if (simslot == 0) {
            ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(null, simTwoData);
            updateConfigData(updateEvent);
        } else if (simslot == 2) {
            ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, null);
            updateConfigData(updateEvent);
        }
    }

    /**
     * Common method to handle all possible events
     *
     * @param event
     */
    private void handleRxEvents(RxEvent event) {
        switch (event.getEventName()) {
            case AppConstants.RxEventName.TOOLBAR_INFO:
                ToolbarInfo info = (ToolbarInfo) event.getEventData();
                setActionBarVisibility(info.isVissible ? View.VISIBLE : View.GONE);
                setToolBarTitle(info.toolbarTitle);
                break;
            case AppConstants.RxEventName.SHOW_REGISTRATION_SCREEN:
                //FragmentUtils.popBackStackImmediate(this);
                if (!PreferenceManager.getInstance(this).isSim1RegistrationDone() && !PreferenceManager.getInstance(this).isSim1RegistrationDone() && !PreferenceManager.getInstance(this).isManualRegistrationTried()) {
                    //loadRegisterScreen();
                    loadSkipScreen();
                } else {
                    loadHomeScreen();
                }

                break;
            case AppConstants.RxEventName.SHOW_SKIP_SCREEN:
                if (!PreferenceManager.getInstance(this).isRegistrationDone()) {
                    if (!PreferenceManager.getInstance(this).isManualRegistrationTried())
                        loadSkipScreen();
                    else
                        loadHomeScreen();
                } else {
                    loadHomeScreen();
                }
                break;
            case AppConstants.RxEventName.SHOW_SKIP_SCREEN_FOR_RETRY:
                try {
                    loadSkipScreen();
                } catch (Exception e) {
                    //TODO:check cause and handle...
                }
                break;
            case AppConstants.RxEventName.SHOW_HOME_SCREEN:
                // FragmentUtils.popBackStackImmediate(this);
                showHomeScreenAfterSkipFragment();
                setActionBarVisibility(View.VISIBLE);
                setToolBarTitle(getString(R.string.message));
                //TODO: @Gaurav please check below line i commented
                //PreferenceManager.getInstance(this).setManualRegistrationTried(true);

                //loadHomeScreen((ConfigUpdateEvent) event.getEventData());
                break;
            case AppConstants.RxEventName.SHOW_OTP_SCREEN:

                break;
            case AppConstants.RxEventName.SHOW_SEARCH_VIEW:
                RxEvent tabShowEvent = new RxEvent(AppConstants.RxEventName.SHOW_HIDE_VIEW, View.GONE);
                rxBus.send(tabShowEvent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(R.id.search_toolbar, 1, true, true);
                else {
                    mBinding.searchToolbar.setVisibility(View.VISIBLE);
                }
                mItemSearch.expandActionView();
                break;
            case AppConstants.RxEventName.SHOW_MR_TWO_SKIP_SCREEN:
                loadMrTwoScreen();
                break;

            case AppConstants.RxEventName.AUTO_REGISTRATION_VERIFICATION_STATUS:

                RegistrationInfo simData = (RegistrationInfo) event.getEventData();
                LogUtility.d("auto", "Auto reg verification for sim slot :" + simData.simSlot);


                LogUtility.d("auto", "Auto registration verification response...");
                AppUtils.logToFile(getBaseContext(), "Auto registration verification response...");
                //Stored data
                RegistrationInfo simOneData = AppUtils.getSimOneData(this);
                RegistrationInfo simTwoData = AppUtils.getSimTwoData(this);
                PreferenceManager manager = PreferenceManager.getInstance(this);
                List<SimInformation> simList = AppUtils.getSimInfoList();

                //Auto verification success.
                //LogUtility.d("auto", "Auto verification state for sims : SIM 1 "+simOneData.simOneState+ " SIM 2 : "+simTwoData.simTwoState);
                if (simList.size() > 1) {
                    if (simOneData != null && simTwoData != null) {
                        if (RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS == simOneData.simOneState
                            && RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS == simTwoData.simTwoState) {
                            //Both success.
                            LogUtility.d("auto", "Both SIMs registered successfully");
                            AppUtils.logToFile(getBaseContext(), "Both SIMS registered succesfully..");
                            ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
                            updateConfigData(updateEvent);
                            //UiUtils.showToast(this, getString(R.string.registration_success));
                        } else {
                            //Either of one sim auto registration failed
                            LogUtility.d("auto", "One or Both SIMs NOT registered...");
                            AppUtils.logToFile(getBaseContext(), "Or or Both SIMS NOT registered..");
                            autoRegistrationHandlingBasedOnStatus(simOneData, simTwoData, simData.simSlot);
                        }
                    } else {
                        //Either of one sim auto registration failed
                        LogUtility.d("auto", "One of the sims have null data");
                        AppUtils.logToFile(getBaseContext(), "One of the sims have null data");
                        autoRegistrationHandlingBasedOnStatus(simOneData, simTwoData, simData.simSlot);
                    }
                } else if (simList.size() == 1) {
                    LogUtility.d("auto", "For single SIm handling auto reg response..");
                    AppUtils.logToFile(getBaseContext(), "For single SIm handling auto reg response..");
                    autoRegistrationHandlingBasedOnStatus(simOneData, simTwoData, simData.simSlot);
                }
                break;
            case AppConstants.RxEventName.UPDATE_CONFIG_DATA:
                ConfigUpdateEvent updateEvent = (ConfigUpdateEvent) event.getEventData();
                updateConfigData(updateEvent);
                break;

            case AppConstants.RxEventName.INSTRUCTION_OVERLAY:
                mMainActivityViewModel.mInstructionOverLayVisibility.set(true);
                break;

            case AppConstants.RxEventName.ABOUT_FRAGMENT_EVENT:
                loadAboutFragment();
                break;

            case AppConstants.RxEventName.WEB_VIEW_FRAG_EVENT:
                WebViewURLEvent urlData = (WebViewURLEvent) event.getEventData();
                setToolBarTitle(urlData.getmToolBarTitle());
                loadWebViewFragment(urlData.getURL());
                break;

            case AppConstants.RxEventName.NOTIFICATION_FRAGMENT_EVENT:
                loadNotificationFragment();
                break;

            case AppConstants.RxEventName.SETTING_RE_REGISTRATION:
                ReRegistrationEvent registrationInfo = (ReRegistrationEvent) event.getEventData();
                resetRegistrationStatesAndVariablesForSim(registrationInfo.simSlot);
                new AutoRegistrationTask(AppUtils.getSimInfoList(), registrationInfo.registrationInfo,
                    registrationInfo.simSlot).execute();
                break;

           /* case AppConstants.RxEventName.REGISTER_ON_SIM_STATE_CHANGE:
                final RegistrationInfo simOneItem = AppUtils.getSimOneData(this);
                final RegistrationInfo simTwoItem = AppUtils.getSimTwoData(this);
                checkAllCasesForRegistrationAndDeregistration(simOneItem, simTwoItem);
                break;*/

            case AppConstants.RxEventName.RE_REGISTRATION_FRAGMENT_EVENT:
                loadReRegistrationScreen();
                break;

            case AppConstants.RxEventName.REGISTER_DEREGISTER_EVENT_ON_SIM_STATE_CHANGE:
                final RegistrationInfo simOneItem = AppUtils.getSimOneData(this);
                final RegistrationInfo simTwoItem = AppUtils.getSimTwoData(this);
                checkAllCasesForRegistrationAndDeregistration(simOneItem, simTwoItem);

                break;
        }
    }

    private void resetRegistrationStatesAndVariablesForSim(int simSlot) {
        if (simSlot == 0) {
            RegistrationService.isLongCode1CalledForSIM1 = false;
            RegistrationService.isLongCode2CalledForSIM1 = false;
        } else if (simSlot == 1) {
            RegistrationService.isLongCode1CalledForSIM2 = false;
            RegistrationService.isLongCode2CalledForSIM2 = false;
        }
    }

    private void loadWebViewFragment(String url) {
        WebViewFragment webViewFrag = new WebViewFragment();
        Bundle bundle = new Bundle();
        //bundle.putString(AppConstants.BundleKey.MOBILE_NUMBER, mobileNumber);
        bundle.putString(AppConstants.BundleKey.WEB_URL, url);
        webViewFrag.setArguments(bundle);
        FragmentUtils.addFragmentWithAnimation(this, webViewFrag,
            R.id.fragment_container);
    }

    private void loadAboutFragment() {
        FragmentUtils.addFragmentWithAnimation(this, new AboutFragment(),
            R.id.fragment_container);
    }

    private void loadNotificationFragment() {
        FragmentUtils.addFragmentWithAnimation(this, new NotificationSettingFragment(),
            R.id.fragment_container);
    }

    private void loadMrTwoScreen() {
        hideOverlayScreen();
        setToolBarTitle(getString(R.string.verification_error_toolbar_title));
        FragmentUtils.replaceAndAddFragment(this, new RegistrationVerificationErrorFragment(),
            R.id.fragment_container);
    }


    private void loadReRegistrationScreen() {
        FragmentUtils.addFragmentWithAnimation(this, new ReRegistrationFragment(),
            R.id.fragment_container);
    }

    private void setToolBarTitle(String toolbarTitle) {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setTitle(toolbarTitle);
    }

    public void setActionBarVisibility(int visibility) {
        mBinding.toolbar.setVisibility(visibility);
    }

    public static class InsertConfigTask extends AsyncTask<Void, Void, Void> {
        public WeakReference<Context> context;
        public ConfigEntity apiKeyEntity, privateKeyEntity, phoneNumberEntity;

        public InsertConfigTask(Context context, ConfigEntity apiKeyEntity, ConfigEntity privateKeyEntity, ConfigEntity phoneNumberEntity) {
            this.context = new WeakReference<>(context);
            this.apiKeyEntity = apiKeyEntity;
            this.privateKeyEntity = privateKeyEntity;
            this.phoneNumberEntity = phoneNumberEntity;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // get a reference to the activity if it is still there
            Context ctx = context.get();
            AppDatabase.getInstance(ctx).configDAO().insert(apiKeyEntity, privateKeyEntity, phoneNumberEntity);
            return null;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setSearchController() {
        if (mBinding.searchToolbar != null) {
            mBinding.searchToolbar.getMenu().clear();
            mBinding.searchToolbar.inflateMenu(R.menu.menu_search_message);
            mSearchMenu = mBinding.searchToolbar.getMenu();

            mBinding.searchToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        circleReveal(R.id.search_toolbar, 1, true, false);
                    else {
                        mBinding.searchToolbar.setVisibility(View.GONE);
                    }
                }
            });

            mItemSearch = mSearchMenu.findItem(R.id.action_search);

            mItemSearch.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    // Do something when collapsed
                    RxEvent tabShowEvent = new RxEvent(AppConstants.RxEventName.SHOW_HIDE_VIEW, View.VISIBLE);
                    rxBus.send(tabShowEvent);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.search_toolbar, 1, true, false);
                    } else {
                        mBinding.searchToolbar.setVisibility(View.GONE);
                    }
                    Fragment searchFragment = FragmentUtils.getCurrentFragment(MainActivity.this,
                        R.id.fragment_container);
                    if (searchFragment != null
                        && searchFragment instanceof MessageSearchResultFragment) {
                        FragmentUtils.popBackStackImmediate(MainActivity.this);
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    // Do something when expanded
                    return true;
                }
            });

            initSearchView();
        }
    }

    public void initSearchView() {
        final SearchView searchView =
            (SearchView) mSearchMenu.findItem(R.id.action_search).getActionView();
        // Enable/Disable Submit button in the keyboard
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }

            private void callSearch(String query) {
                loadSearchResultFragment(query.trim());
            }
        });
        searchView.setQueryHint(getString(R.string.search_messages));
    }

    private void clearSearchView() {

        SearchView searchView =
            (SearchView) mSearchMenu.findItem(R.id.action_search).getActionView();
        if (searchView != null) {
            if (!TextUtils.isEmpty(searchView.getQuery())) {
                searchView.setQuery("", false);
                mItemSearch.collapseActionView();
            }
        }
    }

    private void loadSearchResultFragment(String query) {


        if (TextUtils.isEmpty(query)) {
            FragmentUtils.popBackStackImmediate(this);
        }
        if (query.trim().length() > 0) {
            Fragment searchFragment = FragmentUtils.getCurrentFragment(this,
                R.id.fragment_container);
            if (searchFragment != null
                && searchFragment instanceof MessageSearchResultFragment) {
                ((MessageSearchResultFragment) searchFragment).setSearchQuery(query);

            } else {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BundleKey.SEARCH_QUERY, query);
                mMessageSearchResultFragment.setArguments(bundle);
                FragmentUtils.addFragment(this, mMessageSearchResultFragment,
                    R.id.fragment_container);
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow,
                             final boolean isShow) {
        final View myView = findViewById(viewID);

        int width = myView.getWidth();

        if (posFromRight > 0)
            width -= (posFromRight * getResources().getDimensionPixelSize(R.dimen.dimen_48dp))
                - (getResources().getDimensionPixelSize(R.dimen.dimen_48dp) / 2);
        if (containsOverflow)
            width -= getResources().getDimensionPixelSize(R.dimen.dimen_36dp);

        int cx = width;
        int cy = myView.getHeight() / 2;

        Animator anim;
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, (float) width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float) width, 0);

        anim.setDuration((long) 220);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isShow) {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if (isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();


    }
    //Auto registration part begins here.

    /**
     * Auto registration logic.
     *
     * @param registrationInfo
     * @param simSlotIndex
     */
    private void autoRegistrationLogic(RegistrationInfo registrationInfo, int simSlotIndex/*, RegistrationInfo simTwoItem*/) {

        if (registrationInfo != null && registrationInfo.getSimOneState() != null && registrationInfo.getSimOneState().equals(RegistrationSimOneState.SIM_ONE_STATE_CHANGED)) {
            new AutoRegistrationTask(AppUtils.getSimInfoList(), registrationInfo/*, simTwoItem*/, simSlotIndex).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (registrationInfo != null && registrationInfo.getSimTwoState() != null && registrationInfo.getSimTwoState().equals(RegistrationSimTwoState.SIM_TWO_STATE_CHANGED)) {
            new AutoRegistrationTask(AppUtils.getSimInfoList(), registrationInfo/*, simTwoItem*/, simSlotIndex).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (!checkRegistrationDone(simSlotIndex)) {
            new AutoRegistrationTask(AppUtils.getSimInfoList(), registrationInfo/*, simTwoItem*/, simSlotIndex).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private boolean checkRegistrationDone(int simSlotNo) {
        if (simSlotNo == 0) {
            return PreferenceManager.getInstance(this).isSim1RegistrationDone();
        } else {
            return PreferenceManager.getInstance(this).isSim2RegistrationDone();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class AutoRegistrationTask extends AsyncTask<Void, Void, Void> {
        private List<SimInformation> simList;
        private RegistrationInfo registrationInfo;
        private int simSlotIndex;
        //private RegistrationInfo simTwoItem;

        private AutoRegistrationTask(List<SimInformation> simList, RegistrationInfo registrationInfo, int simSlotIndex/*, RegistrationInfo simTwoItem*/) {
            this.simList = simList;
            this.registrationInfo = registrationInfo;
            this.simSlotIndex = simSlotIndex;
            //this.simTwoItem = simTwoItem;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // get a reference to the activity if it is still there
            if (registrationInfo == null) {
                mMainActivityViewModel.callAutoRegistrationAPI(MainActivity.this, simSlotIndex);
            } else {
                if (simSlotIndex == 0) {
                    if (registrationInfo.simOneState == RegistrationSimOneState.SIM_ONE_STATE_CHANGED
                        || registrationInfo.simOneState == RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE) {
                        updateRetryPolicy(registrationInfo);
                        mMainActivityViewModel.callAutoRegistrationAPI(MainActivity.this, simSlotIndex);
                    }
                } else if (simSlotIndex == 1) {
                    if (registrationInfo.simTwoState == RegistrationSimTwoState.SIM_TWO_STATE_CHANGED
                        || registrationInfo.simTwoState == RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE) {
                        updateRetryPolicy(registrationInfo);
                        mMainActivityViewModel.callAutoRegistrationAPI(MainActivity.this, simSlotIndex);
                    }
                }
            }
            return null;
        }

    }

    private void updateRetryPolicy(RegistrationInfo simInfo) {
        if (simInfo.simSlot == 0) {
            if (simInfo.retryPolicySim1 == AutoRegistrationRetryPolicy.TWO_TIME) {
                simInfo.retryPolicySim1 = AutoRegistrationRetryPolicy.ONE_TIME;
            } else {
                simInfo.retryPolicySim1 = AutoRegistrationRetryPolicy.DONE;
            }
        } else {
            if (simInfo.retryPolicySim2 == AutoRegistrationRetryPolicy.TWO_TIME) {
                simInfo.retryPolicySim2 = AutoRegistrationRetryPolicy.ONE_TIME;
            } else {
                simInfo.retryPolicySim2 = AutoRegistrationRetryPolicy.DONE;
            }
        }
        AppUtils.setSimRequestItemData(this, simInfo);
    }

    /**
     * Any of one or both sim auto registration failed.
     *
     * @param simOneData
     * @param simTwoData
     */
    private void autoRegistrationHandlingBasedOnStatus(RegistrationInfo simOneData, RegistrationInfo simTwoData, int simSLotIndex) {

        ConfigUpdateEvent updateEvent = new ConfigUpdateEvent(simOneData, simTwoData);
        if (simSLotIndex == 0) {
            if (simOneData != null && simOneData.simOneState != null) {
                switch (simOneData.simOneState) {
                    case SIM_ONE_REGISTRATION_SUCCESS:
                        LogUtility.d("auto", "SIM 1 auto registration success");
                        AppUtils.logToFile(getBaseContext(), "SIM 1 auto registration success");
                        PreferenceManager.getInstance(this).setSim1RegistrationDone(true);
                        updateConfigData(updateEvent);
                        LogUtility.d("auto", "SIM 1 reg success");
                        break;
                    case SIM_ONE_REGISTRATION_FAILURE:
                        //Sim one auto failed
                        LogUtility.d("auto", "SIM 1 auto registration failure...");
                        AppUtils.logToFile(getBaseContext(), "SIM 1 auto registration failure...");
                        PreferenceManager.getInstance(this).setSim1RegistrationDone(false);
                        if (!RegistrationService.isLongCode2CalledForSIM1 && AppUtils.isNetworkAvailable(this)) {
                            LogUtility.d("auto", "Second time auto registration called...for SIM 1 ...");
                            AppUtils.logToFile(getBaseContext(), "second time auto registration called...for SIM 1 ...");
                            autoRegistrationLogic(simOneData, simOneData.simSlot);
                        } else {
                            simOneData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            AppUtils.setSimRequestItemData(this, simOneData);
                            LogUtility.d("auto", "manual registration called for SIM 1..");
                            LogUtility.d("reg", "Triggering manual reg.....for sim 1");
                            AppUtils.logToFile(getBaseContext(), "Triggering manual reg.....for sim 1");
                            //Fire event for manual registration.

                            final ConfigUpdateEvent finalUpdateEvent = updateEvent;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //LogUtility.d("auto", "manual registration called...for SIM 1 ...");
                                    RxEvent event = new RxEvent(AppConstants.RxEventName.SIM_ONE_REGISTRATION_FAILURE, finalUpdateEvent);
                                    rxBus.send(event);
                                }
                            }, 5000);

                        }
                        break;
                }
            }
        } else if (simSLotIndex == 1) {

            if (simTwoData != null && simTwoData.simTwoState != null) {
                switch (simTwoData.simTwoState) {
                    case SIM_TWO_REGISTRATION_SUCCESS:
                        LogUtility.d("auto", "SIM 2 auto registration success");
                        AppUtils.logToFile(getBaseContext(), "SIM 2 auto registration success");
                        PreferenceManager.getInstance(this).setSim2RegistrationDone(true);
                        updateConfigData(updateEvent);
                        LogUtility.d("auto", "SIM 2 reg success");
                        break;
                    case SIM_TWO_REGISTRATION_FAILURE:
                        //Sim two auto failed
                        LogUtility.d("auto", "SIM 2 auto registration failure...");
                        AppUtils.logToFile(getBaseContext(), "Sim 2 auto registration failure");

                        PreferenceManager.getInstance(this).setSim2RegistrationDone(false);
                        LogUtility.d("auto", "the sim 2 retry policy is " + simTwoData.retryPolicySim2 + "  and islong code 2 called is " + RegistrationService.isLongCode2CalledForSIM2);
                        LogUtility.d("auto", "Metwork state here is :" + AppUtils.isNetworkAvailable(this));
                        if (!RegistrationService.isLongCode2CalledForSIM2 && AppUtils.isNetworkAvailable(this)) {
                            autoRegistrationLogic(simTwoData, simTwoData.simSlot);
                            LogUtility.d("auto", "Second time auto registration called...for SIM 2");
                            AppUtils.logToFile(getBaseContext(), "second time auto registration called...for SIM 2");
                        } else {
                            LogUtility.d("auto", "manual registration called for SIM 2..");
                            AppUtils.logToFile(getBaseContext(), "Manual registration called for sim 2");
                            simTwoData.isManualRegistration = AppConstants.RegistrationType.MANUAL_REGISTRATION;
                            AppUtils.setSimRequestItemData(this, simTwoData);
                            //Fire event for manual registration.
                            final ConfigUpdateEvent finalUpdateEvent = updateEvent;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //LogUtility.d("auto", "calling manual reg for sim 2");
                                    RxEvent event = new RxEvent(AppConstants.RxEventName.SIM_TWO_REGISTRATION_FAILURE, finalUpdateEvent);
                                    rxBus.send(event);
                                }
                            }, 5000);
                        }
                        break;
                }
            }
        }
    }

    /**
     * To update config item to DB.
     *
     * @param item
     */
    private void updateConfigData(ConfigUpdateEvent item) {

        if (item != null) {
            RegistrationInfo itemOne = item.getSimOneData();
            RegistrationInfo itemTwo = item.getSimTwoData();

            if (itemOne != null) {
                final ConfigEntity apiKeyEntity = new ConfigEntity();
                apiKeyEntity.setKey(ConfigKey.API_KEY_1);
                apiKeyEntity.setValue(itemOne.apiKey);

                final ConfigEntity privateKeyOneEntity = new ConfigEntity();
                privateKeyOneEntity.setKey(ConfigKey.IP_PRIVATE_KEY_1);
                privateKeyOneEntity.setValue(itemOne.privateKey);

                final ConfigEntity phoneNumberEntity = new ConfigEntity();
                phoneNumberEntity.setKey(ConfigKey.PHONE_NUMBER_SIM_1);
                phoneNumberEntity.setValue(itemOne.phoneNumber);

                new InsertConfigTask(this, apiKeyEntity, privateKeyOneEntity, phoneNumberEntity).execute();
            }
            if (itemTwo != null) {
                final ConfigEntity apiKeyEntity = new ConfigEntity();
                apiKeyEntity.setKey(ConfigKey.API_KEY_2);
                apiKeyEntity.setValue(itemTwo.apiKey);

                final ConfigEntity privateKeyOneEntity = new ConfigEntity();
                privateKeyOneEntity.setKey(ConfigKey.IP_PRIVATE_KEY_2);
                privateKeyOneEntity.setValue(itemTwo.privateKey);

                final ConfigEntity phoneNumberEntity = new ConfigEntity();
                phoneNumberEntity.setKey(ConfigKey.PHONE_NUMBER_SIM_2);
                phoneNumberEntity.setValue(itemTwo.phoneNumber);

                new InsertConfigTask(this, apiKeyEntity, privateKeyOneEntity, phoneNumberEntity).execute();
            }
        }
    }

    public void hideOverlayScreen() {
        mMainActivityViewModel.mInstructionOverLayVisibility.set(false);
    }

    /*@Override
    protected void onStop() {
        super.onStop();
        if (!hasCalledAnotherActivity) {
            LogUtility.d(TAG, "onStop() and finish() has been called");
            finish();
        } else {
            hasCalledAnotherActivity = false;
        }
    }*/
}
