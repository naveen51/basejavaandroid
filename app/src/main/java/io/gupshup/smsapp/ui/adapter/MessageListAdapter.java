package io.gupshup.smsapp.ui.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.databinding.AdapterRecentMessageItemBinding;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewPageListAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.home.viewmodel.MessageViewModel;
import io.gupshup.smsapp.utils.FlipAnimator;
import io.gupshup.smsapp.utils.MessageUtils;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */

public class MessageListAdapter extends RecyclerViewPageListAdapter<ConversationEntity, MessageListAdapter.MessageViewHolder> {

    // array used to perform multiple animation at once
    private SparseBooleanArray mAnimationItemsIndex;
    private boolean mReverseAllAnimations = false;

    // index is used to animate only the selected row
    private static int mCurrentSelectedIndex = -1;
    private BaseFragment mBaseFragment;
    private List<ConversationEntity> mSelectedConversation;
    private boolean mSelectAllClicked;

    public MessageListAdapter(BaseFragment fragment) {
        super(MessageUtils.CONVERSATION_ENTITY_DIFF_CALLBACK);
        mSelectedConversation = new ArrayList<>();
        mAnimationItemsIndex = new SparseBooleanArray();
        mBaseFragment = fragment;
        mOnItemClickListener = fragment;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AdapterRecentMessageItemBinding binding = DataBindingUtil.inflate(inflater,
            R.layout.adapter_recent_message_item, parent, false);
        return new MessageViewHolder(binding);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        final ConversationEntity item = getItem(position);
        ((AdapterRecentMessageItemBinding) holder.getBinding()).setMessageItem(item);
        MessageViewModel messageViewModel = new MessageViewModel(MainApplication.getInstance());
        ((AdapterRecentMessageItemBinding) holder.getBinding()).setPersonalViewModel(messageViewModel);
        ((AdapterRecentMessageItemBinding) holder.getBinding()).setBaseFragment(mBaseFragment);
        String displayName = "", displayThumb = "";

        if (item != null) {
            if (item.getDisplay() != null) {
                displayName = item.getDisplay();
            } else {
                displayName = item.getMask();
            }
            displayThumb = item.getImage();
            holder.itemView.setTag(item); //.isUnread()
        }

        //holder.getBinding().getRoot().setActivated(mSelectedConversation.contains(item));//mSelectedItems.get(position, false)
        messageViewModel.displayName.set(displayName);
        messageViewModel.displayPic.set(displayThumb);
        iconAnimation((AdapterRecentMessageItemBinding) holder.getBinding(), position, item);
        super.onBindViewHolder(holder, position);
    }

    @Override
    public void onDestroy() {

    }

    private void iconAnimation(AdapterRecentMessageItemBinding binding, int position, ConversationEntity item) {

        /**
         * To work select all functionality while pagination.
         */
        if (mSelectAllClicked
            && !mSelectedConversation.contains(item)) {
            mSelectedConversation.add(item);
            mOnItemClickListener.onSelectAllAndScroll();
        }

        if (mSelectedConversation.contains(item)) {
            binding.iconUnSelected.setVisibility(View.GONE);
            resetIconYAxis(binding.iconSelected);
            binding.iconSelected.setVisibility(View.VISIBLE);
            binding.iconSelected.setAlpha(1);
            if (mCurrentSelectedIndex == position) {
                FlipAnimator.flipView(binding.getRoot().getContext(), binding.iconSelected,
                    binding.iconUnSelected, true);
                resetCurrentIndex();
            }
        } else {
            binding.iconSelected.setVisibility(View.GONE);
            resetIconYAxis(binding.iconUnSelected);
            binding.iconUnSelected.setVisibility(View.VISIBLE);
            binding.iconUnSelected.setAlpha(1);
            if ((mReverseAllAnimations && mAnimationItemsIndex.get(position,
                false))
                || mCurrentSelectedIndex == position) {
                FlipAnimator.flipView(binding.getRoot().getContext(), binding.iconSelected,
                    binding.iconUnSelected, false);
                resetCurrentIndex();
            }
        }
        binding.getRoot().setActivated(mSelectedConversation.contains(item));
    }

    public void toggleSelection(int pos, ConversationEntity item) {

        mCurrentSelectedIndex = pos;
        mSelectAllClicked = false;
        if (mSelectedConversation.contains(item)) {
            mSelectedConversation.remove(item);
            mAnimationItemsIndex.delete(pos);
        } else {
            mSelectedConversation.add(item);
            mAnimationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        mReverseAllAnimations = true;
        mSelectAllClicked = false;
        mSelectedConversation.clear();
        resetAnimationIndex();
    }

    public void notifySelectionClear() {

        if (getCurrentList() != null) {
            for (int position = 0; position <= mSelectedConversation.size() - 1; position++) {
                int index = getCurrentList().indexOf(mSelectedConversation.get(position));
                notifyItemChanged(index);
            }
            clearSelections();
        }
    }

    public int getSelectedItemCount() {
        return mSelectedConversation.size();
    }

    public List<ConversationEntity> getSelectedItems() {
        return mSelectedConversation;
    }

    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationY(0);
        }
    }

    public void resetAnimationIndex() {
        mReverseAllAnimations = false;
        mAnimationItemsIndex.clear();
    }

    private void resetCurrentIndex() {
        mCurrentSelectedIndex = -1;
    }

    @Override
    public long getItemId(int position) {
        if (hasStableIds()) {
            return getCurrentList().get(position).getConversationId();
        } else {
            return RecyclerView.NO_ID;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    /**
     * Method used to add all the list conversation entities when select all button clicked
     */
    public void addConversationToList(List<ConversationEntity> conversations) {
        mSelectedConversation.clear();
        mSelectAllClicked = true;
        mSelectedConversation.addAll(conversations);
        notifyDataSetChanged();
    }

    public class MessageViewHolder extends RecyclerViewPageListAdapter.RecyclerViewHolder {

        public MessageViewHolder(final AdapterRecentMessageItemBinding viewBinding) {
            super(viewBinding);
            viewBinding.itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //To handle Index bound of exception
                    if (getAdapterPosition() != -1) {
                        view.setTag(getItem(getAdapterPosition()));
                        mOnItemClickListener.onItemClick(view, getAdapterPosition());
                    }
                }
            });
            viewBinding.iconUnSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //To handle Index bound of exception
                    if (getAdapterPosition() != -1) {
                        view.setTag(getItem(getAdapterPosition()));
                        mOnItemClickListener.onItemClick(view, getAdapterPosition());
                    }
                }
            });


            viewBinding.itemContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    //To handle Index bound of exception
                    if (getAdapterPosition() != -1) {
                        view.setTag(getItem(getAdapterPosition()));
                        mOnItemClickListener.onItemLongClick(view, getAdapterPosition());
                    }
                    return true;
                }
            });
        }
    }
}
