package io.gupshup.smsapp.ui.home.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Ram Prakash Bhat on 9/4/18.
 */

public class MessageViewModel extends BaseFragmentViewModel {

    private static final String TAG = MessageViewModel.class.getSimpleName();
    public LiveData<PagedList<ConversationEntity>> messagesList;
    public LiveData<PagedList<ConversationEntity>> transactionalMessageList;
    public LiveData<PagedList<ConversationEntity>> promotionalMessageList;
    public LiveData<PagedList<ConversationEntity>> blockedMessageList;
    public LiveData<PagedList<ConversationEntity>> archivedMessageList;
    public ObservableField<String> displayName = new ObservableField<>();
    public ObservableField<String> displayPic = new ObservableField<>();


    public ObservableBoolean showPersonalFolder = new ObservableBoolean(false);
    public ObservableBoolean showTransactionalFolder = new ObservableBoolean(false);
    public ObservableBoolean showPromoFolder = new ObservableBoolean(false);
    public ObservableBoolean showBlockOption = new ObservableBoolean(true);
    public ObservableField<String> archiveText = new ObservableField<>();
    public ObservableField<String> blocText = new ObservableField<>();

    @Inject
    RxBus rxBus;

    public MessageViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void initMessageObserver(Context context, ConversationType conversationType) {

        ConversationDAO conversationDAO = AppDatabase.getInstance(context).conversationDAO();

        MessagingService.getInstance(SMSService.class).updatePNStatus();
        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setPageSize(PAGE_SIZE).build();

        messagesList = (new LivePagedListBuilder(conversationDAO.getRecent(conversationType), pagedListConfig))
            .build();
    }

    public void initArchivedMessageObserver() {

        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY) //* PAGE_SIZE
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setPageSize(PAGE_SIZE).build();

        archivedMessageList = (new LivePagedListBuilder(
            MessagingService.getInstance(SMSService.class).getArchivedConversations(),
            pagedListConfig))
            .build();
    }
}
