package io.gupshup.smsapp.ui.base.view;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */

public enum ViewType {

    ITEM(0), FOOTER(1), HEADER(2), MSG_SENDER(3), MSG_RECIEPENT(4), MSG_HEADER(5);
    private final int value;

    ViewType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ViewType valueOf(int value) {
        for (ViewType view : ViewType.values()) {
            if (view.value == value)
                return view;
        }
        return FOOTER;
    }
}