package io.gupshup.smsapp.ui.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.ui.home.view.MessageListFragment;
import io.gupshup.smsapp.utils.AppConstants;

/**
 * Created by Ram Prakash Bhat on 6/4/18.
 */

public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private List<String> mTabsList;

    public HomePagerAdapter(FragmentManager fm, List<String> tabsList) {
        super(fm);
        mTabsList = tabsList;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new MessageListFragment();

        ConversationType type;
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                type = ConversationType.PERSONAL;
                break;
            case 1:
                type = ConversationType.TRANSACTIONAL;
                //ConversationType OTP considered as TRANSACTIONAL.
                break;
            case 2:
                type = ConversationType.PROMOTIONAL;
                break;
            case 3:
                type = ConversationType.BLOCKED;
                break;
            default:
                type = ConversationType.PERSONAL;

        }
        bundle.putSerializable(AppConstants.BundleKey.CONVERSATION_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mTabsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabsList.get(position);
    }
}