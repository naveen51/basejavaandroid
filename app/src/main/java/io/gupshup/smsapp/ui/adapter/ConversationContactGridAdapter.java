package io.gupshup.smsapp.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.databinding.GridItemLytBinding;
import io.gupshup.smsapp.ui.base.adapter.RecyclerViewAdapter;
import io.gupshup.smsapp.ui.base.view.ViewType;
import io.gupshup.smsapp.ui.compose.viewmodel.ContactViewModel;
import io.gupshup.smsapp.utils.ContactSingleton;

/**
 * Created by Naveen BM on 4/11/2018.
 */
public class ConversationContactGridAdapter extends RecyclerViewAdapter<Contact> {

    private ArrayList<Contact> mContactList = new ArrayList<>();
    private ArrayList<Contact> contactList = new ArrayList<>();

    public ConversationContactGridAdapter(ArrayList<Contact> mContactList) {
        super(mContactList);
        this.mContactList.addAll(mContactList);
        this.contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (ViewType.valueOf(viewType)) {
            case ITEM:
                GridItemLytBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.grid_item_lyt, parent, false);
                return new ContactGridViewHolder(binding);
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Contact item = getItemAtPosition(position);
        switch (item.viewType) {

            case ITEM:
                ContactGridViewHolder viewHolder = (ContactGridViewHolder) holder;
                ContactViewModel contactViewModel = new ContactViewModel();
                // item.setSelected(false);
                //select and unselct icon
                if (contactList.contains(item)) {
                    contactViewModel.isIconSelected.set(true);
                    item.setSelected(true);
                } else {
                    contactViewModel.isIconSelected.set(false);
                    item.setSelected(false);
                }
                ((GridItemLytBinding) viewHolder.getBinding()).setContactItem(item);
                ((GridItemLytBinding) viewHolder.getBinding()).setContactViewModel(contactViewModel);
                viewHolder.getBinding().getRoot().setTag(item);
                break;

        }
        super.onBindViewHolder(holder, position);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (mContactList != null) {
            return mContactList.size();
        }
        return 0;
    }

    @Override
    public void onDestroy() {

    }


    public void notifySelectedContactList(Contact contact) {
        contactList.clear();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
        if (mContactList.contains(contact)) {
            int index = mContactList.indexOf(contact);
            notifyItemChanged(index, contact);
        }
    }

    public void updateGridData(ArrayList<Contact> contacts) {
        contactList.clear();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
        if (mContactList != null) {
            mContactList.clear();
            mContactList.addAll(contacts);
            notifyDataSetChanged();
        }
    }

    public class ContactGridViewHolder extends RecyclerViewAdapter.RecyclerViewHolder {
        public ContactGridViewHolder(GridItemLytBinding itemView) {
            super(itemView);
            itemView.iconSelected.setVisibility(View.GONE);
        }

    }

    public void updateGridSingletonLocalContactList() {
        contactList.clear();
        contactList.addAll(ContactSingleton.getInstance().getSelectedContacts().values());
    }
}
