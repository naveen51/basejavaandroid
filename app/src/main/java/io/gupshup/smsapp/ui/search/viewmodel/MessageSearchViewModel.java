package io.gupshup.smsapp.ui.search.viewmodel;

import android.app.Application;
import android.arch.paging.PagedList;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.database.entities.GlobalSearchEntity;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Ram Prakash Bhat on 31/5/18.
 */

public class MessageSearchViewModel extends BaseFragmentViewModel {

    public ObservableField<String> displayName = new ObservableField<>();
    public ObservableField<String> displayPic = new ObservableField<>();

    public MessageSearchViewModel(@NonNull Application application) {
        super(application);
    }

    public PagedList<GlobalSearchEntity> getSearchedData(String query, Context context) {
        return MessagingService.getInstance(SMSService.class).globalSearch(query);
    }
}
