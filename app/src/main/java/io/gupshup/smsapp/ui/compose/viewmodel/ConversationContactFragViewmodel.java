package io.gupshup.smsapp.ui.compose.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PagedList;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 4/11/2018.
 */
public class ConversationContactFragViewmodel extends BaseFragmentViewModel {

    public ObservableBoolean showHideGrid = new ObservableBoolean(true);
    private ArrayList<Contact> mContactList;
    public ObservableBoolean mPlusShow = new ObservableBoolean(false);
    private LiveData<PagedList<ContactModel>> mPersonalContact = new MutableLiveData<>();
    public ObservableBoolean numericKeyborad = new ObservableBoolean(true);

    public ObservableBoolean getShowHideGrid() {
        return showHideGrid;
    }

    @Inject
    public RxBus rxBus;

    public ConversationContactFragViewmodel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        LogUtility.d("tag", "onTextChanged " + s);
        if (s.length() > 0) {  //AMA-918 Fix removed threshold of 3 characters.
            //handle grid visibility
            rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_GRID_BEHAVIOUR_EVENT, false));
            if (Utils.isValidPhoneNumber(s.toString())) {
                mPlusShow.set(true);
            } else {
                mPlusShow.set(false);
            }
            showHideGrid.set(false);
        } else {
            //handle grid visibility
            rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_GRID_BEHAVIOUR_EVENT, true));
            showHideGrid.set(true);
            mPlusShow.set(false);
        }
        rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_SEARCH_FILTER, s.toString()));

    }
}
