package io.gupshup.smsapp.ui.settings.viewmodel;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 6/26/2018.
 */
public class SettingsFragViewModel extends BaseFragmentViewModel {

    @Inject
    public RxBus rxBus;
    public ObservableField<String> mDefaultSmsAppName = new ObservableField<>();
    public ObservableField<String> mSimOneMobileNumber = new ObservableField<>(AppConstants.EMPTY_TEXT);
    public ObservableField<String> mSimTwoMobileNumber = new ObservableField<>(AppConstants.EMPTY_TEXT);
    public ObservableBoolean mSimOneVisibility = new ObservableBoolean(false);
    public ObservableBoolean mSimTwoVisibility = new ObservableBoolean(false);
    public ObservableField<String> mSimOneRightButtonText = new ObservableField<>(AppConstants.EMPTY_TEXT);
    public ObservableField<String> mSimTwoRightButtonText = new ObservableField<>(AppConstants.EMPTY_TEXT);
    public ObservableField<Drawable> mSimOneButtonBg = new ObservableField<>();
    public ObservableField<Drawable> mSimTwoButtonBg = new ObservableField<>();

    public SettingsFragViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    /**
     * Handling about click
     *
     * @param view
     */
    public void onAboutClicked(View view) {
        LogUtility.d("ABOUTCLICK", "------ About Clicked -----");
        RxEvent event = new RxEvent(AppConstants.RxEventName.ABOUT_FRAGMENT_EVENT, null);
        rxBus.send(event);
    }

    /**
     * Handling notification settings click
     *
     * @param view
     */
    public void onNotificationSettingsClicked(View view) {
        LogUtility.d("ABOUTCLICK", "------ About Clicked -----");
        RxEvent event = new RxEvent(AppConstants.RxEventName.NOTIFICATION_FRAGMENT_EVENT, null);
        rxBus.send(event);
    }

}
