package io.gupshup.smsapp.ui.registration.viewmodel;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Naveen BM on 9/7/2018.
 */
public class ReRegistrationViewModel extends BaseFragmentViewModel {
    private static final String TAG = ReRegistrationViewModel.class.getSimpleName();
    public ObservableBoolean mSimOneContainerVisibility = new ObservableBoolean(true);
    public ObservableBoolean mSimTwoContainerVisibility = new ObservableBoolean(true);
    public ObservableBoolean mDividerVisibility = new ObservableBoolean(true);
    public ObservableField<String> mEditTextOne = new ObservableField<>();
    public ObservableField<String> mEditTextTwo = new ObservableField<>();

    public ObservableBoolean mEditTextOneEnable = new ObservableBoolean();
    public ObservableBoolean mEditTextTwoEnable = new ObservableBoolean();

    public ObservableField<String> mSimOneHeaderOne = new ObservableField<>();
    public ObservableField<String> mSimOneHeaderTwo = new ObservableField<>();
    public ObservableField<String> mSimTwoHeaderOne = new ObservableField<>();
    public ObservableField<String> mSimTwoHeaderTwo = new ObservableField<>();
    public ObservableBoolean mNextButtonEnable = new ObservableBoolean(false);
    public ObservableField<Float> mNextButtonAlpha = new ObservableField<>(0.4f);

    public ObservableField<String> mPreviousSimOneNumber = new ObservableField<>();
    public ObservableField<String> mPreviousSimTwoNumber = new ObservableField<>();

    public ObservableBoolean mIsSimOneEdited = new ObservableBoolean(false);
    public ObservableBoolean mIsSimTwoEdited = new ObservableBoolean(false);

    public ObservableField<String> mSimOneRegistrationError = new ObservableField<>();
    public ObservableField<String> mSimTwoRegistrationError = new ObservableField<>();

    public ObservableBoolean mSimOneErrorVisibility = new ObservableBoolean(false);
    public ObservableBoolean mSimTwoErrorVisibility = new ObservableBoolean(false);


    public ReRegistrationViewModel(@NonNull Application application) {
        super(application);
    }


    public void onTextChangedEditOne(CharSequence charSequence, int start, int before, int count) {
        if ((!TextUtils.isEmpty(mEditTextTwo.get()) && mEditTextTwo.get().length() >= 10) || charSequence.length() >= 10) {
            mNextButtonAlpha.set(1.0f);
            mNextButtonEnable.set(true);
        } else {
            mNextButtonAlpha.set(0.4f);
            mNextButtonEnable.set(false);
        }


    }

    public void onHandleButtonColor() {
        if (((!TextUtils.isEmpty(mEditTextOne.get()) && mEditTextOne.get().length() >= 10) ||
            (!TextUtils.isEmpty(mEditTextTwo.get()) && mEditTextTwo.get().length() >= 10))) {
            mNextButtonAlpha.set(1.0f);
            mNextButtonEnable.set(true);
        } else {
            mNextButtonAlpha.set(0.4f);
            mNextButtonEnable.set(false);
        }
    }

    public void onTextChangedEditTwo(CharSequence charSequence, int start, int before, int count) {
        if ((!TextUtils.isEmpty(mEditTextOne.get()) && mEditTextOne.get().length() >= 10) || charSequence.length() >= 10) {
            mNextButtonAlpha.set(1.0f);
            mNextButtonEnable.set(true);
        } else {
            mNextButtonAlpha.set(0.4f);
            mNextButtonEnable.set(false);
        }


    }

}
