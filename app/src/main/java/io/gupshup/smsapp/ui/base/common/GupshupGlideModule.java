package io.gupshup.smsapp.ui.base.common;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */
@GlideModule
public class GupshupGlideModule extends AppGlideModule {
}
