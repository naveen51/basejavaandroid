package io.gupshup.smsapp.ui.base.adapter;

import android.arch.paging.PagedListAdapter;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Ram Prakash Bhat on 17/4/18.
 */

public abstract class RecyclerViewPageListAdapter<T, V extends RecyclerView.ViewHolder> extends PagedListAdapter<T, V> {

    public interface onItemClickListener {

        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);

        void onSelectAllAndScroll();
    }

    protected onItemClickListener mOnItemClickListener;

    protected RecyclerViewPageListAdapter(@NonNull DiffUtil.ItemCallback<T> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public V onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //TODO: override in child class
        throw new RuntimeException("Invalid adapter view type in " + getClass().getName());
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {

        //TODO: override in child class
    }

    public abstract static class RecyclerViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
        private ViewDataBinding binding;

        public RecyclerViewHolder(ViewDataBinding viewBinding) {
            super(viewBinding.getRoot());
            binding = viewBinding;
        }

        public ViewDataBinding getBinding() {
            return binding;

        }

        @Override
        public void onClick(View v) {

        }
    }

    public abstract void onDestroy();
}
