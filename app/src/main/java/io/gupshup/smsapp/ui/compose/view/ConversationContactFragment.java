package io.gupshup.smsapp.ui.compose.view;

import android.content.Context;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.databinding.ConversationContactFragBinding;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.adapter.ConversationContactGridAdapter;
import io.gupshup.smsapp.ui.adapter.CustomCursorRecyclerViewAdapter;
import io.gupshup.smsapp.ui.base.common.BaseHandlers;
import io.gupshup.smsapp.ui.compose.viewmodel.ConversationContactFragViewmodel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 4/10/2018.
 */

public class ConversationContactFragment extends Fragment implements BaseHandlers, RxBusCallback {

    private static final String TAG = "CONVERSATIONCONTACTFRAGMENT";
    private ConversationContactFragBinding mBinding;
    private RecyclerView mRecyclerView, mRecyclerGrid;
    private ConversationContactFragViewmodel mViewModel;
    private ConversationContactGridAdapter mGridAdapter;
    private LinearLayout mContainerLinearLyt;
    private EditText mEditText;
    private RxBusHelper rxBusHelper;
    private HorizontalScrollView mHorizontalScrollView;
    private boolean isKeyboardVisible;
    private ArrayList<Contact> mTopContactsList;
    private CustomCursorRecyclerViewAdapter mAdapter;

    String[] projection = new String[]{
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
        ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI,
        ContactsContract.CommonDataKinds.Email.CONTACT_ID,
        ContactsContract.CommonDataKinds.Email.ADDRESS
        //plus any other properties you wish to query
    };

    Cursor mCursor = null;

    @Inject
    public RxBus rxBus;
    private Menu mMenu;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainApplication.getInstance().getAppComponent().inject(this);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.conversation_contact_frag, container, false);
        registerEvents();
        mBinding.setHandlers(this);
        mViewModel = new ConversationContactFragViewmodel(MainApplication.getInstance());
        mBinding.setContactFragViewModel(mViewModel);
        mTopContactsList = new ArrayList<>();
        initializeComponentViews();
        LogUtility.d(TAG, "------ Contact Fragment ------");
        detectKeyboardVisibility(mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            mCursor = getActivity().getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                    null, null,
                    ContactsContract.Contacts.SORT_KEY_PRIMARY + " ASC");
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        getContactFromPhone();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_compose, menu);
        mMenu = menu;
        addContactToContainer();
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_navigate) {
            //after adding navigate to conversation clicked
            rxBus.send(new RxEvent(AppConstants.EVENT_NAVIGATION_CLICKED, null));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void handleNavigation(boolean arrowVisibility) {
        if (mMenu != null) {
            mMenu.findItem(R.id.action_navigate).setVisible(arrowVisibility);
        }
    }

    /**
     * Method used to get top contacts as well as contacts from phone,
     * Top contacts will be fetched based on preferences AMA-931
     */
    private void getContactFromPhone() {
        LogUtility.i("ContactsDebug", "getContactFromPhone");
        List<ContactModel> mContactTopList = MessagingService.getInstance(SMSService.class).getTopContactsNew();
        if (mContactTopList != null && mContactTopList.size() > 0) {
            mTopContactsList.clear();
            for (ContactModel model : mContactTopList
                ) {

                Contact contact = new Contact();
                contact.setmContactPhoneNumber(model.getOriginalPhoneNumber());
                contact.setmContactThumbNail(model.getImage());
                contact.setmContactName(model.getDisplay() == null ? model.getMask() : model.getDisplay());
                mTopContactsList.add(contact);
            }

            if (mGridAdapter == null) {
                mGridAdapter = new ConversationContactGridAdapter(mTopContactsList);
                mRecyclerGrid.setAdapter(mGridAdapter);
            } else {
                mGridAdapter.updateGridData(mTopContactsList);
            }
        }

        mAdapter = new CustomCursorRecyclerViewAdapter(getActivity(), mCursor);
        mRecyclerView.setAdapter(mAdapter);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Method used to initialize the view components
     */
    private void initializeComponentViews() {
        mRecyclerView = mBinding.contactVerticalRecycler;
        setScrollingBehaviour(true);
        mRecyclerGrid = mBinding.gridRecycler;
        mContainerLinearLyt = mBinding.contactContainer;
        mEditText = mBinding.contactEditText;
        mHorizontalScrollView = mBinding.horizontalScrollView;
        mHorizontalScrollView.postDelayed(new Runnable() {
            public void run() {
                mHorizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        }, 100L);
        if (getActivity() != null) {
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(manager);
            int numberOfColumns = 4;
            GridLayoutManager gridLytManager = new GridLayoutManager(getActivity(), numberOfColumns);
            mRecyclerGrid.setLayoutManager(gridLytManager);
        }
        mEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //Identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return false;
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    deleteContact();
                }
                return false;
            }
        });


        mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId & EditorInfo.IME_MASK_ACTION) != 0) {
                    LogUtility.d("ENTER", "---- ACTION DONE ----");
                    addmanuallyEnteredContact();
                    return false;
                } else {
                    return false;
                }
            }
        });


    }


    /**
     * Method used to show keyboard based on the INPUT TYPE
     */
    private void showSoftKeyboard() {
        if (getActivity() != null) {
            InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (!isKeyboardVisible) {
                inputMethodManager.toggleSoftInputFromWindow(
                    mBinding.contactEditText.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
            }

            if (mEditText.getInputType() == InputType.TYPE_CLASS_TEXT) {
                mViewModel.numericKeyborad.set(false);
                mEditText.setInputType(InputType.TYPE_CLASS_PHONE);
            } else {
                mViewModel.numericKeyborad.set(true);
                mEditText.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }
    }


    /**
     * Method used to check if keyboard is open or closed,
     * It will help when user try to change keyboard INPUT TYPE
     * @param contentView
     */
    private void detectKeyboardVisibility(final View contentView) {
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;


                if (keypadHeight > (screenHeight * 0.15)) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    isKeyboardVisible = true;
                } else {
                    // keyboard is closed
                    isKeyboardVisible = false;
                }

            }
        });

    }

    @Override
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.key_board_image:
                mBinding.contactEditText.requestFocus();
                showSoftKeyboard();
                break;

            case R.id.plus_image:
                DialogUtils.dismissKeyboard(getActivity(), mBinding.contactEditText);

                //When user enters some text which may be a new phone numbe dat case we construct new contact
                addmanuallyEnteredContact();
                break;

        }
    }

    /**
     * Method used to add manually entered contacts on the top bar
     */
    private void addmanuallyEnteredContact() {
        String phone = mEditText.getText().toString();
        if (Utils.isValidPhoneNumber(phone)) {
            Contact contact = new Contact();
            contact.setmContactName(phone);
            contact.setmContactPhoneNumber(phone);
            LogUtility.d(TAG, "-- Contact Clicked -------- " + contact.getContactName());
            contact.setFromPlus(true);
            ContactSingleton.getInstance().addContact(contact);
            addContactToContainer();
            if (ContactSingleton.getInstance().getSelectedContacts().size() == 1) {
                rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_SELECTED, null));
            }
        }
    }

    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }


    @Override
    public void onEventTrigger(Object object) {
        RxEvent event = (RxEvent) object;
        if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_CONTACT_SEARCH_FILTER)) {
            String searchFilter = (String) event.getEventData();
            if (mAdapter != null) {
                if (!TextUtils.isEmpty(searchFilter)) {
                    Cursor cursor = null;
                    try {
                        String[] selectionArgs = {"%" + searchFilter + "%", "%" + searchFilter + "%"};
                        cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE ? OR " + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ?", selectionArgs,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                    mAdapter.updateCursor(cursor);
                } else {
                    mAdapter.updateCursor(mCursor);
                }

            }
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_CONTACT_GRID_BEHAVIOUR_EVENT)) {
            boolean isShow = (boolean) event.getEventData();
            setScrollingBehaviour(isShow);
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.DELETE_CONTACT)) {
            Contact contact = (Contact) event.getEventData();
            Map<String, Contact> contactList = ContactSingleton.getInstance().getSelectedContacts();
            if (contact != null) {
                Iterator<Map.Entry<String, Contact>> it = contactList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Contact> pair = it.next();
                    Contact contactSingle = pair.getValue();
                    if (contactSingle != null) {
                        if ((contact.getContactPhoneNumber() != null &&
                            contact.getContactPhoneNumber().toLowerCase().equalsIgnoreCase(contactSingle.getContactPhoneNumber()))) {
                            it.remove();
                        }
                    }
                }
                addContactToContainer();
                updateAdapter(contact);

            }
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.UPDATE_CONTACT_TOP_BAR)) {
            addContactToContainer();
        } else if (event.getEventName().equalsIgnoreCase(AppConstants.EVENT_NOTIFY_ADAPTER)) {
            Contact contact = (Contact) event.getEventData();
            notifyAdapter(contact);
        }
    }

    /**
     * Method used to notify adapter after each remove of the contact at the top bar
     */
    private void updateAdapter(Contact contact) {
        if (mAdapter != null) {
            mAdapter.notifySelectedContactList(contact);
        }

        if (mGridAdapter != null) {
            mGridAdapter.notifySelectedContactList(contact);
        }
    }

    @Override
    public void onDestroyView() {
        unregisterEvents();
        super.onDestroyView();
    }


    /**
     * Method used to add contacts to top bar container
     */
    public void addContactToContainer() {
        if (mEditText.getText().length() > 0) {
            mEditText.setText("");
        }
        mContainerLinearLyt.removeAllViews();
        ArrayList<Contact> contactList = new ArrayList<>(ContactSingleton.getInstance().getSelectedContacts().values());
        if (contactList.size() > 0) {
            handleNavigation(true);
            for (int i = 0; i < contactList.size(); i++) {
                Contact contact = contactList.get(i);
                if (contact != null) {
                    View bubleView = null;
                    if (contact.getContactName() != null && !TextUtils.isEmpty(contact.getContactPhoneNumber())) {
                        bubleView = AppUtils.createContactTextView(contact, getActivity(), mContainerLinearLyt);
                    } else if (contact.getContactPhoneNumber() != null && !TextUtils.isEmpty(contact.getContactPhoneNumber())) {
                        bubleView = AppUtils.createContactTextView(contact, getActivity(), mContainerLinearLyt);
                    }

                    if (bubleView != null) {
                        mContainerLinearLyt.addView(bubleView);
                    }
                }

            }
        } else {
            if (ContactSingleton.getInstance().getSelectedContacts().size() == 0) {
                rxBus.send(new RxEvent(AppConstants.HANDLE_TOOLBAR_CONTENT_EVENT, null));
                handleNavigation(false);
            }
        }
    }


    public void setScrollingBehaviour(boolean isShow) {

        CoordinatorLayout.LayoutParams params =
            (CoordinatorLayout.LayoutParams) mRecyclerView.getLayoutParams();
        if (isShow) {
            params.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        } else {
            params.setBehavior(null);
        }

        mRecyclerView.requestLayout();
    }

    /**
     * Notify perticular adapter
     */
    private void notifyAdapter(Contact contact) {
        if (contact.getClickedFrom() == 1) {
            if (mAdapter != null) {
                if (mGridAdapter != null) {
                    mGridAdapter.updateGridSingletonLocalContactList();
                }
                mAdapter.notifySelectedContactList(contact);
            }

        } else if (contact.getClickedFrom() == 2) {
            if (mGridAdapter != null) {
                if (mAdapter != null) {
                    mAdapter.updateSingletonLocalContactList();
                }
                mGridAdapter.notifySelectedContactList(contact);
            }
        }
    }

    /**
     * Method used to delete contact from list and update views accordingly
     */
    private void deleteContact() {
        Map<String, Contact> contactList = ContactSingleton.getInstance().getSelectedContacts();
        if (contactList.size() > 0) {
            Contact contact = null;
            for (Map.Entry<String, Contact> pair : contactList.entrySet()) {
                contact = pair.getValue();
            }
            contactList.remove(contact.getContactPhoneNumber());
            addContactToContainer();
            updateAdapter(contact);
        }
    }

}
