package io.gupshup.smsapp.ui.base.common;

import android.view.View;

public interface BaseHandlers {
     void onViewClick(View view);
}
