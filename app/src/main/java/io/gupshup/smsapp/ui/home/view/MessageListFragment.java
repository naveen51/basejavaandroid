package io.gupshup.smsapp.ui.home.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.databinding.FragmentMessageListingBinding;
import io.gupshup.smsapp.enums.ActionModeType;
import io.gupshup.smsapp.enums.ConversationType;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.adapter.MessageListAdapter;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.home.viewmodel.MessageViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.SwipeItemTouchHelperCallback;
import io.gupshup.smsapp.utils.UiUtils;

/**
 * Created by Ram Prakash Bhat on 9/4/18.
 */

public class MessageListFragment extends BaseFragment<FragmentMessageListingBinding,
    MessageViewModel> implements ActionMode.Callback, /*SwipeHandler,*/ SwipeItemTouchHelperCallback.OnItemSwipeListener/*, RxBusCallback*/ {

    private static final String TAG = MessageListFragment.class.getSimpleName();
    private MessageListAdapter mMessageListAdapter;

    private ActionMode mActionMode;
    private boolean mActionItemClicked;
    @Inject
    public RxBus rxBus;
    private ItemTouchHelper mItemTouchHelper;
    protected List<ConversationEntity> mConversationEntities;
    private boolean mShowMarkAsRead;
    protected boolean mIsScrollNeeded = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCurrentViewModel().initMessageObserver(getActivity(), getConversationTypeFromBundle());
        mMessageListAdapter = new MessageListAdapter(this);
        final LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
            false);
        getCurrentViewModel().messagesList.observe(this, new Observer<PagedList<ConversationEntity>>() {
            @Override
            public void onChanged(@Nullable PagedList<ConversationEntity> conversationEntities) {
                getCurrentViewModel().showHideEmptyView(conversationEntities.size() <= 0,
                    getString(R.string.personal_inbox_empty));
                mMessageListAdapter.submitList(conversationEntities);
                if (manager.findFirstCompletelyVisibleItemPosition() == 0
                    || manager.findFirstCompletelyVisibleItemPosition() == 1) {
                    //this is the top of the RecyclerView
                    mBinding.recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBinding.recyclerView.scrollToPosition(0);
                        }
                    }, 200);
                }
            }
        });
        mBinding.recyclerView.setLayoutManager(manager);
        mMessageListAdapter.setHasStableIds(true);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mBinding.recyclerView.setAdapter(mMessageListAdapter);
        swipeCallback = new SwipeItemTouchHelperCallback
            .Builder(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
            .setSwipeEnabled(true)
            .onItemSwipeLeftListener(this)
            .onItemSwipeRightListener(this)
            .build();
        mItemTouchHelper = new ItemTouchHelper(swipeCallback);
        mItemTouchHelper.attachToRecyclerView(mBinding.recyclerView);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ConversationDAO conversationDAO = AppDatabase.getInstance(getActivity()).conversationDAO();
        ConversationType type = getConversationTypeFromBundle();
        conversationDAO.getUnreadMessages(type)
            .observe(this, new Observer<List<ConversationEntity>>() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onChanged(@Nullable List<ConversationEntity> conversationEntities) {
                    mConversationEntities = conversationEntities;
                    mShowMarkAsRead = mConversationEntities != null && mConversationEntities.size() > 0;
                    setMarkAsReadVisible();
                }
            });
    }


    @Override
    public int getBindingVariable() {
        return BR.personalMessageViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_message_listing;
    }

    @Override
    public MessageViewModel getViewModel() {
        return ViewModelProviders.of(this).get(MessageViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsScrollNeeded) {
            if (mBinding != null
                && mBinding.recyclerView != null) {
                mBinding.recyclerView.smoothScrollToPosition(0);
            }
        }
        mIsScrollNeeded = true;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.menu_base, menu);
        mMenu = menu;
        menu.findItem(R.id.action_mark_as_read).setVisible(mShowMarkAsRead);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_SEARCH_VIEW, View.VISIBLE);
                rxBus.send(event);
                return true;
            case R.id.action_mark_as_read:
                for (ConversationEntity entity : mConversationEntities) {
                    MessagingService.getInstance(SMSService.class).
                        readConversation(entity.getMask(),
                            DateUtils.currentTimestamp());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
        if (getConversationTypeFromBundle() == ConversationType.BLOCKED) {
            menu.findItem(R.id.action_block).setVisible(false);
            menu.findItem(R.id.action_un_block).setVisible(true);
        } else {
            menu.findItem(R.id.action_block).setVisible(true);
            menu.findItem(R.id.action_un_block).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

        mActionItemClicked = true;
        mActionMode = mode;
        if (getCurrentViewModel().messagesList == null) return false;
        PagedList<ConversationEntity> personalMessageList = getCurrentViewModel()
            .messagesList.getValue();
        List<ConversationEntity> selectedConversation = mMessageListAdapter.getSelectedItems();
        switch (item.getItemId()) {
            case R.id.action_block:
                // block all the selected messages
                updateConversation(ActionModeType.BLOCKED, personalMessageList, selectedConversation);
                return true;
            case R.id.action_un_block:
                // un block all the selected messages
                updateConversation(ActionModeType.UN_BLOCK, personalMessageList, selectedConversation);
                return true;
            case R.id.action_archive:
                // archive all the selected messages
                updateConversation(ActionModeType.ARCHIVE, personalMessageList, selectedConversation);
                return true;

            case R.id.action_mute:
                // mute all the selected messages
                updateConversation(ActionModeType.MUTE, personalMessageList, selectedConversation);
                return true;

            case R.id.action_un_mute:
                // mute all the selected messages
                updateConversation(ActionModeType.UN_MUTE, personalMessageList, selectedConversation);
                return true;

            case R.id.action_delete:
                // delete all the selected messages
                mActionItemClicked = false;
                updateConversation(ActionModeType.DELETE, personalMessageList, selectedConversation);
                return true;

            case R.id.action_add_contact:
                // add the selected messages
                updateConversation(ActionModeType.ADD_TO_CONTACT, personalMessageList, selectedConversation);
                mode.finish();
                return true;

            case R.id.action_move:
                // move all the selected messages
                showMoveDialogBasedOnConversationType();
                updateConversation(ActionModeType.MOVE, personalMessageList, selectedConversation);
                mActionItemClicked = false;
                return true;

            case R.id.action_mark_as_read:
                // mark as read all the selected messages
                updateConversation(ActionModeType.MARK_AS_READ, personalMessageList, selectedConversation);
                return true;

            case R.id.action_select_all:
                mActionItemClicked = false;
                mSelectAllClicked = true;
                selectAllMessages(mMessageListAdapter, getCurrentViewModel()
                    .messagesList.getValue(), mActionMode, getConversationTypeFromBundle()); //ConversationType.PERSONAL
                return true;

            case R.id.action_deselect_all:
                //mActionItemClicked = false;
                mSelectAllClicked = false;
                deselectAllMessages();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

        mSelectAllClicked = false;
        mUnreadMessageCount = 0;
        if (!mActionItemClicked) {
            mMessageListAdapter.notifySelectionClear();
        }
        mActionItemClicked = false;
        mMessageListAdapter.clearSelections();
        mActionMode = null;
        swipeCallback.setSwipeEnabled(true);
        RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_HIDE_VIEW, View.VISIBLE);
        rxBus.send(event);
    }

    @Override
    public void clearSelectionAndActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    protected void notifySelectionClear() {
        super.notifySelectionClear();
        if (mMessageListAdapter != null) {
            mMessageListAdapter.notifySelectionClear();
        }
        clearSelectionAndActionMode();

    }

    private void enableActionMode(int position, ConversationEntity item) {
        if (mActionMode == null) {
            if (getActivity() != null) {
                mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this);
            } else {
                return;
            }
        }
        toggleSelection(position, item);
    }


    private void toggleSelection(int position, ConversationEntity item) {

        mMessageListAdapter.toggleSelection(position, item);
        int count = mMessageListAdapter.getSelectedItemCount();
        boolean isSelected;
        if (mMessageListAdapter.getSelectedItems().contains(item)) {
            isSelected = true;
        } else {
            isSelected = false;
        }
        Menu menu = mActionMode.getMenu();
        actionModeSetup(count, mActionMode, getCurrentViewModel().messagesList.getValue());
        if (count == 0) {
            mUnreadMessageCount = 0;
            mActionMode.finish();
            swipeCallback.setSwipeEnabled(true);
            RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_HIDE_VIEW, View.VISIBLE);
            rxBus.send(event);
        } else {
            RxEvent event = new RxEvent(AppConstants.RxEventName.SHOW_HIDE_VIEW, View.GONE);
            rxBus.send(event);
            handleActionMenuForMultiItem(position, count, menu, null, item, isSelected);
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
        if (count > 0) {
            swipeCallback.setSwipeEnabled(false);
        }
    }

    @Override
    public void onItemSwipedLeft(int position) {
        if (mMessageListAdapter != null && mMessageListAdapter.getCurrentList() != null) {
            ConversationEntity conversationEntity = mMessageListAdapter.getCurrentList().get(position);
            if (conversationEntity != null) {
                if (conversationEntity.isUnread()) {
                    try {
                        MessagingService.getInstance(SMSService.class).
                            readConversation(conversationEntity.getMask(),
                                DateUtils.currentTimestamp());
                    } catch (Exception e) {
                        LogUtility.d("PerMsgFrag LeftSwipe :", e.getMessage());
                    }
                } else {
                    MessagingService.getInstance(SMSService.class).markConversationAsUnread(conversationEntity.getMask());
                    final ConversationEntity ce = conversationEntity;
                    new AsyncTask<Void, Void, Void>() {
                        protected Void doInBackground(Void... unused) {
                            try {
                                AppUtils.updateConversationAsUnreadInDefaultSMSDb(MainApplication.getContext(), new ConversationEntity[]{ce});
                            } catch (Exception ex) {
                                //LogUtility.d("auto2", "Exception in updating read in default db table.."+ex.getMessage());
                            }
                            return null;
                        }
                    }.execute();
                }
            }
            // Workaround to reset swiped out views
            mItemTouchHelper.attachToRecyclerView(null);
            mItemTouchHelper.attachToRecyclerView(mBinding.recyclerView);
        }
    }

    @Override
    public void onItemSwipedRight(int position) {
        if (mMessageListAdapter != null && mMessageListAdapter.getCurrentList() != null) {
            List<ConversationEntity> conversationEntityList = new ArrayList<>();

            ConversationEntity conversation = mMessageListAdapter.getCurrentList().get(position);
            if (getConversationTypeFromBundle() == ConversationType.BLOCKED) {
                MessagingService.getInstance(SMSService.class).unBlockMessage(conversation);
                UiUtils.showToast(getActivity(), getString(R.string.unblocked_successfully));
            } else {
                showMoveDialogBasedOnConversationType();
                conversationEntityList.add(conversation);
                showMessageDetail(conversationEntityList);
            }
            // Workaround to reset swiped out views
            mItemTouchHelper.attachToRecyclerView(null);
            mItemTouchHelper.attachToRecyclerView(mBinding.recyclerView);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        int count = mMessageListAdapter.getSelectedItemCount();
        ConversationEntity item = (ConversationEntity) view.getTag();

        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated
        switch (view.getId()) {
            case R.id.item_container:
                if (count > 0) {
                    enableActionMode(position, item);
                } else {
                    MainActivity.hasCalledAnotherActivity = true;
                    mIsScrollNeeded = false;
                    LogUtility.d(TAG, "---- Item Clicked ---------");
                    AppUtils.launchMessageReplyActivity(view.getContext(), item, position, ConversationType.BLOCKED == item.getType());
                    getActivity().overridePendingTransition(R.anim.enter_anim_right_to_left, R.anim.exit_anim_left_to_right);
                }
                break;
            case R.id.icon_un_selected:
                if (count > 0) {
                    enableActionMode(position, item);
                } else {
                    MainActivity.hasCalledAnotherActivity = true;
                    mIsScrollNeeded = false;
                    if (Utils.getConversationType(item.getMask(), Utils.getLastMessageInConversation(item)) == ConversationType.PERSONAL) {
                        int phoneContactID = canAddToContact(item.getMask());
                        if (phoneContactID < 0) {
                            showAddToContactDialog(item.getMask());
                        } else if (Utils.getConversationType(item.getMask(), Utils.getLastMessageInConversation(item))
                            == ConversationType.PERSONAL) {
                            Utils.showContactDetail(getActivity(), item.getMask(), phoneContactID);
                        }
                    }
                }
                break;
        }
        //updateConversation(item);
        super.onItemClick(view, position);
    }

    @Override
    public void onItemLongClick(View view, int position) {
        super.onItemLongClick(view, position);
        ConversationEntity item = (ConversationEntity) view.getTag();
        if (item != null) {
            enableActionMode(position, item);
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
        }
    }

    @Override
    public void onSelectAllAndScroll() {
        if (mActionMode == null) return;
        int count = mMessageListAdapter.getSelectedItemCount();
        if (count > 0) {
            mActionMode.setTitle(String.valueOf(count));
        }
        mActionMode.invalidate();
        super.onSelectAllAndScroll();
    }

    private ConversationEntity updateConversation(ConversationEntity conversationEntity) {
        //LogUtility.d("GDC", "Exec updateConversation before");

        AppDatabase appDatabase = AppDatabase.getInstance();
        conversationEntity.setNoOfUnreadMessages(0);
        conversationEntity.setUnread(false);
        conversationEntity.setPNRequired(false);
        int noOfRowsUpdated = AppDatabase.getInstance().conversationDAO().update(conversationEntity);
        LogUtility.d("test", "the no of rows updated is :" + noOfRowsUpdated);
        return conversationEntity;
    }

    @Override
    public void onItemSwiped(int position) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.ADD_CONTACT_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                if (mMessageListAdapter != null) {
                    mMessageListAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void showMoveDialogBasedOnConversationType() {

        ConversationType conversationType = getConversationTypeFromBundle();

        getCurrentViewModel().showTransactionalFolder.set(conversationType != ConversationType.TRANSACTIONAL);
        getCurrentViewModel().showPromoFolder.set(conversationType != ConversationType.PROMOTIONAL);
        getCurrentViewModel().showPersonalFolder.set(conversationType != ConversationType.PERSONAL);
        getCurrentViewModel().archiveText.set(getString(R.string.archive));
        if (conversationType == ConversationType.BLOCKED) {
            getCurrentViewModel().blocText.set(getString(R.string.unblock));
        } else {
            getCurrentViewModel().blocText.set(getString(R.string.block));
        }
    }


}
