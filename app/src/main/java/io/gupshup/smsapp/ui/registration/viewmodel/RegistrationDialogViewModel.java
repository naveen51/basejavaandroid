package io.gupshup.smsapp.ui.registration.viewmodel;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.events.ManualRegistrationEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 6/11/2018.
 */
public class RegistrationDialogViewModel extends BaseFragmentViewModel {

    private static final String TAG = RegistrationDialogViewModel.class.getSimpleName();
    public ObservableBoolean mSimOneVisibility = new ObservableBoolean();
    public ObservableBoolean mSimTwoVisibility = new ObservableBoolean();
    public ObservableBoolean mTwoVerifiedText = new ObservableBoolean();
    public ObservableField<String> mSimOneEditTextPhoneNumber = new ObservableField<>("");
    public ObservableField<String> mSimTwoEditTextPhoneNumber = new ObservableField<>("");
    public ObservableField<String> mSimOneCountryCode = new ObservableField<>("");
    public ObservableField<String> mSimTwoCountryCode = new ObservableField<>("");
    public ObservableField<String> mVerificationTextValue = new ObservableField<>();
    public ObservableField<String> mHeaderTextValue = new ObservableField<>();

    public ObservableBoolean mSubmitButtonEnable = new ObservableBoolean(false);
    public ObservableField<Float> mSubmitButtonAlpha = new ObservableField<>(0.4f);

    @Inject
    public RxBus rxBus;

    public RegistrationDialogViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void handleSimOneVisibility(boolean isShow) {
        mSimOneVisibility.set(isShow);
    }

    public void handleSimTwoVisibility(boolean isShow) {
        mSimTwoVisibility.set(isShow);
    }

    /**
     * Method used to handle the submit button
     */
    public void submitClicked(View view) {
        view.setEnabled(false);
        String mSimOnePhone = mSimOneEditTextPhoneNumber.get();
        String mSimTwoPhone = mSimTwoEditTextPhoneNumber.get();
        DialogUtils.hideKeyboard(view.getContext());

        if (!TextUtils.isEmpty(mSimOnePhone) || !TextUtils.isEmpty(mSimTwoPhone)) {
            LogUtility.d(TAG, "--- First Phone ---" + mSimOnePhone + "    ------ Second Phone -----" + mSimTwoPhone);
            ManualRegistrationEvent manualRegEvent = new ManualRegistrationEvent(mSimOnePhone, mSimTwoPhone);
            RxEvent event = new RxEvent(AppConstants.MANUAL_REGISTRATION_SUBMIT_CLICK, manualRegEvent);
            rxBus.send(event);
        }
    }


    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        if (charSequence.length() >= 10) {
            mSubmitButtonAlpha.set(1.0f);
            mSubmitButtonEnable.set(true);
        } else {
            mSubmitButtonAlpha.set(0.4f);
            mSubmitButtonEnable.set(false);
        }

    }

    public void clearEditText() {
        mSimOneEditTextPhoneNumber.set(AppConstants.EMPTY_TEXT);
        mSimTwoEditTextPhoneNumber.set(AppConstants.EMPTY_TEXT);
    }
}
