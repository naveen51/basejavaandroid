package io.gupshup.smsapp.ui.reply.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppUtils;

/**
 * Created by Naveen BM on 4/20/2018.
 */
public class ReplyMessageReciepentViewModel extends BaseFragmentViewModel {
    public ObservableBoolean headerVisibility = new ObservableBoolean(false);
    public ObservableField<String> headerVal = new ObservableField<>();
    public ObservableField<String> displayPic = new ObservableField<>();
    public ObservableField<String> displayName = new ObservableField<>();
    public ObservableBoolean itemBg =  new ObservableBoolean();
    public ObservableInt themeColorForBg = new ObservableInt();
    @Inject
    RxBus rxBus;

    public ReplyMessageReciepentViewModel(MainApplication instance) {
        super(instance);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    public void checkPreviousItemHeader(long currentTs, long nextTimeStamp) {
        if (currentTs < 0 || nextTimeStamp < 0) {
            return;
        }
        if (nextTimeStamp == 0) {
            headerVisibility.set(true);
            String header = AppUtils.getFormattedTimeDate(currentTs, MainApplication.getContext(),true);
            //Item with header
            headerVal.set(header);


        } else {
            String curHeader = AppUtils.getFormattedTimeDate(currentTs, MainApplication.getContext(),true);
            String nextHeader = AppUtils.getFormattedTimeDate(nextTimeStamp, MainApplication.getContext(),true);

            if (curHeader.equalsIgnoreCase(nextHeader)) {
                //item goes under same header
                headerVisibility.set(false);

            } else {
                //item goes under new header
                headerVisibility.set(true);
                headerVal.set(curHeader);

            }
        }
    }

}
