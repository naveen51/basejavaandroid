package io.gupshup.smsapp.ui.registration.view;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.BR;
import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.RegistrationInfo;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.databinding.FragmentReRegistrationBinding;
import io.gupshup.smsapp.enums.RegistrationSimOneState;
import io.gupshup.smsapp.enums.RegistrationSimTwoState;
import io.gupshup.smsapp.events.ReRegistrationEvent;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.rxbus.RxBusCallback;
import io.gupshup.smsapp.rxbus.RxBusHelper;
import io.gupshup.smsapp.ui.base.view.BaseFragment;
import io.gupshup.smsapp.ui.registration.viewmodel.ReRegistrationViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 9/7/2018.
 */
public class ReRegistrationFragment extends BaseFragment<FragmentReRegistrationBinding, ReRegistrationViewModel> implements RxBusCallback {

    private static final String TAG = ReRegistrationFragment.class.getSimpleName();
    @Inject
    public RxBus rxBus;
    private RxBusHelper rxBusHelper;
    private ProgressDialog progressDlg;
    private int mNumberOfAPICall = 0;
    private int mRecievedCallBack = 0;


    @Override
    public int getBindingVariable() {
        return BR.reregistrationModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_re_registration;
    }

    @Override
    public ReRegistrationViewModel getViewModel() {
        return ViewModelProviders.of(this).get(ReRegistrationViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNumberOfAPICall = 0;
        mRecievedCallBack = 0;
        mBinding.setVariable(BR.handler, this);
        progressDlg = new ProgressDialog(this.getActivity());
        progressDlg.setMessage(getString(R.string.registration_in_progress));
        progressDlg.setCancelable(false);
        getCurrentViewModel().mSimOneErrorVisibility.set(false);
        getCurrentViewModel().mSimTwoErrorVisibility.set(false);
        registerEvents();
        setUpReRegisterUI();
    }

    private void setUpReRegisterUI() {

        List<SimInformation> simList = AppUtils.getSimInfoList();
        if (simList.size() > 0) {
            for (SimInformation simInformation : simList) {
                setInfoToUi(simInformation, simList);
            }
        }
    }


    private void setInfoToUi(SimInformation simInformation, List<SimInformation> simList) {

        RegistrationInfo registrationInfo = AppUtils.getSimDataBasedOnSlot(getActivity(),
            String.valueOf(simInformation.getSimSlotIndex()));
        if (registrationInfo != null) {
            if (registrationInfo.simSlot == 0) {
                setUpSimOneRegistrationUI(registrationInfo, simList.size());
            } else if (registrationInfo.simSlot == 1) {
                setUpSimTwoRegistrationUI(registrationInfo, simList.size());
            }
        } else if (simInformation.getSimSlotIndex() == 0) {
            setUpSimOneRegistrationUI(null, simList.size());
        } else if (simInformation.getSimSlotIndex() == 1) {
            setUpSimTwoRegistrationUI(null, simList.size());
        }
    }

    private void setUpSimOneRegistrationUI(RegistrationInfo registrationInfo, int size) {
        getCurrentViewModel().mSimOneContainerVisibility.set(true);
        if (registrationInfo != null && registrationInfo.getSimOneState() == RegistrationSimOneState.SIM_ONE_REGISTRATION_SUCCESS) {
            //set header text
            getCurrentViewModel().mSimOneHeaderOne.set(getString(R.string.re_register_current_number_sim_one));
            getCurrentViewModel().mSimOneHeaderTwo.set(getString(R.string.re_register_click_below));

            //already registration tried, prefill edit text with number
            getCurrentViewModel().mEditTextOne.set(registrationInfo.phoneNumber);
            getCurrentViewModel().mPreviousSimOneNumber.set(registrationInfo.phoneNumber);
            getCurrentViewModel().mEditTextOneEnable.set(false);

            if (!TextUtils.isEmpty(registrationInfo.phoneNumber)) {
                //handle button enable disable
                getCurrentViewModel().onHandleButtonColor();
            }

        } else {
            //to handle null exception
            getCurrentViewModel().mPreviousSimOneNumber.set(AppConstants.EMPTY_TEXT);

            // set sim one header text
            getCurrentViewModel().mSimOneHeaderOne.set(getString(R.string.re_register_current_number_sim_one_not_register));
            getCurrentViewModel().mSimOneHeaderTwo.set(getString(R.string.re_register_click_on_it));
            getCurrentViewModel().mEditTextOneEnable.set(true);
            getCurrentViewModel().mIsSimOneEdited.set(true);
        }

        //if device has only one sim, and slot index is zero then dont show second sim container
        if (size == 1) {
            getCurrentViewModel().mDividerVisibility.set(false);
            getCurrentViewModel().mSimTwoContainerVisibility.set(false);
        } else {
            getCurrentViewModel().mDividerVisibility.set(true);
            getCurrentViewModel().mSimTwoContainerVisibility.set(true);
        }
    }

    private void setUpSimTwoRegistrationUI(RegistrationInfo registrationInfo, int size) {
        getCurrentViewModel().mSimTwoContainerVisibility.set(true);

        if (registrationInfo != null && registrationInfo.getSimTwoState() == RegistrationSimTwoState.SIM_TWO_REGISTRATION_SUCCESS) {
            //set header text
            getCurrentViewModel().mSimTwoHeaderOne.set(getString(R.string.re_register_current_number_sim_two));
            getCurrentViewModel().mSimTwoHeaderTwo.set(getString(R.string.re_register_click_below));

            //already registration tried, prefill edit text with number
            getCurrentViewModel().mEditTextTwo.set(registrationInfo.phoneNumber);
            getCurrentViewModel().mPreviousSimTwoNumber.set(registrationInfo.phoneNumber);
            getCurrentViewModel().mEditTextTwoEnable.set(false);

            if (!TextUtils.isEmpty(registrationInfo.phoneNumber)) {
                //handle button enable disable
                getCurrentViewModel().onHandleButtonColor();
            }

        } else {

            //to handle null exception
            getCurrentViewModel().mPreviousSimTwoNumber.set(AppConstants.EMPTY_TEXT);

            // set sim two header text
            getCurrentViewModel().mSimTwoHeaderOne.set(getString(R.string.re_register_current_number_sim_two_not_register));
            getCurrentViewModel().mSimTwoHeaderTwo.set(getString(R.string.re_register_click_on_it));
            getCurrentViewModel().mEditTextTwoEnable.set(true);
            getCurrentViewModel().mIsSimTwoEdited.set(true);
        }

        //if device has only one sim, and slot index is zero then dont show second sim container
        if (size == 1) {
            getCurrentViewModel().mDividerVisibility.set(false);
            getCurrentViewModel().mSimOneContainerVisibility.set(false);
        } else {
            getCurrentViewModel().mDividerVisibility.set(true);
            getCurrentViewModel().mSimOneContainerVisibility.set(true);
        }
    }

    @Override
    public void onViewClick(View view) {
        super.onViewClick(view);
        switch (view.getId()) {
            case R.id.sim_one_edit_btn:
                getCurrentViewModel().mEditTextOneEnable.set(true);
                getCurrentViewModel().mIsSimOneEdited.set(true);
                break;

            case R.id.sim_two_edit_btn:
                getCurrentViewModel().mEditTextTwoEnable.set(true);
                getCurrentViewModel().mIsSimTwoEdited.set(true);
                break;

            case R.id.next_button:
                LogUtility.d(TAG, "---------- Next Button Clicked ----------");
                launchReRegistrationProcess();
                break;

            case R.id.cancel_button:
                backToPreviousPage();
                break;
        }
    }

    /**
     * Method used to trigger Re-Restration process
     */
    private void launchReRegistrationProcess() {
        getCurrentViewModel().mSimOneErrorVisibility.set(false);
        getCurrentViewModel().mSimTwoErrorVisibility.set(false);

        if (getCurrentViewModel().mIsSimOneEdited.get() ||
            getCurrentViewModel().mIsSimTwoEdited.get()) {

            String phoneOne = getViewModel().mEditTextOne.get();
            String phoneTwo = getViewModel().mEditTextTwo.get();

            //getViewModel().showProgressBar.set(true);
            progressDlg.show();
            List<SimInformation> simList = AppUtils.getSimInfoList();

            //check if sim one edited
            if (!TextUtils.isEmpty(phoneOne) && getCurrentViewModel().mIsSimOneEdited.get()) {
                if (simList.size() > 0) {
                    mNumberOfAPICall++;
                    RegistrationInfo registrationInfo = AppUtils.getSimDataBasedOnSlot(getActivity(),
                        String.valueOf(simList.get(0).getSimSlotIndex()));
                    ReRegistrationEvent simOneEvent = new ReRegistrationEvent();
                    if(registrationInfo != null){
                        registrationInfo.simOneState = RegistrationSimOneState.SIM_ONE_REGISTRATION_FAILURE;
                    }
                    simOneEvent.registrationInfo = registrationInfo;
                    simOneEvent.simSlot = 0;
                    RxEvent eventSimOne = new RxEvent(AppConstants.RxEventName.SETTING_RE_REGISTRATION, simOneEvent);
                    rxBus.send(eventSimOne);
                }
            }

            //check if sim one edited
            if (!TextUtils.isEmpty(phoneTwo) && getCurrentViewModel().mIsSimTwoEdited.get()) {
                if (simList.size() > 0 ) {
                    int slotIndex = 0;
                    if(simList.size() > 1){
                        slotIndex = simList.get(1).getSimSlotIndex();
                    }else {
                        slotIndex = simList.get(0).getSimSlotIndex();
                    }

                    mNumberOfAPICall++;
                    RegistrationInfo registrationInfo = AppUtils.getSimDataBasedOnSlot(getActivity(),
                        String.valueOf(slotIndex));
                    ReRegistrationEvent simTwoEvent = new ReRegistrationEvent();
                    if(registrationInfo != null){
                        registrationInfo.simTwoState = RegistrationSimTwoState.SIM_TWO_REGISTRATION_FAILURE;
                    }
                    simTwoEvent.registrationInfo = registrationInfo;
                    simTwoEvent.simSlot = 1;
                    RxEvent eventSimTwo = new RxEvent(AppConstants.RxEventName.SETTING_RE_REGISTRATION, simTwoEvent);
                    rxBus.send(eventSimTwo);
                }
            }
        }

    }

    /**
     * Method used to check if user has edited previous number or entered new number
     * Allow reregister only when user has been edited previous number or entered new number
     *
     * @param phoneOne
     * @param phoneTwo
     */
    private boolean anyNumberEdited(String phoneOne, String phoneTwo) {
        String simOnePreviousNumber = getCurrentViewModel().mPreviousSimOneNumber.get();
        String simTwoPreviousNumber = getCurrentViewModel().mPreviousSimTwoNumber.get();

        if (!TextUtils.isEmpty(phoneOne) && !phoneOne.equalsIgnoreCase(simOnePreviousNumber)) {
            return true;
        }

        if (!TextUtils.isEmpty(phoneTwo) && !phoneTwo.equalsIgnoreCase(simTwoPreviousNumber)) {
            return true;
        }

        return false;
    }


    private void registerEvents() {
        if (rxBusHelper == null) {
            rxBusHelper = new RxBusHelper();
            rxBusHelper.registerEvents(rxBus, TAG, this);
        }
    }

    private void unregisterEvents() {
        if (rxBusHelper != null) {
            rxBusHelper.unSubScribe();
            rxBusHelper = null;
        }

    }

    @Override
    public void onEventTrigger(Object event) {
        if (event instanceof RxEvent) {
            handleRxEvents((RxEvent) event);
        }
    }

    private void handleRxEvents(RxEvent event) {
        switch (event.getEventName()) {
            case AppConstants.RxEventName.EXIT_RE_REGISTRATION_SCREEN_EVENT:
                mRecievedCallBack++;
                // getViewModel().showProgressBar.set(false);
                progressDlg.dismiss();
                if (event != null) {
                    RegistrationInfo simData = (RegistrationInfo) event.getEventData();
                    if (simData.simSlot == 0) {

                        if (simData.status.equalsIgnoreCase(AppConstants.SUCCESS_STATUS) && mRecievedCallBack == mNumberOfAPICall) {
                            backToPreviousPage();
                        } else {
                            String phoneOne = getViewModel().mEditTextOne.get();
                            //Checking sim edit field edit beacuse there is a issue for first time registration if it get registered in settings screen it will update status as verified for empty edit text
                            if (!TextUtils.isEmpty(phoneOne) && getCurrentViewModel().mIsSimOneEdited.get()) {
                                //set error message
                                getCurrentViewModel().mSimOneErrorVisibility.set(true);
                                getCurrentViewModel().mSimOneRegistrationError.set(simData.status);
                            }
                        }

                    } else if (simData.simSlot == 1) {
                        if (simData.status.equalsIgnoreCase(AppConstants.SUCCESS_STATUS) && mRecievedCallBack == mNumberOfAPICall) {
                            backToPreviousPage();
                        } else {
                            //Checking sim edit field edit beacuse there is a issue for first time registration if it get registered in settings screen it will update status as verified for empty edit text
                            String phoneTwo = getViewModel().mEditTextTwo.get();
                            if (!TextUtils.isEmpty(phoneTwo) && getCurrentViewModel().mIsSimTwoEdited.get()) {
                                //set failure error message
                                getCurrentViewModel().mSimTwoErrorVisibility.set(true);
                                getCurrentViewModel().mSimTwoRegistrationError.set(simData.status);
                            }
                        }
                    }

                }
                break;
        }
    }

    private void backToPreviousPage() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        unregisterEvents();
        super.onDestroy();
    }
}
