package io.gupshup.smsapp.ui.settings.viewmodel;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Naveen BM on 7/11/2018.
 */
public class NotificationFragmentViewModel extends BaseFragmentViewModel {

    private static final String TAG = NotificationFragmentViewModel.class.getSimpleName();

    public ObservableBoolean mIsNotificationEnabled = new ObservableBoolean();

    public ObservableField<String> mNotificationSoundName = new ObservableField<>();

    public ObservableBoolean mIsNotificationVibrateEnabled = new ObservableBoolean();

    public ObservableBoolean mIsNotificationLightEnabled = new ObservableBoolean();

    public NotificationFragmentViewModel(@NonNull Application application) {
        super(application);
    }


    /**
     * Method used to handle the notification switched based on status
     */
    public void handleSwitchesOnNotificationStatus(boolean isEnabled) {
        mIsNotificationEnabled.set(isEnabled);
        if (!isEnabled) {
            mIsNotificationVibrateEnabled.set(false);
            mIsNotificationLightEnabled.set(false);
        }
    }

    public void setVibrateOnOff(boolean isChecked) {
        mIsNotificationVibrateEnabled.set(isChecked);
    }

    public void setNotificationLightOnOff(boolean isChecked) {
        mIsNotificationLightEnabled.set(isChecked);
    }
}
