package io.gupshup.smsapp.ui.reply.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.text.TextUtils;

import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.receiver.SmsReceiver;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.PreferenceManager;

/**
 * Created by Naveen BM on 4/20/2018.
 */
public class MessageReplyViewModel extends BaseFragmentViewModel {
    private static final String TAG = "MESSAGEREPLYVIEWMODEL";
    //Observable Live Data
    private LiveData<PagedList<MessageEntity>> data = new MutableLiveData<>();
    public ObservableField<String> editTextVal = new ObservableField<>();

    public ObservableBoolean mShowSimListLyt = new ObservableBoolean(false);
    public ObservableField<String> mSimOneName = new ObservableField<>();
    public ObservableField<String> mSimTwoName = new ObservableField<>();
    public ObservableBoolean mSimTwoVisibility = new ObservableBoolean(false);
    public ObservableBoolean mShowSendImage = new ObservableBoolean(false);
    public ObservableBoolean mIsFirstSim = new ObservableBoolean(false);

    public ObservableField<String> mSimSlotNo = new ObservableField<>();
    public ObservableBoolean mSimVisibility = new ObservableBoolean(false);
    public ObservableField<Integer> mFirstSimColor = new ObservableField<>(0);
    public ObservableField<Integer> mSecondSimColor = new ObservableField<>(0);
    private LiveData<PagedList<ContactModel>> mPersonalContact = new MutableLiveData<>();
    public ObservableBoolean mSendViewClickable = new ObservableBoolean();
    public ObservableBoolean mTypeAreaVisibility = new ObservableBoolean();
    public ObservableBoolean mSearchCountVisibility = new ObservableBoolean(false);
    public ObservableField<String> mTotalSearchMatcedCount = new ObservableField<>();
    public ObservableInt mSearchPosition = new ObservableInt(0);

    public ObservableBoolean showPersonalFolder = new ObservableBoolean(true);
    public ObservableBoolean showTransactionalFolder = new ObservableBoolean(true);
    public ObservableBoolean showPromoFolder = new ObservableBoolean(true);
    public ObservableBoolean showBlockOption = new ObservableBoolean(true);
    public ObservableField<String> archiveText = new ObservableField<>();
    public ObservableField<String> blocText = new ObservableField<>();

    public ObservableBoolean mArrowContainer = new ObservableBoolean();
    public ObservableField<Integer> themeColor = new ObservableField<>(0);


    public MessageReplyViewModel(@NonNull Application application) {
        super(application);
    }

    /**
     * Get messages for the selected contact
     * @param mask
     */
    public void getMessageForSelectedContact(String mask) {
        MessageDAO messageDAO = AppDatabase.getInstance(MainApplication.getContext()).messageDAO();

        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY)
                .setPrefetchDistance(PREFETCH_DISTANCE)

                .setPageSize(PAGE_SIZE).build();
        data = new LivePagedListBuilder(messageDAO.getByConversation(mask), pagedListConfig)
            .build();
    }

    public LiveData<PagedList<MessageEntity>> observeData() {
        return data;
    }


    /**
     * Method used to get the forward contact list
     */
    public void callPersonalList() {
        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setPageSize(PAGE_SIZE).build();
        ConversationDAO convDao = AppDatabase.getInstance(MainApplication.getContext()).conversationDAO();
        mPersonalContact = new LivePagedListBuilder(convDao.getPersonalContactList(), pagedListConfig)
            .build();
    }

    public LiveData<PagedList<ContactModel>> observeContact() {
        return mPersonalContact;
    }

    /**
     * Method used to send the message and save message to DB
     */
    public void sendMessage(Context context, String mask, String msg, int subscriptionID, int simSlot, long draftMessageCreationTime) {
        LogUtility.d(TAG, "--------- TEXT ----------" + editTextVal.get());

        if (TextUtils.isEmpty(msg)) {
            return;
        }
        String carrierName = AppConstants.EMPTY_TEXT;
        Pair<String, Integer> info = AppUtils.getChanelName(context, simSlot);
        if (info != null) {
            carrierName = info.first;
        }
        Uri smsUri = MessageUtils.insertSms(mask, msg, context, Telephony.Sms.Sent.CONTENT_URI);
        String messageId = MessageUtils.extractMessageId(smsUri);
        SmsReceiver.preSendMessage(mask, msg, carrierName, simSlot, messageId, draftMessageCreationTime);
        MessageUtils.sendSMS(context, mask, msg, messageId, subscriptionID);
    }

    /**
     * Method used to handle the visibility of sim list layout
     *
     * @param
     */
    public void showSimListLayout(List<SimInformation> simList, Context context) {
        if (simList != null && simList.size() > 0) {
            mShowSimListLyt.set(true);
            if (simList.size() == 1) {
                if (simList.get(0) != null) {
                    mSimOneName.set(simList.get(0).getDisplayName());
                    mSimTwoVisibility.set(false);
                }
            } else if (simList.size() == 2) {
                setSimVisibilityInPreference(context, true);
                if (simList.get(0) != null) {
                    mSimOneName.set(context.getString(R.string.sim_one_title, simList.get(0).getDisplayName()));
                    mFirstSimColor.set(simList.get(0).getIconTint());

                }

                if (simList.get(1) != null) {
                    mSimTwoName.set(context.getString(R.string.sim_two_title, simList.get(1).getDisplayName()));
                    mSecondSimColor.set(simList.get(1).getIconTint());
                    mSimTwoVisibility.set(true);
                }
            }
        }
    }

    public void setSimVisibilityInPreference(Context context, boolean visibility) {
        PreferenceManager.getInstance(context).setSimLayoutVisibility(visibility);
    }


    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String text = s.toString().trim();
        if (!TextUtils.isEmpty(text)
            && AppUtils.getSimInfoList().size() > 0) {
            mShowSendImage.set(true);
        } else {
            mShowSendImage.set(false);
        }

        //set charecter count
        String charecterCount = AppUtils.characterCounter(text);
        messageCounterVisibility.set(!TextUtils.isEmpty(charecterCount));
        messageCounter.set(charecterCount);
    }

    public void handleSimLayoutVisibility() {
        if (mShowSimListLyt.get()) {
            mShowSimListLyt.set(false);
        }
    }

    public boolean getSimLayoutVisibility() {
        return mShowSimListLyt.get();
    }

    public MessageEntity getLastMessageData(String lastMessageId) {
        List<MessageEntity> messageEntities = MessagingService.getInstance(SMSService.class).getMessageEntity(lastMessageId);
        if (messageEntities.size() > 0) {
            return messageEntities.get(0);
        }
        return null;
    }

    public void insertOrUpdateDraft(String mask, String conv, long messageTimestamp) {
        ConversationDAO conversationDAO = AppDatabase.getInstance(MainApplication.getContext()).conversationDAO();

        long draftCreationTime = -1;
        long lastMessageTimeStamp = -1;
        if (!TextUtils.isEmpty(conv)) {
            draftCreationTime = messageTimestamp;
            lastMessageTimeStamp = draftCreationTime;
        } else {
            List<MessageEntity> messageEntityList = MessagingService.getInstance(SMSService.class).getLastMessageForAMask(mask);
            if (messageEntityList != null && messageEntityList.size() > 0) {
                MessageEntity entityMsg = messageEntityList.get(0);
                lastMessageTimeStamp = entityMsg.getMessageTimestamp();
            }
        }
        conversationDAO.updateDraftMessage(mask, conv, draftCreationTime, lastMessageTimeStamp);
    }

    public ConversationEntity getConversationByMask(String mask, Context ctx) {
        List<ConversationEntity> convList = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (convList != null && convList.size() > 0) {
            return convList.get(0);
        }
        return null;
    }

    public void upDateLastMessageIdOnDelete(Context ctx, String mask) {
        //update last message id of the conversation
        List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        List<MessageEntity> messageEnt = MessagingService.getInstance(SMSService.class).getLastMessageForAMask(mask);
        if (conversationEntity != null) {
            if (messageEnt != null && messageEnt.get(0) != null && messageEnt.get(0).getMessageId() != null) {
                conversationEntity.get(0).setLastMessageId(messageEnt.get(0).getMessageId());
                MessagingService.getInstance(SMSService.class).updateConversation(conversationEntity.get(0));
            }
        }

    }

    public void handleSearchCountVisibility(String searchText, int count) {
        if (!TextUtils.isEmpty(searchText) && mSearchCountVisibility.get() == false) {
            //search text is not empty and Count visibility is gone then make VISIBLE
            mSearchCountVisibility.set(true);
        }
        if (TextUtils.isEmpty(searchText) && mSearchCountVisibility.get() == true) {
            //search text is  empty and Count visibility is there then make visibility GONE
            mSearchCountVisibility.set(false);
        }

        if (count == 0 || count == 1) {
            mArrowContainer.set(false);
        } else {
            mArrowContainer.set(true);
        }
    }

    /**
     * Method used to set the total results found
     *
     * @param count
     */
    public void setTotalMatchCount(int count) {
        mTotalSearchMatcedCount.set(String.valueOf(count));
        if (count > 0) {
            mSearchPosition.set(1);
        } else {
            mSearchPosition.set(0);
        }
    }

    public void increaseSearchCount() {
        if (mSearchPosition.get() < Integer.valueOf(mTotalSearchMatcedCount.get())) {
            mSearchPosition.set(mSearchPosition.get() + 1);
        }
    }


    public void decreaseSearchCount() {
        if (mSearchPosition.get() > 1) {
            mSearchPosition.set(mSearchPosition.get() - 1);
        }
    }

    /**
     * method used to get the current value of the position which is in the top left
     */
    public int getCurrentPosition() {
        return mSearchPosition.get();
    }


    public void setColor(List<SimInformation> mSimList, Context context) {
        mFirstSimColor.set(ContextCompat.getColor(context, R.color.default_sim_one_color));
        mSecondSimColor.set(ContextCompat.getColor(context, R.color.default_sim_two_color));
        if (mSimList != null && mSimList.size() > 1) {
            mSimVisibility.set(true);
            mFirstSimColor.set(mSimList.get(0).getIconTint());
            mSecondSimColor.set(mSimList.get(1).getIconTint());
        } else {
            //make sim visibility gone
            mSimVisibility.set(false);
        }
    }
}
