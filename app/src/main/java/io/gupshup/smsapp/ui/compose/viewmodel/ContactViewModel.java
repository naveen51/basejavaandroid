package io.gupshup.smsapp.ui.compose.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 4/12/2018.
 */
public class ContactViewModel {
    private static final String TAG = "CONTACTVIEWMODEL";

    public ObservableBoolean isIconSelected = new ObservableBoolean(false);
    public ObservableBoolean headerLayoutVisibility = new ObservableBoolean(false);
    public ObservableField<String> contactHeaderLetter = new ObservableField<>();
    public ObservableBoolean isMultiple = new ObservableBoolean();
    public ObservableField<String> mContactType = new ObservableField<>();

    @Inject
    public RxBus rxBus;

    public ContactViewModel() {
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    /**
     * Handling contact click event
     */
    public void onItemClick(View view, int clickedFrom) {
        if (view != null && view.getTag() != null) {
            Contact contact = (Contact) view.getTag();
            int beforeSize, afterSize = 0;
            LogUtility.d(TAG, "-- Contact Clicked -------- " + contact.getContactName());
            beforeSize = ContactSingleton.getInstance().getSelectedContacts().size();
            if (contact.isSelected()) {
                ContactSingleton.getInstance().removeSelectedContact(contact);
                contact.setSelected(false);
                isIconSelected.set(false);
            } else {
                ContactSingleton.getInstance().addContact(contact);
                contact.setSelected(true);
                isIconSelected.set(true);
            }

            contact.setClickedFrom(clickedFrom);

            //notify respected adapter
            rxBus.send(new RxEvent(AppConstants.EVENT_NOTIFY_ADAPTER, contact));

            if (ContactSingleton.getInstance().getSelectedContacts().size() == 1 && beforeSize == 0) {
                rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_SELECTED, contact));
            } else {
                rxBus.send(new RxEvent(AppConstants.UPDATE_CONTACT_TOP_BAR, null));
            }

            //handle right arrow and title in the toolbar when list size is zero
            if (ContactSingleton.getInstance().getSelectedContacts().size() == 0) {
                rxBus.send(new RxEvent(AppConstants.HANDLE_TOOLBAR_CONTENT_EVENT, null));
            }
        }
    }




    /**
     * Handling contact click event
     */
    public void onShareItemClick(View view) {
        if (view != null && view.getTag() != null) {
            Contact contact = (Contact) view.getTag();
            int beforeSize, afterSize = 0;
            LogUtility.d(TAG, "-- Contact Clicked -------- " + contact.getContactName());
            beforeSize = ContactSingleton.getInstance().getSelectedSharedContacts().size();
            if (contact.isSelected()) {
                ContactSingleton.getInstance().removeSelectedSharedContact(contact);
                contact.setSelected(false);
                isIconSelected.set(false);
            } else {
                ContactSingleton.getInstance().addShareContact(contact);
                contact.setSelected(true);
                isIconSelected.set(true);
            }

            //notify respected adapter
            rxBus.send(new RxEvent(AppConstants.EVENT_NOTIFY_ADAPTER, contact));

        }
    }


}
