package io.gupshup.smsapp.ui.base.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import io.gupshup.smsapp.R;

/**
 * Created by Ram Prakash Bhat on 9/5/18.
 */

public class CustomViewPager extends ViewPager {
    private boolean mSwipeable;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.customviewpager);
        try {
            mSwipeable = a.getBoolean(R.styleable.customviewpager_swipeable, true);
        } finally {
            a.recycle();
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return mSwipeable && super.onInterceptTouchEvent(event);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mSwipeable && super.onTouchEvent(event);
    }

    public void setViewPagerSwipeAble(boolean swipeAble) {
        mSwipeable = swipeAble;
    }
}
