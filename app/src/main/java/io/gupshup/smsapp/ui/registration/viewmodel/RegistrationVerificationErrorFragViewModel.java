package io.gupshup.smsapp.ui.registration.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.view.View;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 6/14/2018.
 */
public class RegistrationVerificationErrorFragViewModel extends BaseFragmentViewModel {
    private static final String TAG = RegistrationVerificationErrorFragViewModel.class.getSimpleName();
    @Inject
    public RxBus rxBus;

    public RegistrationVerificationErrorFragViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void skipClicked(View view) {
        LogUtility.d(TAG, "--- Error Skip called ------");
        RxEvent event = new RxEvent(AppConstants.SKIP_ERROR_LYT_CLICK, AppConstants.SKIP_ERROR_LYT_CLICK);
        rxBus.send(event);
    }


    public void retryClicked(View view) {
        LogUtility.d(TAG, "--- Error Retry called ------");
        RxEvent event = new RxEvent(AppConstants.SKIP_RETRY_CLICK, AppConstants.SKIP_RETRY_CLICK);
        rxBus.send(event);
    }

}
