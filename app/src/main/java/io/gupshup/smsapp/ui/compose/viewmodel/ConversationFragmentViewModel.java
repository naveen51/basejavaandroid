package io.gupshup.smsapp.ui.compose.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.text.TextUtils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.data.model.contacts.Contact;
import io.gupshup.smsapp.data.model.sim.SimInformation;
import io.gupshup.smsapp.database.AppDatabase;
import io.gupshup.smsapp.database.dao.ConversationDAO;
import io.gupshup.smsapp.database.dao.MessageDAO;
import io.gupshup.smsapp.database.entities.ContactModel;
import io.gupshup.smsapp.database.entities.ConversationEntity;
import io.gupshup.smsapp.database.entities.MessageEntity;
import io.gupshup.smsapp.enums.MessageChannel;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.exceptions.UnparsablePhoneNumberException;
import io.gupshup.smsapp.message.Utils;
import io.gupshup.smsapp.message.services.MessagingService;
import io.gupshup.smsapp.message.services.impl.SMSService;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.AppUtils;
import io.gupshup.smsapp.utils.ContactSingleton;
import io.gupshup.smsapp.utils.DateUtils;
import io.gupshup.smsapp.utils.DialogUtils;
import io.gupshup.smsapp.utils.LogUtility;
import io.gupshup.smsapp.utils.MessageUtils;
import io.gupshup.smsapp.utils.Triple;
import io.gupshup.soip.sdk.message.GupshupTextMessage;


/**
 * Created by Naveen BM on 4/16/2018.
 */
public class ConversationFragmentViewModel extends BaseFragmentViewModel {

    private static final String TAG = "CONVERSATIONFRAGMENTVIEWMODEL";
    @Inject
    public RxBus rxBus;
    public ObservableField<String> mFreshConversation = new ObservableField<>();
    public ObservableBoolean mIsShowFreshConv = new ObservableBoolean(false);
    public ObservableBoolean mIsShowOldConv = new ObservableBoolean(false);
    public ObservableField<String> mEditText = new ObservableField<>();
    public ObservableBoolean mShowSimLayout = new ObservableBoolean(false);
    //Observable Live Data
    private LiveData<PagedList<MessageEntity>> data = new MutableLiveData<>();
    private MutableLiveData<String> mMaskCheck = new MutableLiveData<>();

    public ObservableField<String> mSim1Name = new ObservableField<>();
    public ObservableField<String> mSim2Name = new ObservableField<>();
    public ObservableBoolean mSim2Visibility = new ObservableBoolean(false);
    public ObservableBoolean mShowSendImage = new ObservableBoolean(false);

    public ObservableField<String> mSimSlotNo = new ObservableField<>();
    public ObservableBoolean mSimVisibility = new ObservableBoolean(false);
    public ObservableBoolean mConversationStarted = new ObservableBoolean(false);
    public ObservableField<Integer> mFirstSimColor = new ObservableField<>(0);
    public ObservableField<Integer> mSecondSimColor = new ObservableField<>(0);
    public ObservableBoolean mIsFirstSim = new ObservableBoolean(false);
    public ObservableBoolean mSendViewClickable = new ObservableBoolean();
    public ObservableBoolean mBroadCastTextVisibility = new ObservableBoolean(false);


    public ObservableBoolean showPersonalFolder = new ObservableBoolean(true);
    public ObservableBoolean showTransactionalFolder = new ObservableBoolean(true);
    public ObservableBoolean showPromoFolder = new ObservableBoolean(true);
    public ObservableBoolean showBlockOption = new ObservableBoolean(true);
    public ObservableField<String> archiveText = new ObservableField<>();
    public ObservableField<String> blocText = new ObservableField<>();
    private LiveData<PagedList<ContactModel>> mPersonalContact = new MutableLiveData<>();
    public ObservableBoolean mSearchCountVisibility = new ObservableBoolean(false);
    public ObservableInt mSearchPosition = new ObservableInt(0);
    public ObservableField<String> mTotalSearchMatcedCount = new ObservableField<>();
    public ObservableBoolean mArrowContainer = new ObservableBoolean();
    public ObservableField<Integer> themeColor = new ObservableField<>(0);


    public ConversationFragmentViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void constructConversationWith(String contactName) {
        String conversationWith = MainApplication.getContext().getString(R.string.conversation_with) + " " + contactName + " - " + MainApplication.getContext().getString(R.string.now);
        mFreshConversation.set(conversationWith);
    }

    public void getMessageForSelectedContact(String mask) {
        MessageDAO messageDAO = AppDatabase.getInstance(MainApplication.getContext()).messageDAO();

        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setPageSize(PAGE_SIZE).build();
        data = new LivePagedListBuilder(messageDAO.getByConversation(mask), pagedListConfig)
            .build();
    }

    /**
     * Method used to get the conversation entity by mask
     *
     * @param mask
     */
    public ConversationEntity getConversationEntityFromDB(String mask) {
        List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (conversationEntity != null && conversationEntity.size() > 0) {
            return conversationEntity.get(0);
        }
        return null;
    }

    public LiveData<PagedList<MessageEntity>> observeData() {
        return data;
    }

    /**
     * Method used to send the message when there is a existing conversation for the contact
     */
    public void sendMessage(Context context, String mask, ArrayList<String> phone, String smsBody,
                            boolean isFresh, int subscriptionID, int simSlot, long draftMessageCreationTime) {
        mEditText.set("");
        mIsShowFreshConv.set(false);
        mIsShowOldConv.set(true);
        LogUtility.d(TAG, "--------- TEXT ----------" + smsBody);
        if (TextUtils.isEmpty(smsBody)) {
            return;
        }
        String carrierName = AppConstants.EMPTY_TEXT;
        Pair<String, Integer> info = AppUtils.getChanelName(context, simSlot);
        if (info != null) {
            carrierName = info.first;
        }
        if (phone != null && phone.size() > 0) {
            boolean isValid = true;
            for (String singlePhoneNumber : phone) {

                if (!Utils.isValidPhoneNumber(singlePhoneNumber)) {

                    if (isValid) {
                        isValid = false;
                    }
                    //show toast reciepient is invalid
                    DialogUtils.showAlertDialog(context, context.getString(R.string.no_valid_recipient),
                        context.getString(R.string.unable_to_send_title), context.getString(R.string.ok_btn), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                rxBus.send(new RxEvent(AppConstants.ACTIVITY_EXIT_EVENT, null));
                                dialogInterface.dismiss();
                            }
                        });

                } else {
                    /*Uri smsUri = MessageUtils.insertSms(mask, smsBody, context, Telephony.Sms.Sent.CONTENT_URI);
                    String messageId = MessageUtils.extractMessageId(smsUri);
                    SmsReceiver.preSendCompose(isFresh, mask, singlePhoneNumber, smsBody, carrierName, simSlot, messageId);
                    MessageUtils.sendSMS(context, isFresh ? singlePhoneNumber : mask, smsBody, messageId, subscriptionID);*/

                    Utils.trySendingDataMessage(context, new GupshupTextMessage(smsBody),
                        isFresh ? singlePhoneNumber : mask,
                        simSlot, draftMessageCreationTime, isFresh);
                }
            }

            if (phone.size() > 1 && isValid) {
                rxBus.send(new RxEvent(AppConstants.ACTIVITY_EXIT_EVENT, null));
            }

        } else {

            Utils.trySendingDataMessage(context, new GupshupTextMessage(smsBody), mask,
                simSlot, draftMessageCreationTime, isFresh);
            /*Uri smsUri = MessageUtils.insertSms(mask, smsBody, context, Telephony.Sms.Sent.CONTENT_URI);
            String messageId = MessageUtils.extractMessageId(smsUri);
            SmsReceiver.preSendCompose(isFresh, mask, null, smsBody, carrierName, simSlot, messageId);
            MessageUtils.sendSMS(context, mask, smsBody, messageId, subscriptionID);*/
        }
    }

    public boolean getViewVisibility() {
        if (mIsShowOldConv.get()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method used to get only the phone numbers fron selected contacts
     * @return returns array list of phone numbers
     */
    public ArrayList<String> getContactNumber() {
        ArrayList<String> mPhoneNumbersList = new ArrayList<>();
        ArrayList<Contact> contactList = new ArrayList<>(ContactSingleton.getInstance().getSelectedContacts().values());
        if (contactList != null && contactList.size() > 0) {
            for (int i = 0; i < contactList.size(); i++) {
                Contact contact = contactList.get(i);
                if (contact != null) {
                    mPhoneNumbersList.add(contact.getContactPhoneNumber());
                }
            }
        }
        return mPhoneNumbersList;
    }

    /**
     * Method used to get the mask from the phone number which we sent
     *
     * @param phoneNumber
     */
    public void getMaskByPhone(ArrayList<String> phoneNumber) {
        if (phoneNumber != null && phoneNumber.size() > 0) {
            String maskCheck = null;
            try {
                if (phoneNumber.size() == 1) {
                    maskCheck = Utils.getMaskFromPhoneNumber(phoneNumber.get(0));
                } else {
                    maskCheck = Utils.getMaskFromPhoneNumbers(phoneNumber, MainApplication.getContext());
                }
                mMaskCheck.setValue(maskCheck);
            } catch (UnparsablePhoneNumberException e) {
                //Todo: Temp fix
                mMaskCheck.setValue(String.valueOf(new SecureRandom().nextInt()));
                e.printStackTrace();
            }
        }
    }

    public MutableLiveData<String> observeMaskData() {
        return mMaskCheck;
    }

    /**
     * Method used to handle the visibility of sim list layout
     *
     * @param
     * @param context
     */
    public void showSimListLayout(List<SimInformation> simList, Context context) {
        if (simList != null && simList.size() > 0) {
            mShowSimLayout.set(true);
            if (simList.size() == 1) {
                if (simList.get(0) != null) {
                    mSim1Name.set(simList.get(0).getDisplayName());
                    mSim2Visibility.set(false);
                }
            } else if (simList.size() == 2) {
                if (simList.get(0) != null) {
                    mSim1Name.set(context.getString(R.string.sim_one_title, simList.get(0).getDisplayName()));
                }

                if (simList.get(1) != null) {
                    mSim2Name.set(context.getString(R.string.sim_two_title, simList.get(1).getDisplayName()));
                    mSim2Visibility.set(true);
                }
            }
        }
    }

    /**
     * Method will get called whne text changes in the edit text
     * @param s
     * @param start
     * @param before
     * @param count
     */
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String text = s.toString().trim();
        if (text != null && !TextUtils.isEmpty(text)
            && AppUtils.getSimInfoList().size() > 0) {
            mShowSendImage.set(true);
        } else {
            mShowSendImage.set(false);
        }

        String charecterCount = AppUtils.characterCounter(text);
        messageCounterVisibility.set(!TextUtils.isEmpty(charecterCount));
        messageCounter.set(charecterCount);
    }

    /**
     * Method used to get the numbers to which message to be sent
     * @return list of numbers
     */
    public ArrayList<String> getContactsNumbersToSend() {
        ArrayList<Contact> contactList = new ArrayList<>(ContactSingleton.getInstance().getSelectedContacts().values());
        ArrayList<String> contactsNumbers = new ArrayList<>();
        for (Contact contact : contactList
            ) {
            contactsNumbers.add(contact.getContactPhoneNumber());
        }

        return contactsNumbers;
    }

    public String getRecepientThumbNail(Context activity, String contactNumber) {
        Pair<String, String> userInfo = MessageUtils.getContactUserDetail(activity, contactNumber);
        if (userInfo != null) {
            return userInfo.second;
        }
        return null;
    }

    public void handleSimLayoutVisibility() {
        if (mShowSimLayout.get()) {
            mShowSimLayout.set(false);
        }

    }

    public boolean getSimLayoutVisibility() {
        return mShowSimLayout.get();

    }

    /**
     * Method used to insert or update the draft
     * @param mask phone number added with country code
     * @param conv the text which is in edit text
     */
    public void insertOrUpdateDraft(String mask, String conv) {
        long draftCreationTime = -1;
        long lastMessageTimeStamp = -1;
        if (!TextUtils.isEmpty(conv)) {
            draftCreationTime = DateUtils.currentTimestamp();
            lastMessageTimeStamp = draftCreationTime;
        } else {
            List<MessageEntity> messageEntityList = MessagingService.getInstance(SMSService.class).getLastMessageForAMask(mask);
            if (messageEntityList != null && messageEntityList.size() > 0) {
                MessageEntity entityMsg = messageEntityList.get(0);
                lastMessageTimeStamp = entityMsg.getMessageTimestamp();
            }
        }
        MessagingService.getInstance(SMSService.class).updateDraftMessage(mask, conv, draftCreationTime, lastMessageTimeStamp);
    }


    /**
     * Method used to get the last message data based on last message id
     * @param lastMessageId
     * @return
     */
    public MessageEntity getLastMessageData(String lastMessageId) {
        List<MessageEntity> messageEntities = MessagingService.getInstance(SMSService.class).getMessageEntity(lastMessageId);
        if (messageEntities.size() > 0) {
            return messageEntities.get(0);
        }
        return null;

    }

    /**
     * Method used to create new conversation if its new conversation , and ud update draft message
     * @param mask  phone number added with country code
     * @param phoneNumber list of numbers
     * @param channel message channel
     * @param draft draft message
     */
    public void createNewConversationAndUpdateDraft(String mask, ArrayList<String> phoneNumber, MessageChannel channel, String draft) {
        if (phoneNumber != null && phoneNumber.size() > 0) {
            for (String phone : phoneNumber) {
                ConversationEntity convEntity;
                List<ConversationEntity> conversationEntities = MessagingService.getInstance(SMSService.class).getConversationByMask(Utils.getMaskFromPhoneNumber(phone));
                if (!Utils.getMaskFromPhoneNumber(phone).matches("[0-9 +]+")) {
                    continue;
                }
                if (conversationEntities.size() > 0) {
                    convEntity = conversationEntities.get(0);
                    convEntity.setDraftMessage(draft);
                    convEntity.setDraftCreationTime(DateUtils.currentTimestamp());
                    convEntity.setLastMessageTimestamp(DateUtils.currentTimestamp());
                    MessagingService.getInstance(SMSService.class).updateConversation(convEntity);
                } else {
                    convEntity = Utils.getNewConversation(Utils.getMaskFromPhoneNumber(phone),
                        phone, channel, draft);
                    long[] conversationIds = MessagingService.getInstance(SMSService.class).insertConversation(convEntity);

                    Triple<String, String, String> userInfo = MessageUtils.getContactUserDetails
                        (MainApplication.getContext(), Utils.getMaskFromPhoneNumber(phone));
                    if (userInfo != null) {
                        convEntity.setDisplay(userInfo.getFirst());
                        convEntity.setImage(userInfo.getSecond());
                        convEntity.setContactId(userInfo.getThird());
                    }
                    convEntity.setConversationId(conversationIds[0]);
                    convEntity.setDraftMessage(draft);
                    convEntity.setDraftCreationTime(DateUtils.currentTimestamp());
                    convEntity.setLastMessageTimestamp(DateUtils.currentTimestamp());
                    MessagingService.getInstance(SMSService.class).updateConversation(convEntity);
                }
            }
        }
    }

    /**
     * method used to handle the broadcasyt text visibility
     */
    public void handleBroadcastTextVisibility() {
        ArrayList<Contact> contactList = new ArrayList<>(ContactSingleton.getInstance().getSelectedContacts().values());
        if (contactList != null && contactList.size() > 0) {
            if (contactList.size() == 1) {
                //make visibility gone
                mBroadCastTextVisibility.set(false);
            } else {
                //make visible
                if (!mBroadCastTextVisibility.get()) {
                    mBroadCastTextVisibility.set(true);
                }
            }
        }
    }

    public void upDateLastMessageIdOnDelete(Context ctx, String mask) {
        //update last message id of the conversation
        List<ConversationEntity> conversationEntity = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        List<MessageEntity> messageEnt = MessagingService.getInstance(SMSService.class).getLastMessageForAMask(mask);
        if (conversationEntity != null) {
            if (messageEnt != null && messageEnt.get(0) != null && messageEnt.get(0).getMessageId() != null) {
                conversationEntity.get(0).setLastMessageId(messageEnt.get(0).getMessageId());
                MessagingService.getInstance(SMSService.class).updateConversation(conversationEntity.get(0));
            }
        }

    }


    public void callPersonalList() {
        PagedList.Config pagedListConfig =
            (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(INITIAL_LOAD_KEY)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setPageSize(PAGE_SIZE).build();
        ConversationDAO convDao = AppDatabase.getInstance(MainApplication.getContext()).conversationDAO();
        mPersonalContact = new LivePagedListBuilder(convDao.getPersonalContactList(), pagedListConfig)
            .build();
    }

    public LiveData<PagedList<ContactModel>> observeContact() {
        return mPersonalContact;
    }


    public ConversationEntity getConversationByMask(String mask, Context ctx) {
        List<ConversationEntity> convList = MessagingService.getInstance(SMSService.class).getConversationByMask(mask);
        if (convList != null && convList.size() > 0) {
            return convList.get(0);
        }
        return null;
    }

    public String getContactName() {
        ArrayList<Contact> contactLst = new ArrayList<>(ContactSingleton.getInstance().getSelectedContacts().values());
        if (contactLst != null && contactLst.size() > 0) {
            return contactLst.get(0).getContactName();
        }
        return "";
    }

    public void handleSearchCountVisibility(String searchText, int count) {
        if (!TextUtils.isEmpty(searchText))
            if (!mSearchCountVisibility.get()) {
                //search text is not empty and Count visibility is gone then make VISIBLE
                mSearchCountVisibility.set(true);
            }
        if (TextUtils.isEmpty(searchText))
            if (mSearchCountVisibility.get()) {
                //search text is  empty and Count visibility is there then make visibility GONE
                mSearchCountVisibility.set(false);
            }

        if (count == 0 || count == 1) {
            mArrowContainer.set(false);
        } else {
            mArrowContainer.set(true);
        }

    }

    /**
     * Method used to set the total results found
     *
     * @param count
     */
    public void setTotalMatchCount(int count) {
        mTotalSearchMatcedCount.set(String.valueOf(count));
        if (count > 0) {
            mSearchPosition.set(1);
        } else {
            mSearchPosition.set(0);
        }
    }

    public void increaseSearchCount() {
        if (mSearchPosition.get() < Integer.valueOf(mTotalSearchMatcedCount.get())) {
            mSearchPosition.set(mSearchPosition.get() + 1);
        }
    }


    public void decreaseSearchCount() {
        if (mSearchPosition.get() > 1) {
            mSearchPosition.set(mSearchPosition.get() - 1);
        }
    }

    /**
     * method used to get the current value of the position which is in the top left
     */
    public int getCurrentPosition() {
        return mSearchPosition.get();
    }


}
