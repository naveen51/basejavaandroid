package io.gupshup.smsapp.ui.earlierSMS;

public class EarlierSMS {
    public String messageSource;
    public String messageBody;

    public EarlierSMS(String msgSrc, String msgBody)
    {
        this.messageSource=msgSrc;
        this.messageBody=msgBody;
    }
}
