package io.gupshup.smsapp.ui.reply.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import io.gupshup.smsapp.ui.base.viewmodel.BaseFragmentViewModel;

/**
 * Created by Naveen BM on 7/17/2018.
 */
public class MessageReplyPagerFragmentViewModel extends BaseFragmentViewModel {
    private LiveData<List<String>> data = new MutableLiveData<>();

    public MessageReplyPagerFragmentViewModel(@NonNull Application application) {
        super(application);
    }
}
