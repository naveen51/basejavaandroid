package io.gupshup.smsapp.ui.compose.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.gupshup.smsapp.app.MainApplication;
import io.gupshup.smsapp.events.RxEvent;
import io.gupshup.smsapp.rxbus.RxBus;
import io.gupshup.smsapp.utils.AppConstants;
import io.gupshup.smsapp.utils.LogUtility;

/**
 * Created by Naveen BM on 8/22/2018.
 */
public class ShareContactViewModel extends AndroidViewModel {

    @Inject
    public RxBus rxBus;
    public ObservableBoolean showShareDoneButton = new ObservableBoolean(false);

    public ShareContactViewModel(@NonNull Application application) {
        super(application);
        MainApplication.getInstance().getAppComponent().inject(this);
    }


    public void onTextChanged(CharSequence s, int start, int before, int count) {
        LogUtility.d("tag", "onTextChanged " + s);
        rxBus.send(new RxEvent(AppConstants.EVENT_CONTACT_SEARCH_FILTER, s.toString()));
    }


}
