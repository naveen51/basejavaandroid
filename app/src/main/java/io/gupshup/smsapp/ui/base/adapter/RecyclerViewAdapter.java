package io.gupshup.smsapp.ui.base.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.gupshup.smsapp.R;
import io.gupshup.smsapp.databinding.FooterProgressBarBinding;
import io.gupshup.smsapp.ui.base.view.ViewType;

/**
 * Created by Ram Prakash Bhat on 11/4/18.
 */

public abstract class RecyclerViewAdapter<E> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<E> mList;
    protected ItemClickListener mClickListener;


    public RecyclerViewAdapter(List<E> objects) {
        mList = objects;
    }


    protected static class FooterView extends RecyclerViewHolder {

        public FooterView(FooterProgressBarBinding binding) {
            super(binding);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (ViewType.valueOf(viewType)) {
            case FOOTER:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                FooterProgressBarBinding binding = DataBindingUtil.inflate(inflater,
                        R.layout.footer_progress_bar, parent, false);
                return new FooterView(binding);

            default:
                throw new RuntimeException("Invalid adapter view type in " + getClass().getName());
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }


    /*
     * (non-Javadoc)
     *
     * @see android.support.v7.widget.RecyclerView.Adapter#getItemCount()
     */
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public List<E> getItems() {
        return mList;
    }

    public E getItemAtPosition(int position) {
        return mList.get(position);
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);

    }

    public abstract static class RecyclerViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private ViewDataBinding binding;

        public RecyclerViewHolder(ViewDataBinding viewBinding) {
            super(viewBinding.getRoot());
            binding = viewBinding;
        }

        public ViewDataBinding getBinding() {
            return binding;

        }

        @Override
        public void onClick(View v) {

        }
    }

    public abstract void onDestroy();
}
