package io.gupshup.smsapp.exceptions;

public class UnparsablePhoneNumberException extends Exception {
    public UnparsablePhoneNumberException(String message) {
        super(message);
    }
}
