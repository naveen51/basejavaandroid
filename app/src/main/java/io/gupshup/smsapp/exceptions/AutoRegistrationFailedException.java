package io.gupshup.smsapp.exceptions;

public class AutoRegistrationFailedException extends Exception {

    private final ExceptionCode exceptionCode;

    public enum ExceptionCode {
        SIMSUB_NOT_FOUND, REG_FAILED, GENERIC_EXCEPTION
    }

    public AutoRegistrationFailedException(ExceptionCode exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }
}
