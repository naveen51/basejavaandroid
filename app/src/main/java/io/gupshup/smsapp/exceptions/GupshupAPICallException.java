package io.gupshup.smsapp.exceptions;

public class GupshupAPICallException extends Throwable {
    public GupshupAPICallException(String responseBody) {
        super(responseBody);
    }

    public GupshupAPICallException(Exception e) {
        super(e);
    }
}
