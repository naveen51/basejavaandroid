 ## 0.3.34 (334)
----------------
### Improvements(Sprint 4):
     - Version Name update
     - AMA-1107
     - AMA-1108
     - AMA-1109
     - AMA-1094
     - AMA-1106

## 0.3.33 (333)
----------------
### Improvements(Sprint 4):
     - Version Name update
     - AMA-1067
     - AMA-1078
     - AMA-1088

## 0.3.33 (32)
----------------
### Improvements(Sprint 4):
     - Version Name update
     - AMA-1046
     - AMA-1050
     - AMA-1047
     - AMA-1045
     - AMA-1051
     - AMA-1049(Commented)
     - AMA-1048
     - AMA-751
     - Includes all issues fixed in RC1
     - Vendor-specific issues of Karbonn and Android Go addressed
     - AMA-1058
     - AMA-1055
     - AMA-1063

## 0.3.32 (32)
----------------
### Improvements(Sprint 4):
    - AMA-1046
    - AMA-1050
    - AMA-1047
	- AMA-1045
	- AMA-1051
	- AMA-1049(Commented)
	- AMA-1048
	- AMA-751
	- Includes all issues fixed in RC1
	- Vendor-specific issues of Karbonn and Android Go addressed
	- AMA-1058
	- AMA-1055
	- AMA-1063


## 0.3.31 (31)
----------------
### Improvements(Sprint 4):
    - AMA-931
    - AMA 450
    - AMA 239
	- AMA-1012
	- AMA-751
	- Known issue fixes
    - Fix for - Deregistration after manual registration.
    - One time generation of API key.
    - Marking messages as read updated in system DB.
    - Marking messages as unread updated in system DB.
    - Manual reg dialog comes when auto reg still in background.
    - Xender issue was already fixed in an earlier build.

## 0.3.30 (30)
----------------
### Improvements(Sprint 4):
  - AMA-1023 Added comment
  - AMA-1012 In progress
  - Known issue fixes


## 0.3.29 (29)
----------------
### Improvements(Sprint 4):
  - AMA-1024
  - AMA-1025
  - AMA-1023 Commented in JIRA
  - AMA-1012 In progress
  - Known issue fixes (Selection Message)


## 0.3.28 (28)
----------------
### Improvements(Sprint 4):
  - AMA-931
  - AMA-721
  - AMA-751 code cleanup
  - Known issue fixes (Selection Message)


## 0.3.27 (27)
----------------
### Improvements(Sprint 4):
  - AMA-1005 InProgress
  - AMA-751 code cleanup
  - Known issue fixes (Selection Message)

## 0.3.26 (26)
----------------
### Improvements(Sprint 4):
  - AMA-1001
  - AMA-751 code cleanup
  - AMA-1005 InProgress
  - New conversation related crash fixed

## 0.3.25 (25)
----------------
### Improvements(Sprint 4):
  - AMA-1011

### Commented
  - AMA-1001
  - AMA-1005
  - AMA-970


## 0.3.24 (24)
----------------
### Changes :
  - AMA-751 Code Cleanup

## 0.3.23 (23)
----------------
### Changes :
   1. RC1 merged..
   2. Version name and code made similar.


## 0.3.21 (22)
----------------
### Improvements(Sprint 4):
  - AMA-972
  - AMA-751 Code Cleanup
  - AMA-983 Fixed once after Rc1 merge
  - AMA-984 Fixed once after Rc1 merge

### Known issues(Sprint 4):
  - AMA-981 In progress
  - AMA-239 In progress


## 0.3.20 (21)
----------------
### Improvements(Sprint 4):
  - AMA-988
  - AMA-985
  - AMA-972
  - AMA-989
  - AMA-996

### Known issues(Sprint 4):
  - AMA-970 ==========> Not able o reproduce checked in Mi Note 5, Moto g2
  - AMA-931 In progress
  - AMA-721 In Progress

## 0.3.16 (17)
----------------
### Improvements(Sprint 4):
  - AMA-969
  - AMA-544

## 0.3.15 (16)
----------------
### Improvements(Sprint 4):
  - AMA-964
  - AMA-697

### Known issues(Sprint 4):
  - AMA-721 R&D


## 0.3.14 (15)
----------------
### Improvements(Sprint 4):
  - AMA-723

### Known issues(Sprint 4):
  - AMA-964 R&D (Pushed changes in other branch need to check by Gaurav)
  - AMA-721 R&D

## 0.3.13 (14)
----------------
### Improvements(Sprint 4):
  - AMA-818    : Samsung s6, Redme note5 not able to reproduce (Device not available here Redme Note 4 and 5A)
  - AMA-785
  - AMA-585
  - AMA-696
  - AMA-542
  - AMA-886
  - AMA-885
  - AMA-632
  - AMA-542
  - AMA-958  ==> This is fixed by gaurav

### Known issues(Sprint 4):
   - AMA-964 Investigation in progress.
   - AMA-899 Commented in JIRA
   - AMA-323 Commented in JIRA
   - AMA-721 Investigation in progress


## 0.3.12 (13)
----------------
### Improvements(Sprint 4):
  - AMA-887
  - AMA-912
  - AMA-778
  - AMA-792
  - AMA-961
  - AMA-960

### In progress(Sprint 4):
    - AMA-944 Lint error fixes in progress.

## 0.3.11 (12)
----------------
### Improvements(Sprint 4):
  - AMA-904
  - AMA-936
  - AMA-887
  - AMA-751 removed redundant code.
  - AMA-919

### In progress(Sprint 4):
    - AMA-944 Lint error fixes in progress.


## 0.3.10 (11)
----------------
### Improvements(Sprint 4):
  - AMA-915
  - AMA-852
  - AMA-751 removed redundant code.

### In progress(Sprint 4):
    - AMA-944 Lint error fixes in progress.

## 0.3.9 (10)
----------------
### Improvements(Sprint 4):
  - AMA-819
  - AMA-870
  - AMA-901
  - AMA-943
  - AMA-868

    ### In progress(Sprint 4):
    - AMA-944 Lint error fixes in progress.
    - AMA-915

NOTE: App is crashing while sending messages. (Need to Check by Gaurav Related to Data Messages)

## 0.3.8 (9)
----------------
### Improvements(Sprint 4):
  - AMA-820

    ### In progress(Sprint 4):
    - AMA-924 Lint error fixes in progress.
    - AMA-870 DB method needs to update
    - AMA-915
    - AMA-819 DB method needs to update

## 0.3.7 (8)
----------------
### Improvements(Sprint 4):
  - AMA-900
  - AMA-875
  - AMA-400
  - AMA-935

###  Known issues
   ## DB method/back-end support required.
   - AMA-870
   - AMA-820
   - AMA-819
   - AMA-721
   - AMA-931

## 0.3.6 (7)
----------------
### Improvements(Sprint 4):
  - AMA-917
  - AMA-913
  - AMA-918
  - AMA-914
  - AMA-911
  - AMA-871

### Can't Reproduce.(Sprint 4):
  - AMA-784
  - AMA-916

 ### Comments Added.(Sprint 4):
  - AMA-887
  - AMA-912
  - AMA-819
  - AMA-919
  - AMA-721

## 0.3.5 (6)
----------------
### New features(Sprint 4):
 - AMA-890
 - AMA-910
 - AMA-895
 - AMA-816
 - AMA-811
 - AMA-914
 - AMA-787

 ###  Known issues
   - AMA-888  in progress
   - AMA-870 In progress
   - AMA-887 Proper icon required

## 0.3.4 (5)
----------------
### New features(Sprint 4):
 - AMA-828
 - AMA-880
 - AMA-823
 - AMA-891
 - AMA-813
 - AMA-719
 - AMA-884

###  Known issues
  - AMA-890  Icons required
  - AMA-870 In progress
  - AMA-816 Added comment in JIRA


## 0.3.3 (4)
----------------
### New features(Sprint 4):
 - AMA-872
 - AMA-865

###  Known issues
 AMA-855,AMA-856 Need clarity comment added in JIRA.

## 0.3.2 (3)
----------------
### New features(Sprint 4):
     - AMA-849
     - AMA-853
     - AMA-824


## 0.3.1 (2)
----------------
### New features(Sprint 4):
     - AMA-810
     - AMA-819
     - AMA-782
     - AMA-833


## 0.3.0 (1)
----------------
### New features(Sprint 4):
     - AMA-690
     - AMA-689
     - AMA-811
     - AMA-813
     - AMA-812

### Improvements:
     - AMA-584
     - AMA-761
     - AMA-805
     - AMA-400

## 0.2.25 (26)
----------------
### New features(Sprint 3):
     - AMA-670

### Improvements:
     - AMA-752
     - AMA-720
     - AMA-746
     - AMA-747
     - AMA-732
     - AMA-741
     - AMA-728
     - AMA-809

###  Known issues
    - AMA-646,AMA-730  Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-696 Working on it.



## 0.2.24 (25)
----------------
### Improvements:
     - AMA-683
     - AMA-681
     - Added Log Method

##
     - AMA-720
     - AMA-437 Invalid
     - AMA-703 Not able to Reproduce
     - AMA-672 Not able to Reproduce
     - AMA-710 Not able to Reproduce
     - AMA-727 this issue is device specific


###  Known issues
    - AMA-646,AMA-730  Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-696 Working on it.


## 0.2.23 (24)
----------------
### Improvements:
     - AMA-623
     - AMA-470
     - AMA-703 Not able to Reproduce
     - AMA-672 Not able to Reproduce


###  Known issues
    - AMA-646 Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-696 Working on it.



## 0.2.22 (23)
----------------
### Improvements:
     - AMA-702
     - AMA-710
     - AMA-711
     - AMA-706
     - AMA-408
     - AMA-705
     - AMA-549
     - AMA-707
     - AMA-644
     - AMA-722
     - AMA-703 Not able to reproduce.
     - AMA-491
     - AMA-663
     - AMA-714

###  Known issues
    - AMA-646 Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-696 We are facing some issue its taking time to display.

## 0.2.21 (22)
----------------
### Improvements:
     - AMA-590
     - AMA-666
     - AMA-702  Not able to reproduce
     - AMA-491
     - AMA-269  Added Comment
     - AMA-693  Added Comment
     - AMA-323  Added Comment
     - AMA-682
     - AMA-703 Not Able to Reproduce (Asked to cross verify)

###  Known issues
    - AMA-646 Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-696 We are facing some issue its taking time to display.


## 0.2.19 (20)
----------------
### Improvements:
     - AMA-662
     - AMA-664
     - AMA-675
     - AMA-652
     - AMA-646
     - AMA-623
     - AMA-667
     - AMA-648

### Fixes:
   - AMA-659
   - AMA-624 Asked to kept as is.
   - AMA-669 (Checked in MI Note5, Moto G5, Moto G2 working fine)
   - AMA-545
   - AMA-687
   - AMA-551
   - AMA-699
   - AMA-701
   - AMA-611
   - AMA-695
   - AMA-653
   - AMA-631
   - AMA-593
   - AMA-508

###  Known issues
    - AMA-646 Reply functionality is working, Mark as read not working if app in background (Investigation in progress).




## 0.2.18 (19)
----------------
### Improvements(Sprint 3):
     - AMA-404
     - AMA-468
     - AMA-401
     - AMA-461

### Fixes:
    - AMA-422 (Checked in Moto G5 and MI Note5, Nokia1 is not available)
    - AMA-368 (Checked in Moto G5, MI Note5, Moto G2 - Not able to reproduce)
    - AMA-633
    - AMA-598 (Checked in Moto G5, MI Note5, Moto G2 - Not able to reproduce)
    - AMA-519 (Kept as is as per Discussion)
    - AMA-649 (Checked in Moto G5, MI Note5, Moto G2 working fine , samsung c7 pro is not available)
    - AMA-414
    - AMA-634


###  Known issues
    - AMA-508 Need to check by reduce waiting time.
    - AMA-624 Have Redirected as per requirement but screen flickering is happening
    - AMA-646 Reply functionality is working, Mark as read not working if app in background (Investigation in progress).
    - AMA-545  Commented in JIRA

## 0.2.17 (18)
----------------
### Improvements(Sprint 3):
    - AMA-639
    - AMA-655
    - AMA-642
    - AMA-644

### Fixes:
   - AMA-276
   - AMA-619
   - AMA-600
   - AMA-603
   - AMA-618
   - AMA-566
   - AMA-569
   - AMA-515
   - AMA-421


###  Known issues
    - AMA-508	Need to check by reduce waiting time.


## 0.2.15 (16)
----------------
### New features(Sprint 3):
    - AMA-460
    - AMA-465
    - AMA-423

### Improvements(Sprint 3):
    - AMA-467

### Fixes:
   - AMA-600
   - AMA-602
   - AMA-559
   - AMA-464
   - AMA-339
   - AMA-568
   - AMA-561
   - AMA-514


###  Known issues
    - AMA-508	Need to check by reduce waiting time.
    - AMA-593   Need to check specific scenarios.

## 0.2.14 (15)
----------------
### New features(Sprint 3):
    - AMA-503
    - AMA-548
    - AMA-424
    - AMA-549

### Improvements(Sprint 3):
    - AMA-452
    - AMA-431

### Fixes:
   - AMA-505
   - AMA-543
   - AMA-612
   - AMA-613
   - AMA-609
   - AMA-550
   - AMA-614


###  Known issues
    - 6 Months validation not yet integrated Gaurav implemented due to some issue it is commented
    - AMA-508	Need to check by reduce waiting time.
    - AMA-465 Single Image needed.
    - AMA-460 Single Image Needed

## 0.2.12 (13)
----------------
### Fixes:
   - AMA-589
   - AMA-506
   - AMA-575
   - AMA-576
   - AMA-595
   - AMA-558
   - AMA-570
   - AMA-597
   - AMA-599

## 0.2.12 (13)
----------------
### Fixes:
   - AMA-582
   - AMA-575
   - AMA-579
   - AMA-480
   - AMA-580
   - AMA-557
   - AMA-556

## 0.2.11 (12)
----------------
### Improvements(Sprint 3):
    - AMA-528
    - AMA-540
    - AMA-539
    - AMA-533
    - AMA-536
    - AMA-535
    - AMA-534
   	- AMA-532
   	- AMA-531
   	- AMA-530
   	- AMA-525
    - AMA-529
    - AMA-483
   	- AMA-382
   	- AMA-527
   	- AMA-526
   	- AMA-521
   	- AMA-518

### Fixes:
   - AMA-480
   - AMA-490

###  Known issues
  - 6 Months validation not yet integrated Gaurav implemented due to some issue it is commented
  - AMA-508	Need to check by reduce waiting time.

## 0.2.10 (11)
----------------
### Improvements(Sprint 3):
   - AMA-479
   - AMA-483

### Fixes:
   - AMA-513
   - AMA-515
   - AMA-516
   - AMA-502
   - AMA-509
   - AMA-510


###  Known issues
  - 6 Months validation not yet integrated Gaurav implemented due to some issue it is commented
  - AMA-508	Need to check by reduce waiting time.


## 0.2.9 (10)
----------------
### Improvements(Sprint 3):
   - AMA-489
   - AMA-483

### Fixes:
   - AMA-490
   - AMA-488
   - AMA-276
   - AMA-277
   - AMA-364
   - AMA-389
   - AMA-416
   - AMA-209
   - AMA-520

###  Known issues
  - 6 Months validation not yet integrated
  - AMA-383 Icons needed
  - AMA-479 and related flow not yet implemented

## 0.2.8 (9)
----------------
### New features(Sprint 3):
   - Push Notification registration not handled.

### Fixes:
   - AMA-451
   - AMA-382
   - AMA-381
   - AMA-380
   - AMA-483
   - AMA-493
   - AMA-463
   - AMA-462
   - Registration flow bug fixes.

###  Known issues
  - 6 Months validation not yet integrated
  - AMA-383 Icons needed



## 0.2.7 (8)
----------------
### New features(Sprint 3):
    - Skip Flow MR3.1 MR3.2
    - MR2 verification Error

### Fixes:
   - AMA-454
   - AMA-453
   - AMA-435
   - AMA-444
   - AMA-434

###  Known issues
  - 6 Months validation not yet integrated
  - Push Notification registration not handled,API dependency for two sim.


## 0.2.6 (7)
----------------
### New features(Sprint 3):
   - AMA-334

###  Known issues
   - Skip Flow MR3.1 MR3.2 Not handled
   - 6 Months validation not yet integrated
   - MR2 verification Error not handled

## 0.2.5 (6)
----------------
### New features(Sprint 3):
   - AMA-331

### Fixes:
   - AMA-331
   - AMA-358
   - AMA-415
   - AMA-399
   - AMA-398


## 0.2.3 (4)
----------------
### New features(Sprint 3):
   - AMA-308

### Fixes:
   - AMA-419
   - AMA-420
   - AMA-412
   - AMA-396
   - AMA-417
   - AMA-418
   - AMA-89
   - AMA-397

###  Known issues
   - In AMA-415 color transperency needs to be added as it is not in sympli.

## 0.2.2 (3)
----------------
### New features(Sprint 3):
   - AMA-336
   - AMA-332
   - AMA-258

### Fixes:
   - AMA-343
   - AMA-393
   - AMA-394
   - AMA-391

###  Known issues
    - Icons and Fonts is not integrated as design is not provided.


## 0.2.1 (2)
----------------
### New features(Sprint 3):
   - AMA-313

### Fixes:
   - AMA-367
   - AMA-378
   - AMA-356
   - AMA-373
   - AMA-352
   - AMA-363
   - AMA-357
   - AMA-384
   - AMA-369
   - AMA-351
   - AMA-340
   - AMA-344
   - AMA-342
   - AMA-327



## 0.2.0 (1)
----------------
### New features(Sprint 3):
   - AMA-310
   - AMA-252

### Fixes:
   - AMA-304

## 0.1.20 (32)
----------------
### New features:
   - AMA-224

## 0.1.19 (31)
----------------

### New features:
   - AMA-333
   - AMA-253(Empty Image not provided)
   - AMA-238


### Fixes:
   - AMA-238
   - AMA-272
   - AMA-269
   - AMA-321

## 0.1.18 (30)
----------------

### Fixes:
   - AMA-311
   - AMA-289
   - AMA-317
   - AMA-292
   - AMA-319
   - AMA-316
   - AMA-284
   - AMA-293
   - AMA-178


## 0.1.16 (28)
----------------

### New features:
   - AMA-305

### Fixes:
   - AMA-269
   - AMA-232
   - AMA-296
   - AMA-301
   - AMA-303
   - AMA-297
   - AMA-299
   - AMA-298
   - AMA-311
   - AMA-304
   - AMA-284 (Not an issue)


## 0.1.15 (27)
----------------

### New features:
   - AMA-242

### Fixes:
   - AMA-294
   - AMA-282
   - AMA-281
   - AMA-186
   - AMA-178
   - AMA-283
   - AMA-302
   - AMA-229 (Not reproduced)
   - AMA-291
   - AMA-289

## 0.1.14 (26)
----------------

### New features:
   - AMA-157

### Fixes:
   - AMA-262
   - AMA-259
   - AMA-267
   - AMA-240
   - AMA-236
   - AMA-235
   - AMA-234
   - AMA-233
   - AMA-263
   - AMA-271
   - AMA-260
   - AMA-279

## 0.1.13 (25)
----------------

### New features:
  - AMA-172
  - AMA-9
  - AMA-172
  - AMA-255
  - AMA-216
  - AMA-196
  - AMA-163
  - AMA-205
  - AMA-214

### Fixes:
  - AMA-193/194
  - AMA-244
  - AMA-95
  - AMA-237
  - AMA-231
  - AMA-182
  - AMA-229
  - AMA-230
  - AMA-162
  - AMA-213

###  Known issues
  - AMA-105 (Not able to send MMS from any of the devices here)

## 0.1.12 (24)
----------------

### Fixes:
  - AMA-162
  - AMA-167
  - AMA-158
  - AMA-160
  - AMA-80
  - AMA-127
  - AMA-168
  - AMA-164
  - AMA-205
  - AMA-219
  - AMA-126  ==> Not able to test.


## 0.1.11 (23)
----------------

### Fixes:
  -  AMA-155
  -  AMA-67
  -  AMA-132
  -  AMA-203
  -  AMA-183
  -  AMA-187
  -  AMA-181
  -  AMA-189
  -  AMA-178
  -  AMA-185
  -  AMA-210
  -  AMA-208
  -  AMA-207
  -  AMA-204
  -  AMA-201
  -  AMA-198
  -  AMA-81
  -  AMA-103
  -  AMA-179
  -  AMA-134
  -  AMA-191
  -  AMA-215
  -  AMA-202
  -  AMA-195
  -  AMA-199
  -  AMA-200



###  Known issues
  - AMA-142 (Dependency on the DB implementation)
  - AMA-172 (R&D requiered)
  -	AMA-138 (OnePlus 2 Not Available here and Samsun Galaxy S8 working fine)
  -	AMA-105 (Not able to send MMS from any of the devices here)
  -	AMA-95  (Android GO Not Available here)
  -	AMA-205 (Need to check a way to identify promotional number)
  - Message Reply screen overflow menu other than Block feature not implemented its having dependency.


## 0.1.10 (22)
----------------

### Fixes:
  -  AMA-165
  -  AMA-162

###  Known issues
##   Need a DB method for below feature:
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same recipient for the second time in conversation screen.
  - Message Reply screen overflow menu other than Block feature not implemented its having dependency.


## 0.1.9 (21)
----------------

### Fixes:
  -  AMA-144
  -  AMA-171
  -  AMA-73

###  Known issues
##   Need a DB method for below feature:
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same recipient for the second time in conversation screen.


## 0.1.8 (20)
----------------

### Fixes:
  -  AMA-100
  -  AMA-120
  -  AMA-148
  -  AMA-151
  -  AMA-146
  -  AMA-120
  -  AMA-95

###  Known issues
##   Need a DB method for below feature:
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same recipient for the second time in conversation screen.


## 0.1.7 (19)
----------------

### Fixes:
  -  AMA-71
  -  AMA-113
  -  AMA-145
  -  AMA-170
  -  AMA-174
  -  AMA-154
  -  AMA-150

### New features
  - Bug fixes.

###  Known issues
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same reciepient for the second time in conversation screen.Need a DB method to get conversation for the group of reciepient.


## 0.1.6 (18)
----------------

### Fixes:
  -  AMA-92
  -  AMA-109
  -  AMA-54
  -  AMA-143
  -  AMA-133
  -  AMA-135
  -  AMA-136
  -  AMA-137

### New features
  - Multi SIM detection: Check if phone has multiple SIM.
  - Swipe restriction in home page.

###  Known issues
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same reciepient for the second time in conversation screen.Need a DB method to get conversation for the group of reciepient.


## 0.1.5 (17)
----------------

### Fixes:
  -  AMA-66
  -  AMA-125
  -  AMA-92
  -  AMA-91
  -  AMA-70

### New features
  - Long message support (SMS)
  - User should be able to Move the messages to folders with RIGHT SWIPE
  - Read/Unread' option should available on LEFT SWIPE
  - Multi SIM detection: Check if phone has multiple SIM.

###  Known issues
  - Delete (is crashing), Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same reciepient for the second time in conversation screen.Need a DB method to get conversation for the group of reciepient.



## 0.1.4 (16)
----------------

### Fixes:
  -  AMA-74
  -  AMA-67
  -  AMA-78
  -  AMA-61
  -  AMA-97
  -  AMA-70

### New features

  - Multi SIM detection: Check if phone has multiple SIM.
  - Single sender thread for messages sent/received for different SIM. Flag at message level to indicate which SIM was used.
  - Allow SIM to be selected on sending messages; make last used SIM as default SIM

###  Known issues
  - Delete, Archive, Mute, Unblock and Add to contact in progress.
  - Not showing conversation when selecting same reciepient for the second time in conversation screen.Need a DB method to get conversation for the group of reciepient.

