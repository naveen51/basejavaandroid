package io.gupshup.soip.sdk.message;

import io.gupshup.crypto.common.message.GupshupMessage;
import io.gupshup.crypto.common.message.MessageType;

public final class GupshupTextMessage implements GupshupMessage {

    private static final long serialVersionUID = 7550517174067560055L;

    private final String text;

    public GupshupTextMessage(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public MessageType getType () {
        return MessageType.TEXT;
    }

}
