package io.gupshup.crypto.common.constants;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public interface Constants {

    String HEXADECIMAL_ALPHABET = "0123456789abcdef";
    String BASE58_ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

    String UTF8_CHARSET = "UTF-8";

    String SHA256_ALGORITHM = "SHA-256";

    String BRAINPOOL_ELLIPTIC_CURVE = "brainpoolp256r1";
    String ECDH_ALGORITHM = "ECDH";
    String SEC_ELLIPTIC_CURVE = "secp256k1";
    String ECDSA_ALGORITHM = "ECDSA";

    String SPONGYCASTLE_PROVIDER = "SC";

    String AES_ALGORITHM = "AES";
    String AES_GCM_TRANSFORMATION = "AES/GCM/NoPadding";

}
