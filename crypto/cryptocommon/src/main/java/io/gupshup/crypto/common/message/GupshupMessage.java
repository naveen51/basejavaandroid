package io.gupshup.crypto.common.message;

import java.io.Serializable;

public interface GupshupMessage extends Serializable {

    MessageType getType();

}