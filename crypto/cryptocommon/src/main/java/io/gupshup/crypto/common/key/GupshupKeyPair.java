package io.gupshup.crypto.common.key;

import java.io.Serializable;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public final class GupshupKeyPair implements Serializable {

    private static final long serialVersionUID = -1262958681014394061L;

    private final GupshupPublicKey publicKey;
    private final GupshupPrivateKey privateKey;

    public GupshupKeyPair(GupshupPublicKey publicKey, GupshupPrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public GupshupKeyPair(String publicKey, String privateKey) {
        this.publicKey = new GupshupPublicKey(publicKey);
        this.privateKey = new GupshupPrivateKey(privateKey);
    }

    public GupshupPublicKey getPublicKey() {
        return publicKey;
    }

    public GupshupPrivateKey getPrivateKey() {
        return privateKey;
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append("PublicKey").append(':').append(publicKey).append('\n')
            .append("PrivateKey").append(':').append(privateKey)
            .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GupshupKeyPair)) {
            return false;
        }

        return publicKey.equals(((GupshupKeyPair) obj).publicKey) && privateKey.equals(((GupshupKeyPair) obj).privateKey);
    }

}
