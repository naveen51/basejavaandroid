package io.gupshup.crypto.common.message;

import io.gupshup.crypto.common.key.GupshupPublicKey;

import java.io.Serializable;

/*
 * @author  Bhargav Kolla
 * @since   Apr 08, 2018
 */
public final class EncryptedGupshupMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private final GupshupPublicKey from;
    private final GupshupPublicKey to;
    private final byte[] nonce;
    private final byte[] encryptedMessage;

    public EncryptedGupshupMessage(
            GupshupPublicKey from,
            GupshupPublicKey to,
            byte[] nonce,
            byte[] encryptedMessage
    ) {
        this.from = from;
        this.to = to;
        this.nonce = nonce;
        this.encryptedMessage = encryptedMessage;
    }

    public GupshupPublicKey getFrom() {
        return from;
    }

    public GupshupPublicKey getTo() {
        return to;
    }

    public byte[] getNonce() {
        return nonce;
    }

    public byte[] getEncryptedMessage() {
        return encryptedMessage;
    }

}
