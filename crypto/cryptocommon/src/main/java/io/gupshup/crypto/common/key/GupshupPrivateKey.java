package io.gupshup.crypto.common.key;

import java.io.Serializable;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public final class GupshupPrivateKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String privateKey;

    public GupshupPrivateKey(String privateKey) {
        if (privateKey == null) {
            throw new NullPointerException();
        }

        this.privateKey = privateKey;
    }

    @Override
    public String toString() {
        return privateKey;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GupshupPrivateKey)) {
            return false;
        }

        return privateKey.equals(((GupshupPrivateKey) obj).privateKey);
    }

}
