package io.gupshup.crypto.common.key;

import java.io.Serializable;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public final class GupshupPublicKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String publicKey;

    public GupshupPublicKey(String publicKey) {
        if (publicKey == null) {
            throw new NullPointerException();
        }

        this.publicKey = publicKey;
    }

    @Override
    public String toString() {
        return publicKey;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GupshupPublicKey)) {
            return false;
        }

        return publicKey.equals(((GupshupPublicKey) obj).publicKey);
    }

}
