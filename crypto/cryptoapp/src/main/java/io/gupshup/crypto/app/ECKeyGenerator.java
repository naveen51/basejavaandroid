package io.gupshup.crypto.app;

import io.gupshup.crypto.common.constants.Constants;
import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import org.spongycastle.jce.ECNamedCurveTable;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.jce.spec.ECNamedCurveParameterSpec;
import org.spongycastle.jce.spec.ECPrivateKeySpec;
import org.spongycastle.jce.spec.ECPublicKeySpec;
import org.spongycastle.math.ec.ECCurve;
import org.spongycastle.math.ec.ECFieldElement;
import org.spongycastle.math.ec.ECPoint;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
final class ECKeyGenerator {

    private static final String PROVIDER = "SC";

    static {
        Security.addProvider(new BouncyCastleProvider());
        Security.setProperty("crypto.policy", "unlimited");
    }

    private static final ECKeyGenerator instance = new ECKeyGenerator();

    public static ECKeyGenerator getInstance() {
        return instance;
    }

    public final KeyPair generateKeyPair() throws InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchAlgorithmException {
        ECNamedCurveParameterSpec parameterSpec
                = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

        KeyPairGenerator keyPairGenerator
                = KeyPairGenerator.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);
        keyPairGenerator.initialize(parameterSpec);

        return keyPairGenerator.generateKeyPair();
    }

    public final KeyPair generateKeyPair(GupshupKeyPair gupshupKeyPair) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
        if (gupshupKeyPair == null) {
            return null;
        }

        PrivateKey privateKey = generatePrivateKey(gupshupKeyPair.getPrivateKey());

        PublicKey publicKey = generatePublicKey(gupshupKeyPair.getPublicKey());

        return new KeyPair(publicKey, privateKey);
    }

    public final PrivateKey generatePrivateKey(GupshupPrivateKey gupshupPrivateKey) throws InvalidKeySpecException, NoSuchProviderException, NoSuchAlgorithmException {
        if (gupshupPrivateKey == null) {
            return null;
        }

        byte[] privateValue = Utils.decodeKey(gupshupPrivateKey.toString());

        privateValue = removePrefixAndChecksumFromPrivateValue(privateValue);

        ECNamedCurveParameterSpec parameterSpec
                = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

        BigInteger s = new BigInteger(1, privateValue);

        ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(s, parameterSpec);

        KeyFactory keyFactory
                = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

        return keyFactory.generatePrivate(privateKeySpec);
    }

    public final PublicKey generatePublicKey(GupshupPublicKey gupshupPublicKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
        if (gupshupPublicKey == null) {
            return null;
        }
        byte[] publicValue = Utils.decodeKey(gupshupPublicKey.toString());

        byte[] publicPointXCoordinate = Arrays.copyOfRange(publicValue, 1, publicValue.length);

        byte prefix = publicValue[0];
        boolean isPublicPointYCoordinateOdd = (prefix == 0x03);
        ECNamedCurveParameterSpec parameterSpec
                = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

        ECCurve curve = parameterSpec.getCurve();

        ECFieldElement x = curve.fromBigInteger(new BigInteger(1, publicPointXCoordinate));
        ECFieldElement rhs = x.square().add(curve.getA()).multiply(x).add(curve.getB());
        ECFieldElement y = rhs.sqrt();

        if (y == null) {
            throw new IllegalArgumentException("Invalid point compression");
        }

        if (y.testBitZero() != isPublicPointYCoordinateOdd) {
            y = y.negate();
        }

        ECPoint q = curve.createPoint(x.toBigInteger(), y.toBigInteger());

        ECPublicKeySpec publicKeySpec = new ECPublicKeySpec(q, parameterSpec);

        KeyFactory keyFactory
                = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

        return keyFactory.generatePublic(publicKeySpec);
    }

    public final PublicKey generatePublicKey(PrivateKey privateKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
        ECNamedCurveParameterSpec parameterSpec
                = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

        ECPoint q = parameterSpec.getG().multiply(((ECPrivateKey) privateKey).getS());

        ECPublicKeySpec publicKeySpec = new ECPublicKeySpec(q, parameterSpec);

        KeyFactory keyFactory
                = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

        return keyFactory.generatePublic(publicKeySpec);
    }

    public final SecretKey generateSecretKey(PrivateKey privateKey, PublicKey publicKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        KeyAgreement keyAgreement
                = KeyAgreement.getInstance(Constants.ECDH_ALGORITHM, PROVIDER);
        keyAgreement.init(privateKey);
        keyAgreement.doPhase(publicKey, true);

        return keyAgreement.generateSecret(Constants.AES_ALGORITHM);
    }

    public final SecretKey generateSecretKey(GupshupPrivateKey gupshupPrivateKey, GupshupPublicKey gupshupPublicKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException {
        return
                generateSecretKey(
                        generatePrivateKey(gupshupPrivateKey),
                        generatePublicKey(gupshupPublicKey)
                );
    }

    private byte[] removePrefixAndChecksumFromPrivateValue(byte[] privateValue) {
        return Arrays.copyOfRange(privateValue, 1, privateValue.length - 4);
    }

}
