package io.gupshup.crypto.app;

import io.gupshup.crypto.common.constants.Constants;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.crypto.common.message.EncryptedGupshupMessage;
import io.gupshup.crypto.common.message.GupshupMessage;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

/*
 * @author  Bhargav Kolla
 * @since   Apr 08, 2018
 */
public final class GupshupMessageCrypter {

    private static final GupshupMessageCrypter instance = new GupshupMessageCrypter();
    private static final GupshupKeyGenerator gupshupKeyGenerator = GupshupKeyGenerator.getInstance();
    private static final ECKeyGenerator ecKeyGenerator = ECKeyGenerator.getInstance();

    public static GupshupMessageCrypter getInstance() {
        return instance;
    }

    public final String encrypt(
            GupshupMessage gupshupMessage,
            GupshupPrivateKey senderGupshupPrivateKey,
            GupshupPublicKey receiverGupshupPublicKey
    ) throws NoSuchProviderException, InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException, IOException {
        byte[] gupshupMessageBytes = Utils.serialize(gupshupMessage);

        SecretKey secretKey =
                ecKeyGenerator.generateSecretKey(senderGupshupPrivateKey, receiverGupshupPublicKey);

        IvParameterSpec ivParameterSpec
                = new IvParameterSpec(UUID.randomUUID().toString().getBytes());

        Cipher cipher = Cipher.getInstance(Constants.AES_GCM_TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);

        byte[] encryptedGupshupMessageBytes = cipher.doFinal(gupshupMessageBytes);

        EncryptedGupshupMessage encryptedGupshupMessage =
                new EncryptedGupshupMessage(
                        gupshupKeyGenerator.generatePublicKey(senderGupshupPrivateKey),
                        receiverGupshupPublicKey,
                        ivParameterSpec.getIV(),
                        encryptedGupshupMessageBytes
                );

        return Utils.base64Encode(Utils.serialize(encryptedGupshupMessage));
    }

    public final GupshupMessage decrypt(
            String encryptedMessageString,
            GupshupPrivateKey gupshupPrivateKey
    ) throws IOException, ClassNotFoundException, NoSuchProviderException, InvalidKeySpecException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Object deserialize = Utils.deserialize(Utils.base64Decode(encryptedMessageString));
        EncryptedGupshupMessage encryptedGupshupMessage =
                (EncryptedGupshupMessage) deserialize;

        GupshupPublicKey gupshupPublicKey =
                gupshupKeyGenerator.generatePublicKey(gupshupPrivateKey);
        GupshupPublicKey otherGupshupPublicKey =
                (gupshupPublicKey.equals(encryptedGupshupMessage.getTo())) ?
                        encryptedGupshupMessage.getFrom() : encryptedGupshupMessage.getTo();

        SecretKey secretKey =
                ecKeyGenerator.generateSecretKey(gupshupPrivateKey, otherGupshupPublicKey);

        IvParameterSpec ivParameterSpec
                = new IvParameterSpec(encryptedGupshupMessage.getNonce());

        Cipher cipher = Cipher.getInstance(Constants.AES_GCM_TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

        byte[] gupshupMessageBytes = cipher.doFinal(encryptedGupshupMessage.getEncryptedMessage());

        return (GupshupMessage) Utils.deserialize(gupshupMessageBytes);
    }
}
