package io.gupshup.crypto.app;

import io.gupshup.crypto.common.constants.Constants;
import org.spongycastle.util.encoders.Base64;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public final class Utils {

    public static byte[] sha256 (byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(Constants.SHA256_ALGORITHM);
        return md.digest(bytes);
    }


    public static byte[] serialize (Object obj) throws IOException {
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos)
        ) {
            oos.flush();
            oos.writeObject(obj);
            return baos.toByteArray();
        }
    }


    public static Object deserialize (byte[] bytes) throws IOException, ClassNotFoundException {
        try (
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
        ) {
            return ois.readObject();
        }
    }


    public static String base64Encode (byte[] bytes) {
        return Base64.toBase64String(bytes);
    }


    public static byte[] base64Decode (String s) {
        return Base64.decode(s);
    }


    public static String encodeKey (byte[] bytes) {
        return base64Encode(bytes);
    }


    public static byte[] decodeKey (String s) {
        return base64Decode(s);
    }

}
