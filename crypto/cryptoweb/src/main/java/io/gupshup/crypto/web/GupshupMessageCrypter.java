package io.gupshup.crypto.web;

import io.gupshup.crypto.common.constants.Constants;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import io.gupshup.crypto.common.message.EncryptedGupshupMessage;
import io.gupshup.crypto.common.message.GupshupMessage;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/*
 * @author  Bhargav Kolla
 * @since   Apr 08, 2018
 */
public final class GupshupMessageCrypter {

    private static final GupshupMessageCrypter instance            = new GupshupMessageCrypter();
    private static final GupshupKeyGenerator   gupshupKeyGenerator = GupshupKeyGenerator.getInstance();
    private static final ECKeyGenerator        ecKeyGenerator      = ECKeyGenerator.getInstance();


    public static GupshupMessageCrypter getInstance () {
        return instance;
    }


    public final String encrypt (
            GupshupMessage gupshupMessage,
            GupshupPrivateKey senderGupshupPrivateKey,
            GupshupPublicKey receiverGupshupPublicKey
                                ) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] gupshupMessageBytes = Utils.serialize(gupshupMessage);
        SecretKey secretKey =
                ecKeyGenerator.generateSecretKey(senderGupshupPrivateKey, receiverGupshupPublicKey);
        IvParameterSpec ivParameterSpec
                = new IvParameterSpec(UUID.randomUUID().toString().getBytes());
        Cipher cipher = Cipher.getInstance(Constants.AES_GCM_TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
        byte[] encryptedGupshupMessageBytes = cipher.doFinal(gupshupMessageBytes);
        io.gupshup.crypto.common.message.EncryptedGupshupMessage encryptedGupshupMessage =
                new EncryptedGupshupMessage(
                        gupshupKeyGenerator.generatePublicKey(senderGupshupPrivateKey),
                        receiverGupshupPublicKey,
                        ivParameterSpec.getIV(),
                        encryptedGupshupMessageBytes
                );
        return Utils.base64Encode(Utils.serialize(encryptedGupshupMessage));
    }


    private void showTime (String identifier, long timeFirst, long timeSecond) {
        System.out.println(identifier + " => " + (timeSecond - timeFirst));
    }


    public final GupshupMessage decrypt (
            String encryptedMessageString,
            GupshupPrivateKey gupshupPrivateKey
                                        ) throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, IOException, ClassNotFoundException {
        EncryptedGupshupMessage encryptedGupshupMessage =
                (EncryptedGupshupMessage) Utils.deserialize(Utils.base64Decode(encryptedMessageString));

        GupshupPublicKey gupshupPublicKey =
                gupshupKeyGenerator.generatePublicKey(gupshupPrivateKey);
        GupshupPublicKey otherGupshupPublicKey =
                (gupshupPublicKey.equals(encryptedGupshupMessage.getTo())) ?
                encryptedGupshupMessage.getFrom() : encryptedGupshupMessage.getTo();

        SecretKey secretKey =
                ecKeyGenerator.generateSecretKey(gupshupPrivateKey, otherGupshupPublicKey);

        IvParameterSpec ivParameterSpec
                = new IvParameterSpec(encryptedGupshupMessage.getNonce());

        Cipher cipher = Cipher.getInstance(Constants.AES_GCM_TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

        byte[] gupshupMessageBytes = cipher.doFinal(encryptedGupshupMessage.getEncryptedMessage());

        return (GupshupMessage) Utils.deserialize(gupshupMessageBytes);
    }
}
