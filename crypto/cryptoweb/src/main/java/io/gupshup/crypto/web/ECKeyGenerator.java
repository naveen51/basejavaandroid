package io.gupshup.crypto.web;

import io.gupshup.crypto.common.constants.Constants;
import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECPoint;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
final class ECKeyGenerator {

    private static final String PROVIDER = "BC";

    static {
        Security.addProvider(new BouncyCastleProvider());
        Security.setProperty("crypto.policy", "unlimited");
    }

    private static final ECKeyGenerator instance = new ECKeyGenerator();


    public static ECKeyGenerator getInstance () {
        return instance;
    }


    public final KeyPair generateKeyPair () {
        try {
            ECNamedCurveParameterSpec parameterSpec
                    = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

            KeyPairGenerator keyPairGenerator
                    = KeyPairGenerator.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);
            keyPairGenerator.initialize(parameterSpec);

            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return null;
        }
    }


    public final KeyPair generateKeyPair (GupshupKeyPair gupshupKeyPair) {
        if (gupshupKeyPair == null) {
            return null;
        }

        PrivateKey privateKey = generatePrivateKey(gupshupKeyPair.getPrivateKey());

        PublicKey publicKey = generatePublicKey(gupshupKeyPair.getPublicKey());

        return new KeyPair(publicKey, privateKey);
    }


    public final PrivateKey generatePrivateKey (GupshupPrivateKey gupshupPrivateKey) {
        if (gupshupPrivateKey == null) {
            return null;
        }

        byte[] privateValue = Utils.decodeKey(gupshupPrivateKey.toString());

        privateValue = removePrefixAndChecksumFromPrivateValue(privateValue);

        try {
            ECNamedCurveParameterSpec parameterSpec
                    = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

            BigInteger s = new BigInteger(1, privateValue);

            ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(s, parameterSpec);

            KeyFactory keyFactory
                    = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }


    public final PublicKey generatePublicKey (GupshupPublicKey gupshupPublicKey) {
        if (gupshupPublicKey == null) {
            return null;
        }

        byte[] publicValue = Utils.decodeKey(gupshupPublicKey.toString());

        byte[] publicPointXCoordinate = Arrays.copyOfRange(publicValue, 1, publicValue.length);

        byte    prefix                      = publicValue[0];
        boolean isPublicPointYCoordinateOdd = (prefix == 0x03);

        try {
            ECNamedCurveParameterSpec parameterSpec
                    = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

            ECCurve curve = parameterSpec.getCurve();

            ECFieldElement x   = curve.fromBigInteger(new BigInteger(1, publicPointXCoordinate));
            ECFieldElement rhs = x.square().add(curve.getA()).multiply(x).add(curve.getB());
            ECFieldElement y   = rhs.sqrt();

            if (y == null) {
                throw new IllegalArgumentException("Invalid point compression");
            }

            if (y.testBitZero() != isPublicPointYCoordinateOdd) {
                y = y.negate();
            }

            ECPoint q = curve.createPoint(x.toBigInteger(), y.toBigInteger());

            ECPublicKeySpec publicKeySpec = new ECPublicKeySpec(q, parameterSpec);

            KeyFactory keyFactory
                    = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }


    public final PublicKey generatePublicKey (PrivateKey privateKey) {
        try {
            ECNamedCurveParameterSpec parameterSpec
                    = ECNamedCurveTable.getParameterSpec(Constants.SEC_ELLIPTIC_CURVE);

            ECPoint q = parameterSpec.getG().multiply(((ECPrivateKey) privateKey).getS());

            ECPublicKeySpec publicKeySpec = new ECPublicKeySpec(q, parameterSpec);

            KeyFactory keyFactory
                    = KeyFactory.getInstance(Constants.ECDSA_ALGORITHM, PROVIDER);

            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }


    public final SecretKey generateSecretKey (PrivateKey privateKey, PublicKey publicKey) {
        try {
            KeyAgreement keyAgreement
                    = KeyAgreement.getInstance(Constants.ECDH_ALGORITHM, PROVIDER);
            keyAgreement.init(privateKey);
            keyAgreement.doPhase(publicKey, true);

            return keyAgreement.generateSecret(Constants.AES_ALGORITHM);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }


    public final SecretKey generateSecretKey (GupshupPrivateKey gupshupPrivateKey, GupshupPublicKey gupshupPublicKey) {
        return
                generateSecretKey(
                        generatePrivateKey(gupshupPrivateKey),
                        generatePublicKey(gupshupPublicKey)
                                 );
    }


    private byte[] removePrefixAndChecksumFromPrivateValue (byte[] privateValue) {
        return Arrays.copyOfRange(privateValue, 1, privateValue.length - 4);
    }

}
