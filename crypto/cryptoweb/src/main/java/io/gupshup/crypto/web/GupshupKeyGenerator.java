package io.gupshup.crypto.web;

import io.gupshup.crypto.common.key.GupshupKeyPair;
import io.gupshup.crypto.common.key.GupshupPrivateKey;
import io.gupshup.crypto.common.key.GupshupPublicKey;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.util.Arrays;

/*
 * @author  Bhargav Kolla
 * @since   Apr 06, 2018
 */
public final class GupshupKeyGenerator {

    private static final GupshupKeyGenerator instance       = new GupshupKeyGenerator();
    private static final ECKeyGenerator      ecKeyGenerator = ECKeyGenerator.getInstance();


    public static GupshupKeyGenerator getInstance () {
        return instance;
    }


    public final GupshupKeyPair generateKeyPair () {
        return generateKeyPair(ecKeyGenerator.generateKeyPair());
    }


    private GupshupKeyPair generateKeyPair (KeyPair keyPair) {
        if (keyPair == null) {
            return null;
        }

        GupshupPrivateKey gupshupPrivateKey = generatePrivateKey(keyPair.getPrivate());

        GupshupPublicKey gupshupPublicKey = generatePublicKey(keyPair.getPublic());

        return new GupshupKeyPair(gupshupPublicKey, gupshupPrivateKey);
    }


    private GupshupPrivateKey generatePrivateKey (PrivateKey privateKey) {
        if (privateKey == null) {
            return null;
        }

        byte[] privateValue = ((ECPrivateKey) privateKey).getS().toByteArray();
        if (privateValue.length == 33 && privateValue[0] == 0x00) {
            privateValue = Arrays.copyOfRange(privateValue, 1, privateValue.length);
        }

        privateValue = addPrefixAndChecksumToPrivateValue(privateValue);

        return new GupshupPrivateKey(Utils.encodeKey(privateValue));
    }


    private GupshupPublicKey generatePublicKey (PublicKey publicKey) {
        if (publicKey == null) {
            return null;
        }

        ECPoint publicPoint = ((ECPublicKey) publicKey).getW();

        byte[] publicPointXCoordinate = publicPoint.getAffineX().toByteArray();
        if (publicPointXCoordinate.length == 33 && publicPointXCoordinate[0] == 0x00) {
            publicPointXCoordinate = Arrays.copyOfRange(publicPointXCoordinate, 1, publicPointXCoordinate.length);
        }

        boolean isPublicPointYCoordinateOdd
                = !publicPoint.getAffineY().mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO);
        byte[] prefix = {(byte) (isPublicPointYCoordinateOdd ? 0x03 : 0x02)};

        byte[] publicValue = new byte[prefix.length + publicPointXCoordinate.length];
        System.arraycopy(prefix, 0, publicValue, 0, prefix.length);
        System.arraycopy(publicPointXCoordinate, 0, publicValue, prefix.length, publicPointXCoordinate.length);

        return new GupshupPublicKey(Utils.encodeKey(publicValue));
    }


    public final GupshupPublicKey generatePublicKey (GupshupPrivateKey gupshupPrivateKey) {
        return
                generatePublicKey(
                        ecKeyGenerator.generatePublicKey(
                                ecKeyGenerator.generatePrivateKey(
                                        gupshupPrivateKey
                                                                 )
                                                        )
                                 );
    }


    private byte[] addPrefixAndChecksumToPrivateValue (byte[] privateValue) {
        byte[] prefix = {(byte) 0x80};

        byte[] privateValueWithPrefix = new byte[prefix.length + privateValue.length];
        System.arraycopy(prefix, 0, privateValueWithPrefix, 0, prefix.length);
        System.arraycopy(privateValue, 0, privateValueWithPrefix, prefix.length, privateValue.length);

        byte[] checksum = Arrays.copyOfRange(Utils.sha256(Utils.sha256(privateValueWithPrefix)), 0, 4);

        byte[] privateValueWithPrefixWithChecksum = new byte[privateValueWithPrefix.length + checksum.length];
        System.arraycopy(privateValueWithPrefix, 0, privateValueWithPrefixWithChecksum, 0, privateValueWithPrefix.length);
        System.arraycopy(checksum, 0, privateValueWithPrefixWithChecksum, privateValueWithPrefix.length, checksum.length);

        return privateValueWithPrefixWithChecksum;
    }

    public boolean isPublicKeyValid(String publicKey) {
        try {
            ecKeyGenerator.generatePublicKey(new GupshupPublicKey(publicKey));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
